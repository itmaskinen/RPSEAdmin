import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { NgbModal, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

import { CheckoutModel } from '../../models';
import { ProductService, OrderService } from '../../services';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
@Component({
    selector: 'app-order-history',
    templateUrl: './order-history.component.html',
    styleUrls: ['../layout/layout.component.scss', '../cart/cart.css', '../products/products.css'],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()]
})
export class OrderHistoryComponent extends BaseComponent implements OnInit { 
    public showSpinner: boolean = false;
    public ordersCount: number = 0;
    constructor(
        private toastr: ToastsManager,
        private location: Location,
        private route: ActivatedRoute,
        private router: Router,
        private productService: ProductService,
        private orderService: OrderService,
        private modalService: NgbModal
    ){
        super(location, router, route)
    }
    public packageData: Subject<any> = new Subject<any[]>();
    
    public ngOnInit(){
        this.loadOrderHistory(super.currentLoggedUser().id);
    }

    public loadOrderHistory(id: number){
        this.showSpinner = true;
        this.orderService.getOrderHistory(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.ordersCount = data.length;
            this.packageData.next(data);
        });
    }

    public returnTotal(item){
        let total = 0;
        item.OrderDetails.map((o) => { 
            total += o.CurrentQty;
         });
         return total;
    }

    public imageUrl(url) {
        return super.imageUrl(url);
    }

    public isSuperAdminOrAdmin(){
        return super.issuperadminoradmin();
    }
}