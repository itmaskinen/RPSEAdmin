import { Component, OnInit, ViewChild } from '@angular/core';
import { componentsTransition } from '../../../router.animations';

import { AccountProductsQtyLogListComponent } from '../../product-log/qty/account-product-qty-log-list.component';
import { AccountProductsCurrentQtyListComponent } from './account-product-current-qty-list.component';
import { AccountProductTransactionLogComponent } from '../account-product-transaction-log.component';

@Component({
    selector: 'app-account-products-qty-component',
    templateUrl: './account-product-qty.component.html',
    styleUrls: ['../../layout/layout.component.scss'],
    animations: [componentsTransition()]
})
export class AccountProductsQtyComponent implements OnInit {
    @ViewChild('accountProductCurrentQtyList') accountProductCurrentQtyList : AccountProductsCurrentQtyListComponent;
    @ViewChild('accountProductQtyLogList') accountProductQtyLogList : AccountProductsQtyLogListComponent;
    @ViewChild('accountProductTransactionLogList') accountProductTransactionLogList : AccountProductTransactionLogComponent;
    
    public isAdmin: boolean = false;
    public productLogwithData: number = 0;

    constructor() {
    };

    public ngOnInit() {
    };

    public loadCurrentQtyData(id: number){
        return new Promise((resolve) => {
            let qtyData = this.accountProductCurrentQtyList.loadDataByAccountProductId(id);
            qtyData.then((data) => {
                resolve(data);
            })
        })
        
    }
    
    public loadQtyLogData(id: number){
        this.accountProductQtyLogList.loadDataByProductId(id).then((res: number) => {
            this.productLogwithData = res;
        });
    }

    public loadTransactionLogData(id: number) {
        this.accountProductTransactionLogList.loadTransactionLogs(id).then((res: any) => {
        });
    }
}
