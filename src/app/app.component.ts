import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private vRef: ViewContainerRef, private toastr: ToastsManager) {
        this.toastr.setRootViewContainerRef(vRef)
    }


    ngOnInit() {
    }
}
