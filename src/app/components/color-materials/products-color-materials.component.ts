import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsColorMaterialsListComponent } from './products-color-materials-list.component';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';

@Component({
    selector: 'app-products-color-materials',
    templateUrl: './products-color-materials.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ProductsColorMaterialsComponent extends BaseComponent implements OnInit {
    @ViewChild('productsColorMaterialsList') productsColorMaterialsList: ProductsColorMaterialsListComponent;
    isAdminOrUserEditor: boolean;
    constructor(private router: Router, 
        private route: ActivatedRoute, 
        private location: Location) {
            super(location, router, route);
    }
    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
    }

    public navigateToCreate(){
        this.router.navigate([`/product-color-materials/create`]);
    }

    public exportToCSV(){
        this.productsColorMaterialsList.exportToCSV();
    }
    public exportToPDF(){
        this.productsColorMaterialsList.exportToPDF();
    }
}
