import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { componentsTransition } from '../../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { AccountProductQtyLogService } from '../../../services/accoutProductQtyLog.service';
import { BaseComponent } from '../../../core/components';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationService } from '../../../services';
import { ProductsCreateEditComponent } from '../../products/products-create-edit.component';
import { MapperService } from '../../../shared';

@Component({
    selector: 'app-account-products-current-qty-list-component',
    templateUrl: './account-product-current-qty-list.component.html',
    styleUrls: ['../../layout/layout.component.scss', '../../cart/cart.css'],
    animations: [componentsTransition()]
})
export class AccountProductsCurrentQtyListComponent extends BaseComponent implements OnInit {
    showSpinner: boolean = false;
    @Input() canEmitLocation = false;
    @Input() productId: number;
    @Input() filteredLocationIds: string;
    @Output() recalculateQty = new EventEmitter();
    @Output() mergedLocations = new EventEmitter();
    @Output() emitLocation = new EventEmitter();

    productQtyLogData: Subject<any> = new Subject<any[]>();
    productQtyLogCartData: any = null
    productQtyLogDataCount: number = 0;
    productQtyTreeData: any;

    lastQtyRecord: any;
    options = {};
    productCurrentLocationData: any = [];
    productCurrentLocationDataCount: number = 0;

    isAdminOrUserEditor: boolean;
    model: any;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    isAddToCart: boolean = false;

    accountId: number = 0;
    totalQty: number = 0;
    orderDetailItems: any = [];

    currentQty: any;
    constructor(
        private productLogService: AccountProductQtyLogService,
        private locationService: LocationService,
        private toastr: ToastsManager,
        private location: Location,
        private router: Router,
        private mapperService: MapperService,
        private route: ActivatedRoute){
            super(location, router, route);
        }

    public ngOnInit(){
        if(this.productId !== undefined && this.productId > 0){
            this.isAddToCart = true;
        } else {
            this.productId = 0;
        }

        this.productId = Number(sessionStorage.getItem('product_grid_selected_row')) > 0 ? Number(sessionStorage.getItem('product_grid_selected_row')) : this.productId;

        this.loadDataByAccountProductId(this.productId).then((data:any) => {
            let locationObject = { };
            if(data.length > 0){
                locationObject = {
                    accountLocationLevelOne: data.filter(x => x.LocationLevelOne.length > 0).flatMap(x => x.LocationLevelOne),
                    accountLocationLevelTwo: data.filter(x => x.LocationLevelTwo.length > 0).flatMap(x => x.LocationLevelTwo),
                    accountLocationLevelThree: data.filter(x => x.LocationLevelThree.length > 0).flatMap(x => x.LocationLevelThree),
                    accountLocationLevelFour: data.filter(x => x.LocationLevelFour.length > 0).flatMap(x => x.LocationLevelFour),
                }
            }

            locationObject['accountLocationLevelTwo'].forEach(e => {
                e['AccountLocationLevelOneId'] = e.ParentId;
            });

            locationObject['accountLocationLevelThree'].forEach(e => {
                e['AccountLocationLevelTwoId'] = e.ParentId;
            });

            locationObject['accountLocationLevelFour'].forEach(e => {
                e['AccountLocationLevelThreeId'] = e.ParentId;
            });

            let mergedLocations = this.mapperService.mergedLocationLevels(locationObject);
            this.mergedLocations.emit(mergedLocations);
        });
    }

    public loadDataByAccountProductId(id: number){
        return new Promise((resolve) => {
            this.productId = id;
            this.productLogService.getProductCurrentQtyByAccountProductId(id, super.currentAccountId(), this.filteredLocationIds)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                this.productQtyLogDataCount = data.length;
                this.productQtyLogData.next(data);
                this.constructProductTreeData(data).then((treeData) => {
                    this.productQtyTreeData = treeData;
                    this.sortProductList(this.productQtyTreeData);
                });
                resolve(data);
            });
        });
    }
    ///TREE
    public constructProductTreeData(data){
        return new Promise((resolve) => {
            let treeViewData = [];

            let levelOneData: any = [];
            let levelTwoData: any = [];
            let levelThreeData: any = [];
            let levelFourData: any = [];

            data.forEach(item => {
                if(item.LocationLevelOneId > 0) {
                    item.LocationLevelOne.forEach(item1 => {
                        levelOneData = {
                            parentId: item1.ParentId,
                            name: item1.LocationName,
                            id: item1.Id,
                            unique_id: item1.Id,
                            qty: item1.Qty,
                            totalQty: item1.TotalQty,
                            children: []
                        };

                        // var locationTwo = item.LocationLevelTwo.filter(x => x.TotalQty !== 0);

                        item.LocationLevelTwo.forEach(item2 => {
                            levelTwoData = {
                                parentId: item2.ParentId,
                                name: item2.LocationName,
                                id: item2.Id + item2.ParentId,
                                unique_id: item2.Id,
                                qty: item2.TotalQty === item2.Qty ? 0 : item2.Qty,
                                totalQty: item2.TotalQty,
                                children: []
                            }
                                if(levelOneData.unique_id === item2.ParentId)
                                {
                                    if (levelTwoData.totalQty > 0) {
                                        levelOneData.children.push(levelTwoData);
                                    }   
                                }

                                // var locationThree = item.LocationLevelThree.filter(x => x.TotalQty !== 0);
                                
                                item.LocationLevelThree.forEach(item3 => {
                                    levelThreeData = {
                                        parentId: item3.ParentId,
                                        name: item3.LocationName,
                                        id: item3.Id + item3.ParentId,
                                        unique_id: item3.Id,
                                        qty: 0,//item3.Qty,
                                        totalQty: item3.TotalQty,
                                        children: []
                                    }

                                    if(levelTwoData.unique_id === item3.ParentId)
                                    {
                                        if (levelThreeData.totalQty > 0) {
                                            levelTwoData.children.push(levelThreeData);
                                        }
                                    }

                                    // var locationFour = item.LocationLevelFour.filter(x => x.TotalQty !== 0);

                                    item.LocationLevelFour.forEach(item4 => {
                                        levelFourData = {
                                            parentId: item4.ParentId,
                                            name: item4.LocationName,
                                            id: item4.Id + item4.ParentId,
                                            unique_id: item4.Id,
                                            qty: 0,//item3.Qty,
                                            totalQty: item4.TotalQty,
                                            children: []
                                        }

                                        if(levelThreeData.unique_id === item4.ParentId)
                                        {
                                            if (levelFourData.totalQty > 0) {
                                                levelThreeData.children.push(levelFourData);
                                            }   
                                        }
                                        
                                    });
                                    levelThreeData.totalQty = (levelThreeData.children && levelThreeData.children.length > 0) ? levelThreeData.children.map(e => e.totalQty).reduce((a,b) => a + b, 0) : levelThreeData.totalQty;
                                });
                                levelTwoData.totalQty = (levelTwoData.children && levelTwoData.children.length > 0) ? levelTwoData.children.map(e => e.totalQty).reduce((a,b) => a + b, 0) : levelTwoData.totalQty;
                        });
                        levelOneData.totalQty = (levelOneData.children && levelOneData.children.length > 0) ? levelOneData.children.map(e => e.totalQty).reduce((a,b) => a + b, 0) : levelOneData.totalQty;
                    });
                    
                    if (levelOneData.totalQty > 0) treeViewData.push(levelOneData);
                }

            });

            resolve(treeViewData);
        });
    }

    public onSelectedChange(){}
    public onFilterChange(){}

    public showDrillDown(item: any){
        if(!this.lastQtyRecord){
            this.lastQtyRecord = item;
        }
        else {
            if(this.lastQtyRecord.Id !== item.Id){
                this.lastQtyRecord.ShowDrilldown = false;
                this.lastQtyRecord = item;
            }
        }
        this.loadDataAccountLocationId(this.productId, item.LocationLevelOneId, item)
    }

     //expand drill down
     public loadDataAccountLocationId(productId: number, locationId: number, item: any){
        this.productLogService.getProductCurrentQtyByAccountLocationId(productId, locationId)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            item.ShowDrilldown = true;
            let data = response.Data;
            this.productCurrentLocationDataCount = data.length;
            this.productCurrentLocationData = data;

        })
    }

    public applyQtyToCart(item: any, mode: number)
    {
        if(mode === 1){
            if(item.CurrentQty === this.currentQty || item.Qty == 0)
            {
                return false;
            }
            item.CurrentQty++;
            this.totalQty++;

            item.Qty--;
        } else {
            if(item.CurrentQty === 0){
                return false;
            }
            item.CurrentQty--;
            this.totalQty--;

            item.Qty++;
        }

        let order = this.orderDetailItems.find(e => e === item);

        if(!order){
            this.orderDetailItems.push(item);
        }

        let data = {
            totalQty : this.totalQty,
            items: this.orderDetailItems
        }

        this.recalculateQty.emit(data);
    }

    sortProductList(productQtyLogData: any) {
        const dataLocationList = []
        if(productQtyLogData) {
            productQtyLogData.forEach(log => {
                let dataLocation = {
                    'LocationLevelFourName': null,
                    'LocationLevelThreeName': null,
                    'LocationLevelTwoName': null,
                    'LocationLevelOneName': null,
                    'LocationLevelOneId': 0,
                    'LocationLevelTwoId': 0,
                    'LocationLevelThreeId': 0,
                    'LocationLevelFourId': 0,
                    'Qty': 0,
                    'ShowDrilldown': null,
                    'CurrentQty': 0
                }

                if (log.parentId == 0 && log.children.length == 0)
                {
                    dataLocation.LocationLevelOneName = log.name;
                    dataLocation.LocationLevelOneId = log.unique_id;
                    dataLocation.Qty = log.totalQty;
                    dataLocationList.push(dataLocation);
                    
                }
                else if (log.parentId == 0 && log.children.length > 0) {
                    log.children.forEach(log2 => {

                        if (log2.children.length == 0) {
                            dataLocation.LocationLevelOneName = log.name;
                            dataLocation.LocationLevelOneId = log.unique_id;
                            dataLocation.LocationLevelTwoName = log2.name;
                            dataLocation.LocationLevelTwoId = log2.unique_id;
                            dataLocation.Qty = log2.totalQty;
                            dataLocationList.push(dataLocation);
                            dataLocation = {
                                'LocationLevelFourName': null,
                                'LocationLevelThreeName': null,
                                'LocationLevelTwoName': null,
                                'LocationLevelOneName': null,
                                'LocationLevelOneId': 0,
                                'LocationLevelTwoId': 0,
                                'LocationLevelThreeId': 0,
                                'LocationLevelFourId': 0,
                                'Qty': 0,
                                'ShowDrilldown': null,
                                'CurrentQty': 0
                            }
                        }
                        else if (log2.children.length > 0)
                        {
                            log2.children.forEach(log3 => {
                                if (log3.children.length == 0) {
                                    dataLocation.LocationLevelOneName = log.name;
                                    dataLocation.LocationLevelOneId = log.unique_id;
                                    dataLocation.LocationLevelTwoName = log2.name;
                                    dataLocation.LocationLevelTwoId = log2.unique_id;
                                    dataLocation.LocationLevelThreeName = log3.name;
                                    dataLocation.LocationLevelThreeId = log3.unique_id;
                                    dataLocation.Qty = log3.totalQty;
                                    dataLocationList.push(dataLocation);
                                    dataLocation = {
                                        'LocationLevelFourName': null,
                                        'LocationLevelThreeName': null,
                                        'LocationLevelTwoName': null,
                                        'LocationLevelOneName': null,
                                        'LocationLevelOneId': 0,
                                        'LocationLevelTwoId': 0,
                                        'LocationLevelThreeId': 0,
                                        'LocationLevelFourId': 0,
                                        'Qty': 0,
                                        'ShowDrilldown': null,
                                        'CurrentQty': 0
                                    }
                                }
                                else if (log3.children.length > 0)
                                {
                                    log3.children.forEach(log4 => {
                                        dataLocation.LocationLevelOneName = log.name;
                                        dataLocation.LocationLevelOneId = log.unique_id;
                                        dataLocation.LocationLevelTwoName = log2.name;
                                        dataLocation.LocationLevelTwoId = log2.unique_id;
                                        dataLocation.LocationLevelThreeName = log3.name;
                                        dataLocation.LocationLevelThreeId = log3.unique_id;
                                        dataLocation.LocationLevelFourName = log4.name;
                                        dataLocation.LocationLevelFourId = log4.unique_id;
                                        dataLocation.Qty = log4.totalQty;
                                        dataLocationList.push(dataLocation);
                                        dataLocation = {
                                            'LocationLevelFourName': null,
                                            'LocationLevelThreeName': null,
                                            'LocationLevelTwoName': null,
                                            'LocationLevelOneName': null,
                                            'LocationLevelOneId': 0,
                                            'LocationLevelTwoId': 0,
                                            'LocationLevelThreeId': 0,
                                            'LocationLevelFourId': 0,
                                            'Qty': 0,
                                            'ShowDrilldown': null,
                                            'CurrentQty': 0
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            });
            this.productQtyLogCartData = dataLocationList;
        }
    }

    emitSelectedLocation(data) {
        if(this.canEmitLocation && data.children.length === 0){
            this.emitLocation.emit(`${data.parentId > 0 ? (data.parentId + ',') : ''}${data.unique_id}`);
        }
    }
}
