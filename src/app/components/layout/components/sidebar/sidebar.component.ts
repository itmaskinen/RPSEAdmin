import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { Location } from '@angular/common';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../../../core/interfaces';
import { routerTransition } from '../../../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { BaseComponent } from '../../../../core/components';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';
import * as $ from 'jquery';
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent extends BaseComponent implements OnInit {
    isActive: boolean = false;
    showModule = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    pushRLeftClass: string = 'push-left';
    currentUser: any;

    isSuperAdmin: boolean = super.issuperadmin() ? super.issuperadmin() : false;
    isAdmin: boolean = super.isadmin() || super.isEditor();
    isMenuOpen: boolean = false;
    public showSpinner: boolean = false;
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public modules: Array<any>;

    public accountId : number = 0;
    public secondaryUserData : any;
    public superAdminAccess : any;
    public withSecondaryUser: boolean = false;

    public withSuperAccess: boolean = false;

    constructor(
        private translate: TranslateService,
        public router: Router,
        private route: ActivatedRoute,
        private location: Location
        ) {
        super(location, router, route);
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events
        .filter(e => e instanceof NavigationEnd)
        .subscribe((val : any) =>  {
            if
            (
                val instanceof NavigationEnd
            )
            {
                this.toggleSidebar();
            }
        });
    }

    public ngOnInit(){
        let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = currentUserData;
        if (this.currentUser.role === 'superadmin') {
            setTimeout(() => {
                $('.sidebar').css({'top' : '56px'});
            }, 100);
        }
        this.withSecondaryUser = super.withSecondaryUserData();
        this.withSuperAccess = super.hasSuperAccess();
        if(super.issuperadmin()){
            this.addExpandClass('administration')
            if(this.withSecondaryUser)
            {
                this.accountId = super.secondaryAdminUser().AccountId;
            }
        }
        else if(super.isadmin() || this.isEditor()){
            this.accountId = this.currentUser.accountId;
            this.addExpandClass('administration');
        }
        else {
            this.accountId = this.currentUser.accountId;
            $('.main-container').css({ 'margin-left' : '0px' });
        }

    }

    public eventCalled() {
        this.isActive = !this.isActive;
    }

    public addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRLeftClass);
    }

    public toggleSidebar() {
        const dom: any = document.querySelector('body');

        if(this.isSuperAdmin || super.issuperadmin()) {
            // dom.classList.toggle(this.pushRightClass);
            dom.classList.toggle(this.pushRLeftClass);
        }
        else{
            dom.classList.remove(this.pushRLeftClass);
            dom.classList.remove(this.pushRightClass);

            dom.classList.toggle(this.pushRLeftClass);
           
        }
        this.isMenuOpen = !this.isMenuOpen;
    }

    public rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    public changeLang(language: string) {
        this.translate.use(language);
    }

    public navigate(url){
        if(!url)
        {
            localStorage.removeItem('productFilter');
            return false;
        }
        if(url.indexOf('/products') === -1){ // remove the localstorage when navigating to other pages
            localStorage.removeItem('productFilter');
        };
        this.router.navigateByUrl(url);
    }

    public onLoggedout() {
        localStorage.removeItem('productFilter');
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('secondaryAdminUser');
    }
}
