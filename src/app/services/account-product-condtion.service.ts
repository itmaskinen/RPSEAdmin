import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { IPagedResults } from '../core/interfaces';

@Injectable()
export class AccountProductConditionService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getProductConditionByAccount(accountId: number): Observable<IPagedResults<any>>{
        let url = `AccountProductCondition/GetByAccountId?accountId=${accountId}`;    
        return super.getAll<any>(url);
    }

    public saveAccountProductCondition(data){
        let url = `/AccountProductCondition/Save`;
        return this.dataService.put(`${API_URL}${url}`,data);
    }
}
