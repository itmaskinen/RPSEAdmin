export class AccountModel {
    public Id :number = 0;
    public Name: string = "";
    public Company:string = "";
    public EmailAddress:string = "";
    public IsActive:boolean = false;
    public IsDeleted:boolean = false;
    public CreatedDate:string = "";
    public LastUpdatedDate:string = "";

    public ContactName: string = "";
    public ContactNumber: string = "";
    public FullAddress: string = "";
    public Details: string = "";

    public LocationLevelOneName: string = "";
    public LocationLevelTwoName: string = "";
    public LocationThreeName: string = "";
    public IsTestAccount: boolean = false;
    public Locations: any = []

    public MainPhotoByteString: string;
    public AccountLogoURL: string;

    public emailCheckoutCCs: string;
}