import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';
import { ProductService } from '../../services';
import { ManufacturerModel } from '../../models/manufacturerModel';
import * as $ from 'jquery';
@Component({
    selector: 'app-manufacturers-create-edit',
    templateUrl: './manufacturers-create-edit.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ManufacturersCreateEdiComponent extends BaseComponent implements OnInit { 
    model: any;
    accountId: 0;
    title: string = 'Create a New Manufacturer';
    mode: string = 'create';

    showSpinner = false;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

        public ngOnInit() { 
            this.model = new ManufacturerModel();
            this.mode = 'create';
            this.route.params.subscribe((params) => {
            const id = params['id']; // id
            const accountId = params['accountId']; //account id
            if (id)
            {
                this.loadData(Number(id));
                this.title =  'Edit Manufacturer';
                this.accountId = accountId;
                this.mode = 'edit';
            }
            });
            $('#company-name').focus();
        }

        public loadData(id: number){
            this.showSpinner = true;
            this.productService.getManufacturer(id)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                this.model = response.Data;
            });
        }

        public saveChanges(){
            this.showSpinner = true;
            this.productService.saveManufacturer(this.model)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.ErrorCode) {
                    this.toastr.error(response.Message, "Error"); 
                }
                else {
                    this.toastr.success(`Successfully ${this.mode == 'create' ? 'added' : 'updated'} manufacturer.`,'Success!');
                    this.router.navigate([`product-manufacturers`]);
                }
            });
        }
}