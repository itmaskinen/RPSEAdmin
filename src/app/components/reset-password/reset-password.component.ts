import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { of } from 'rxjs/observable/of';
import { ActivatedRoute, Router, ActivatedRouteSnapshot } from '@angular/router';
import { UserService, AuthenticationService } from '../../services';
import { UserModel } from '../../models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['../login/login.component.scss', '../layout/layout.component.scss'],
  providers: [UserService]
})
export class ResetPasswordComponent implements OnInit {
  user: UserModel;
  formGroup: FormGroup = this._fb.group({});
  errors: any[] = [];
  showSpinner: boolean = false;
  model: { UserName: string; Password: string; };
  isSubmitting: boolean = false;
  constructor(private _fb: FormBuilder, private router: Router, 
    private route: ActivatedRoute, private _userService: UserService, private _authenticationService: AuthenticationService) {


  }

  ngOnInit() {
    this.showSpinner = true;

    this.formGroup = this._fb.group({
      newPassword: ['',Validators.required],
      confirmNewPassword: ['',Validators.required],
      userId: ['',Validators.required]
    });

    this.formGroup.setValidators(this.passwordValidator);

    this.formGroup.valueChanges.subscribe((data)=>{
      this.errors = [];

      if(this.formGroup.invalid)
      {
        if(this.formGroup.errors){
          Object.values(this.formGroup.errors).forEach(error=>{
            this.errors.push(error);
          });
        }

      }
    });

    this.route.queryParams.subscribe((params)=>{
      
      let userId = params["userId"];
      if (!params || !userId) 
      {
          this.router.navigate(['/login']);
          this.showSpinner = false;
          return;
      }

      let decodedUserId = decodeURIComponent(atob(userId));
  
      if(decodedUserId){
        this._userService.getUser(+decodedUserId).subscribe((resp)=>{
          this.user = resp.Data;
          this.formGroup.get('userId').patchValue(this.user.Id);
          this.showSpinner = false;
        }, (error)=>{
          this.router.navigate(['/login']);
          this.showSpinner = false;
        });
      }else{
        this.router.navigate(['/login']);
        this.showSpinner = false;
      }
    });
    
  }

  passwordValidator(fg: FormGroup){
    return fg.get('newPassword').value === fg.get('confirmNewPassword').value
      ? null : {'mismatch': 'Passwords do not match.'};
  }

  get isValid(){
    return this.errors.length === 0
  }

  submit(){
    for (const i in this.formGroup.controls) {
      this.formGroup.controls[i].markAsDirty();
      this.formGroup.controls[i].updateValueAndValidity();
    }
    
    if(this.formGroup.valid && this.formGroup.dirty){
      this.isSubmitting = true;
      this._authenticationService.resetPassword(this.formGroup.getRawValue()).subscribe((resp)=>{
        if(resp.Success){
          this.logInUser(resp.Data.userName,resp.Data.password);
         
        }
      }, (error)=>{
        this.isSubmitting = false;
      });
    }
  }

  public logInUser(userName?:string, password?:string){
    if(userName && password)
    {
        this.model = {
            UserName: userName,
            Password : password
        }
    }
    this._authenticationService.auth(this.model)
        .catch((err: any) => 
        {
          this.isSubmitting = false;
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => {
            if(response.Success){
                let currentUser = response.Data.CurrentUser;
                let displayName = currentUser.FirstName || currentUser.LastName ? currentUser.FirstName + ' ' + currentUser.LastName : currentUser.UserName
                let userData = {
                    id : currentUser.Id,
                    isLoggedIn : true,
                    username: currentUser.UserName,
                    role: currentUser.RoleName,
                    displayName: displayName,
                    accountId: currentUser.AccountId,
                    currentUserRaw : currentUser
                };
                localStorage.setItem('currentUser', JSON.stringify(userData));
                localStorage.setItem('isLoggedin', JSON.stringify(true));
                this.router.navigate([`/account/${currentUser.AccountId}/products`]);
                this.isSubmitting = false;
            }
            else {
              this.isSubmitting = false;
            }
    });
}
}
