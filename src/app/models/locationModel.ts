export class LocationModel {
    public Id: number = 0;
    public BuildingNameNum: string = "";
    public Street: string = "";
    public City: string = "";
    public Region: string = "";
    public Country: string = "";

    public AccountId: number = 0;
    public LocationLevel: number = 0;
}

export class AccountLocationLevelOneModel {
    public Id: number = 0;
    public Name: string;
    public Address: string;
    public AccountId: number = 0;
}

export class AccountLocationLevelTwoModel {
    public Id: number = 0;
    public Name: string;
    public Address: string;
    public AccountId: number = 0;
    public AccountLocationLevelOneId: number = 0;
    public IsFirstLocation: boolean = false;
}

export class AccountLocationLevelThreeModel {
    public Id: number = 0;
    public Name: string;
    public Address: string;
    public AccountId: number = 0;
    public AccountLocationLevelTwoId: number = 0;
    public IsFirstLocation: boolean = false;
}

export class AccountLocationLevelFourModel {
    public Id: number = 0;
    public Name: string;
    public Address: string;
    public AccountId: number = 0;
    public AccountLocationLevelThreeId: number = 0;
    public IsFirstLocation: boolean = false;
}
