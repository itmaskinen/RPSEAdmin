import {
    Component,
    OnInit,
    ElementRef,
    Output,
    EventEmitter,
    ViewChild,
    TemplateRef,
    Input,
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { componentsTransition } from "../../router.animations";
import { Observable } from "rxjs/Observable";

import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";

import {
    ProductModel,
    AccountProductModel,
    CartProductModel,
    AccountProductQtyLogModel,
    ProductFilterModel,
} from "../../models";
import { ProductService, LocationService, OrderService } from "../../services";
import { BaseComponent } from "../../core/components";
import { Location } from "@angular/common";
import { BehaviorSubject } from "rxjs";
import * as moment from "moment";
import { ButtonGroupComponent } from "../../core/components/ag-grid-controls/button-group/button-group.component";
import * as $ from "jquery";
import { ToastrService } from "ngx-toastr";
import { MapperService, ModalService } from "../../shared";

declare var jsPDF: any; // Important

import * as modalClass from "../../shared/modules/dialogOptions";
import { ProductsCreateEditComponent, ProductBookingComponent } from "..";
import { GridApi } from "ag-grid-community";
import { OrderModel } from "../../models/orderModel";
import { AccountProductQtyLogService } from "../../services/accoutProductQtyLog.service";
import { HttpEventType } from "@angular/common/http";

@Component({
    selector: "app-account-products-list-aggrid",
    templateUrl: "./account-products-list-aggrid.component.html",
    styleUrls: [
        "../layout/layout.component.scss",
        "./products.css",
        "../cart/cart.css",
    ],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()],
})
export class AccountProductsListAgGridComponent
    extends BaseComponent
    implements OnInit
{
    //UPDATE
    public applyQtyTo: any;
    //UPDATE
    @ViewChild("productQuantityFormContent") productQuantityForm: ElementRef;
    @ViewChild("cartFormContent") addToCartForm: ElementRef;
    @ViewChild("productTransferQtyFormContent")
    productTransferQtyFormContent: ElementRef;
    @ViewChild("imageModal") imageModal: ElementRef;
    @ViewChild("locationModal") locationModal: ElementRef;
    @ViewChild("exportFile") exportModal: ElementRef;
    @ViewChild("myGrid") myGrid;
    @ViewChild("confirmCartContent")
    @Output() productQtyUpdate = new EventEmitter();
    @Output() gridColumnFilterChange = new EventEmitter();
    @Output() triggerParentSearch = new EventEmitter();
    @Output() productAddedToCart = new EventEmitter();
    @Input() userOrders: any;

    private confirmCartContentTpl: TemplateRef<any>;

    rowIndex = null;
    isAdminOrUserEditor: boolean;
    loadingMessage = "";
    model: any;
    accountProductModel: any;
    productQtyLogModel: any;
    volume: number;

    showSpinner = false;
    showAddQuantitySpinner = false;
    isSuperAdmin = false;
    isAdmin = false;
    accountId = 0;
    productList: any;
    productCount = 1;

    systemProductSelected = false;
    showLevelOne = false;
    showLevelTwo = false;
    showLevelThree = false;
    showLevelFour = false;
    enableQty = false;
    hasLocationData = true;

    mode?: number;

    locationOneList: any = [];
    locationTwoList: any = [];
    locationThreeList: any = [];
    locationFourList: any = [];

    filteredLocationTwoList: any;
    filteredLocationThreeList: any;
    filteredLocationFourList: any;

    selectedAccountId = 0;
    selectedGroupId = [];
    selectedManufacturerId = [];
    selectedColorMatId = [];
    selectedModelId = [];
    selectedLocationId = 0;
    selectedKeyword = "";
    selectedDisplaying = 0;
    selectedViewType: number = 0;
    selectedQtyView = 0;
    selectedConditionId = [];
    selectedTags = [];

    quantityStock = 0;

    public locationLevelOneList: any = [];
    public locationLevelTwoList: any = [];
    public locationLevelThreeList: any = [];
    public locationLevelFourList: any = [];

    public locationLevelOneId: number = 0;
    public locationLevelTwoId: number = 0;
    public locationLevelThreeId: number = 0;
    public locationLevelFourId: number = 0;

    public confirmPopupMessage: string =
        "Are you sure you want to add this product in your database?";
    public qtyLabel: string = "Initial Quantity";
    public confirmIconClass = "fa fa-plus fa-lg success";
    public manufactuererFiltered = [];
    public groupFiltered = [];
    public colorMatFiltered = [];

    public qtyAllowed: boolean = true;
    public productFilter: any;
    p: number = 1;

    public colDef: any;
    public defaultColDef: any;
    public frameworkComponents: any;
    public params: any;
    public gridApi: GridApi;
    public pageSize;
    public pageNumber;
    public payload;
    public returnData;
    public totalRecords = 0;
    public totalQuantity = 0;
    public totalBookedCount = 0;
    public gridScrolledToPosition = false;
    public initBodyScroll = true;
    public gridOptions: any;

    public rowBuffer: any;
    public rowSelection: any;
    public rowModelType: any;
    public paginationPageSize: any;
    public cacheOverflowSize: any;
    public maxConcurrentDatasourceRequests: any;
    public infiniteInitialRowCount: any;
    public maxBlocksInCache: any;
    public gridColumnApi: any;
    public cacheBlockSize: any;

    public removeQty: number;
    public removeQtyExceed: boolean;

    public currentTotalQty: number = 0;
    public cartModel: any;
    public orderModel: any;

    public accountProductDstModel: any;
    public productQtyLogDstModel: any;
    public applyQtyDstTo: any;

    public showMoveQuantitySpinner = false;
    public moveQtyExceed = false;
    public hideDestination = true;
    public allLocationsCopy = [];
    public mergedLocations = [];
    public allLocations = [];
    public availableQtyTobeMoved = 0;

    public focusPhotoSrc: string;

    public quantityColumnHeaderName = "Quantity (0)";
    public bookCountColumnHeaderName = "Booked (0)";

    public productQtyTreeData: any;
    public isExporting: boolean = false;
    public exportProgress: number = 0;

    constructor(
        private locationService: LocationService,
        private productService: ProductService,
        private orderService: OrderService,
        private toastr: ToastrService,
        private mapperService: MapperService,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: ModalService,
        private rateConfig: NgbRatingConfig,
        private productLogService: AccountProductQtyLogService
    ) {
        super(location, router, route);
    }

    packageData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
    productsList: any[] = [];

    public key: string = "ProductName";
    public reverse: boolean = false;
    public sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    public ngOnInit() {
        this.accountId = super.currentAccountId();
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();
        this.productFilter = new ProductFilterModel();
        this.rateConfig.max = 5;
        this.prepareColumnDef();

        this.removeQty = 0;
        this.removeQtyExceed = false;

        this.cacheBlockSize = 50;
        this.rowBuffer = 0;
        this.rowSelection = "multiple";
        this.rowModelType = "infinite";
        this.paginationPageSize = 1000;
        this.cacheOverflowSize = 2;
        this.maxConcurrentDatasourceRequests = 1;
        this.infiniteInitialRowCount = 1;
        this.maxBlocksInCache = 1000;
    }

    public exportImagesToZip() {
        const activeToast = this.showGeneratingToastr("Generating Images...");
        this.loadingMessage = `Fetching...`;
        this.showSpinner = true;
        this.productService
            .getProductsFilteredImages(
                this.selectedAccountId,
                this.selectedGroupId,
                this.selectedColorMatId,
                this.selectedManufacturerId,
                this.selectedModelId,
                this.selectedLocationId,
                this.selectedKeyword,
                this.accountId,
                1000,
                0,
                this.productFilter.qtyView
            )
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
                this.toastr.remove(activeToast.toastId);
            })
            .subscribe((response) => {
                const blob = new Blob([response], { type: "application/zip" });
                const url = window.URL.createObjectURL(blob);
                window.open(url);
            });
    }

    public initGridData(
        accountId,
        groupId,
        colorMatId,
        manufacturerId,
        modelId,
        locationIds,
        keyWord,
        displaying,
        viewType,
        qtyView,
        pageNumber,
        pageSize,
        conditionId,
        tagIds
    ): any {
        this.showSpinner = true;
        this.selectedAccountId = Number(accountId);
        this.selectedGroupId = groupId;
        this.selectedColorMatId = colorMatId;
        this.selectedManufacturerId = manufacturerId;
        this.selectedModelId = modelId;
        this.selectedLocationId = locationIds;
        this.selectedKeyword = keyWord;
        this.selectedDisplaying = displaying;
        this.selectedViewType = viewType;
        this.selectedQtyView = qtyView;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        this.selectedConditionId = conditionId;
        this.selectedTags = tagIds;

        return this.reloadProductList();
    }

    public loadGrid() {
        this.loadingMessage =
            this.selectedDisplaying > 100 ? `Fetching...` : this.loadingMessage;

        this.totalQuantity = 0;
        this.totalBookedCount = 0;

        const sortOrder = this.getSortOrder().Value;
        const columnFilters = this.getColumnFilters();

        this.payload = {
            accountId: this.selectedAccountId,
            groupId: this.selectedGroupId,
            colorMatId: this.selectedColorMatId,
            manufacturerId: this.selectedManufacturerId,
            modelId: this.selectedModelId,
            locationIds: this.selectedLocationId,
            conditionId: this.selectedConditionId,
            tagIds: this.selectedTags,
            keyWord: this.selectedKeyword,
            currentAccountId: super.currentLoggedUser().accountId,
            viewType: this.selectedQtyView,
            qtyView: this.selectedQtyView,
            columnFilters: columnFilters,
        };

        return new Promise((resolve) => {
            this.productService
                .getGrid(
                    this.payload,
                    this.pageNumber,
                    this.pageSize,
                    sortOrder
                )
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    this.gridApi.hideOverlay();
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    const data = response.Products;
                    localStorage.setItem(
                        "product_labels_for_print",
                        JSON.stringify(response.ProductIdsForLabelPrint)
                    );
                    let groupIds = [];
                    let manufacturerIds = [];
                    let colorMaterialIds = [];
                    let modelIds = [];

                    if (
                        !super.issuperadminoradmin() &&
                        this.productFilter.GroupId === undefined
                    ) {
                        groupIds = data.map((item) => {
                            return item.GroupId;
                        });
                        groupIds = Array.from(new Set(groupIds));
                    }

                    if (this.productFilter.GroupId !== undefined) {
                        if (
                            this.selectedGroupId !== this.productFilter.GroupId
                        ) {
                            manufacturerIds = data.map((item) => {
                                return item.ManufacturerId;
                            });
                            manufacturerIds = Array.from(
                                new Set(manufacturerIds)
                            );
                        }
                    }

                    if (
                        this.productFilter.GroupId !== undefined &&
                        this.productFilter.ManufacturerId !== undefined
                    ) {
                        if (
                            this.selectedGroupId !==
                                this.productFilter.GroupId ||
                            this.selectedManufacturerId !==
                                this.productFilter.ManufacturerId
                        ) {
                            colorMaterialIds = data.map((item) => {
                                return item.ColorMatId;
                            });
                            colorMaterialIds = Array.from(
                                new Set(colorMaterialIds)
                            );
                        }
                    }

                    modelIds = data.map((item) => {
                        return item.ModelId;
                    });
                    modelIds = Array.from(new Set(modelIds));

                    //qtyTotals = data.reduce(this.getSum, 0);

                    this.returnData = {
                        groups: groupIds,
                        manufacturers: manufacturerIds,
                        colorMaterials: colorMaterialIds,
                        models: modelIds,
                        recordCount: data.length,
                        totalRecords: response.TotalRecords,
                        products: data,
                    };

                    this.productCount = data.length;
                    this.totalRecords = response.TotalRecords;

                    if (data.length > 0) {
                        this.totalQuantity = data
                            .map((x) => x.CurrentQty)
                            .reduce(
                                (var1, var2) => var1 + var2,
                                this.totalQuantity
                            );
                        this.totalBookedCount = data
                            .map((x) => x.BookedCount)
                            .reduce(
                                (var1, var2) => var1 + var2,
                                this.totalBookedCount
                            );
                    }

                    this.quantityColumnHeaderName = `Quantity (${this.totalQuantity})`;
                    let quantityColumn = this.colDef.find(
                        (x) => x.field === "CurrentQty"
                    );
                    quantityColumn.headerName = this.quantityColumnHeaderName;

                    this.bookCountColumnHeaderName = `Booked (${this.totalBookedCount})`;
                    let bookedCountColumn = this.colDef.find(
                        (x) => x.field === "BookedCount"
                    );
                    bookedCountColumn.headerName =
                        this.bookCountColumnHeaderName;

                    this.packageData.next(data);

                    setTimeout(() => {
                        this.gridApi.setColumnDefs(this.colDef);
                        this.gridApi.sizeColumnsToFit();
                        this.gridApi.hideOverlay();
                    }, 500);

                    // store filter properties
                    this.productFilter = {
                        AccountId: this,
                        GroupId: this.selectedGroupId,
                        ManufacturerId: this.selectedManufacturerId,
                        ColorMatId: this.selectedColorMatId,
                        ModelId: this.selectedModelId,
                        Keyword: this.selectedKeyword,
                        Displaying: this.selectedDisplaying,
                    };

                    resolve(this.returnData);
                });
        });
    }

    private populateProductDataToGrid() {
        var dataSource = {
            rowCount: undefined,
            getRows: (params) => {
                const rowsThisPage = this.productsList.slice(
                    params.startRow,
                    params.endRow
                );
                let lastRow = -1;

                if (this.productsList.length <= params.endRow) {
                    lastRow = this.productsList.length;
                } else {
                    this.pageNumber = this.pageNumber + 1;
                    this.productService
                        .getGrid(
                            this.payload,
                            this.pageNumber,
                            this.pageSize,
                            this.getSortOrder().Value
                        )
                        .toPromise()
                        .then((res: any) => {
                            const result = res.Products;
                            localStorage.setItem(
                                "product_labels_for_print",
                                JSON.stringify(res.ProductIdsForLabelPrint)
                            );
                            if (result.length > 0) {
                                this.productsList =
                                    this.productsList.concat(result);

                                this.totalQuantity = result
                                    .map((x) => x.CurrentQty)
                                    .reduce(
                                        (var1, var2) => var1 + var2,
                                        this.totalQuantity
                                    );
                                this.quantityColumnHeaderName = `Quantity (${this.totalQuantity})`;
                                let quantityColumn = this.colDef.find(
                                    (x) => x.field === "CurrentQty"
                                );
                                quantityColumn.headerName =
                                    this.quantityColumnHeaderName;

                                this.totalBookedCount = result
                                    .map((x) => x.BookedCount)
                                    .reduce(
                                        (var1, var2) => var1 + var2,
                                        this.totalBookedCount
                                    );
                                this.bookCountColumnHeaderName = `Booked (${this.totalBookedCount})`;
                                let bookedCountColumn = this.colDef.find(
                                    (x) => x.field === "BookedCount"
                                );
                                bookedCountColumn.headerName =
                                    this.bookCountColumnHeaderName;

                                setTimeout(() => {
                                    this.gridApi.setColumnDefs(this.colDef);
                                }, 500);
                            } else {
                                lastRow = this.productsList.length;
                            }
                        });
                }

                params.successCallback(rowsThisPage, lastRow);
            },
        };

        this.params.api.setDatasource(dataSource);
        this.autoSizeAll();
    }

    public clearProductDatadToGrid() {
        this.pageNumber = 0;
        this.productsList = [];
        const dataSource = {
            getRows(params) {
                params.successCallback([], 0);
            },
        };

        this.params.api.setDatasource(dataSource);
    }

    public onGridSortChangedProductList(params) {
        this.clearProductDatadToGrid();
        this.reloadProductList();
    }

    public showPrototype(content: any) {
        this.modalService.open(content, { size: "lg" });
    }

    public confirmProductAdd(
        productFormContent: any,
        item: any,
        mode?: number,
        rowIndex?: number
    ) {
        this.rowIndex = null;
        this.rowIndex = rowIndex;

        this.hideDestination = true;
        this.allLocations = [];
        this.mergedLocations = [];
        this.availableQtyTobeMoved = 0;

        if (mode === 2) {
            this.loadProductLocations(item, this.accountId, null, true).then(
                () => {
                    this.loadProductLocations(
                        item,
                        this.accountId === 0
                            ? super.currentAccountId()
                            : this.accountId,
                        false,
                        true,
                        false
                    ).then((data) => {
                        this.allLocations = [];
                        this.allLocations =
                            this.mapperService.mergedLocationLevels(data);
                        this.allLocationsCopy = this.allLocations;
                    });
                }
            );
        } else if (mode === 1)
            this.loadProductLocations(item, this.accountId, mode == 1);
        else this.loadProductLocations(item, this.accountId, null, true);

        this.mode = mode;
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;
        this.model.CurrentQty = item.CurrentQty;
        this.model.Id = item.Id;
        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.AccountId = this.accountId;
        this.accountProductModel.Description = item.Description;
        this.accountProductModel.Location = item.Location;
        this.accountProductModel.Price = item.Price;

        this.productQtyLogModel.Mode = mode;
        this.productQtyLogModel.AccountProductId = item.AccountProductId;
        // this.productQtyLogModel.Qty = 0;//this.accountProductModel.QuantityOnStock;
        // check maximum quantity
        this.quantityStock = item.CurrentQty;

        if (item.CurrentQty > 0) {
            this.confirmPopupMessage =
                mode === 1
                    ? "Please indicate the quantity youll be adding up."
                    : "Please indicate the quantity youll be removing.";
            this.confirmIconClass =
                mode === 1 || mode === 2
                    ? "fa fa-plus fa-lg success"
                    : "fa fa-minus fa-lg danger";
            this.qtyLabel = mode === 1 ? "Add Quantity" : "Remove Quantity";
        } else {
            this.confirmPopupMessage =
                "Are you sure you want to add this product in your database?";
            this.qtyLabel = "Initial Quantity";
        }

        this.modalService.open(
            productFormContent,
            mode === 2 ? { windowClass: "fit-content" } : { size: "lg" }
        );
    }

    public validateValue(val, maxval) {
        if (Number(val) > maxval && this.productQtyLogModel.Mode === -1) {
            //this.accountProductModel.QuantityOnStock = maxval;
            this.qtyAllowed = false;
            this.toastr.error(
                "Input value not allowed, should not exceed the current qty stock",
                "Not Allowed"
            );
            return false;
        }
    }

    public onRemoveQuantityValueChange() {
        this.removeQtyExceed =
            this.accountProductModel.QuantityOnStock &&
            this.removeQty > this.accountProductModel.QuantityOnStock;
    }

    public saveProductToAccount() {
        setTimeout(() => {
            this.gridApi.showLoadingOverlay();
        }, 0);
        if (this.applyQtyTo == null) {
            this.applyQtyTo = 1;
        }
        this.showSpinner = true;
        this.productQtyLogModel.Qty =
            this.mode === 1
                ? this.accountProductModel.QuantityOnStock
                : this.removeQty;
        this.productQtyLogModel.Location;
        this.productQtyLogModel.IsMobile = false;
        this.productQtyLogModel.UpdatedBy = super.currentLoggedUser().id;
        let newMergedProductModel: any;

        if (this.mode !== -1) {
            newMergedProductModel = Object.assign(
                {
                    ProductQtyLogModel: this.productQtyLogModel,
                    AvailableDate: this.accountProductModel.AvailableDate,
                    AppliedTo:
                        this.applyQtyTo === 1
                            ? this.accountProductModel.LocationLevelOneId
                            : this.applyQtyTo === 2
                            ? this.accountProductModel.LocationLevelTwoId
                            : this.applyQtyTo === 3
                            ? this.accountProductModel.LocationLevelThreeId
                            : this.accountProductModel.LocationLevelFourId,
                },
                this.accountProductModel
            );
        } else {
            newMergedProductModel = Object.assign(
                {
                    ProductQtyLogModel: this.productQtyLogModel,
                    AvailableDate: this.accountProductModel.AvailableDate,
                    AppliedTo:
                        this.applyQtyTo === 1
                            ? this.accountProductModel.LocationLevelOneId
                            : this.applyQtyTo === 2
                            ? this.accountProductModel.LocationLevelTwoId
                            : this.applyQtyTo === 3
                            ? this.accountProductModel.LocationLevelThreeId
                            : this.accountProductModel.LocationLevelFourId,
                },
                this.accountProductModel
            );
        }

        this.productService
            .saveToAccount(newMergedProductModel)
            .catch((err: any) => {
                this.showSpinner = false;
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
                setTimeout(() => {
                    this.gridApi.hideOverlay();
                }, 500);
            })
            .subscribe((response) => {
                if (response.StatusCode === 409) {
                    this.showSpinner = false;
                    this.toastr.error(response.Message, "Conflict");
                } else if (response.ErrorCode) {
                    this.showSpinner = false;
                    this.toastr.error(response.Message, "Error");
                } else {
                    if (this.selectedAccountId == 0) {
                        this.toastr.success(
                            "Product Added to your database",
                            "Success"
                        );
                    }

                    let product = this.productsList.find((data) => {
                        return data.Id == newMergedProductModel.ProductId;
                    });
                    product.CurrentQty =
                        this.mode === 1
                            ? product.CurrentQty +
                              newMergedProductModel.QuantityOnStock
                            : product.CurrentQty - this.productQtyLogModel.Qty;

                    this.removeQty = 0;
                    this.productQtyUpdate.emit(this.productQtyLogModel);
                }
            });
    }

    public getProductTowQuantity(params) {
        return params.data.CurrentQty;
    }

    public reloadProductList() {
        this.showSpinner = true;
        return new Promise((resolve) => {
            this.loadGrid().then((res) => {
                if (this.productsList.length > 0) {
                    let currentProductIds = this.productsList.map((x) => x.Id);
                    let dataToConcat = res["products"].filter(
                        (e) => !currentProductIds.includes(e.Id)
                    );
                    if (dataToConcat.length > 0) {
                        this.productsList =
                            this.productsList.concat(dataToConcat);
                    }
                } else {
                    this.productsList = res["products"];
                }

                this.populateProductDataToGrid();
                this.showSpinner = false;
                resolve(res);
            });
        });
    }

    public addtoCart(addCartFormContent: string, item) {
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.QuantityOnStock;
        this.accountProductModel.AccountId = this.accountId;

        this.modalService.open(addCartFormContent, { size: "lg" });
    }

    public isHideEditQtyButton(accountHasThisProduct: boolean): boolean {
        return (
            this.isSuperAdmin ||
            (this.selectedAccountId === 0 && accountHasThisProduct === true)
        );
    }

    public cartQuantityChange() {
        this.cartModel.TotalPrice =
            this.cartModel.Quantity * this.accountProductModel.Price;
    }

    public navigateToEdit(id) {
        this.setSelectedProduct(id);

        if (!super.issuperadminoradmin() && this.currentRole() !== "editor") {
            this.router.navigate([`products/${id}/details`]);
        } else {
            let hasChanges;
            this.modalService.getData$.subscribe((data) => {
                hasChanges = data;
            });
            this.modalService
                .open(ProductsCreateEditComponent, modalClass.modalMyClass)
                .result.then(() => {
                    if (hasChanges === true) {
                        setTimeout(() => {
                            this.gridApi.showLoadingOverlay();
                        }, 0);
                        this.productsList = [];
                        this.clearProductDatadToGrid();
                        this.reloadProductList();
                    }
                });
        }
    }

    public navigateToProductBooking(item) {
        this.setSelectedProduct(
            item.data.Id,
            item.data.ProductNumber,
            item.data.ProductName
        );
        if (super.issuperadminoradmin() || this.currentRole() === "editor") {
            this.modalService.open(
                ProductBookingComponent,
                modalClass.modalMyClass
            );
        }
    }

    //LOCATION
    public loadProductLocations(
        item: any,
        accountId: number,
        addProductQty?: boolean,
        moveQuantiy = false,
        sourceLocation: boolean = true
    ) {
        this.reloadModalData();

        return new Promise((resolve) => {
            this.accountId = accountId;
            this.showMoveQuantitySpinner = moveQuantiy;
            this.showAddQuantitySpinner = !moveQuantiy;
            this.locationService
                .getAccountProductLocations(
                    item.Id,
                    accountId,
                    moveQuantiy
                        ? sourceLocation
                            ? false
                            : true
                        : addProductQty,
                    moveQuantiy ? "" : this.selectedLocationId
                )
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showAddQuantitySpinner = false;
                    this.showMoveQuantitySpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;
                    if (addProductQty !== null) {
                        this.locationOneList = this.mapLocations(
                            data.accountLocationLevelOne
                        );
                        this.locationTwoList = this.mapLocations(
                            data.accountLocationLevelTwo
                        );
                        this.locationThreeList = this.mapLocations(
                            data.accountLocationLevelThree
                        );
                        this.locationFourList = this.mapLocations(
                            data.accountLocationLevelFour
                        );
                        this.getChildToParentLocationQty();
                        this.showLevelOne =
                            this.enableQty =
                            this.hasLocationData =
                                this.locationOneList.length > 0;
                    } else {
                        this.mergedLocations =
                            this.mapperService.mergedLocationLevels(data);
                    }

                    resolve(data);
                });
        });
    }

    public getChildToParentLocationQty() {
        if (this.locationOneList.length > 0) {
            this.locationOneList.forEach((item) => {
                item.children = [];

                this.locationTwoList.forEach((item2) => {
                    item2.children = [];
                    if (item.Id === item2.AccountLocationLevelOneId) {
                        item.children.push(item2);
                    }

                    this.locationThreeList.forEach((item3) => {
                        item3.children = [];
                        if (item2.Id === item3.AccountLocationLevelTwoId) {
                            item2.children.push(item3);
                        }

                        this.locationFourList.forEach((item4) => {
                            item4.children = [];

                            if (item3.Id === item.AccountLocationLevelThreeId) {
                                item3.children.push(item4);
                            }
                        });
                        item3.TotalQty =
                            item3.children && item3.children.length > 0
                                ? item3.children
                                      .map((e) => e.TotalQty)
                                      .reduce((a, b) => a + b, 0)
                                : item3.TotalQty;
                    });
                    item2.TotalQty =
                        item2.children && item2.children.length > 0
                            ? item2.children
                                  .map((e) => e.TotalQty)
                                  .reduce((a, b) => a + b, 0)
                            : item2.TotalQty;
                });
                item.TotalQty =
                    item.children && item.children.length > 0
                        ? item.children
                              .map((e) => e.TotalQty)
                              .reduce((a, b) => a + b, 0)
                        : item.TotalQty;
            });
        }
    }

    public loadAccountLocationLevelOneList(accountId: number) {
        return new Promise((resolve) => {
            //clear dropdown data
            this.accountProductModel.LocationLevelOneId = 0;
            this.accountProductModel.LocationLevelTwoId = 0;
            this.accountProductModel.LocationLevelThreeId = 0;
            this.accountProductModel.LocationLevelFourId = 0;
            this.locationLevelTwoList = [];
            this.locationLevelThreeList = [];
            this.locationLevelFourList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService
                .getAccountLocationsLevelOne(accountId)
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;
                    if (data.length > 0) {
                        this.locationLevelOneList = data;
                        this.accountProductModel.LocationLevelOneId =
                            this.locationLevelOneList[0].Id;
                        this.selectLevelOne(
                            this.accountProductModel.LocationLevelOneId
                        );
                    } else {
                        this.accountProductModel.LocationLevelOneId = 0;
                    }
                    resolve(data);
                });
        });
    }

    public loadAccountLocationLevelTwoList(
        accountId: number,
        levelOneId?: number
    ) {
        return new Promise((resolve) => {
            //clear dropdown data
            this.accountProductModel.LocationLevelTwoId = 0;
            this.accountProductModel.LocationLevelThreeId = 0;
            this.accountProductModel.LocationLevelFourId = 0;
            this.locationLevelThreeList = [];
            this.locationLevelFourList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService
                .getAccountLocationsLevelTwo(accountId)
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;
                    if (data.length > 0) {
                        if (levelOneId > 0) {
                            let locationLevelTwoFilteredList = data.filter(
                                (item) => {
                                    return (
                                        item.AccountLocationLevelOneId ===
                                        levelOneId
                                    );
                                }
                            );
                            if (locationLevelTwoFilteredList.length > 0) {
                                this.locationLevelTwoList =
                                    locationLevelTwoFilteredList;
                                this.accountProductModel.LocationLevelTwoId =
                                    locationLevelTwoFilteredList[0].Id;
                                this.selectLevelTwo(
                                    this.accountProductModel.LocationLevelTwoId
                                );
                            } else {
                                this.accountProductModel.LocationLevelTwoId = 0;
                            }
                        } else {
                            this.locationLevelTwoList = data;
                        }
                    } else {
                        this.accountProductModel.LocationLevelTwoId = 0;
                    }
                    resolve(data);
                });
        });
    }

    public loadAccountLocationLevelThreeList(
        accountId: number,
        levelTwoId?: number
    ) {
        return new Promise((resolve) => {
            //clear dropdown data
            this.accountProductModel.LocationLevelThreeId = 0;
            this.accountProductModel.LocationLevelFourId = 0;
            this.locationLevelThreeList = [];
            this.locationLevelFourList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService
                .getAccountLocationsLevelThree(accountId)
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;
                    if (data.length > 0) {
                        if (levelTwoId > 0) {
                            let locationLevelThreeFilteredList = data.filter(
                                (item) => {
                                    return (
                                        item.AccountLocationLevelTwoId ===
                                        levelTwoId
                                    );
                                }
                            );

                            if (locationLevelThreeFilteredList.length > 0) {
                                this.locationLevelThreeList =
                                    locationLevelThreeFilteredList;
                                this.accountProductModel.LocationLevelThreeId =
                                    locationLevelThreeFilteredList[0].Id;
                                this.selectLevelThree(
                                    this.accountProductModel
                                        .LocationLevelThreeId
                                );
                            } else {
                                this.accountProductModel.LlocationLevelThreeId = 0;
                            }
                        } else {
                            this.locationLevelThreeList = data;
                        }
                    } else {
                        this.accountProductModel.LlocationLevelThreeId = 0;
                    }
                    resolve(data);
                });
        });
    }

    public loadAccountLocationLevelFourList(
        accountId: number,
        levelThreeId?: number
    ) {
        return new Promise((resolve) => {
            //clear dropdown data
            this.accountProductModel.LocationLevelFourId = 0;
            this.locationLevelFourList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService
                .getAccountLocationsLevelFour(accountId)
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;
                    if (data.length > 0) {
                        if (levelThreeId > 0) {
                            let locationLevelFourFilteredList = data.filter(
                                (item) => {
                                    return (
                                        item.AccountLocationLevelThreeId ===
                                        levelThreeId
                                    );
                                }
                            );

                            if (locationLevelFourFilteredList.length > 0) {
                                this.locationLevelFourList =
                                    locationLevelFourFilteredList;
                                this.accountProductModel.LocationLevelFourId =
                                    locationLevelFourFilteredList[0].Id;
                            } else {
                                this.accountProductModel.LocationLevelFourId = 0;
                            }
                        } else {
                            this.locationLevelFourList = data;
                        }
                    } else {
                        this.accountProductModel.LocationLevelFourId = 0;
                    }
                    resolve(data);
                });
        });
    }

    public selectLevelOne(id) {
        this.onLevelOneChange(id);
    }

    public selectLevelTwo(id) {
        this.onLevelTwoChange(id);
    }

    public selectLevelThree(id) {
        this.onLevelThreeChange(id);
    }

    public onLevelOneChange(val) {
        this.applyQtyTo = 1;
        let value = Number(val);
        if (value > 0) {
            this.loadAccountLocationLevelTwoList(this.accountId, value);
        }
    }

    public onLevelTwoChange(val) {
        this.applyQtyTo = 2;
        let value = Number(val);
        if (value > 0) {
            this.loadAccountLocationLevelThreeList(this.accountId, value);
        }
    }

    public onLevelThreeChange(val) {
        this.applyQtyTo = 3;
        let value = Number(val);
        if (value > 0) {
            this.loadAccountLocationLevelFourList(this.accountId, value);
        }
    }

    public deleteProduct(item) {
        this.showSpinner = true;
        this.productService
            .delete(item)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if (response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                } else {
                    this.toastr.success("Product Deleted!", "Success");
                    this.reloadProductList();
                    this.productQtyUpdate.emit(this.productQtyLogModel);
                }
            });
    }

    public imageUrl(url) {
        return super.imageUrl(url);
    }

    public showGeneratingToastr(message) {
        return this.toastr.warning(
            `<span class="fa fa-circle-o-notch fa-spin"></span> ${message}`,
            null,
            {
                enableHtml: true,
                timeOut: 0,
                extendedTimeOut: 0,
                tapToDismiss: false,
                toastClass: "ngx-toastr toast-generating",
            }
        );
    }

    public computeVolume(item): number {
        if (item.Height > 0 && item.Width > 0 && item.Length > 0) {
            this.volume = Number(
                (
                    (item.Length / 100) *
                    (item.Width / 100) *
                    (item.Height / 100)
                ).toFixed(2)
            );
        } else {
            this.volume = 0.0;
        }
        return this.volume;
    }

    //EXPORT
    public exportToCSVOrExcel() {
        this.showSpinner = true;
        this.isExporting = true;
        // const activeToast = this.showGeneratingToastr("Generating Excel...");

        this.loadingMessage = `Fetching...`;
        let promise = new Promise((resolve) => {
            const columnFilters = this.getColumnFilters();
            const sortOrder = this.getSortOrder().Value;
            this.isExporting = true;
            this.exportProgress = 0;

            const payload = {
                accountId: this.accountId,
                groupId: this.selectedGroupId,
                colorMatId: this.selectedColorMatId,
                manufacturerId: this.selectedManufacturerId,
                modelId: this.selectedModelId,
                locationIds: this.selectedLocationId,
                keyWord: this.selectedKeyword,
                currentAccountId: this.selectedAccountId,
                viewType: this.selectedViewType,
                qtyView: this.selectedQtyView,
                columnFilters: columnFilters,
                conditionId: this.selectedConditionId,
                tagIds: this.selectedTags,
            };
            this.loadingMessage = `Creating file...`;
            // override pageNumber variable without affecting the main variable
            // API use offset and fetch in SQL
            const newPageNumber = 0;
            // check if the list is empty
            if (this.productsList.length === 0) {
                this.toastr.error("Total Product cannot be zero", "Error");
            } else {
                let modalRef = this.modalService.open(this.exportModal, {
                    centered: true,
                });

                this.productService
                    .export(
                        JSON.stringify(payload),
                        newPageNumber,
                        this.productsList.length,
                        sortOrder,
                        false
                    )
                    .subscribe((response: any) => {
                        if (response.type === HttpEventType.DownloadProgress) {
                            var percentage = Math.round(
                                (100 * response.loaded) / response.total
                            );
                            if (percentage > 0)
                                this.exportProgress = percentage;
                        } else if (response.type == HttpEventType.Response) {
                            const blob = new Blob([response.body], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;",
                            });
                            const url = window.URL.createObjectURL(blob);

                            var fileLink = document.createElement("a");
                            fileLink.href = url;

                            fileLink.download = "Products.xlsx";

                            fileLink.click();
                            this.isExporting = false;

                            if (this.exportProgress == 100) modalRef.close();
                        }

                        resolve("Success");
                    });
            }
            this.loadingMessage = ``;
        });

        promise.then(() => {
            // this.toastr.remove(activeToast.toastId);
            this.showSpinner = false;
        });
    }

    public exportToTilePdf() {
        let activeToast;
        activeToast = this.showGeneratingToastr("Generating PDF...");

        const columnFilters = this.getColumnFilters();
        const sortOrder = this.getSortOrder().Value;
        const payload = {
            accountId: this.selectedAccountId,
            groupId: this.selectedGroupId,
            colorMatId: this.selectedColorMatId,
            manufacturerId: this.selectedManufacturerId,
            modelId: this.selectedModelId,
            locationIds: this.selectedLocationId,
            keyWord: this.selectedKeyword,
            currentAccountId: this.accountId,
            viewType: this.selectedViewType,
            qtyView: this.selectedQtyView,
            columnFilters: columnFilters,
            conditionId: this.selectedConditionId,
            tagIds: this.selectedTags,
        };

        this.productService
            .getGridExport(
                payload,
                0,
                this.pageSize,
                sortOrder,
                "ProductTileCatalog"
            )
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                this.toastr.remove(activeToast.toastId);
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
                this.toastr.remove(activeToast.toastId);
            })
            .subscribe((response) => {
                this.showSpinner = false;
                const url = window.URL.createObjectURL(response);
                const a: HTMLAnchorElement = document.createElement(
                    "a"
                ) as HTMLAnchorElement;

                a.href = url;
                a.download = "Product Catalog.pdf";
                document.body.appendChild(a);
                a.click();

                document.body.removeChild(a);
                URL.revokeObjectURL(url);
                this.toastr.remove(activeToast.toastId);

                localStorage.setItem(
                    "product_labels_for_print",
                    JSON.stringify(response.ProductIdsForLabelPrint)
                );
            });
    }

    public exportToPDF(data?) {
        this.showSpinner = true;

        const activeToast = this.showGeneratingToastr("Generating PDF...");

        this.loadingMessage = `Fetching...`;
        let parsePromise = new Promise((resolve) => {
            const columnFilters = this.getColumnFilters();
            const sortOrder = this.getSortOrder().Value;
            const payload = {
                accountId: this.selectedAccountId,
                groupId: this.selectedGroupId,
                colorMatId: this.selectedColorMatId,
                manufacturerId: this.selectedManufacturerId,
                modelId: this.selectedModelId,
                locationIds: this.selectedLocationId,
                keyWord: this.selectedKeyword,
                currentAccountId: this.accountId,
                viewType: this.selectedViewType,
                qtyView: this.selectedQtyView,
                columnFilters: columnFilters,
                conditionId: this.selectedConditionId,
                tagIds: this.selectedTags,
            };

            if (data) {
                let array = [];
                array.push(data);
                let parsedData = array.map((item) => {
                    return {
                        ProductNumber: item.ProductNumber || "",
                        ProductName: item.ProductName || "",
                        Location:
                            item.LocationLevelOneName || item.MajorLocations,
                        GroupName: item.GroupName || "",
                        Manufacturer: item.ManufacturerName || "",
                        ColorMaterial: item.ColorMaterialName || "",
                        CurrentQty: item.CurrentQty,
                        Model: item.ModelName || "",
                        KgCo2: item.KgCo2 || "",
                    };
                });

                resolve(parsedData);
            } else {
                this.productService
                    .getGrid(payload, this.pageNumber, this.pageSize, sortOrder)
                    .catch((err: any) => {
                        this.toastr.error(err, "Error");
                        return Observable.throw(err);
                    })
                    .finally(() => {
                        this.showSpinner = false;
                    })
                    .subscribe((response) => {
                        let data = response.Products;
                        localStorage.setItem(
                            "product_labels_for_print",
                            JSON.stringify(response.ProductIdsForLabelPrint)
                        );
                        this.loadingMessage = `Parsing...`;
                        let parsedData = data.map((item) => {
                            return {
                                ProductNumber: item.ProductNumber || "",
                                ProductName: item.ProductName || "",
                                Location:
                                    item.LocationLevelOneName ||
                                    item.MajorLocations,
                                GroupName: item.GroupName || "",
                                Manufacturer: item.ManufacturerName || "",
                                ColorMaterial: item.ColorMaterialName || "",
                                CurrentQty: item.CurrentQty,
                                Model: item.ModelName || "",
                                KgCo2: item.KgCo2 || "",
                            };
                        });
                        this.loadingMessage = ``;
                        resolve(parsedData);
                    });
            }
        });

        let doc = new jsPDF("l", "pt");
        let totalPagesExp = "{total_pages_count_string}";
        let dateInfo = moment(new Date()).format("YYYY-MM-DD");
        let headerTitle = `Products`;
        let pdfName = `Products-${dateInfo}.pdf`;
        let pageWidth = doc.internal.pageSize.getWidth();
        let leftStart = 20;
        let rightStart = pageWidth / 2;
        let centerStart = rightStart - 60;
        let contentYStart = 30;
        doc.page = 1;
        doc.addImage(super.logo64base(), "JPEG", leftStart, 20, 60, 30);
        contentYStart += 20;

        let contentColumns = [
            {
                title: "Product Number",
                dataKey: "ProductNumber",
            },
            {
                title: "Name",
                dataKey: "ProductName",
            },
            {
                title: "Product Group",
                dataKey: "GroupName",
            },
            {
                title: "Manufacturer",
                dataKey: "Manufacturer",
            },
            {
                title: "Color/Material",
                dataKey: "ColorMaterial",
            },
            {
                title: "Model",
                dataKey: "Model",
            },
            {
                title: "Location",
                dataKey: "Location",
            },
            {
                title: "Qty",
                dataKey: "CurrentQty",
            },
            {
                title: "Kg CO2",
                dataKey: "KgCo2",
            },
        ];

        doc.setFontType("bold");
        let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
        doc.text(headerTitle, pageWidth - (40 + headerWidth), contentYStart);
        contentYStart += 10;

        parsePromise.then((data) => {
            var pageContent = (data) => {
                //HEADER
                doc.addImage(super.logo64base(), "JPEG", leftStart, 20, 60, 30);
                let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
                doc.text(headerTitle, pageWidth - (40 + headerWidth), 50);
                contentYStart += 10;

                // FOOTER
                var str = "Page " + data.pageCount;
                if (typeof doc.putTotalPages === "function") {
                    str = str + " ";
                }
                doc.setFontSize(10);
                var pageHeight =
                    doc.internal.pageSize.height ||
                    doc.internal.pageSize.getHeight();
                doc.text(str, leftStart, pageHeight - 10);
            };

            doc.autoTable(contentColumns, data, {
                margin: { top: contentYStart, left: 20, right: 20, bottom: 20 },
                styles: { overflow: "linebreak", cellPadding: 2, fontSize: 9 },
                theme: "plain",
                headerStyles: {
                    fillColor: [40, 167, 69],
                    textColor: "white",
                },
                columnStyles: {
                    text: {
                        columnWidth: "wrap",
                    },
                    ProductName: {
                        columnWidth: 140,
                    },
                    ProductNumber: {
                        columnWidth: 110,
                    },
                    ColorMaterial: {
                        columnWidth: 90,
                    },
                    Qty: {
                        columnWidth: 50,
                    },
                    Location: {
                        columnWidth: 70,
                    },
                    Manufacturer: {
                        columnWidth: 70,
                    },
                    GroupName: {
                        columnWidth: 70,
                    },
                    Model: {
                        columnWidth: 170,
                    },
                    KgCo2: {
                        columnWidth: 50,
                    },
                },
                addPageContent: pageContent,
            });

            contentYStart += 50;

            doc.setFontSize(10);
            var pageHeight =
                doc.internal.pageSize.height ||
                doc.internal.pageSize.getHeight();
            doc.text("Printed: " + dateInfo, leftStart, pageHeight - 20);
            doc.text("© Copyright 2018 RP.SE ", 190, pageHeight - 20);
            if (typeof doc.putTotalPages === "function") {
                doc.putTotalPages(totalPagesExp);
            }

            doc.save(pdfName);
            this.showSpinner = false;
            this.toastr.remove(activeToast.toastId);
        });
    }

    public openImage(data) {
        this.focusPhotoSrc = data.value
            ? this.imageUrl(data.value)
            : `/assets/images/upload-empty.png`;
        this.modalService.open(this.imageModal, { windowClass: "fit-content" });
    }

    public prepareColumnDef() {
        this.colDef = [
            {
                headerName: "ID",
                valueGetter: "node.id",
                field: "ProductId",
                minWidth: 50,
                cellRenderer: (params) => {
                    return params.rowIndex + 1;
                },
            },
            {
                headerName: "",
                field: "MainPhotoUrl",
                cellStyle: { cursor: "pointer" },
                minWidth: 30,
                sortable: false,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (data) => {
                                if (data.value != undefined) {
                                    let url = data.value
                                        ? this.imageUrl(data.value)
                                        : `/assets/images/upload-empty.png`;
                                    return `<img width="30" height="30" src="${url}" class="main-photo-url"/>`;
                                } else {
                                    return `<img width="30" height="30" src="/assets/images/upload-empty.png"/>`;
                                }
                            },
                            onClick: (data) => {
                                this.openImage(data);
                            },
                        },
                    ],
                },
            },
            {
                headerName: "Product Number",
                field: "ProductNumber",
                minWidth: 150,
                cellStyle: { cursor: "pointer" },
                filter: true,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (data) => {
                                if (data.value !== undefined) {
                                    return `<u>${data.value}</u>`;
                                }
                            },
                            onClick: (params) => {
                                this.navigateToEdit(params.data.Id);
                            },
                        },
                    ],
                },
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Product Name",
                field: "ProductName",
                minWidth: super.issuperadmin() ? 200 : 200,
                cellStyle: { cursor: "pointer" },
                filter: true,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (data) => {
                                if (data.value !== undefined) {
                                    return `<u>${data.value}</u>`;
                                }
                            },
                            onClick: (params) => {
                                this.navigateToEdit(params.data.Id);
                            },
                        },
                    ],
                },
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Location",
                field: "Location",
                hide: super.issuperadmin(),
                filter: true,
                minWidth: 50,
                cellStyle: { cursor: "pointer" },
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (params) => {
                                if (this.selectedAccountId !== 0) {
                                    if (params.data !== undefined) {
                                        return `${
                                            params.data.FilteredLocation
                                                ? params.data.FilteredLocation
                                                : params.data.MajorLocations +
                                                  (params.data.MajorLocations
                                                      ? `<a href="javascript:void(0)">[...]</a>`
                                                      : ``)
                                        }`;
                                    }
                                }
                                return "";
                            },
                            onClick: (params) => {
                                // this.navigateToEdit(params.data.Id);
                                // if (!params.data.FilteredLocation) {
                                // }
                                this.openLocationModal(params.data.Id);
                            },
                        },
                    ],
                },
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Product Group",
                field: "GroupName",
                filter: true,
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Manufacturer",
                field: "ManufacturerName",
                minWidth: 50,
                filter: true,
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Color/Material",
                field: "ColorMaterialName",
                minWidth: 50,
                filter: true,
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Model",
                field: "ModelName",
                minWidth: 120,
                filter: true,
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Dimensions",
                field: "Width",
                minWidth: 100,
                filter: true,
                cellRenderer: (params) => {
                    if (params.data !== undefined) {
                        return `${
                            !!params.data.Width ? params.data.Width : 0
                        } x ${
                            !!params.data.Height ? params.data.Height : 0
                        } x ${!!params.data.Length ? params.data.Length : 0}`;
                    }
                },
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: this.quantityColumnHeaderName,
                field: "CurrentQty",
                minWidth: 100,
                cellStyle: { textAlign: "center", cursor: "pointer" },
                hide: super.issuperadmin(),
                sortable: true,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: () => {
                                let className = "fa fa-plus fa-lg success";
                                return `<i class="${className}"}"></i>`;
                            },
                            onClick: (params) => {
                                this.confirmProductAdd(
                                    this.productQuantityForm,
                                    params.data,
                                    1,
                                    params.rowIndex
                                );
                            },
                            visible: (params) => {
                                return this.isShowActions(params.data);
                            },
                        },
                        {
                            getContent: (data) => {
                                if (data.value != undefined) {
                                    return `<strong>${this.getProductTowQuantity(
                                        data
                                    )}</strong>`;
                                } else {
                                    return "";
                                }
                            },
                            visible: (params) => {
                                return this.selectedAccountId !== 0;
                            },
                        },
                        {
                            getContent: (data) => {
                                return `<i class="fa fa-minus fa-lg ${
                                    data.value > 0 ? "danger" : "default"
                                }" ${data.value <= 0 ? "disabled" : ""} ></i>`;
                            },
                            onClick: (params) => {
                                if (params.data.CurrentQty <= 0) {
                                    return;
                                }

                                this.confirmProductAdd(
                                    this.productQuantityForm,
                                    params.data,
                                    -1
                                );
                            },
                            visible: (params) => {
                                return (
                                    this.isShowActions(params.data) &&
                                    (params.data.AccountHasThisProduct ===
                                        true ||
                                        this.selectedAccountId !== 0)
                                );
                            },
                        },
                    ],
                },
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: this.bookCountColumnHeaderName,
                field: "BookedCount",
                minWidth: 50,
                cellStyle: { textAlign: "center", cursor: "pointer" },
                hide: super.issuperadmin(),
                sortable: true,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (data) => {
                                if (data.value != undefined) {
                                    let count = data.value > 0;
                                    return `<strong>${data.value}</strong>`;
                                } else {
                                    return "";
                                }
                            },
                            onClick: (params) => {
                                if (params.data.CurrentQty === 0) {
                                    this.toastr.info(
                                        "Please add quantity first to the product.",
                                        "Invalid Quantity"
                                    );
                                } else {
                                    this.navigateToProductBooking(params);
                                }
                            },
                            visible: () => {
                                return this.selectedAccountId !== 0;
                            },
                        },
                    ],
                },
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "Condition",
                field: "ProductCondition",
                minWidth: 50,
                filter: true,
                tooltipValueGetter: (params) => params.value,
            },
            {
                headerName: "",
                field: "AddedFromMobile",
                filter: true,
                cellStyle: { textAlign: "center" },
                minWidth: 30,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (data) => {
                                if (data.value != undefined) {
                                    let icon = data.value
                                        ? `fa fa-mobile fa-2x`
                                        : `fa fa-desktop`;
                                    return `<i class="${icon}" style="font-size: 15px;"></i>`;
                                } else {
                                    return ``;
                                }
                            },
                        },
                    ],
                },
            },
            {
                headerName: "Action",
                minWidth: 100,
                cellStyle: { textAlign: "center", cursor: "pointer" },
                hide: super.issuperadmin(),
                sortable: true,
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: () => {
                                let className = "fa fa-file-pdf-o";
                                return `<i class="${className}"}" style="font-size: 15px;"></i>`;
                            },
                            onClick: (params) => {
                                this.exportToPDF(params.data);
                            },
                        },
                        {
                            getContent: () => {
                                let className = "fa fa-arrows";
                                return `<i class="${className}"}"></i>`;
                            },
                            onClick: (params) => {
                                this.moveProductQty(
                                    this.productTransferQtyFormContent,
                                    params.data,
                                    1
                                );
                            },
                        },
                        {
                            getContent: () => {
                                let className = "fa fa-cart-plus";
                                return `<i class="${className}"}" style="font-size: 15px;"></i>`;
                            },
                            onClick: (params) => {
                                this.openCart(
                                    event,
                                    this.addToCartForm,
                                    params.data
                                );
                            },
                        },
                    ],
                },
            },
        ];

        this.defaultColDef = {
            resizable: true,
            sortable: true,
            flex: 1,
            minWidth: 50,
            suppressMovable: true,
            cellRenderer: (params) => {
                return params.value || "";
            },
            comparator: (str1, str2) => {
                return false; //return (str1 || '').localeCompare((str2 || ''), 'sv');
            },
            filter: false,
            filterParams: {
                filterOptions: ["contains", "equals", "startsWith", "endsWith"],
                newRowsAction: "keep",
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                },
            },
        };

        this.frameworkComponents = {
            buttonGroup: ButtonGroupComponent,
        };

        this.gridOptions = {
            getRowStyle: (params) => {
                if (params.data.Id === Number(this.getSelectedProduct())) {
                    return {
                        background: "#b7e4ff",
                    };
                }
            },
            alwaysShowHorizontalScroll: true,
        };
    }

    public onGridReady(params) {
        setTimeout(() => {
            this.gridApi.showLoadingOverlay();
        }, 0);
        this.params = params;
        this.gridApi = params.api;

        setTimeout(() => {
            this.gridApi.sizeColumnsToFit();
            const sortOrder = this.getSortOrder();

            if (sortOrder.Column !== "" && sortOrder.Column !== "") {
                params.columnApi
                    .getColumn(sortOrder.Column)
                    .setSort(sortOrder.Sort);
            }

            params.api.setFilterModel(this.getColumnFilters());
        }, 500);

        this.params = params;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    public autoSizeAll() {
        var allColIds = this.gridColumnApi
            .getAllColumns()
            .map((column) => column.colId);
        this.gridColumnApi.autoSizeColumns(allColIds, true);
    }

    public isShowActions(item) {
        if (item !== undefined) {
            return (
                (!this.isHideEditQtyButton(item.AccountHasThisProduct) ||
                    (item.AccountName &&
                        item.AccountHasThisProduct === false)) &&
                this.isAdmin
            );
        }
    }

    public gridModelUpdated(event) {
        if (this.totalRecords > 0 && !this.gridScrolledToPosition) {
            this.gridScrolledToPosition = true;

            const scrollPosition = JSON.parse(
                sessionStorage.getItem("product_grid_scroll_position")
            );

            if (scrollPosition) {
                $(".ag-body-viewport").scrollTop(scrollPosition.down || 0);
            }
        }
    }

    public gridScrollChanged(event: any) {
        if (this.initBodyScroll) {
            // Prevent to continue on initialize.
            this.initBodyScroll = false;
            return;
        }

        sessionStorage.setItem(
            "product_grid_scroll_position",
            JSON.stringify(event.api.getVerticalPixelRange())
        );
    }

    public gridSortChanged(event) {
        let sortOrder = "";
        const sortModel: any[] = event.api.getSortModel();

        if (sortModel.length > 0) {
            const queries: string[] = [];

            sortModel.forEach((sm) => {
                switch (sm.colId) {
                    case "Width":
                        queries.push(
                            `Width ${sm.sort}, Height ${sm.sort}, Length ${sm.sort}`
                        );
                        break;
                    default:
                        queries.push(`${sm.colId} ${sm.sort}`);
                }
            });

            sortOrder = queries.join(", ");
        }
        this.setSortOrder(sortOrder);
        this.clearProductDatadToGrid();
        this.reloadProductList();
    }

    public initSortOrder() {
        const sortOrder = this.getSortOrder();

        if (!sortOrder.Column) {
            this.setSortOrder("CreatedDate Desc");
        }
    }

    public setSortOrder(sortOrder) {
        sessionStorage.setItem("product_grid_sortorder", sortOrder);
    }

    public getSortOrder() {
        const sortOrder =
            sessionStorage.getItem("product_grid_sortorder") || ``;
        if (!sortOrder) {
            return { Column: "", Sort: "" };
        }

        const sortOrderSplit = sortOrder.split(" ");

        return {
            Value: sortOrder,
            Column: sortOrderSplit[0],
            Sort: sortOrderSplit[1],
        };
    }

    public clearColumnFilters() {
        sessionStorage.setItem("product_grid_column_filters", "");
        this.gridApi.setFilterModel(this.getColumnFilters());
    }

    public setColumnFilters(filters) {
        sessionStorage.setItem(
            "product_grid_column_filters",
            JSON.stringify(filters)
        );
    }

    public getColumnFilters() {
        const filters = sessionStorage.getItem("product_grid_column_filters");

        if (!filters) {
            return {};
        }
        return JSON.parse(filters);
    }

    public gridFilterChanged(event: any) {
        // Prevent to continue if change not from UI.
        // event.afterFloatingFilter is undefined if change not from UI
        if (event.afterFloatingFilter === undefined) {
            return;
        }

        const columnFilters = event.api.getFilterModel();
        this.setColumnFilters(columnFilters);
        this.setSelectedProduct(0);

        this.reloadProductList();
        this.gridColumnFilterChange.emit(columnFilters);
    }

    public setSelectedProduct(productId, number = "", name = "") {
        sessionStorage.setItem("product_grid_selected_row", productId);
        sessionStorage.setItem("product_number_grid_selected_row", number);
        sessionStorage.setItem("product_name_grid_selected_row", name);
    }

    public getSelectedProduct() {
        return sessionStorage.getItem("product_grid_selected_row") || 0;
    }

    public onLevelOneClick({ target: { value } }): void {
        let parent = Number(value);
        this.locationOneList = this.clearSelection(
            parent,
            this.locationOneList
        );
        this.clearSelection(0, this.locationTwoList, true);
        this.clearSelection(0, this.locationThreeList, true);
        this.clearSelection(0, this.locationFourList, true);
        this.accountProductModel.LocationLevelOneId =
            this.accountProductModel.LocationLevelTwoId =
            this.accountProductModel.LocationLevelThreeId =
            this.accountProductModel.LocationLevelFourId =
                0;
        this.applyQtyTo = 1;
        this.showLevelTwo = false;
        this.showLevelThree = false;
        this.showLevelFour = false;

        if (parent === this.accountProductModel.LocationLevelOneId) {
            let index = this.locationOneList.findIndex((x) => x.Id == parent);
            this.locationOneList[index].isSelected = false;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock = 0;
            }

            return;
        } else {
            this.showLevelTwo = true;
            this.accountProductModel.LocationLevelOneId = parent;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock =
                    this.locationOneList.find(
                        (x) =>
                            x.Id == this.accountProductModel.LocationLevelOneId
                    ).TotalQty;
            }
        }

        this.filteredLocationTwoList = this.locationTwoList.filter(
            (x) => x.AccountLocationLevelOneId == parent
        );
        this.showLevelTwo = this.filteredLocationTwoList.length > 0;
    }

    public onLevelTwoClick({ target: { value } }): void {
        let parent = Number(value);
        this.locationTwoList = this.clearSelection(
            parent,
            this.locationTwoList
        );
        this.clearSelection(0, this.locationThreeList, true);
        this.clearSelection(0, this.locationFourList, true);
        this.accountProductModel.LocationLevelTwoId =
            this.accountProductModel.LocationLevelThreeId =
            this.accountProductModel.LocationLevelFourId =
                0;
        this.applyQtyTo = 2;
        this.showLevelThree = false;
        this.showLevelFour = false;
        if (parent === this.accountProductModel.LocationLevelTwoId) {
            let index = this.filteredLocationTwoList.findIndex(
                (x) => x.Id == parent
            );
            this.filteredLocationTwoList[index].isSelected = false;
            this.applyQtyTo = 1;

            if (this.mode === -1)
                this.accountProductModel.QuantityOnStock =
                    this.locationOneList.find(
                        (x) =>
                            x.Id == this.accountProductModel.LocationLevelOneId
                    ).TotalQty;

            return;
        } else {
            this.showLevelThree = true;
            this.accountProductModel.LocationLevelTwoId = parent;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock =
                    this.filteredLocationTwoList.find(
                        (x) =>
                            x.Id == this.accountProductModel.LocationLevelTwoId
                    ).TotalQty;
            }
        }

        this.filteredLocationThreeList = this.locationThreeList.filter(
            (x) => x.AccountLocationLevelTwoId == parent
        );
        this.showLevelThree = this.filteredLocationThreeList.length > 0;
    }

    public onLevelThreeClick({ target: { value } }): void {
        let parent = Number(value);
        this.locationThreeList = this.clearSelection(
            parent,
            this.locationThreeList
        );
        this.clearSelection(0, this.locationFourList, true);
        this.accountProductModel.LocationLevelThreeId =
            this.accountProductModel.LocationLevelFourId = 0;
        this.applyQtyTo = 3;
        this.showLevelFour = false;
        if (parent === this.accountProductModel.LocationLevelThreeId) {
            let index = this.filteredLocationThreeList.findIndex(
                (x) => x.Id == parent
            );
            this.filteredLocationThreeList[index].isSelected = false;
            this.applyQtyTo = 2;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock =
                    this.locationTwoList.find(
                        (x) =>
                            x.Id == this.accountProductModel.LocationLevelTwoId
                    ).TotalQty;
            }

            return;
        } else {
            this.showLevelFour = true;
            this.accountProductModel.LocationLevelThreeId = parent;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock =
                    this.filteredLocationThreeList.find(
                        (x) =>
                            x.Id ==
                            this.accountProductModel.LocationLevelThreeId
                    ).TotalQty;
            }
        }

        this.filteredLocationFourList = this.locationFourList.filter(
            (x) => x.AccountLocationLevelThreeId == parent
        );
        this.showLevelFour = this.filteredLocationFourList.length > 0;
    }

    public onLevelFourClick({ target: { value } }): void {
        let parent = Number(value);
        this.locationFourList = this.clearSelection(
            parent,
            this.locationFourList
        );
        this.accountProductModel.LocationLevelFourId = 0;
        this.applyQtyTo = 4;
        if (parent === this.accountProductModel.LocationLevelFourId) {
            let index = this.filteredLocationFourList.findIndex(
                (x) => x.Id == parent
            );
            this.filteredLocationFourList[index].isSelected = false;
            this.applyQtyTo = 3;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock =
                    this.locationThreeList.find(
                        (x) =>
                            x.Id ==
                            this.accountProductModel.LocationLevelThreeId
                    ).TotalQty;
            }

            return;
        } else {
            this.accountProductModel.LocationLevelFourId = parent;

            if (this.mode === -1) {
                this.accountProductModel.QuantityOnStock =
                    this.filteredLocationFourList.find(
                        (x) =>
                            x.Id == this.accountProductModel.LocationLevelFourId
                    ).TotalQty;
            }
        }

        this.onRemoveQuantityValueChange();
    }

    private clearSelection(
        id: number,
        data: any,
        clearAll: boolean = false
    ): any {
        data.forEach((element) => {
            if (!clearAll) element.isSelected = id === element.Id;
            else element.isSelected = false;
        });

        return data;
    }

    private mapLocations(data: any): any {
        data.map((x) => {
            x["isSelected"] = false;
        });

        return data;
    }

    private reloadModalData(): void {
        this.showLevelOne =
            this.showLevelTwo =
            this.showLevelThree =
            this.showLevelFour =
            this.enableQty =
                false;
        this.hasLocationData = true;
        this.accountProductModel.LocationLevelOneId =
            this.accountProductModel.LocationLevelTwoId =
            this.accountProductModel.LocationLevelThreeId =
            this.accountProductModel.LocationLevelFourId =
            this.accountProductModel.QuantityOnStock =
            this.applyQtyTo =
                0;
        this.clearSelection(0, this.locationOneList, true);
        this.clearSelection(0, this.locationTwoList, true);
        this.clearSelection(0, this.locationThreeList, true);
        this.clearSelection(0, this.locationFourList, true);
    }

    public openCart(event: any, cartFormContent: any, item) {
        this.orderModel = new OrderModel();
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.CurrentQty;
        this.accountProductModel.AccountId = this.accountId;

        this.currentTotalQty = item.CurrentQty;

        this.modalService.open(cartFormContent);
        event.stopPropagation();
    }

    public recalculateQty(data) {
        let totalQty = data.totalQty;
        this.cartModel.Quantity = totalQty;
        this.accountProductModel.QuantityOnStock =
            this.currentTotalQty - this.cartModel.Quantity;

        this.orderModel.OrderDetails = data.items;
    }

    public addToCartSave() {
        this.showSpinner = true;
        this.populateOrderData();
        this.orderService
            .Save(this.orderModel)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if (response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                } else {
                    this.productAddedToCart.emit(true);
                    //    //add new product?
                    //    this.modalService.open(this.confirmCartContentTpl);
                }
            });
    }

    public checkOut() {
        this.router.navigateByUrl("/cart");
    }

    public moveProductQty(productFormContent: any, item: any, mode?: number) {
        this.confirmProductAdd(productFormContent, item, 2);

        this.accountProductDstModel = new AccountProductModel();
        this.accountProductDstModel.ProductId = item.Id;
        this.accountProductDstModel.AccountId = this.accountId;
        this.accountProductDstModel.Description = item.Description;
        this.accountProductDstModel.Location = item.Location;
        this.accountProductDstModel.Price = item.Price;

        this.productQtyLogDstModel = new AccountProductQtyLogModel();
        this.productQtyLogDstModel.Mode = mode;
        this.productQtyLogDstModel.AccountProductId = this.accountId;
        this.productQtyLogDstModel.Qty = 0;

        this.applyQtyDstTo = 1;
    }

    public populateOrderData() {
        this.orderModel.ProductId = this.accountProductModel.ProductId;
        if (this.userOrders.length > 0) {
            const order = this.orderService.updateIfExistingPendingOrder(
                this.orderModel,
                this.userOrders
            );
            if (order.length > 0) {
                this.orderModel = order.filter(
                    (e) => e.ProductId == this.orderModel.ProductId
                )[0];
            }
        }
        this.orderModel.UserId = super.currentLoggedUser().id;
        this.orderModel.CreatedBy = super.currentLoggedUser().username;
        this.orderModel.LastUpdatedBy = super.currentLoggedUser().username;
        this.orderModel.UserShippingAddress = this.cartModel.ShippingAddress;
    }

    public disableMoveQtySaving(): boolean {
        return (
            this.showMoveQuantitySpinner ||
            !this.accountProductModel.QuantityOnStock ||
            this.accountProductModel.QuantityOnStock === 0 ||
            this.accountProductModel.LocationLevelOneId === 0 ||
            this.accountProductDstModel.LocationLevelOneId === 0 ||
            this.moveQtyExceed
        );
    }

    getItemQuantity(item: any) {
        const quantity =
            item.location.length >= 4 && item.location[3].Id > 0
                ? item.location[3].TotalQty
                : item.location.length >= 3 && item.location[2].Id > 0
                ? item.location[2].TotalQty
                : item.location.length >= 2 && item.location[1].Id > 0
                ? item.location[1].TotalQty
                : item.location.length >= 1 && item.location[0].Id > 0
                ? item.location[0].TotalQty
                : 0;

        return quantity;
    }

    public onDestinationLocationClick(data): void {
        let locationOneId = data.location[0].Id;
        let locationTwoId = data.location.length >= 2 ? data.location[1].Id : 0;
        let locationThreeId =
            data.location.length >= 3 ? data.location[2].Id : 0;
        let locationFourId =
            data.location.length >= 4 ? data.location[3].Id : 0;

        this.accountProductDstModel.LocationLevelOneId =
            locationOneId > 0 ? locationOneId : null;
        this.accountProductDstModel.LocationLevelTwoId =
            locationTwoId > 0 ? locationTwoId : null;
        this.accountProductDstModel.LocationLevelThreeId =
            locationThreeId > 0 ? locationThreeId : null;
        this.accountProductDstModel.LocationLevelFourId =
            locationFourId > 0 ? locationFourId : null;

        this.applyQtyDstTo =
            locationFourId > 0
                ? 4
                : locationThreeId > 0
                ? 3
                : locationTwoId > 0
                ? 2
                : 1;
        let item = this.allLocations.find(
            (x) => x.location[0].isSelected === true
        );
        if (item) {
            item.location[0].isSelected = false;
        }
        data.location[0].isSelected = true;
    }

    public moveProductToAccount(dialog: any) {
        this.productQtyLogModel.Qty = this.accountProductModel.QuantityOnStock;
        this.productQtyLogModel.Mode = 2;
        this.productQtyLogModel.IsMobile = false;
        this.productQtyLogModel.UpdatedBy = super.currentLoggedUser().id;
        this.productQtyLogDstModel.Qty =
            this.accountProductModel.QuantityOnStock;
        this.productQtyLogDstModel.IsMobile = false;
        this.productQtyLogDstModel.UpdatedBy = super.currentLoggedUser().id;
        this.accountProductModel.QuantityOnStock = this.availableQtyTobeMoved;

        let srcId =
            this.applyQtyTo === 1
                ? this.accountProductModel.LocationLevelOneId
                : this.applyQtyTo === 2
                ? this.accountProductModel.LocationLevelTwoId
                : this.applyQtyTo === 3
                ? this.accountProductModel.LocationLevelThreeId
                : this.accountProductModel.LocationLevelFourId;
        let srcMergedProductModel = Object.assign(
            {
                ProductQtyLogModel: this.productQtyLogModel,
                AppliedTo: srcId,
            },
            this.accountProductModel
        );

        var dstId =
            this.applyQtyDstTo === 1
                ? this.accountProductDstModel.LocationLevelOneId
                : this.applyQtyDstTo === 2
                ? this.accountProductDstModel.LocationLevelTwoId
                : this.applyQtyTo === 3
                ? this.accountProductDstModel.LocationLevelThreeId
                : this.accountProductDstModel.LocationLevelFourId;
        let dstMergedProductModel = Object.assign(
            {
                ProductQtyLogModel: this.productQtyLogDstModel,
                AppliedTo: dstId,
                QuantityOnStock: this.accountProductModel.QuantityOnStock,
            },
            this.accountProductDstModel
        );

        if (this.availableQtyTobeMoved < this.productQtyLogModel.Qty) {
            this.toastr.error(
                "Input value not allowed, should not exceed the available qty stock",
                "Not Allowed"
            );
            return;
        }

        dialog("Close click");

        this.showSpinner = true;

        this.productService
            .saveToAccount(srcMergedProductModel)
            .catch((err) => {
                this.showSpinner = false;
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .subscribe((res) => {
                if (res.StatusCode === 409) {
                    this.toastr.error(res.Message, "Conflict");
                } else if (res.ErrorCode) {
                    this.toastr.error(res.Message, "Error");
                } else {
                    this.productService
                        .saveToAccount(dstMergedProductModel)
                        .subscribe(() => {
                            this.toastr.success(
                                "Product Added to your database",
                                "Success"
                            );
                            this.uncheckMoveQuantityLocations(
                                this.mergedLocations
                            );
                            this.uncheckMoveQuantityLocations(
                                this.allLocations
                            );
                            // this.ngOnInit();
                            this.confirmProductAdd(
                                this.productTransferQtyFormContent,
                                this.model,
                                2
                            );
                            // this.reloadGrid();
                        });
                }
                this.showSpinner = false;
            });
    }

    uncheckMoveQuantityLocations(data): any {
        return data.map((x) => {
            x.location[0].isSelected = false;
        });
    }

    public onSourceLocationClick(data): void {
        let locationOneId = data.location[0].Id;
        let locationTwoId = data.location[1].Id;
        let locationThreeId = data.location[2].Id;
        let locationFourId = data.location[3].Id;

        this.allLocations = [...this.allLocationsCopy];

        let indexToRemoved = this.allLocations.findIndex(
            (x) =>
                x.location[0].Id === locationOneId &&
                x.location[1].Id === locationTwoId &&
                x.location[2].Id === locationThreeId &&
                x.location[3].Id === locationFourId
        );

        if (indexToRemoved > -1) {
            this.allLocations.splice(indexToRemoved, 1);
        }

        this.accountProductModel.LocationLevelOneId =
            locationOneId > 0 ? locationOneId : null;
        this.accountProductModel.LocationLevelTwoId =
            locationTwoId > 0 ? locationTwoId : null;
        this.accountProductModel.LocationLevelThreeId =
            locationThreeId > 0 ? locationThreeId : null;
        this.accountProductModel.LocationLevelFourId =
            locationFourId > 0 ? locationFourId : null;

        let item = this.mergedLocations.find(
            (x) => x.location[0].isSelected === true
        );

        if (item) {
            item.location[0].isSelected = false;
        }

        this.applyQtyTo =
            locationFourId > 0
                ? 4
                : locationThreeId > 0
                ? 3
                : locationTwoId > 0
                ? 2
                : 1;

        data.location[0].isSelected = true;

        this.availableQtyTobeMoved =
            this.applyQtyTo === 4
                ? data.location[3].Qty > 0
                    ? data.location[3].Qty
                    : data.location[3].TotalQty
                : this.applyQtyTo === 3
                ? data.location[2].Qty > 0
                    ? data.location[2].Qty
                    : data.location[2].TotalQty
                : this.applyQtyTo === 2
                ? data.location[1].Qty > 0
                    ? data.location[1].Qty
                    : data.location[1].TotalQty
                : data.location[0].Qty > 0
                ? data.location[0].Qty
                : data.location[0].TotalQty;

        this.hideDestination = false;
    }

    public onMoveQuantityValueChange() {
        this.moveQtyExceed =
            this.accountProductModel.QuantityOnStock &&
            this.accountProductModel.QuantityOnStock > 0 &&
            this.accountProductModel.QuantityOnStock >
                this.availableQtyTobeMoved;
    }

    private disableSaving(): boolean {
        if (
            this.showLevelOne &&
            !this.showLevelTwo &&
            !this.showLevelThree &&
            !this.showLevelFour
        ) {
            return (
                this.showAddQuantitySpinner ||
                !this.accountProductModel.QuantityOnStock ||
                this.accountProductModel.QuantityOnStock === 0 ||
                this.accountProductModel.LocationLevelOneId === 0 ||
                this.removeQtyExceed
            );
        }
        if (
            this.showLevelOne &&
            this.showLevelTwo &&
            !this.showLevelThree &&
            !this.showLevelFour
        ) {
            return (
                this.showAddQuantitySpinner ||
                !this.accountProductModel.QuantityOnStock ||
                this.accountProductModel.QuantityOnStock === 0 ||
                this.accountProductModel.LocationLevelOneId === 0 ||
                this.accountProductModel.LocationLevelTwoId === 0 ||
                this.removeQtyExceed
            );
        }
        if (
            this.showLevelOne &&
            this.showLevelTwo &&
            this.showLevelThree &&
            !this.showLevelFour
        ) {
            return (
                this.showAddQuantitySpinner ||
                !this.accountProductModel.QuantityOnStock ||
                this.accountProductModel.QuantityOnStock === 0 ||
                this.accountProductModel.LocationLevelOneId === 0 ||
                this.accountProductModel.LocationLevelTwoId === 0 ||
                this.accountProductModel.LocationLevelThreeId === 0 ||
                this.removeQtyExceed
            );
        }
        if (
            this.showLevelOne &&
            this.showLevelTwo &&
            this.showLevelThree &&
            this.showLevelFour
        ) {
            return (
                this.showAddQuantitySpinner ||
                !this.accountProductModel.QuantityOnStock ||
                this.accountProductModel.QuantityOnStock === 0 ||
                this.accountProductModel.LocationLevelOneId === 0 ||
                this.accountProductModel.LocationLevelTwoId === 0 ||
                this.accountProductModel.LocationLevelThreeId === 0 ||
                this.accountProductModel.LocationLevelFourId === 0 ||
                this.removeQtyExceed
            );
        }
    }

    public openLocationModal(productId) {
        this.modalService.open(this.locationModal, {
            windowClass: "fit-content",
        });
        this.loadDataByAccountProductId(productId);
    }

    public loadDataByAccountProductId(id) {
        return new Promise((resolve) => {
            this.productLogService
                .getProductCurrentQtyByAccountProductId(
                    id,
                    super.currentAccountId(),
                    ""
                )
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    let data = response.Data;
                    this.constructProductTreeData(data).then((treeData) => {
                        this.productQtyTreeData = treeData;
                    });
                    resolve(data);
                });
        });
    }

    public addMoreProducts() {
        this.router.navigateByUrl("/products");
    }

    ///TREE
    public constructProductTreeData(data) {
        return new Promise((resolve) => {
            let treeViewData = [];

            let levelOneData: any = [];
            let levelTwoData: any = [];
            let levelThreeData: any = [];
            let levelFourData: any = [];

            data.forEach((item) => {
                if (item.LocationLevelOneId > 0) {
                    item.LocationLevelOne.forEach((item1) => {
                        levelOneData = {
                            parentId: item1.ParentId,
                            name: item1.LocationName,
                            id: item1.Id,
                            unique_id: item1.Id,
                            qty: item1.Qty,
                            totalQty: item1.TotalQty,
                            children: [],
                            isExpanded: true,
                        };

                        // var locationTwo = item.LocationLevelTwo.filter(x => x.TotalQty !== 0);

                        item.LocationLevelTwo.forEach((item2) => {
                            levelTwoData = {
                                parentId: item2.ParentId,
                                name: item2.LocationName,
                                id: item2.Id + item2.ParentId,
                                unique_id: item2.Id,
                                qty:
                                    item2.TotalQty === item2.Qty
                                        ? 0
                                        : item2.Qty,
                                totalQty: item2.TotalQty,
                                children: [],
                                isExpanded: true,
                            };
                            if (levelOneData.unique_id === item2.ParentId) {
                                levelOneData.children.push(levelTwoData);
                            }

                            // var locationThree = item.LocationLevelThree.filter(x => x.TotalQty !== 0);

                            item.LocationLevelThree.forEach((item3) => {
                                levelThreeData = {
                                    parentId: item3.ParentId,
                                    name: item3.LocationName,
                                    id: item3.Id + item3.ParentId,
                                    unique_id: item3.Id,
                                    qty: 0, //item3.Qty,
                                    totalQty: item3.TotalQty,
                                    children: [],
                                    isExpanded: true,
                                };

                                if (levelTwoData.unique_id === item3.ParentId) {
                                    levelTwoData.children.push(levelThreeData);
                                }

                                // var locationFour = item.LocationLevelFour.filter(x => x.TotalQty !== 0);

                                item.LocationLevelFour.forEach((item4) => {
                                    levelFourData = {
                                        parentId: item4.ParentId,
                                        name: item4.LocationName,
                                        id: item4.Id + item4.ParentId,
                                        unique_id: item4.Id,
                                        qty: 0, //item3.Qty,
                                        totalQty: item4.Qty,
                                        children: [],
                                        isExpanded: true,
                                    };

                                    if (
                                        levelThreeData.unique_id ===
                                        item4.ParentId
                                    ) {
                                        levelThreeData.children.push(
                                            levelFourData
                                        );
                                    }
                                });
                            });
                        });
                    });
                    treeViewData.push(levelOneData);
                }
            });

            resolve(treeViewData);
        });
    }

    public deductQty(data): void {
        this.applyQtyTo = data.location.length;
        if (data.location[0])
            this.accountProductModel.LocationLevelOneId = data.location[0].Id;
        if (data.location[1])
            this.accountProductModel.LocationLevelTwoId = data.location[1].Id;
        if (data.location[2])
            this.accountProductModel.LocationLevelThreeId = data.location[2].Id;
        if (data.location[3])
            this.accountProductModel.LocationLevelFourId = data.location[3].Id;
        this.accountProductModel.QuantityOnStock = this.getItemQuantity(data);
        this.enableQty = true;
    }

    public reloadGrid() {
        this.triggerParentSearch.next();
    }
}
