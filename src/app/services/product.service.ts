import { Injectable, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';
import { IPagedResults } from '../core/interfaces';
import { BaseComponent } from '../core/components';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ProductService extends BaseDataService {
    private manufacturers: any;
    private groups: any;
    private colorAndMaterials: any;
    private models: any;

    @ViewChild('base') base : BaseComponent;

    constructor(protected dataService: DataService, private httpClient: HttpClient) {
        super(dataService, API_URL);
    }
    
    get getManufacturers(): any {
       return this.manufacturers;
    }

    get getGroups(): any {
        return this.groups;
     }

     get getColorAndMaterials(): any {
        return this.colorAndMaterials;
     }

     get getModels(): any {
        return this.models;
     }


    // PRE-LOADED DATA
    public load() {
        let requests: any = [];

        let getManufacturersUrl = `Product/GetAllManufacturers?id=${0}`;
        let getGroupsUrl = `Product/GetAllGroups?id=${0}`;
        let getColorAndMaterialsUrl = `Product/GetAllColorMaterials?id=${0}`;
        let getModelsUrl = `Product/GetAllModels`;

        requests.push(super.getAll<any[]>(getManufacturersUrl).map((response: any) => {
            return { 'data' : response.Data, 'type': 'manufacturer' };
        }));

        requests.push(super.getAll<any[]>(getGroupsUrl).map((response: any) => {
            return { 'data' : response.Data, 'type': 'group' };
        }));

        requests.push(super.getAll<any[]>(getColorAndMaterialsUrl).map((response: any) => {
            return { 'data' : response.Data, 'type': 'colorAndMaterial' };
        }));

        requests.push(super.getAll<any[]>(getModelsUrl).map((response: any) => {
            return { 'data' : response.Data, 'type': 'models' };
        }));

        forkJoin(requests).subscribe((response: any) => {
            this.manufacturers = response.find(e => e.type === 'manufacturer').data;
            this.groups = response.find(e => e.type === 'group').data;
            this.colorAndMaterials = response.find(e => e.type === 'colorAndMaterial').data;
            this.models = response.find(e => e.type === 'models').data;
        }, err => {
            return Observable.throw(err);
        });

    }

    public getProductCounter(model: any){
        let url = `Product/Counter?AccountId=${model.AccountId}&GroupId=${model.GroupId}&ColorMatId=${model.ColorMatId}&ManufacturerId=${model.ManufacturerId}&ModelId=${model.ModelId}&KeyWord=${model.Keyword}&Displaying=${model.Displaying}&ViewType=${model.ViewType}&QtyView=${model.QtyView}&RoleName=${model.RoleName}`;   
        return super.getAll<any>(url);
    }

    public getProduct(id, accountId, location = '') {
        let url = `Product/Get?id=${id}&accountId=${accountId}&locationIds=${location}`;    
        return super.getAll<any>(url);
    }

    public getAll(){
        let url = `Product/GetAll`;    
        return super.getAll<any>(url);
    }

    public getAllIsSystemOnly(): Observable<IPagedResults<any>>{
        let url = `Product/GetAllIsSystemOnly`;    
        return super.getAll<any>(url);
    }

    public getByAccountList(id: number): Observable<IPagedResults<any>>{
        let url = `Product/GetAllByAccountId?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getProductsFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, locationId, keyWord, currentAccountId = 0, displaying = 100, viewType = 0, qtyView = 0): Observable<IPagedResults<any>>{
        let url = `Product/GetProductsFiltered?accountId=${accountId}&groupId=${groupId}&colorMatId=${colorMatId}&manufacturerId=${manufacturerId}&modelId=${modelId}&locationIds=${locationId || ''}&modelId=${modelId}&keyWord=${keyWord}&currentAccountId=${currentAccountId}&displaying=${displaying}&viewType=${viewType}&qtyView=${qtyView}&fromWeb=true`;    
        return super.getAll<any>(url);
    }

    public getProductsFilteredImages(accountId, groupId, colorMatId, manufacturerId, modelId, locationId, keyWord, currentAccountId = 0, displaying = 100, viewType = 0, qtyView = 0): Observable<IPagedResults<any>>{
        let url = `Product/GetProductImages?accountId=${accountId}&groupId=${groupId}&colorMatId=${colorMatId}&manufacturerId=${manufacturerId}&modelId=${modelId}&locationId=${locationId || '0'}&locationIds=${locationId || ''}&keyWord=${keyWord}&currentAccountId=${currentAccountId}&displaying=${displaying}&viewType=${viewType}&qtyView=${qtyView}`;    
        return super.savePDF<any>(url,'application/pdf');
    }

    public getAllManufacturers(id: number): Observable<any>{
        let url = `Product/GetAllManufacturers?id=${id}`;
        return super.getAll<any>(url);
    }

    public getAllColorMat(id: number): Observable<any>{
        let url = `Product/GetAllColorMaterials?id=${id}`;
        return super.getAll<any>(url);
    }

    public getAllGroups(id: number): Observable<any>{
        let url = `Product/GetAllGroups?id=${id}`;
        return super.getAll<any>(url);
    }

    public getAllModels(): Observable<any>{
        let url = `Product/GetAllModels`;
        return super.getAll<any>(url);
    }

    public getModelsFiltered(groupId: any, manufacturerId: any): Observable<any>{
        let url = `Product/GetModelsFiltered?groupId=${groupId}&manufacturerId=${manufacturerId}`;
        return super.getAll<any>(url);
    }

    public getProductNumberIncrementValue(groupCode, colorMatCode, manufacturerCode, modelCode){
        let url = `Product/ProductNumberIncrement?groupCode=${groupCode}&colorMatCode=${colorMatCode}&manufacturerCode=${manufacturerCode}&modelCode=${modelCode}`;
        return super.getAll<any>(url);
    }

    //LISTS
    public getAllManufacturersList(): Observable<any>{
        let url = `Product/GetAllManufacturersList`;
        return super.getAll<any>(url);
    }

    public getAllColorMatList(): Observable<any>{
        let url = `Product/GetAllColorMaterialsList`;
        return super.getAll<any>(url);
    }

    public getAllGroupsList(): Observable<any>{
        let url = `Product/GetAllGroupsList`;
        return super.getAll<any>(url);
    }

    public getAllModelsList(): Observable<any>{
        let url = `Product/GetAllModelList`;
        return super.getAll<any>(url);
    }

    public getAccountProducts(accountId: number, productId: number){
        let url = `Product/GetAccountProducts?accountId=${accountId}&productId=${productId}`;
        return super.getAll<any>(url);
    }

    public save(data, params?, header?){
        let url = `Product/Save`;
        return super.save<any>(url, data, params, header);
    }

    public book(data, params?, header?){
        let url = `Product/Booked`;
        return super.save<any>(url, data, params, header);
    }

    public deleteBooking(data, params?, header?){
        let url = `Product/Delete/Booked`;
        return super.save<any>(url, data, params, header);
    }

    public getProductBooking(id: number) : any {
        let url = `Product/Booked?productId=${id}`;
        return super.getAll<any>(url);
    }

    public saveToAccount(data){
        let url = `Product/SaveToAccount`;
        return super.save<any>(url,data);
    }

    public moveQtyFromBooking(data){
        let url = `Product/MoveQtyFromBooking`;
        return super.save<any>(url,data);
    }

    public delete(data){
        let url = `Product/Delete`;
        return super.save<any>(url,data);
    }

    public deleteToAccount(data){
        let url = `Product/DeleteFromAccount`;
        return super.save<any>(url,data);
    }

    public saveProductModel(data){
        let url = `Product/SaveProductModel`;
        return super.save<any>(url,data);
    }

    public deleteProductModel(data){
        let url = `Product/DeleteProductModel`;
        return super.save<any>(url,data);
    }

    public getProductModelListData(accountId: number){
        let url = `Product/GetAllModelsByAccount?accountId=${accountId}`;
        return super.getAll<any>(url);
    }
    
    public getProductPhoto(productId: number){
        let url = `Product/GetProductPhotos?productId=${productId}`;
        return super.getAll<any>(url);
    }

    public saveProductPhoto(id, accountId, isMain, file, rotationAngle, otherPhotoUrl){
        let url = `Product/${id}/${accountId}/${isMain}/${rotationAngle}/${otherPhotoUrl}/SaveProductPhoto`;
        return super.upload(url, file);
    }

    public saveOtherProductPhotos(id, file){
        let url = `Product/${id}/SaveOtherProductPhotos`;
        return super.uploadBulk(url, file);
    }

    public deleteProductPhoto(data){
        let url = `Product/DeleteProductPhoto`;
        return super.save<any>(url,data);
    }

    public setMainPhoto(photo){
        let url = `Product/SetMainPhoto`;
        return super.save<any>(url,photo);
    }

    public getManufacturer(id: number){
        let url = `Product/GetManufacturer?id=${id}`;
        return super.getAll<any>(url);
    }

    public saveManufacturer(data){
        let url = `Product/SaveManufacturer`;
        return super.save<any>(url, data);
    }

    public deleteManufacturer(data){
        let url = `Product/DeleteManufacturer`;
        return super.save<any>(url, data);
    }

    public getColorMaterial(id: number){
        let url = `Product/GetProductColorMaterial?id=${id}`;
        return super.getAll<any>(url);
    }

    public saveProductColorMaterial(data){
        let url = `Product/SaveColorMaterial`;
        return super.save<any>(url, data);
    }

    public deleteProductColorMaterial(data){
        let url = `Product/DeleteColorMaterial`;
        return super.save<any>(url, data);
    }

    public getProductGroup(id: number){
        let url = `Product/GetProductGroup?id=${id}`;
        return super.getAll<any>(url);
    }

    public saveProductGroup(data){
        let url = `Product/SaveProductGroup`;
        return super.save<any>(url, data);
    }

    public deleteProductGroup(data){
        let url = `Product/DeleteProductGroup`;
        return super.save<any>(url, data);
    }

    public print(ids, accountId = '', location = '0', pdf = true, report = ''): any {
        let userId = report == "ProductCart" ? `&userId=` + super.currentLoggedUser().id : null
        let url = `Product/Print?ids=${ids}&accountId=${accountId}&locations=${location}&pdf=${pdf}&report=${report}${userId ? userId : ''}`;
        return super.savePDF<any>(url, pdf ? 'application/pdf' : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }

    public email(model, accountId, userId) {
        const url = `${API_URL}/Product/Email?accountId=${accountId}&userId=${userId}`;

        return this.dataService.post(url, model);
    }

    public changeToSystemProduct(productId) { 
        return this.dataService.put(`${API_URL}/Product/ChangeToSystemProduct?productId=${productId}`);
    }

    public getGrid(model, pageNumber = 0 , pageSize = 200, sortOrder = '') {
        let includeLabel = this.issuperadminoradmin() || this.currentRole() === 'editor'
        const url = `${API_URL}/Product/Grid?pageSize=${pageSize}&pageNumber=${pageNumber}&sortOrder=${sortOrder}&includeLabels=${includeLabel}`;

        return this.dataService.post(url, model);
    }

    public getGridExport(model, pageNumber = 0 , pageSize = 200, sortOrder = '', report = "ProductTileCatalog") {
        let includeLabel = this.issuperadminoradmin() || this.currentRole() === 'editor'
        const url = `${API_URL}/Product/Grid/Export?pageSize=${pageSize}&pageNumber=${pageNumber}&sortOrder=${sortOrder}&includeLabels=${includeLabel}&report=${report}`;
        return this.dataService.downloadBlob(url, "application/octet-stream", model);
    }

    public export(model, pageNumber = 0 , pageSize = 200, sortOrder = '', isPdf = true) {
        const url = `${API_URL}/Product/Export?model=${model}&pageSize=${pageSize}&pageNumber=${pageNumber}&sortOrder=${sortOrder}&isPdf=${isPdf}`;
        return this.httpClient.get(url, {
            observe: 'events',
            reportProgress: true,
            responseType: 'blob'
        });
    }

    public printJob(model, pageNumber = 0 , pageSize = 200, sortOrder = '', isPdf = true) {
        return `${API_URL}/Product/PrintJob?model=${model}&pageSize=${pageSize}&pageNumber=${pageNumber}&sortOrder=${sortOrder}&isPdf=${isPdf}`;
    }

    public getAllIdsForLabelPrinting(model): Observable<any>{
        let url = `${API_URL}/Product/GetAllIdsForLabelPrinting`;
        return this.dataService.post(url, model);
    }
}
