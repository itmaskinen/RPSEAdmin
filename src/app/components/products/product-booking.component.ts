import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/observable/forkJoin';
import { BaseComponent } from '../../core/components';
import { componentsTransition } from '../../router.animations';
import { MapperService, ModalService } from '../../shared';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ColDef, GridApi } from 'ag-grid-community';
import { AccountProductModel, AccountProductQtyLogModel, ProductBookingModel, ProductModel } from '../../models';
import { ProductsCreateEditComponent } from './products-create-edit.component';
import { LocationService, ProductService } from '../../services';
import { Observable } from 'rxjs/Observable';
import { ButtonGroupComponent } from '../../core/components/ag-grid-controls/button-group/button-group.component';
import { ToastsManager } from 'ng2-toastr';
import { finalize } from 'rxjs/operators';
import { concat } from 'rxjs/observable/concat';
import * as moment from 'moment-timezone'

@Component({
    selector: 'app-product-booking',
    templateUrl: './product-booking.component.html',
    styleUrls: ['../layout/layout.component.scss', '../cart/cart.css',"./products.css"],
    animations: [componentsTransition()]
})
export class ProductBookingComponent extends BaseComponent implements OnInit {
    @ViewChild('productsCreateEditComponent') productsCreateEditComponent: ProductsCreateEditComponent;
    @ViewChild("productTransferQtyFormContent") productTransferQtyFormContent: ElementRef;
    @ViewChild('bookingAgGrid') bookingAgGrid: ElementRef;

    public hasChanges: boolean = false;
    public showSpinner: boolean;
    public showQuantityLogs: boolean;
    public currentDate: any;
    public productId: number;
    public productNumber: string;
    public productName: string;

    public groupBySrc: any;

    //#region Book Form Variables
    public bookFormModel: ProductBookingModel;
    public destinationLocations: any;
    public destinationLocationsCopy: any;
    public sourceLocations: any;
    public isEdit: boolean;
    //#endregion

    //#region Grid variables
    public columnDefs: ColDef[];
    public defaultColDef: any;
    public gridApi: GridApi;
    public gridColumnApi: any;
    public params: any;
    public rowData: Array<ProductBookingModel>;
    public deletedRowData: Array<ProductBookingModel>;
    public frameworkComponents: any;
    //#endregion

    // rowIndex = null;
    public hideDestination = true;
    public allLocations = [];
    public moveProductSourceLocation: any = [];
    public moveProductSourceLocationIds: any = [];
    public productToBeModeQty: any;
    public productToBeMoveQuantityOnStock: any;
    public moveQtyExceed = false;
    public productQtyLogModel: any;
    public accountProductModel: any;
    public moveProductDestinationLocationIds: any = []; 
    public productQtyLogDstModel: any;
    public accountProductDstModel: any;
    public moveBookingModel: ProductBookingModel;
    public isSelectAll = true;
    public numberOfUnsaved: any = 0;

    constructor(
        private productService: ProductService,
        private modalService: ModalService,
        private activeModal: NgbActiveModal,
        private mapperService: MapperService,
        private locationService: LocationService,
        private toastr: ToastsManager,
        private elementRef: ElementRef,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
    ) {
        super(location, router, route);
    }
    
    public ngOnInit()
    {
       
        this.productId = Number(sessionStorage.getItem('product_grid_selected_row'));
        this.productNumber = sessionStorage.getItem('product_number_grid_selected_row');
        this.productName = sessionStorage.getItem('product_name_grid_selected_row');
        this.currentDate = moment(new Date()).format('YYYY-MM-DD');
        this.isEdit = false;
        this.showQuantityLogs = false;
        this.showSpinner = true;
        this.prepareColumnDef();   
        this.sourceLocations = [];
        this.destinationLocations = [];
        this.bookFormModel = new ProductBookingModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();
        this.accountProductModel = new AccountProductModel();
        this.productQtyLogDstModel = new AccountProductModel();
        this.accountProductDstModel = new AccountProductModel();
        this.moveBookingModel = new ProductBookingModel();
        this.bookFormModel.BookedDate = this.currentDate;
        this.rowData = [];
        this.deletedRowData = [];
        this.groupBySrc = [];

        let currentUser = super.currentLoggedUser();
        let productFilter = super.getProductFilterStore();
        productFilter = productFilter[productFilter.length-1];
        let accountId = productFilter ? productFilter.ownerId : currentUser.accountId;
        
        let bookingPromise = new Promise((resolve) => {
            return this.productService.getProductBooking(this.productId)
                .catch((err: any) => {
                    return Observable.throw(err);
                }).subscribe((response: any) => {
                    response.Data.forEach(e => {
                        this.rowData.push({
                            Id: e.Id,
                            ProductId: e.ProductId,
                            Quantity: e.Quantity,
                            BookedDate: moment(new Date(e.BookedDate)).format('YYYY-MM-DD'),
                            DestinationLocationId: e.DestinationLocationId,
                            DestinationLocation: '',
                            SourceLocationId: e.SourceLocationId,
                            SourceLocation: '',
                            Room: e.Room,
                            Comment: e.Comment,
                            IsUpdatingRow: false,
                            IsAddRow: false
                        });
                    });

                    if (response.Data) {
                        this.groupBySrc = response.Data.reduce((acc, item) => {
                            let accItem = acc.find(ai => ai.SourceLocationId === item.SourceLocationId)

                            if(accItem) accItem.Quantity += item.Quantity
                            else acc.push(item)

                            return acc;
                        }, [])
                    }

                    resolve(response);
                })
        })
        
        bookingPromise.then(() => {
            this.locationService.getAccountProductLocations(this.productId, accountId === 0 ? super.currentAccountId() : accountId, true, '')
            .catch((err: any) => {
                return Observable.throw(err);
            }).finally(() => {
                this.showQuantityLogs = true;
            })
            .subscribe((response: any) => {
            let mergedLocations = this.mapperService.mergedLocationLevels(response.Data);

                mergedLocations.forEach(e => {
                    let obj = {
                        id: (e.location.filter(x => x.Id > 0)).map(x => x.Id).join(','),
                        name: (e.location.filter(x => x.Id > 0)).map(x => x.Name).join(' > ')
                    }
                    this.destinationLocations.push(obj);
                    this.destinationLocationsCopy = [...this.destinationLocations];
                });
            });
        })

        
    }

    get hasValidFormData () {
        return this.bookFormModel.Quantity > 0 && this.bookFormModel.SourceLocationId != '0' && this.bookFormModel.DestinationLocationId != '0'
            // && new Date(this.bookFormModel.BookedDate) >= new Date(this.currentDate);
    }

    public navigateToList(bookingUnsavedChanges: string){
        sessionStorage.removeItem('product_grid_selected_row');
        sessionStorage.removeItem('product_number_grid_selected_row');
        sessionStorage.removeItem('product_name_grid_selected_row');
        let hasAddedRows = this.rowData.some(e => e.IsAddRow);
        if (this.isEdit || this.deletedRowData.length > 0 || hasAddedRows || this.numberOfUnsaved > 0) {
            this.modalService.open(bookingUnsavedChanges).result.then((result) => {
                if (result === "Yes") {
                    this.activeModal.close();
                    this.modalService.getData(this.hasChanges);
                }
            }, (reason) => {});
        }
        else {
            this.activeModal.close();
            this.modalService.getData(this.hasChanges);
        }
    }

    getItemQuantity(item: any, id: any) {
        let bookedQty = 0;
        if (this.groupBySrc.find(e => e.SourceLocationId === id)) {
            bookedQty = this.groupBySrc.length > 0 ? this.groupBySrc.filter((x) => x.SourceLocationId === id)[0].Quantity : 0;
        }

        const quantity = item.location.length > 4 && item.location[3].Id > 0 ? (item.location[3].Qty > 0 && item.location[3].TotalQty == bookedQty ? 0 : bookedQty > item.location[3].TotalQty ? item.location[3].TotalQty : item.location[3].TotalQty - bookedQty) :
        item.location.length > 3 && item.location[2].Id > 0 ? (item.location[2].Qty > 0 && item.location[2].TotalQty == bookedQty ? 0 : bookedQty > item.location[2].TotalQty ? item.location[2].TotalQty : item.location[2].TotalQty - bookedQty) :
        item.location.length > 2 &&item.location[1].Id > 0 ?  (item.location[1].Qty > 0 && item.location[1].TotalQty == bookedQty ? 0 : bookedQty > item.location[1].TotalQty ? item.location[1].TotalQty : item.location[1].TotalQty - bookedQty) :
        item.location.length > 1 &&item.location[0].Id > 0 ? (item.location[0].Qty > 0 && item.location[0].TotalQty == bookedQty ? 0 : bookedQty > item.location[0].TotalQty ? item.location[0].TotalQty : item.location[0].TotalQty - bookedQty) : 0;

        return quantity;
    }

    getTotalQuantity(item: any) {
        const quantity = item.location.length > 4 && item.location[3].Id > 0 ? item.location[3].TotalQty :
        item.location.length > 3 && item.location[2].Id > 0 ? item.location[2].TotalQty :
        item.location.length > 2 && item.location[1].Id > 0 ?  item.location[1].TotalQty :
        item.location.length > 1 && item.location[0].Id > 0 ? item.location[0].TotalQty : 0;

        return quantity;
    }

    //#region Grid Functions
    public prepareColumnDef() {
        this.columnDefs = [
            {
                headerName: "Action",
                field: "Id",
                width: 100,
                cellStyle: { textAlign: "center", cursor: "pointer" },
                editable: false,
                cellRenderer: "buttonGroup",
                checkboxSelection: true,
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: () => {
                                return `<i class="fa fa-pencil" style="font-size: 15px;"></i>`;
                            },
                            onClick: (params) => {
                                this.editProductBooking(params);
                            }
                        },
                        {
                            getContent: () => {
                                return `<i class="fa fa-exchange" style="font-size: 15px;"></i>`;
                            },
                            onClick: (params) => {
                                this.moveProductBooking(this.productTransferQtyFormContent, params.data);
                            }
                        },
                        {
                            getContent: () => {
                                return `<i class="fa fa-trash" style="font-size: 15px;"></i>`;
                            },
                            onClick: (params) => {
                                this.deleteProductBooking(params);
                            }
                        }
                    ],
                },
            },
            { headerName: 'From', field: 'SourceLocation', width: 180 },
            { headerName: 'Quantity', field: 'Quantity', width: 80 },
            { headerName: 'To', field: 'DestinationLocation', width: 180 },
            { headerName: 'Room', field: 'Room', width: 150 },
            { headerName: 'Date', field: 'BookedDate', width: 150 },
            { headerName: 'Comment', field: 'Comment', width: 350 }
        ];

        this.defaultColDef = {
            'flex': 1,  
            'resizable': true,
            'editable': false,
            'minWidth': 110,
        };

        this.frameworkComponents = {
            buttonGroup: ButtonGroupComponent,
        };
    }

    public onGridReady(params) {
        this.params = params;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

        let verticialScrollElement: HTMLElement = this.elementRef.nativeElement.querySelector('.ag-body-viewport.ag-layout-normal');
        verticialScrollElement.style.paddingBottom = '12px';
    }

    public addProductBooking() {
        let sourceLocationTotalQty = this.rowData.filter(x => x.SourceLocationId === this.bookFormModel.SourceLocationId)
                        .map(x => x.Quantity)
                        .reduce((currVal,prevVal) => Number(currVal) + Number(prevVal), 0);
        
        if(this.isEdit){
            let item = this.rowData.find(x => x.SourceLocationId === this.bookFormModel.SourceLocationId && x.Id == this.bookFormModel.Id && x.IsUpdatingRow);

            let quantity = this.bookFormModel.Quantity;

            let sourceLocationItems = this.rowData.filter(x => x.SourceLocationId === this.bookFormModel.SourceLocationId);
            sourceLocationTotalQty = sourceLocationItems.map(x => x.Quantity).reduce((currVal,prevVal) => Number(currVal) + Number(prevVal), 0);

            this.sourceLocations.filter(x => x.id === this.bookFormModel.SourceLocationId).map(y => y.availableQty = y.availableQty + item.Quantity)

            if(!this.hasEnoughQuantity(sourceLocationTotalQty)){
                this.toastr.error(`Quantity exceed to the limit.`, 'Error');
                this.sourceLocations.filter(x => x.id === this.bookFormModel.SourceLocationId).map(y => y.availableQty = y.availableQty - item.Quantity);
                return;
            }

            this.sourceLocations.filter(x => x.id === this.bookFormModel.SourceLocationId).map(y => y.availableQty = y.availableQty - quantity)

            item.Quantity = quantity;
            item.Room = this.bookFormModel.Room === '' ? item.Room : item.Room = this.bookFormModel.Room;
            item.Comment = this.bookFormModel.Comment === '' ? item.Comment : item.Comment = this.bookFormModel.Comment;
            item.DestinationLocationId = this.bookFormModel.DestinationLocationId;
            item.DestinationLocation = this.destinationLocations.find(x => x.id === this.bookFormModel.DestinationLocationId).name;
            item.BookedDate = this.bookFormModel.BookedDate;

            this.gridApi.updateRowData({ update: this.rowData });
            this.isEdit = false;
            this.checkRowDataHasChanges();
        } else {
            if(!this.hasEnoughQuantity(sourceLocationTotalQty)){
                this.toastr.error(`Quantity exceed to the limit.`, 'Error');
                return;
            }
            
            this.sourceLocations.filter(x => x.id === this.bookFormModel.SourceLocationId)
                                .map(y => y.availableQty = y.availableQty - this.bookFormModel.Quantity)

            this.bookFormModel.IsAddRow = true;
            this.rowData.push(this.bookFormModel);

            this.gridApi.updateRowData({
                add: [this.bookFormModel],
                addIndex: 0,
            });

            this.checkRowDataHasChanges();
        }

        this.toastr.warning(`Successfully added product booking to list. Please click save to add list to database.`, 'Added');

        this.bookFormModel = new ProductBookingModel();
    }

    public editProductBooking(params) {
        this.bookFormModel = new ProductBookingModel();
        this.bookFormModel = {...params.data};
        this.bookFormModel.BookedDate =  moment(new Date(params.data.BookedDate)).format('YYYY-MM-DD');
        params.data.IsUpdatingRow = true;
        this.isEdit = true;
    }

    // Checking for save button disable/enabled state.
    public checkRowDataHasChanges() {
        let count = this.rowData.filter(e => (e.IsAddRow && e.IsAddRow == true) || (e.IsUpdatingRow && e.IsUpdatingRow == true));
        let deleteCount = this.deletedRowData.length;
        this.numberOfUnsaved = count.length + deleteCount;
        return this.numberOfUnsaved == 0
    }

    public deleteProductBooking(params){
        this.rowData = this.rowData.filter(e => e !== params.data);

        if(params.data.Id > 0) {
            this.deletedRowData.push(params.data);
        }
        this.sourceLocations.filter(x => x.id === params.data.SourceLocationId)
            .map(y => y.availableQty = y.availableQty + params.data.Quantity)

        this.gridApi.updateRowData({ remove: [params.data] });
        this.checkRowDataHasChanges();
    }
    //#endregion
    

    //#endregion Book Form Functions
    public saveChanges() {
        this.showSpinner = true;
        if(this.rowData.length > 0 || this.deletedRowData.length > 0) {
            this.toastr.info(`Please dont' close this window`, 'Booking...');

            let httpRequests = this.rowData.map(e => {
                e.ProductId = this.productId;
                return this.productService.book(e);
            });
            
            let deleteHttpRequests = this.deletedRowData.map(e => {
                return this.productService.deleteBooking(e);
            });

            httpRequests = httpRequests.concat(deleteHttpRequests);

            concat(...httpRequests).pipe(finalize(() => {
                this.toastr.success(`Successfully saved product booking/s.`, 'Saved');
                this.deletedRowData = [];
                this.showSpinner = false;
            })).subscribe((response) => {
                let item = this.rowData.find(x => x.SourceLocationId === response.Data.SourceLocationId 
                    && x.DestinationLocationId === response.Data.DestinationLocationId 
                    && x.BookedDate === moment(new Date(response.Data.BookedDate)).format('YYYY-MM-DD'));
                if(!!item) {
                    item.Id = response.Extras;
                }

                this.rowData.map(x => x.IsAddRow = false);
                this.rowData.map(x => x.IsUpdatingRow = false);
            }, err => {
                this.toastr.error(`OOOPS! Looks like the developer forgot to do a round test. Please contact support.`, 'Internal server error!');
                this.showSpinner = false;
            });

            this.showSpinner = false;
            this.isEdit = false;
        }
        else{
            this.showSpinner = false;
            this.isEdit = false;
            this.toastr.warning('Please add a booking data.', 'No data')
        }
    }

    private hasEnoughQuantity(totalQty){
        let isValid = false;
        const availableQty = this.sourceLocations.find(x => x.id === this.bookFormModel.SourceLocationId).availableQty
        isValid = (availableQty > 0 && this.bookFormModel.Quantity <= availableQty) || totalQty == 0;
        return isValid;
    }

    public locationChange(id: any, source: boolean = false) {
        if(id === '0'){
            return;
        }

        if(source) {
            let sourceItem = this.sourceLocations.find(x => x.id === id);
            this.bookFormModel.SourceLocation = sourceItem.name;
            this.bookFormModel.Quantity = sourceItem.availableQty;
            this.destinationLocations = this.destinationLocationsCopy.filter(x => x.id !== id);
        } else{
            this.bookFormModel.DestinationLocation = this.destinationLocations.find(x => x.id === id).name;
        }
    }

    public mergedLocations(data: any) {
        data.forEach(e => {
            let concatenatedId = (e.location.filter(x => x.Id > 0)).map(x => x.Id).join(',')
            let obj = {
                id: concatenatedId,
                name: (e.location.filter(x => x.Id > 0)).map(x => x.LocationName).join(' > '),
                availableQty: this.getItemQuantity(e, concatenatedId),
                totalQty: this.getTotalQuantity(e)
            }
            if (obj.totalQty > 0) this.sourceLocations.push(obj);
        });

        if(this.sourceLocations.length === 1) {
            this.bookFormModel.SourceLocationId = this.sourceLocations[0].id;
            this.locationChange(this.bookFormModel.SourceLocationId, true);
        }

        this.rowData.forEach(e => {
            let dest = this.destinationLocationsCopy.find(x => x.id === e.DestinationLocationId);
            e.DestinationLocation = !!dest ? dest.name : '';
            let source = this.sourceLocations.find(x => x.id === e.SourceLocationId);
            e.SourceLocation = !!source ?  source.name : '';
        });

        this.rowData = this.rowData.filter(x => x.SourceLocation !== '');

        this.gridApi.updateRowData({
            add: this.rowData,
            addIndex: 0
        })

        this.showSpinner = false;
    }

    public emitLocation(data) {
        // Added condition when filtering source location
        // If it returns params has comma (,) means it has multiple levels then filter use includes
        // Else it is only a single location then filter will use equals 
        let location = this.sourceLocations.find(x => data.split(',').length > 0 ? x.id.includes(data) : x.id === data); 
        this.locationChange(location.id, true);
        this.bookFormModel.SourceLocationId = location.id;
    }
    //#endregion

    public moveProductBooking(
        productFormContent: any,
        item: any
    ) {
        this.moveBookingModel.Id = item.Id;
      
        this.moveProductSourceLocation = item.SourceLocation.split(">")
        this.moveProductSourceLocationIds = item.SourceLocationId.split(",");
        this.productToBeModeQty = item.Quantity;
       
        this.modalService.open(productFormContent, { size: "lg" });
    }

    public onSourceLocationClick(){
        this.productToBeMoveQuantityOnStock = this.productToBeModeQty;
        this.hideDestination = false;
    }

    public onDestinationLocationClick(item) {
       this.moveProductDestinationLocationIds = item.id.split(",");
    }

    public onMoveQuantityValueChange(){
        this.moveQtyExceed = this.productToBeMoveQuantityOnStock && this.productToBeMoveQuantityOnStock <= this.productToBeModeQty;
    }

    public splitDestination(data) {
        return data.split(">");
    }

    public moveProductToAccount(dialog: any) {
        this.productQtyLogModel.Qty = this.productToBeMoveQuantityOnStock;
        this.productQtyLogModel.Mode = 2;
        this.productQtyLogDstModel.Mode = 1;
        this.productQtyLogDstModel.Qty = this.productToBeMoveQuantityOnStock;
        
        this.accountProductModel.ProductId = this.productId

        this.accountProductModel.LocationLevelOneId = +this.moveProductSourceLocationIds[0];
        this.accountProductModel.LocationLevelTwoId = this.moveProductSourceLocationIds && this.moveProductSourceLocationIds[1] ? +this.moveProductSourceLocationIds[1] : 0;
        this.accountProductModel.LocationLevelThreeId = this.moveProductSourceLocationIds && this.moveProductSourceLocationIds[2] ? +this.moveProductSourceLocationIds[2] : 0;
        this.accountProductModel.LocationLevelFourId =  this.moveProductSourceLocationIds && this.moveProductSourceLocationIds[3] ? +this.moveProductSourceLocationIds[3] : 0;
        this.accountProductModel.AccountId = super.currentAccountId();
        this.accountProductModel.QuantityOnStock = this.productToBeMoveQuantityOnStock;
        this.accountProductModel.UpdatedBy = super.currentLoggedUser().id;
        this.accountProductModel.IsMobile = false;

        this.accountProductDstModel.ProductId = this.productId

        this.accountProductDstModel.LocationLevelOneId = +this.moveProductDestinationLocationIds[0];
        this.accountProductDstModel.LocationLevelTwoId = this.moveProductDestinationLocationIds && this.moveProductDestinationLocationIds[1] ? +this.moveProductDestinationLocationIds[1] : 0;
        this.accountProductDstModel.LocationLevelThreeId = this.moveProductDestinationLocationIds && this.moveProductDestinationLocationIds[2] ? +this.moveProductDestinationLocationIds[2] : 0;
        this.accountProductDstModel.LocationLevelFourId =  this.moveProductDestinationLocationIds && this.moveProductDestinationLocationIds[3] ? +this.moveProductDestinationLocationIds[3] : 0;
        this.accountProductDstModel.AccountId = super.currentAccountId();
        this.accountProductDstModel.QuantityOnStock = this.productToBeMoveQuantityOnStock;
        this.accountProductDstModel.UpdatedBy = super.currentLoggedUser().id;
        this.accountProductDstModel.IsMobile = false;

        this.moveBookingModel.SourceLocationId = this.moveProductDestinationLocationIds.toString();

        let srcId = this.moveProductSourceLocationIds[this.moveProductSourceLocationIds.length - 1];

        let srcMergedProductModel = Object.assign({
            ProductQtyLogModel : this.productQtyLogModel,
            AppliedTo: srcId,
            BookedDetailModel: this.moveBookingModel,
            ProductId: this.productId
        }, this.accountProductModel);

        var dstId = this.moveProductDestinationLocationIds[this.moveProductDestinationLocationIds.length - 1];
        let dstMergedProductModel = Object.assign({
            ProductQtyLogModel: this.productQtyLogDstModel,
            AppliedTo: dstId,
            QuantityOnStock: this.accountProductModel.QuantityOnStock,
            ProductId: this.productId
        }, this.accountProductDstModel);

        dialog('Close click');

        this.showSpinner = true;

        this.productService.moveQtyFromBooking(srcMergedProductModel)
        .catch(err => {
            this.showSpinner = false;
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        })
        .subscribe(res => {
            if(res.StatusCode === 409) {
                this.toastr.error(res.Message, "Conflict");
            }
            else if (res.ErrorCode){
                this.toastr.error(res.Message, "Error");
            }
            else {
                this.productService.moveQtyFromBooking(dstMergedProductModel).subscribe(() => {
                    this.toastr.success('Product Added to your database', 'Success')
                    this.ngOnInit();
                })
            }
            this.showSpinner = false;
        })
    }

    public printBookedLabels() {
        let data:any = this.gridApi.getSelectedRows();
        if (data && data.length > 0) {
            localStorage.setItem('book_labels_for_print', JSON.stringify(data));
            const printWindow = window.open(`#/products/public/0/${super.currentAccountId()}/print/booking/label`, "PRINT", "height=400,width=600")
        }
        else {
            this.toastr.error("Please select bookings to print", "Error");
        }
    }

    public selectAllForPrintLabel() {
        if (this.isSelectAll) {
            this.gridApi.forEachNode(function (node) {
                node.setSelected(node.data!.Quantity > 0);
            });
        }
        else {
            this.gridApi.deselectAll();
        }
        this.isSelectAll = !this.isSelectAll;
    }
}
