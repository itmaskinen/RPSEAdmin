import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-products-models',
    templateUrl: './products-models.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ProductsModelsComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    constructor() {}
    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
    }
}
