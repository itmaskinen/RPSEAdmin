export class ProductFilterModel { 
    public recordCount : number = 0;
    public manufacturers: any = [];
    public groups: any = [];
    public colorMaterials: any = [];
}