import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

declare var jsPDF: any; // Important

import { ProductService } from '../../services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { ButtonGroupComponent } from '../../core/components/ag-grid-controls/button-group/button-group.component';
import { ModalService } from '../../shared';

@Component({
    selector: 'app-products-groups-list',
    templateUrl: './products-groups-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ProductsGroupsListComponent extends BaseComponent implements OnInit {
    showSpinner = false;
    isAdminOrUserEditor: boolean;
    productGroupCount = 0;

    selectedGroup: any;
    public colDef: any;
    public defaultColDef: any;
    public frameworkComponents: any;
    @ViewChild('deleteContent') deleteContent: any;

    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private zone: NgZone,
        private modalService: ModalService) {
            super(location, router, route)
        }
        packageData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
        productGroupList: any = [];

    public key: string = 'Name';
    public reverse: boolean = false;
    public sort(key){
        this.key = key;
        this.reverse = !this.reverse;
    }
    public ngOnInit() {
        this.prepareColumnDef();
        this.loadData();
    }

    public  loadData(){
        this.showSpinner = true;
        this.productService.getAllGroupsList()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            this.packageData.next(response.Data);
            this.productGroupCount = response.Data.length;
            this.productGroupList = response.Data;
        });
    }

    public editProductGroup(id){
        this.router.navigate([`product-groups/${id}/edit`])
    }

    public confirmGroupDelete(content: any, item){
        this.selectedGroup = item;
        this.modalService.open(content, { size : 'sm'});
    }

    public deleteProductGroup(){
        if(!this.selectedGroup){
            this.toastr.error('Please select a group to delete.','Error')
            return false;
        }
        this.showSpinner = true;
        this.productService.deleteProductGroup(this.selectedGroup)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //workaround to autorefresh dom
                //need to find a cleaner way but this is it for now
                //set up a router for each listing component that uses datatable
                this.router.navigateByUrl('/product-groups/refresh').then(() => {
                    this.router.navigateByUrl('/product-groups');
                });
            }
        });
    }

    public exportToCSV(){
        let data = this.productGroupList.map((item) => {
            return {
                Name : item.Name,
                Description : item.Description
            }
        });
        super.jsonToCSV(data, 'Product Groups', true);
    }

    public exportToPDF(){
        let doc = new jsPDF('p', 'pt');
        let totalPagesExp = "{total_pages_count_string}";
        let dateInfo = moment(new Date()).format('YYYY-MM-DD');
        let headerTitle = `Product Groups`;
        let pdfName = `ProductGroups-${dateInfo}.pdf`;
        let pageWidth = doc.internal.pageSize.getWidth();
        let leftStart = 40;
        let rightStart = (pageWidth / 2);
        let centerStart = rightStart - 60;
        let contentYStart = 50; 
        doc.page = 1; 
        doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);

        let contentColumns = [
        { 
            title: 'Name', dataKey: 'Name'
        },
        { 
            title: 'Description', dataKey: 'Description'
        }];

        let data = this.productGroupList;

        doc.setFontType('bold');
        let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
        doc.text(headerTitle, pageWidth - (60 + headerWidth), contentYStart);
        contentYStart+= 10;
        
        var pageContent = ((data) => { 
            // FOOTER
            var str = "Page " + data.pageCount;
            if (typeof doc.putTotalPages === 'function') {
                str = str + " ";
            }
            doc.setFontSize(10);
            var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
            doc.text(str, leftStart, pageHeight  - 10);
        });

        doc.autoTable(contentColumns, data, {
            margin: { top: contentYStart, left: leftStart, right: 40, bottom: 0 },
            styles: { cellPadding: 2 },
            theme: 'plain',
            headerStyles : {
                fillColor: [40, 167, 69],
                textColor: 'white'
            },
            addPageContent: pageContent
        });
        contentYStart+= 50;

        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text("Printed: "+ dateInfo, leftStart, pageHeight  - 20);
        doc.text("© Copyright 2018 RP.SE ",190, pageHeight - 20);

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp);
        }

        doc.save(pdfName);
    }

    public prepareColumnDef() {
        this.colDef = [
            { headerName: 'Name', field: 'Name', cellStyle: {  cursor: 'pointer' },
            cellRenderer: 'buttonGroup',
            cellRendererParams : {
                buttonInfo: [
                    {
                      getContent: (params) => {
                        return  `<u>${params.data.Name}</u>`;
                      },
                      onClick: (params) => {
                          this.editProductGroup(params.data.Id);
                      }
                    }
                  ]
            }
            },
            { headerName: 'Description', field: 'Description'},
            { headerName: 'Actions ', field: 'Id', width: 20, cellStyle: { textAlign: 'center', cursor: 'pointer' }, sortable: false,
            cellRenderer: 'buttonGroup',
            cellRendererParams: {
              buttonInfo: [
                {
                  getContent: (params) => {
                    return  '<i class="fa fa-pencil text-center"></i>';
                  },
                  onClick: (params) => {
                      this.editProductGroup(params.data.Id);
                  }
                },
                {
                    getContent: (params) => {
                      return  '<i class="fa fa-trash text-center"></i>';
                    },
                    onClick: (params) => {
                        this.confirmGroupDelete(this.deleteContent, params.data);
                    }
                  }
              ]
            }
           }
        ];

        this.defaultColDef = {
            resizable: true,
            sortable: true,
            cellRenderer: (params) => {
                return params.value || '';
            },
            comparator: (str1, str2) => {
                return (str1 || '').localeCompare((str2 || ''), 'sv');
            },
            filter: false,
            filterParams: {
                newRowsAction: 'keep',
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                }
            }
        };

        
       this.frameworkComponents = {
         buttonGroup: ButtonGroupComponent
      };
    }

    public onGridReady(params) {
        params.api.sizeColumnsToFit();
    }

}
