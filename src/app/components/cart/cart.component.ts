import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { componentsTransition } from "../../router.animations";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { Observable } from "rxjs/Observable";
import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";
import { CheckoutModel } from "../../models";
import { ProductService, OrderService, AccountService } from "../../services";
import { BaseComponent } from "../../core/components";
import { Location } from "@angular/common";
import * as moment from "moment";
import { ModalService } from "../../shared";

declare var jsPDF: any; // Important
@Component({
    selector: "app-cart",
    templateUrl: "./cart.component.html",
    styleUrls: [
        "../layout/layout.component.scss",
        "../cart/cart.css",
        "../products/products.css",
    ],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()],
})
export class CartComponent extends BaseComponent implements OnInit {
    public showSpinner: boolean = false;
    public cartListCount: number = 0;
    public model: any;
    public isSendingEmail = false;
    public emailItems: any[] = [];
    public isSendCopyToMe = true;

    packageData: any[] = [];
    selectedOrder: any;
    selectedItemOrder: any;
    cartList: any = [];
    loadingMessage = "Loading...";
    user: any;

    public sendToEmailModel: {
        Email: string;
        Subject: string;
        Body: string;
        IncludePDF: boolean;
        IncludeExcel: boolean;
        BCC: string;
    };

    constructor(
        private toastr: ToastsManager,
        private location: Location,
        private route: ActivatedRoute,
        private router: Router,
        private productService: ProductService,
        private orderService: OrderService,
        private modalService: ModalService,
        private accountService: AccountService
    ) {
        super(location, router, route);

        this.sendToEmailModel = {
            Email: "",
            Subject: "",
            Body: "",
            IncludePDF: true,
            IncludeExcel: false,
            BCC: "",
        };
    }
    public ngOnInit() {
        this.model = new CheckoutModel();
        let userId = super.currentLoggedUser().id;
        this.loadCartListData(userId);
        this.getAccountSettings();
    }

    public loadCartListData(id: number) {
        this.showSpinner = true;
        this.orderService
            .getOrderByUsers(id, super.currentAccountId())
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                this.cartList = data;
                this.packageData = data;
                this.cartListCount = data.length;
            });
    }

    public getAccountSettings() {
        this.accountService
            .getAccountV3(super.currentAccountId())
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if (response.Data) {
                    this.model.EmailCheckoutCCs =
                        response.Data.EmailCheckoutCCs;
                }
            });
    }

    public completeCheckout() {
        this.showSpinner = true;
        this.loadingMessage = "Processing...";
        this.model.UserId = super.currentLoggedUser().id;
        this.orderService
            .checkOut(this.model)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {})
            .subscribe((response) => {
                let data = response.Data;
                //get back the processed data
                this.getCheckout(data.Id).then((checkoutData) => {
                    //create pdf out of the data
                    this.createCheckoutPDF(checkoutData).then(
                        (pdfString: string) => {
                            let sendEmailRequest = {
                                UserId: data.UserId,
                                CheckoutId: data.Id,
                                PDFByteString: pdfString,
                                EmailCheckoutCCs: this.model.EmailCheckoutCCs,
                            };
                            //finally send the pdf
                            this.sendCheckOutEmail(sendEmailRequest).then(
                                (res) => {
                                    this.ngOnInit();
                                }
                            );
                        }
                    );
                });
            });
    }

    public getCheckout(id) {
        this.loadingMessage = "Fetching...";
        return new Promise((resolve) => {
            this.orderService
                .getCheckOut(id)
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    //this.showSpinner = false;
                })
                .subscribe((response) => {
                    resolve(response.Data);
                });
        });
    }

    public sendCheckOutEmail(data) {
        this.loadingMessage = "Sending...";
        return new Promise((resolve) => {
            this.orderService
                .checkOutEmail(data)
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    resolve(true);
                });
        });
    }

    public createCheckoutPDF(checkoutData) {
        this.loadingMessage = "PDF...";
        return new Promise((resolve) => {
            let doc = new jsPDF("p", "pt");
            //HEADER
            let dateInfo = moment(new Date()).format("YYYY-MM-DD");
            let headerTitle = `ORDER ${checkoutData.OrderNumber}`;
            let pdfName = `ORDER-${checkoutData.OrderNumber}-${dateInfo}.pdf`;
            let pageWidth = doc.internal.pageSize.getWidth();
            let leftStart = 40;
            let rightStart = pageWidth / 2;
            let centerStart = rightStart - 60;
            let contentYStart = 50;
            doc.addImage(super.logo64base(), "JPEG", leftStart, 20, 60, 30);

            contentYStart += 10;
            doc.setDrawColor(40, 167, 69);
            doc.setLineWidth(5);
            doc.line(
                leftStart,
                contentYStart,
                pageWidth - leftStart,
                contentYStart
            );

            contentYStart += 30;

            doc.setDrawColor(0, 0, 0);
            //ORDER DATA
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text(
                `Order Number : ${checkoutData.OrderNumber}`,
                leftStart,
                contentYStart
            );
            contentYStart += 20;
            doc.text(
                `Order Placed : ${moment(checkoutData.CreatedDate).format(
                    "YYYY-MM-DD"
                )}`,
                leftStart,
                contentYStart
            );
            contentYStart += 50;

            doc.setFontSize(10);

            const shippindAddress =
                checkoutData.UserShippingAddress === null
                    ? ""
                    : checkoutData.UserShippingAddress;
            const instructions =
                checkoutData.AdditionalInstructions === null
                    ? ""
                    : checkoutData.AdditionalInstructions;

            doc.text(
                `Shipping to : ${shippindAddress}`,
                leftStart,
                contentYStart
            );
            contentYStart += 20;
            doc.text(
                `Additional instructions : ${instructions}`,
                leftStart,
                contentYStart
            );
            contentYStart += 20;

            doc.setFontSize(8);
            //QTY LIST
            if (checkoutData.UserOrder.length > 0) {
                let contentColumns = [
                    {
                        title: "Product Name/Product #",
                        dataKey: "Product",
                    },
                    {
                        title: "Qty",
                        dataKey: "Qty",
                    },
                    {
                        title: "Location",
                        dataKey: "Location",
                    },
                ];

                let data = checkoutData.UserOrder.map((item) => {
                    return {
                        Product: `${item.ProductName}\n${item.ProductNumber}`,
                        Qty: this.extractOrderQty(item.OrderDetails),
                        Location: this.extractOrderSource(item.OrderDetails),
                    };
                });
                doc.setFontType("bold");
                contentYStart += 20;

                doc.autoTable(contentColumns, data, {
                    margin: {
                        top: contentYStart,
                        left: leftStart,
                        right: 40,
                        bottom: 0,
                    },
                    styles: { overflow: "linebreak", columnWidth: "wrap" },
                    columnStyles: {
                        Product: { columnWidth: "auto" },
                        Qty: { columnWidth: 30 },
                        Location: { columnWidth: "auto" },
                    },
                    theme: "striped",
                    headerStyles: {
                        textColor: "white",
                        fillColor: [40, 167, 69],
                    },
                });
                contentYStart += 50;
            }

            //FOOTER
            doc.setFontSize(10);
            var pageHeight =
                doc.internal.pageSize.height ||
                doc.internal.pageSize.getHeight();
            doc.text("Printed: " + dateInfo, 50, pageHeight - 20);
            doc.text("© Copyright 2018 RP.SE ", 190, pageHeight - 20);
            //doc.save(pdfName);
            let pdfData = doc
                .output("datauri")
                .replace("data:application/pdf;base64,", "");
            resolve(pdfData);
        });
    }

    public extractOrderSource(data) {
        let template = "";
        data.forEach((item) => {
            template += `${item.Location}`;
            template += `\n`;
        });
        return template;
    }

    public extractOrderQty(data) {
        let template = "";
        data.forEach((item) => {
            template += `${item.CurrentQty}`;
            template += `\n`;
        });
        return template;
    }

    public confirmCheckout(content: string) {
        this.model.EmailAddress =
            super.currentLoggedUser().currentUserRaw.EmailAddress;
        this.modalService.open(content, { size: "lg" });
    }

    public confirmDeleteOrder(content: string, item: any) {
        this.selectedOrder = item;
        this.modalService.open(content, { size: "sm" });
    }

    public confirmDeleteItemOrder(content: string, item: any) {
        this.selectedItemOrder = item;
        this.modalService.open(content, { size: "sm" });
    }

    public deleteOrder() {
        if (!this.selectedOrder) {
            this.toastr.error("Please select an order to delete.", "Error");
        }
        this.showSpinner = true;
        this.orderService
            .Delete(this.selectedOrder)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                this.ngOnInit();
            });
    }

    public deleteItemInOrderDetail() {
        if (!this.selectedItemOrder) {
            this.toastr.error(
                "Please select an order item to delete.",
                "Error"
            );
        }
        this.showSpinner = true;
        this.orderService
            .DeleteOrderDetail(this.selectedItemOrder)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                this.packageData = this.packageData.filter(
                    (e) => e.Id != this.selectedItemOrder.UserOrderId
                );
                if (this.packageData.length == 0) this.backToCart();
            });
    }

    public backToCart() {
        this.router.navigateByUrl("products");
    }

    public returnTotal(item) {
        let total = 0;
        item.OrderDetails.map((o) => {
            total += o.CurrentQty;
        });
        return total;
    }

    public imageUrl(url) {
        return super.imageUrl(url);
    }

    public downloadFile(isPdf: boolean) {
        this.showSpinner = true;
        this.emailItems = [...this.packageData].sort((a, b) => {
            return a.ProductName.localeCompare(b.ProductName, "sv");
        });
        let ids = this.emailItems
            .filter((e) => e.IncludeInEmail)
            .map((e) => e.ProductId)
            .join(",");
        // window.location.href = this.productService.print(ids, super.currentAccountId().toString(), '0', isPdf);
        this.productService
            .print(ids, super.currentAccountId().toString(), "0", isPdf)
            .toPromise()
            .then(
                (response) => {
                    this.showSpinner = false;
                    let url = window.URL.createObjectURL(response);
                    let a = document.createElement("a");
                    document.body.appendChild(a);
                    a.setAttribute("style", "display: none");
                    a.href = url;
                    a.download = "Product Catalog";
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove();
                },
                (err) => {
                    this.showSpinner = false;
                }
            );
    }

    public downloadCart(isPdf: boolean) {
        this.showSpinner = true;
        this.emailItems = [...this.packageData].sort((a, b) => {
            return a.ProductName.localeCompare(b.ProductName, "sv");
        });
        let ids = this.emailItems
            .filter((e) => e.IncludeInEmail)
            .map((e) => e.ProductId)
            .join(",");
        // window.location.href = this.productService.print(ids, super.currentAccountId().toString(), '0', isPdf);
        this.productService
            .print(
                ids,
                super.currentAccountId().toString(),
                "0",
                isPdf,
                "ProductCart"
            )
            .toPromise()
            .then(
                (response) => {
                    this.showSpinner = false;
                    let url = window.URL.createObjectURL(response);
                    let a = document.createElement("a");
                    document.body.appendChild(a);
                    a.setAttribute("style", "display: none");
                    a.href = url;
                    a.download = "Product Cart";
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove();
                },
                (err) => {
                    this.showSpinner = false;
                }
            );
    }

    public isSuperAdminOrAdmin() {
        return super.issuperadminoradmin();
    }

    public sendToEmailDialog(content) {
        this.isSendingEmail = false;
        this.emailItems = [...this.packageData].sort((a, b) => {
            return a.ProductName.localeCompare(b.ProductName, "sv");
        });
        this.modalService.open(content, { size: "lg" });
    }

    public currentAccountEmail() {
        return super.currentAccountEmail();
    }

    public sendToEmail(close) {
        let bccAddress = !this.isSendCopyToMe
            ? this.model.EmailCheckoutCCs && this.model.EmailCheckoutCCs != ""
                ? this.model.EmailCheckoutCCs
                : ""
            : this.currentAccountEmail() +
              (this.model.EmailCheckoutCCs && this.model.EmailCheckoutCCs != ""
                  ? ";" + this.model.EmailCheckoutCCs
                  : "");

        let data = {
            Email: this.sendToEmailModel.Email,
            Subject: this.sendToEmailModel.Subject,
            Body: this.sendToEmailModel.Body,
            IncludePDF: this.sendToEmailModel.IncludePDF,
            IncludeExcel: this.sendToEmailModel.IncludeExcel,
            Ids: this.emailItems
                .filter((e) => e.IncludeInEmail)
                .map((e) => e.ProductId)
                .join(","),
            BCC: bccAddress,
        };

        this.isSendingEmail = true;

        this.productService
            .email(
                data,
                this.currentAccountId().toString(),
                parseInt(super.currentLoggedUser().id, 10)
            )
            .subscribe((response) => {
                this.toastr.success("Email Sent", "Success!", {
                    timeOut: 1000,
                });
                close("closed");
                this.isSendingEmail = false;
                this.clearEmailContent();
            });
    }

    public clearEmailContent() {
        this.sendToEmailModel = {
            Email: "",
            Subject: "",
            Body: "",
            IncludePDF: true,
            IncludeExcel: false,
            BCC: "",
        };

        this.emailItems.map((item) => (item.IncludeInEmail = true));
        this.isSendCopyToMe = true;
    }

    private setLocation(location: any, splice: boolean = true): string {
        let data =
            location.LocationLevelOneName +
            (location.LocationLevelTwoName
                ? ", " + location.LocationLevelTwoName
                : "") +
            (location.LocationLevelThreeName
                ? ", " + location.LocationLevelThreeName
                : "") +
            (location.LocationLevelFourName
                ? ", " + location.LocationLevelFourName
                : "");
        if (splice) {
            data = data.length > 23 ? data.slice(0, 23) + "..." : data;
        }
        return data;
    }

    private setProductName(productName: string): string {
        productName =
            productName.length > 70
                ? productName.slice(0, 70) + "..."
                : productName;
        return productName;
    }

    private hover(elementId: string): void {
        var tt = document.getElementById(elementId);
        tt.classList.toggle("show");
    }

    public navigateToDetails(id) {
        this.router.navigate([`products/${id}/details`]);
    }
}
