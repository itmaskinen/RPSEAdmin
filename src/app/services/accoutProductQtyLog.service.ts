import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';
import { AccountProductQtyLogModel } from '../models';

@Injectable()
export class AccountProductQtyLogService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }

    public getProductQtyLogByAccount(id: number) {
        let url = `Product/Log/Qty/Get/Account/${id}`;    
        return super.getAll<any>(url);
    }

    public getProductQtyLog(id) {
        let url = `Product/Log/Qty/Get?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getProductQtyLogByAccountProductId(id, accountId) {
        let url = `Product/Log/Qty/Get/Account/Product?id=${id}&accountId=${accountId}`;    
        return super.getAll<any>(url);
    }

    public getProductCurrentQtyByAccountProductId(id, accountId, location = '') {
        let url = `Product/Current/Qty/Get/Account/Product?id=${id}&accountId=${accountId}&location=${location}`;    
        return super.getAll<any>(url);
    }

    public getProductCurrentQtyByAccountLocationId(id, locationId) {
        let url = `Product/Current/Qty/Get/Account/Product/Location?id=${id}&locationId=${locationId}`;    
        return super.getAll<any>(url);
    }

    public getProductQtyLogByProductId(id, accountId) {
        let url = `Product/Log/Qty/Get/Product?id=${id}&accountId=${accountId}`;    
        return super.getAll<any>(url);
    }

    public getProductTransactionLogByProductId(id) {
        let url = `Product/Log/Transactions?id=${id}`;    
        return super.getAll<any>(url);
    }

    public saveProductQtyLog(model: AccountProductQtyLogModel){
        let url = `Product/Log/Qty/Save`;
        return super.save(url, model);
    }

    public deleteProductQtyLog(model: AccountProductQtyLogModel){
        let url = `Product/Log/Qty/Delete`;
        return super.save(url, model);
    }

    public getProductQtyLogGridByAccount(id: number, filters: any) {
        const url = `Product/Log/Qty/Account/Grid/${id}`;
        return super.save(url, filters);
    }
}