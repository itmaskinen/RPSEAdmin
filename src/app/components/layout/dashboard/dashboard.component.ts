import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastsManager } from 'ng2-toastr';
import { CommonService } from '../../../services';
import { DashboardService } from '../../../services/dashboard.service';
import { DatePipe } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { BaseComponent } from '../../../core/components';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseComponent implements OnInit {
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public summaryData: any;
    public productRecentLogsData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
    public model: any;
    public productLogColDef: any;
    public productLogDefaultColDef: any;
    public topManufacturersData: Array<any> [];
    public usersData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
    public usersColDef: any;
    public usersDefaultColDef: any;
    public isChartDataAvailable = false;
    public isVideoReady = false;
    //CHART
    public chartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
    };

    public chartLabels: string[] = [];
    public chartType: string = 'doughnut';
    public chartLegend: boolean = false;
    public chartData: any[] =  [ ];
    public currentUser: any;
    public totalKgCo2: any;
    public totalProductRemove: any;
    isSuperAdmin = super.issuperadmin();
    pushRLeftClass: string = 'push-left';
    isMenuOpen: boolean = false;
    constructor(
        private router: Router,
        private commonService: CommonService,
        private toastr: ToastsManager,
        private dasboardService: DashboardService,
        route: ActivatedRoute,
        location: Location) {
        super(location, router, route);
    }

    public ngOnInit() {

        this.currentUser = super.currentLoggedUser();

        this.prepareProductLogColumnDef();
        this.prepareUsersColumnDef();

        this.loadSummary();
        this.loadRecentProductLogs();
        this.loadTopManufacturers();
        this.loadTopProductGroups();
        this.loadTopUsers();
        this.loadRemoveProductsLogs();
    }

    public loadSummary() {
        this.dasboardService.getSummary(this.currentUser.accountId, this.currentUser.id).subscribe((response: any) => {
            this.summaryData = response.Data;
        });
    }

    public loadRecentProductLogs() {
        this.dasboardService.getRecentProductLogs(this.currentUser.accountId).subscribe((response: any) => {
            this.productRecentLogsData.next(response.Data);
        });
    }

    public loadTopManufacturers() {
        this.dasboardService.getTopManufacturers(this.currentUser.accountId).subscribe((response: any) => {
            this.topManufacturersData = response.Data;
        });
    }

    public loadTopProductGroups() {
        this.dasboardService.getTopProductGroups(this.currentUser.accountId).subscribe((response: any) => {
            const data = response.Data;
            data.forEach(x => {
                this.chartLabels.push(x.ProductGroupName);
                this.chartData.push(x.NoOfProducts);
            });

            this.isChartDataAvailable = true;
        });
    }

    public loadRemoveProductsLogs() {
        this.dasboardService.getRemoveProductsLogs(this.currentUser.accountId).subscribe((response: any) => {
            const data = response.Data;
            setTimeout(() => {
                this.totalProductRemove = data.TotalOfRemoveProducts;
                this.totalKgCo2 = data.TotalOfKgCo2;
            }, 300);
        })
    }

    public loadTopUsers() {
        this.dasboardService.getTopUsers(this.currentUser.accountId).subscribe((response: any) => {
            this.usersData.next(response.Data);
        });
    }

    private prepareProductLogColumnDef() {
        this.productLogColDef = [
            { headerName: 'Product Name', field: 'ProductName', width: 250 },
            {headerName: 'Location', field: 'LocationName', width: 250 },
            {headerName: 'Quantity', field: 'Qty', type: 'numericColumn', width: 80},
            {
                headerName: 'Mode ', field: 'Mode', width: 60, cellStyle: { textAlign: "center" },
                cellRenderer(params) {
                    return `<span class="text-center">${params.data.Mode >= 1 ? '+' : '-'}</span>`;
                }
            },
            {
                headerName: 'Date', field: 'UpdatedDate', width: 150, cellStyle: { textAlign: "center" },
                cellRenderer(params) {
                    return new DatePipe('en-US').transform(params.data.UpdatedDate, 'dd-MM-yyyy, h:mm a');
                }
            }
        ];

        this.productLogDefaultColDef = {
            resizable: true,
            sortable: false,
            cellRenderer: (params) => {
                return params.value || '';
            },
            comparator: (str1, str2) => {
                return true;
            },
            filter: false,
            filterParams: {
                newRowsAction: 'keep',
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                }
            }
        };
    }


    private prepareUsersColumnDef() {
        this.usersColDef = [
            { headerName: 'User Name', field: 'Name', width: 400 },
            {
                headerName: 'No. of Add/Edit Items', field: 'CreatedOrUpdatedProducts', width: 150,
                cellRenderer(params) {
                    return params.data.CreatedOrUpdatedProducts || 0;
                }
            },
            {
                headerName: 'Last LoggedIn', field: 'LastLoggedIn', width: 200 ,
                cellRenderer(params) {
                    return new DatePipe('en-US').transform(params.data.LastLoggedIn, 'dd-MM-yyyy, H:MM');
                }
            }
        ];

        this.usersDefaultColDef = {
            resizable: true,
            sortable: false,
            cellRenderer: (params) => {
                return params.value || '';
            },
            comparator: (str1, str2) => {
                return true;
            },
            filter: false,
            filterParams: {
                newRowsAction: 'keep',
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                }
            }
        };
    }

    public playVid () {
        const vid: any = document.getElementById('video');
        this.isVideoReady = true;
        vid.muted = true;
        vid.play();
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        if (!this.isMenuOpen) {
            dom.classList.toggle(this.pushRLeftClass);
            this.isMenuOpen = true;
        }
        else {
            dom.classList.toggle(this.pushRLeftClass)
            this.isMenuOpen = false;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        const result =  dom.classList.contains(this.pushRLeftClass);
        return result;
    }
}
