import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { ToastsManager } from 'ng2-toastr';

import { LocationService } from '../../services';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { BaseComponent } from '../../core/components';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

import { AccountLocationLevelOneModel, AccountLocationLevelTwoModel, AccountLocationLevelThreeModel, AccountLocationLevelFourModel } from '../../models';
import { ModalService } from '../../shared';
@Component({
    selector: 'app-locations-list',
    templateUrl: './locations-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})

export class LocationsListComponent extends BaseComponent implements OnInit {
    @Output() editLocationView = new EventEmitter();
    public showSpinner: boolean = false;
    public accountId: number = 0;
    public model: any;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    public isAdmin = false;

    public dataTable: any;

    public selectedAdminId: number = 0;
    public locationLevelOneList: any = [];
    public locationLevelTwoList: any = [];
    public locationLevelThreeList: any = [];
    public locationLevelFourList: any = [];

    levelOneModel: any;
    levelTwoModel: any;
    levelThreeModel: any;
    levelFourModel: any;

    selectedLevelOne: any;
    selectedLevelTwo: any;
    selectedLevelThree: any;
    selectedLevelFour: any;

    selectedLocation: any = {};
    deleteConfirmTitle : string = 'LEVEL ONE';

    showLevelTwo : boolean = false;
    showLevelThree: boolean = false;
    showLevelFour: boolean = false;

    showMoveQtyNote: boolean = false;

    constructor(private router: Router,
        private route:ActivatedRoute,
        private location: Location,
        private toastr: ToastsManager,
        private locationService: LocationService,
        private chRef: ChangeDetectorRef,
        private modalService: ModalService) {
            super(location, router, route);
        }

    packageData: Subject<any> = new Subject<any[]>();
    dataList : Array<any> = [];

    public ngOnInit() {
        this.levelOneModel = new AccountLocationLevelOneModel();
        this.levelTwoModel = new AccountLocationLevelTwoModel();
        this.levelThreeModel = new AccountLocationLevelThreeModel();
        this.levelFourModel = new AccountLocationLevelFourModel();
    }

    public loadDataById(id:number, levelId: number){
        this.accountId = id;
        this.showSpinner = true;
        this.locationService.getLocationsByAccountAndLevelList(id, levelId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            let data = response.Data;
            this.packageData.next(data);
        });
    }

    public loadAccountLocationLevelOneList(accountId: number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelOne(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                this.showSpinner = false;
        })
        .subscribe((response : any) => {
            let data = response.Data;
            if(data.length > 0)
            {
                this.locationLevelOneList = data;
            }
            else {
                this.locationLevelOneList = [];
                this.locationLevelTwoList = [];
                this.locationLevelThreeList = [];
                this.locationLevelFourList = [];
            }
        });
    }

    public loadAccountLocationLevelTwoList(accountId: number, levelOneId?:number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelTwo(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            this.showSpinner = false;

        })
        .subscribe((response : any) => {
            let data = response.Data;
            this.showLevelTwo = levelOneId > 0 ? true : false;
            this.showLevelThree = false;
            this.showLevelFour = false;
            if(data.length > 0)
            {
                if(levelOneId > 0){
                    let filtered = data.filter((item) => {
                        return item.AccountLocationLevelOneId === levelOneId;
                    });
                    if(filtered.length > 0) {
                        this.locationLevelTwoList = filtered;
                    }
                    else {
                        this.locationLevelTwoList = [];
                        this.locationLevelThreeList = [];
                        this.locationLevelFourList = [];
                    }
                }
                else {
                    this.locationLevelTwoList = data;
                }
            }
            else {
                this.locationLevelTwoList = [];
                this.locationLevelThreeList = [];
                this.locationLevelFourList = [];
            }

        });
    }

    public loadAccountLocationLevelThreeList(accountId: number, levelTwoId?:number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelThree(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                this.showSpinner = false;
        })
        .subscribe((response : any) => {
            let data = response.Data;
            this.showLevelThree = levelTwoId > 0 ? true : false;
            this.showLevelFour = false;
            if(data.length > 0)
            {
                if(levelTwoId > 0) {
                    let filtered = data.filter((item) => {
                        return item.AccountLocationLevelTwoId === levelTwoId;
                    });
                    if(filtered.length > 0) {
                        this.locationLevelThreeList = filtered;
                    }
                    else {
                        this.locationLevelThreeList = [];
                        this.locationLevelFourList = [];
                    }
                }
                else {
                    this.locationLevelThreeList = data;
                }
            }
            else {
                this.locationLevelThreeList = [];
                this.locationLevelFourList = [];
            }
        });
    }

    public loadAccountLocationLevelFourList(accountId: number, levelThreeId?:number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelFour(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                this.showSpinner = false;
        })
        .subscribe((response : any) => {
            let data = response.Data;
            this.showLevelFour = levelThreeId > 0 ? true : false;
            if(data.length > 0)
            {
                if(levelThreeId > 0) {
                    let filtered = data.filter((item) => {
                        return item.AccountLocationLevelThreeId === levelThreeId;
                    });
                    if(filtered.length > 0) {
                        this.locationLevelFourList = filtered;
                    }
                    else {
                        this.locationLevelFourList = [];
                    }
                }
                else {
                    this.locationLevelFourList = data;
                }
            }
            else {
                this.locationLevelFourList = [];
            }
        });
    }

    public editLocation(id: number, levelId: number){
        let locParams = {
            locationId: id,
            levelId: levelId
        }
        this.editLocationView.emit(locParams);
    }

    public createLocationLevelOne(content: string, levelId: number) : void{
        this.levelOneModel = new AccountLocationLevelOneModel();
        this.modalService.open(content, { size: 'lg' });
        $('#level-one-name').focus();
    }

    public createLocationLevelTwo(content: string, levelId: number) : void{
        this.levelTwoModel = new AccountLocationLevelTwoModel();
        if(!this.selectedLevelOne){
            this.toastr.warning('Please make sure to select one Level One Location first before creating a level two.', 'Warning')
            return;
        }
        this.locationService.checkIfLocationHasProducts(this.selectedLevelOne.Id, 1).subscribe((response) =>{
            this.showMoveQtyNote = response.Data.Id ? true : false;
        })
        this.levelTwoModel.AccountLocationLevelOneId = this.selectedLevelOne.Id;
        this.modalService.open(content, { size: 'lg'});
        $('#level-two-name').focus();
    }

    public createLocationLevelThree(content: string, levelId: number) : void{
        this.levelThreeModel = new AccountLocationLevelThreeModel();
        if(!this.selectedLevelTwo){
            this.toastr.warning('Please make sure to select one Level Two Location first before creating a level three.', 'Warning')
            return;
        }
        this.locationService.checkIfLocationHasProducts(this.selectedLevelTwo.Id, 2).subscribe((response) =>{
            this.showMoveQtyNote = response.Data.Id ? true : false;
        })
        this.levelThreeModel.AccountLocationLevelTwoId = this.selectedLevelTwo.Id;
        this.modalService.open(content, { size: 'lg'});
        $('#level-three-name').focus();
    }

    public createLocationLevelFour(content: string, levelId: number) : void{
        this.levelFourModel = new AccountLocationLevelFourModel();
        if(!this.selectedLevelThree){
            this.toastr.warning('Please make sure to select one Level Three Location first before creating a level four.', 'Warning')
            return;
        }
        this.locationService.checkIfLocationHasProducts(this.selectedLevelThree.Id, 3).subscribe((response) =>{
            this.showMoveQtyNote = response.Data.Id ? true : false;
        })
        this.levelFourModel.AccountLocationLevelThreeId = this.selectedLevelThree.Id;
        this.modalService.open(content, { size: 'lg'});
        $('#level-four-name').focus();
    }


    public editLocationLevelOne(content: string, item){
        this.levelOneModel = item;
        this.modalService.open(content, { size: 'lg'});
        $('#level-one-name').focus();
    }

    public editLocationLevelTwo(content: string,item){
        this.levelTwoModel = item;
        this.modalService.open(content, { size: 'lg'});
        $('#level-two-name').focus();
    }

    public editLocationLevelThree(content: string,item){
        this.levelThreeModel = item;
        this.modalService.open(content, { size: 'lg'});
        $('#level-three-name').focus();
    }

    public editLocationLevelFour(content: string,item){
        this.levelFourModel = item;
        this.modalService.open(content, { size: 'lg'});
        $('#level-four-name').focus();
    }

    public selectLevelOne(item: any, e: any){
        this.locationLevelTwoList = [];
        this.locationLevelThreeList = [];
        this.locationLevelFourList = [];

        $(e).parent().toggleClass('selected').siblings().removeClass('selected');
        this.selectedLevelOne = item;
        this.onLevelOneChange(item.Id);
    }

    public selectLevelTwo(item: any, e: any){
        this.locationLevelThreeList = [];
        this.locationLevelFourList = [];

        $(e).parent().toggleClass('selected').siblings().removeClass('selected');
        this.selectedLevelTwo = item;
        this.onLevelTwoChange(item.Id);
    }

    public selectLevelThree(item: any, e: any){
        this.locationLevelFourList = [];

        $(e).parent().toggleClass('selected').siblings().removeClass('selected');
        this.selectedLevelThree = item;
        this.onLevelThreeChange(item.Id);
    }

    public selectLevelFour(item: any, e: any){
        $(e).parent().toggleClass('selected').siblings().removeClass('selected');
        this.selectedLevelFour = item;
    }

    public onLevelOneChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelTwoList(this.accountId, value);
        }
    }

    public onLevelTwoChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelThreeList(this.accountId, value);
        }
    }

    public onLevelThreeChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelFourList(this.accountId, value);
        }
    }

    public saveLevelOne(){
        this.levelOneModel.AccountId = this.accountId;
        this.locationService.saveAccountLocationsLevelOne(this.levelOneModel)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.loadAccountLocationLevelOneList(this.accountId);
            }
        });
    }

    public saveLevelTwo(){
        this.levelTwoModel.AccountId = this.accountId;
        this.levelTwoModel.IsFirstLocation = this.locationLevelTwoList.length == 0;
        this.locationService.saveAccountLocationsLevelTwo(this.levelTwoModel)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.loadAccountLocationLevelTwoList(this.accountId, this.levelTwoModel.AccountLocationLevelOneId);
            }
        });
    }

    public saveLevelThree(){
        this.levelThreeModel.AccountId = this.accountId;
        this.levelThreeModel.IsFirstLocation = this.locationLevelThreeList.length == 0;
        this.locationService.saveAccountLocationsLevelThree(this.levelThreeModel)   
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.loadAccountLocationLevelThreeList(this.accountId, this.levelThreeModel.AccountLocationLevelTwoId);
            }
        });
    }

    public saveLevelFour(){
        this.levelFourModel.AccountId = this.accountId;
        this.levelFourModel.IsFirstLocation = this.locationLevelFourList.length == 0;
        this.locationService.saveAccountLocationsLevelFour(this.levelFourModel)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.loadAccountLocationLevelFourList(this.accountId, this.levelFourModel.AccountLocationLevelThreeId);
            }
        });
    }

    public confirmDeleteLocation(content: string, item){
        this.selectedLocation = item;
        if(this.selectedLocation.AccountLocationLevelOneId) {
            this.deleteConfirmTitle = `Level Two - ${this.selectedLevelTwo.Name}`;
        }
        else if(this.selectedLocation.AccountLocationLevelTwoId) {
            this.deleteConfirmTitle = `Level Three - ${this.selectedLevelThree.Name}`;
        }
        else if(this.selectedLocation.AccountLocationLevelThreeId){
            this.deleteConfirmTitle = `Level Four - ${this.selectedLevelFour.Name}`;
        }
        else {
            this.deleteConfirmTitle = `Level One - ${this.selectedLevelOne.Name}`;
        }
        this.modalService.open(content, { size : 'sm' });
    }

    public deleteLocation(){
        if(!this.selectedLocation){
            this.toastr.error('Please select a location before deleting', 'Error')
            return false;
        }
        if(this.selectedLocation.AccountLocationLevelOneId) {
            this.deleteLevelTwo(this.selectedLocation)
        }
        else if(this.selectedLocation.AccountLocationLevelTwoId) {
            this.deleteLevelThree(this.selectedLocation)
        }
        else if(this.selectedLocation.AccountLocationLevelThreeId){
            this.deleteLevelFour(this.selectedLocation);
        }
        else {
            this.deleteLevelOne(this.selectedLocation);
        }
    }

    public deleteLevelOne(item){
        this.locationService.deleteAccountLocationsLevelOne(item)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
            this.showLevelTwo = false;
            this.showLevelThree = false;
            this.showLevelFour = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success(`Successfully deleted Location ${item.Name}. Affected total product quantity is about (${response.TotalRecords} qty).`, 'Success');
                this.loadAccountLocationLevelOneList(this.accountId);
            }
        });
    }

    public deleteLevelTwo(item){
        this.locationService.deleteAccountLocationsLevelTwo(item)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
            this.showLevelThree = false;
            this.showLevelFour = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success(`Successfully deleted Location ${item.Name}. Affected total product quantity is about (${response.TotalRecords} qty).`, 'Success');
                this.loadAccountLocationLevelTwoList(this.accountId);
            }
        });
    }

    public deleteLevelThree(item){
        this.locationService.deleteAccountLocationsLevelThree(item)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
            this.showLevelFour = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success(`Successfully deleted Location ${item.Name}. Affected total product quantity is about (${response.TotalRecords} qty).`, 'Success');
                this.loadAccountLocationLevelThreeList(this.accountId);
            }
        });
    }

    public deleteLevelFour(item){
        this.locationService.deleteAccountLocationsLevelFour(item)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success(`Successfully deleted Location ${item.Name}. Affected total product quantity is about (${response.TotalRecords} qty).`, 'Success');
                this.loadAccountLocationLevelThreeList(this.accountId);
            }
        });
    }
}
