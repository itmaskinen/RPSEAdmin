import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    ChangeDetectorRef,
    HostListener,
    ViewContainerRef,
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { componentsTransition } from "../../router.animations";
import { ProductsListComponent } from "./products-list.component";
import {
    AccountProductsListComponent,
    AccountProductsListCompactComponent,
    AccountProductsListTableComponent,
    AccountProductsListTileComponent,
} from "../../components";

import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { BehaviorSubject, Observable, Subject } from "rxjs";

import {
    AccountProductConditionModel,
    ProductModel,
    ProductsFilter,
} from "../../models";
import {
    ProductService,
    AccountService,
    OrderService,
    LocationService,
    AccountProductConditionService,
} from "../../services";

import { BaseComponent } from "../../core/components";
import { DatePipe, Location } from "@angular/common";
import * as $ from "jquery";
import { AccountProductsListAgGridComponent } from "./account-products-list-aggrid.component";
import { ModalService } from "../../shared";
import { ProductsCreateEditComponent } from "./products-create-edit.component";
import * as modalClass from "../../shared/modules/dialogOptions";
import { ITreeOptions, ITreeState, TreeModel } from "angular-tree-component";
import { TagService } from "../../services/tag.service";
import { TagModel } from "../../models/tagModel";

@Component({
    selector: "app-products",
    templateUrl: "./products.component.html",
    styleUrls: ["../layout/layout.component.scss"],
    animations: [componentsTransition()],
})
export class ProductsComponent extends BaseComponent implements OnInit {
    @ViewChild("tree") treeComponent: ElementRef;
    currentUser: any;
    isFirstLoad: boolean = false;
    isSuperAdmin: boolean = false;
    isAdminOrUserEditor: boolean = false;
    isSystemProductAccess: boolean = false;
    isAccountProductAccess: boolean = false;
    userAccountId: number = 0;

    accounts: any[] = [];

    showSpinner: boolean = false;
    model: any;
    searchKeyword: any = "";
    productManufacturers: any;
    productColorMats: any;
    productGroups: any;
    productModels: any;
    locations: any;
    accountProductConditionList: Array<AccountProductConditionModel> = [];
    notSetCondition: any;

    productManufacturersCopy: any;
    productColorMatsCopy: any;
    productGroupsCopy: any;
    productModelsCopy: any;

    groupsCode: any;
    manufacturerCode: any;
    modelCode: any;
    colorMatsCode: any;
    locationCode: any;

    selectedOwner = 0;
    selectedLocationId = 0;
    selectedQty = 0;

    associatedAccounts: any = [];

    createLink: string = "";
    productDisplayType: number = 1;
    isAddProductEnabled: boolean = false;
    cartCount: number = 0;

    productsHeader: string = "Products";
    diplayingRecords: number = 100;
    ownerFilterValue: number;
    productCounter: number = 0;
    resultCounter: number = 0;
    qtyCounter: number = 0;
    productFilter: any;
    pageSize: number;
    pageNumber: number;
    totalRecords: number;
    hasActiveColumnFilter = false;
    productIds: any = "";
    locationTree: any[];
    temp: any;
    filterNodes: any;
    treeOptions: ITreeOptions = {
        useCheckbox: false,
        useTriState: false,
        idField: "nestedid",
    };
    filterLocation: string;
    locationModalRef: any;
    isModalOpen: boolean;
    selectedNodes: Set<any> = new Set<any>();
    //requests
    filterPromise: any = null;
    @ViewChild("accountProductsList")
    accountProductsList: AccountProductsListComponent;
    @ViewChild("accountProductsListCompact")
    accountProductsListCompact: AccountProductsListCompactComponent;
    @ViewChild("accountProductsListTable")
    accountProductsListTable: AccountProductsListTableComponent;
    @ViewChild("accountProductsListTile")
    accountProductsListTile: AccountProductsListTileComponent;
    @ViewChild("accountProductsListAgGrid")
    accountProductsListAgGrid: AccountProductsListAgGridComponent;
    @ViewChild("productsList") productsList: ProductsListComponent;
    @ViewChild("ownerSelect") ownerSelect: ElementRef;
    @ViewChild("locationFilterContent") locationFilterModal: ElementRef;

    //@ViewChild('displayingSelect') displaying: ElementRef;
    @ViewChild("searchProduct") searchProduct: ElementRef;

    showBtnModel = 0;
    userOrders: any;
    tagsList: any[] = [];
    pushRightClass: string = "push-right";
    pushRLeftClass: string = "push-left";
    isMenuOpen: boolean = false;

    completeTagList: any[] = [];
    completeListCopy: any[];
    tagList: any[] = [];
    tagListSelected: any[] = [];
    tagModel: TagModel = new TagModel();
    constructor(
        private locationService: LocationService,
        private productService: ProductService,
        private accountService: AccountService,
        private orderService: OrderService,
        private accountProductConditionService: AccountProductConditionService,
        private toastr: ToastsManager,
        private router: Router,
        private route: ActivatedRoute,
        private elem: ElementRef,
        private changeDetect: ChangeDetectorRef,
        private modalService: ModalService,
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private tagService: TagService,
        private location: Location,
        private datePipe: DatePipe
    ) {
        super(location, router, route);
    }

    public ngOnInit() {
        this.setGridDefaultPage();
        //@TODO : change the role handling with the use of the basecomponent implementations

        this.searchKeyword = "";
        let currentUser = super.currentLoggedUser();
        this.currentUser = currentUser;
        this.userAccountId = currentUser.accountId;
        this.isFirstLoad = true;
        this.selectedOwner = this.userAccountId;
        super.setProductFilterStore(
            [],
            [],
            [],
            [],
            0,
            3,
            "",
            [],
            [],
            this.userAccountId
        );

        this.productsList.accountId = currentUser.accountId;
        this.isAdminOrUserEditor = this.isAdmin() || super.isEditor();
        if (currentUser.role === "superadmin") {
            this.isSuperAdmin = true;
            this.productsList.isSuperAdmin = true;
            this.productsHeader = "System Products";
        }
        this.model = new ProductModel();

        this.model["AccountProductConditionId"] = -1;

        const conditionPromise = this.getProductConditionByAccount(
            this.userAccountId
        );
        Promise.all([
            conditionPromise,
            this.selectedOwner > 0
                ? this.getLocations(this.userAccountId)
                : null,
        ]);

        this.productFilter = new ProductsFilter();
        let productFilterInStorage = this.getProductFilterStore();

        if (productFilterInStorage.length === 0) {
            this.toggleDefault(this.currentUser.role);
        } else {
            this.productFilter = productFilterInStorage[0];
            this.searchKeyWord =
                this.productFilter.filters.keyword === ""
                    ? ""
                    : this.productFilter.filters.keyword;
            this.setDefaults();
        }

        // this.searchKeyWord();
        this.getCartCount();
        this.getCartItems();
        if (this.isSuperAdmin) {
            this.accountService
                .getAccountsList(0, 0, null)
                .subscribe((response) => {
                    this.accounts = response.Data;
                });
        }

        const columnFilters = sessionStorage.getItem(
            "product_grid_column_filters"
        );
        this.hasActiveColumnFilter =
            columnFilters && Object.keys(columnFilters).length > 0;

        this.locationService
            .getLocationTree(this.userAccountId)
            .subscribe((e) => {
                this.locationTree = e.Data;
                this.filterNodes = this.locationTree;
            });
        this.getTags();
    }

    //#region Getter
    get hasFilter(): boolean {
        return (
            this.model.GroupId.length > 0 ||
            this.model.ManufacturerId.length > 0 ||
            this.model.ColorMatId.length > 0 ||
            this.model.ModelId.length > 0 ||
            this.model.LocationId > "0" ||
            this.model.AccountProductConditionId.length > 0 ||
            this.model.TagIds.length > 0 ||
            this.searchKeyword ||
            this.hasActiveColumnFilter
        );
    }
    //#endregion

    public setDefaults() {
        let filterStore = super.getProductFilterStore();

        if (filterStore.length == 1) {
            filterStore = filterStore[filterStore.length - 1];
        } else if (filterStore.length >= 2) {
            filterStore = filterStore.filter((item) => {
                return (
                    item.ownerId === filterStore[filterStore.length - 1].ownerId
                );
            })[0];
        } else {
            return false;
        }

        this.refreshFilters(filterStore.ownerId);
        this.setFiltersFromStore(filterStore);
    }

    public setFiltersFromStore(storeData) {
        const {
            group,
            manufacturer,
            colorMat,
            model,
            view,
            keyword,
            ownerId,
            location,
            condition,
            tag,
        } = storeData.filters;

        if (ownerId !== undefined) {
            this.ownerFilterValue = ownerId ? ownerId : 0;
            this.selectedOwner = ownerId ? ownerId : 0;
        } else {
            this.ownerFilterValue = super.currentAccountId();
            this.selectedOwner = super.currentAccountId();
        }

        this.model.GroupId = group ? group : [];

        this.model.ManufacturerId = manufacturer ? manufacturer : [];

        this.model.ColorMatId = colorMat ? colorMat : [];

        this.model.ModelId = model ? model : [];
        this.model.TagIds = tag ? tag : [];

        this.model.AccountProductConditionId = condition ? condition : [];

        this.model.LocationId = location ? location : 0;
        this.selectedLocationId = location ? location : 0;

        if (keyword) {
            this.searchProduct.nativeElement.value = keyword;
            this.searchKeyword = keyword;
            $("#searchProduct").keyup();
        }

        //view = !this.issuperadminoradmin() ? 4 : 3;
        if (view) this.toggleProductListView(view);
    }

    public toggleDefault(role: string) {
        if (role == "viewer" || role == "editor") {
            this.toggleProductListView(4);
        } else if (role == "superadmin" || role == "admin") {
            this.toggleProductListView(3);
        } else {
            this.toggleProductListView(1);
        }
    }

    public getProductManufacturers(accountId: number) {
        return new Promise((resolve) => {
            this.productService
                .getAllManufacturers(accountId)
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    let data = response.Data;
                    this.productManufacturers = data;
                    this.productManufacturersCopy = data;
                    resolve(true);
                });
        });
    }

    public getProductColorMaterials(accountId: number) {
        return new Promise((resolve) => {
            this.productService
                .getAllColorMat(accountId)
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    let data = response.Data;
                    this.productColorMats = data;
                    this.productColorMatsCopy = data;
                    resolve(true);
                });
        });
    }

    public getProductConditionByAccount(accountId: number) {
        return new Promise((resolve) => {
            this.accountProductConditionService
                .getProductConditionByAccount(accountId)
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .subscribe((res) => {
                    let data = res.Data;
                    this.notSetCondition = {
                        Id: 0,
                        AccountId: this.userAccountId,
                        Code: "0",
                        Name: "Not Set",
                        Description: "0 - Not Set",
                    };

                    this.accountProductConditionList = data;
                    if (
                        this.accountProductConditionList &&
                        this.accountProductConditionList.length > 0
                    )
                        this.accountProductConditionList.splice(
                            0,
                            0,
                            this.notSetCondition
                        );

                    resolve(data);
                });
        });
    }

    public getProductGroups(accountId) {
        return new Promise((resolve) => {
            this.productService
                .getAllGroups(accountId)
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    let data = response.Data;
                    this.productGroups = data;
                    this.productGroupsCopy = data;
                    resolve(true);
                });
        });
    }

    public getLocations(accountId: number) {
        return this.locationService
            .getProductLocationLevels(accountId)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                this.locations = response.Data;
            });
    }
    public getProductModels() {
        this.productService
            .getAllModels()
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                this.productModels = data;
                this.productModelsCopy = data;
            });
    }

    public getAccountsWithAssocDropdown(accountId) {
        this.accountService
            .getAccountsWithAssocDropdown(accountId)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                if (data.length > 0) {
                    this.associatedAccounts = data;
                }
            });
    }

    public refreshFilters(value: number) {
        return new Promise((resolve) => {
            const groupsPromise = this.getProductGroups(value);
            const manufacturersPromise = this.getProductManufacturers(value);
            const colorMatsPromise = this.getProductColorMaterials(value);
            const modelPromise = this.getProductModels();
            let promiseAll = Promise.all([
                groupsPromise,
                manufacturersPromise,
                colorMatsPromise,
                this.selectedOwner > 0 ? this.getLocations(value) : null,
                modelPromise,
            ]);
            promiseAll.then(() => {
                setTimeout(() => {
                    resolve(true);
                }, 1000);
            });
        });
    }

    public getModelsFiltered() {
        this.showSpinner = true;
        return new Promise((resolve) => {
            if (
                this.model.GroupId.length === 0 ||
                this.model.ManufacturerId.length === 0
            ) {
                resolve(false);
                return false;
            }
            this.productService
                .getModelsFiltered(
                    this.model.GroupId.toString(),
                    this.model.ManufacturerId.toString()
                )
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;

                    if (data && data.length > 0) {
                        this.productModels = data;
                    } else {
                        this.productModels = [];
                    }

                    resolve(true);
                });
        });
    }

    public locationChange() {
        this.model.LocationId =
            this.model.LocationId == null ? "" : this.model.LocationId;
        this.selectedLocationId = this.model.LocationId;
        this.filterChange();
    }

    public searchKeyWord() {
        Observable.fromEvent(this.searchProduct.nativeElement, "keyup")
            .map((evt: any) => evt.target.value)
            .debounceTime(1000)
            .distinctUntilChanged()
            .subscribe((text: string) => {
                this.searchKeyword = text;
                this.filterChange();
            });
    }

    public searchByKeyword() {
        this.filterChange();
    }

    public qtyChange(val: number) {
        this.selectedQty = val;
        this.filterChange();
    }

    public ownerChange(e) {
        if (!!this.accountProductsListAgGrid) {
            setTimeout(() => {
                this.accountProductsListAgGrid.gridApi.showLoadingOverlay();
            }, 0);

            this.accountProductsListAgGrid.showSpinner = true;

            this.accountProductsListAgGrid.packageData = new BehaviorSubject<
                any[]
            >([]);
        }

        if (!!this.accountProductsListTile) {
            this.accountProductsListTile.showSpinner = true;
            this.accountProductsListTile.packageData = new Subject<any[]>();
        }

        let value = Number(e.value);
        this.selectedOwner = value;
        //if owner is changed to system - reset filter values, if no filter values were set
        let filterList = super.getProductFilterStore();
        let filterStore = filterList.filter((item) => {
            return Number(item.ownerId) === value;
        });

        this.refreshFilters(value).then((response) => {
            if (response) {
                if (filterStore.length === 0) {
                    this.resetFiltersToDefault(value);
                } else {
                    this.setFiltersFromStore(filterStore[0]);
                }
                this.filterChange(value);
            }
        });
    }

    public filterChange(id?: number) {
        this.searchKeyword = this.searchProduct.nativeElement.value;
        this.clearScrollTile();
        if (this.accountProductsListAgGrid) {
            this.accountProductsListAgGrid.clearProductDatadToGrid();
        }
        this.showSpinner = true;
        let accountId =
            id <= 0
                ? id
                : this.ownerSelect.nativeElement.value
                ? Number(this.ownerSelect.nativeElement.value)
                : this.userAccountId;
        this.ownerFilterValue = Number(this.ownerSelect.nativeElement.value);
        let displaying = 100;

        let payload = {
            GroupId: this.model.GroupId,
            ManufacturerId: this.model.ManufacturerId,
            ColorMatId: this.model.ColorMatId,
            ModelId: this.model.ModelId,
            LocationIds: this.model.LocationId,
            productDisplayType: this.productDisplayType,
            searchKeyword: this.searchKeyword,
            ConditionId: this.model.AccountProductConditionId,
            AccountId: accountId,
            TagIds: this.model.TagIds,
        };

        if (this.filterPromise) {
            this.filterPromise.then(() => {
                return Promise.resolve();
            });
        }

        if (!super.issuperadminoradmin()) {
            // if the logged in user is viewer / show products that has qty
            this.selectedQty = 1;
            accountId = this.userAccountId;
        }
        this.changeDetect.detectChanges();
        switch (this.productDisplayType) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                this.loadAccountProductsListAgGrid(accountId).then(
                    (result: any) => {
                        this.dataUpdater(result);
                        this.getModelsFiltered();
                        return new Promise((resolve) => {
                            this.showSpinner = false;
                            resolve(true);
                        });
                    }
                );
                this.filterPromise.then(() => {
                    //do this last
                    super.setProductFilterStore(
                        this.model.GroupId,
                        this.model.ManufacturerId,
                        this.model.ColorMatId,
                        this.model.ModelId,
                        this.model.LocationId,
                        this.productDisplayType,
                        this.searchKeyword,
                        this.model.AccountProductConditionId,
                        this.model.TagIds,
                        accountId
                    );
                });
                break;
            case 4:
                this.filterPromise = this.accountProductsListTile
                    .loadDataFiltered(
                        accountId,
                        this.model.GroupId,
                        this.model.ColorMatId,
                        this.model.ManufacturerId,
                        this.model.ModelId,
                        this.model.LocationId,
                        this.searchKeyword,
                        this.userAccountId,
                        displaying,
                        this.selectedQty,
                        this.model.AccountProductConditionId,
                        this.pageNumber,
                        this.model.TagIds
                    )
                    .then((result: any) => {
                        this.totalRecords = result.totalRecords;
                        this.dataUpdater(result);
                        this.getModelsFiltered();
                        this.productService
                            .getAllIdsForLabelPrinting(payload)
                            .subscribe((response) => {
                                this.productIds = response;
                            });
                        return new Promise((resolve) => {
                            this.showSpinner = false;
                            resolve(true);
                        });
                    });
                this.filterPromise.then(() => {
                    //do this last
                    super.setProductFilterStore(
                        this.model.GroupId,
                        this.model.ManufacturerId,
                        this.model.ColorMatId,
                        this.model.ModelId,
                        this.model.LocationId,
                        this.productDisplayType,
                        this.searchKeyword,
                        this.model.AccountProductConditionId,
                        this.model.TagIds,
                        accountId
                    );
                });
                break;
        }
    }

    public toggleView(typeId: number) {
        this.productDisplayType = typeId;
        if (typeId === 3) {
            sessionStorage.removeItem("product_grid_scroll_position");
        }
        this.filterChange();
    }

    public toggleProductListView(typeId: number, reloadList: boolean = true) {
        this.showSpinner = true;
        let accountId = !isNaN(this.ownerFilterValue)
            ? this.ownerFilterValue
            : this.userAccountId;
        typeId = this.currentUser.role === "viewer" ? 4 : typeId;
        if (this.filterPromise) {
            this.filterPromise.then(() => {
                return Promise.resolve();
            });
        }

        if (!super.issuperadminoradmin()) {
            // if the logged in user is viewer / show products that has qty
            this.selectedQty = 1;
            accountId = this.userAccountId;
        }
        this.productDisplayType = typeId;
        let displaying = 100;
        this.changeDetect.detectChanges();
        switch (typeId) {
            case 1: //default
                this.accountProductsList.loadData(accountId);
                this.accountProductsList.isAdmin =
                    this.isAdmin() || super.isEditor();
                break;
            case 2: //compact
                this.accountProductsListCompact.loadData(accountId);
                this.accountProductsListCompact.isAdmin =
                    this.isAdmin() || super.isEditor();
                break;
            case 3: //table
                if (this.selectedOwner === this.userAccountId && reloadList) {
                    this.loadAccountProductsListAgGrid(accountId);
                }
                break;
            case 4: //tile
                this.accountProductsListTile.isAdmin =
                    this.isAdmin() || super.isEditor();
                this.accountProductsListTile.isSuperAdmin = this.isSuperAdmin;
                this.accountProductsListTile.accountId = this.userAccountId;
                this.filterPromise = this.accountProductsListTile
                    .loadDataFiltered(
                        accountId,
                        this.model.GroupId,
                        this.model.ColorMatId,
                        this.model.ManufacturerId,
                        this.model.ModelId,
                        this.model.LocationId,
                        this.searchKeyword,
                        this.userAccountId,
                        displaying,
                        this.selectedQty,
                        this.model.AccountProductConditionId,
                        this.pageNumber,
                        this.model.TagIds
                    )
                    .then((result) => {
                        this.dataUpdater(result);
                        this.getModelsFiltered();
                        this.scrollTile();
                        return new Promise((resolve) => {
                            this.showSpinner = false;
                            this.totalRecords = result.totalRecords;
                            resolve(true);
                        });
                    });
                break;
            default: //default
                this.accountProductsList.isAdmin =
                    this.currentUser.role === "admin" || super.isEditor();
                this.accountProductsList.loadData(accountId);
                break;
        }
    }

    public navigateToCreate() {
        sessionStorage.removeItem("product_grid_selected_row");
        this.modalService
            .open(ProductsCreateEditComponent, modalClass.modalMyClass)
            .result.then(() => {
                setTimeout(() => {
                    this.accountProductsListAgGrid.productsList = [];
                    this.accountProductsListAgGrid.gridApi.showLoadingOverlay();
                }, 0);
                this.accountProductsListAgGrid.reloadProductList();
            });
    }

    //this function will take care of the filtering dropdown data after the first filtering
    //this will also update the counters after the filtering
    public dataUpdater(result) {
        this.isAddProductEnabled = result.recordCount === 0;

        if (this.hasFilter) {
            if (result.groups && result.groups.length > 0) {
                let groupIds = result.groups;
                this.productGroups = this.productGroupsCopy.filter((item) => {
                    return groupIds.indexOf(item.Id) > -1;
                });
            }

            if (result.manufacturers && result.manufacturers.length > 0) {
                let manufacturerIds = result.manufacturers;
                this.productManufacturers =
                    this.productManufacturersCopy.filter((item) => {
                        return manufacturerIds.indexOf(item.Id) > -1;
                    });
            }

            if (result.colorMaterials && result.colorMaterials.length > 0) {
                let colorMatIds = result.colorMaterials;
                this.productColorMats = this.productColorMatsCopy.filter(
                    (item) => {
                        return colorMatIds.indexOf(item.Id) > -1;
                    }
                );
            }
        }
    }

    //this function will reload the counters
    public productQtyUpdate(data) {
        this.toastr.success(
            `Successfully updated product Qty | ${
                data.Mode === -1 ? "Removed" : "Added"
            } : ${data.Qty}`,
            "Product Updated"
        );
    }

    public clearFilter() {
        this.state = null;
        this.resetFiltersToDefault(this.userAccountId);
        this.clearScrollTile();

        //grid
        if (this.accountProductsListAgGrid) {
            this.accountProductsListAgGrid.clearColumnFilters();
            this.accountProductsListAgGrid.setSelectedProduct(0);
        }

        this.model.GroupId = [];

        this.model.ManufacturerId = [];

        this.model.ColorMatId = [];

        this.model.ModelId = [];

        this.model.AccountProductConditionId = [];

        this.model.TagIds = [];

        this.model.LocationId = 0;
        this.selectedLocationId = 0;

        this.searchProduct.nativeElement.value = "";
        this.searchKeyword = "";
        $("#searchProduct").keyup();

        this.toggleProductListView(3, false);

        this.hasActiveColumnFilter = false;

        this.filterChange();
    }

    public resetFiltersToDefault(accountId) {
        this.refreshFilters(accountId);
    }

    public getProductCount() {
        //exit if the it's not admin or superadmin
        //if(!super.issuperadminoradmin()) return false;
        if (this.accountProductsListTable) {
            this.accountProductsListTable.loadingMessage = "Calculating...";
        }
        let accountId = !isNaN(this.ownerFilterValue)
            ? this.ownerFilterValue
            : this.userAccountId;
        this.productFilter = {
            AccountId: accountId,
            GroupId: this.model.GroupId,
            ManufacturerId: this.model.ManufacturerId,
            ColorMatId: this.model.ColorMatId,
            ModelId: this.model.ModelId,
            Keyword: this.searchProduct.nativeElement.value,
            Displaying: 100,
            QtyView: this.selectedQty,
            RoleName: super.currentRole(),
        };
        this.productService
            .getProductCounter(this.productFilter)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                this.productCounter = data.TotalProductCount;
                this.qtyCounter = data.TotalQtyCount;
                this.resultCounter = data.TotalFilterResultCount;
            });
    }

    public exportToCSV() {
        if (this.accountProductsListTable) {
            this.accountProductsListTable.exportToCSV();
        }
        if (this.accountProductsListTile) {
            this.accountProductsListTile.exportToCSV();
        }
        if (this.accountProductsListAgGrid) {
            this.accountProductsListAgGrid.exportToCSVOrExcel();
        }
    }

    public exportToPDF(tileMode = false) {
        if (this.accountProductsListTable) {
            this.accountProductsListTable.exportToPDF();
        }

        if (this.accountProductsListTile) {
            this.accountProductsListTile.exportToPDF();
        }

        if (this.accountProductsListAgGrid) {
            if (tileMode) {
                this.accountProductsListAgGrid.exportToTilePdf();
            } else {
                this.accountProductsListAgGrid.exportToPDF();
            }
        }
    }

    public downloadImages() {
        // this.accountProductsListTable.exportImagesToZip();
        if (this.accountProductsListAgGrid) {
            this.accountProductsListAgGrid.exportImagesToZip();
        }

        if (this.accountProductsListTile) {
            this.accountProductsListTile.exportImagesToZip();
        }
    }

    //cart
    public getCartCount() {
        let userId = this.currentUser.id;
        this.orderService
            .getOrderCount(userId)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if (response.Extras) {
                    this.cartCount = response.Extras;
                }
            });
    }

    public getCartItems() {
        this.orderService
            .getOrderByUsers(this.currentUser.id, super.currentAccountId())
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                this.userOrders = response.Data;
            });
    }

    public goToCart() {
        this.router.navigateByUrl("cart");
    }

    public goToHistory() {
        this.router.navigateByUrl("order/history");
    }

    public productAddedToCart(data) {
        if (data) {
            this.getCartCount();
        }
    }
    //util
    public isAdmin() {
        return super.isadmin();
    }

    public isSuperAdminOrAdmin() {
        return super.issuperadminoradmin();
    }

    //scrolling
    public onScrollDown() {
        let accountId = !isNaN(this.ownerFilterValue)
            ? this.ownerFilterValue
            : this.userAccountId;
        if (this.totalRecords > this.diplayingRecords) {
            this.diplayingRecords += 100;
            this.pageNumber += 1;
        }

        if (this.filterPromise) {
            this.filterPromise.then(() => {
                return Promise.resolve();
            });
        }

        if (this.productDisplayType === 3) {
            this.loadAccountProductsListAgGrid(accountId);
        } else {
            this.filterPromise = this.accountProductsListTile
                .loadDataFiltered(
                    this.selectedOwner,
                    this.model.GroupId,
                    this.model.ColorMatId,
                    this.model.ManufacturerId,
                    this.model.ModelId,
                    this.model.LocationId,
                    this.searchKeyword,
                    this.userAccountId,
                    this.diplayingRecords,
                    this.selectedQty,
                    this.model.AccountProductConditionId,
                    this.pageNumber,
                    this.model.TagIds
                )
                .then((result: any) => {
                    this.dataUpdater(result);
                    return new Promise((resolve) => {
                        resolve(true);
                    });
                });
        }
    }

    public onScrollUp() {
        let accountId = !isNaN(this.ownerFilterValue)
            ? this.ownerFilterValue
            : this.userAccountId;
        if (this.totalRecords > this.diplayingRecords && this.pageNumber > 0) {
            this.diplayingRecords -= 100;
            this.pageNumber -= 1;
        }

        if (this.filterPromise) {
            this.filterPromise.then(() => {
                return Promise.resolve();
            });
        }

        if (this.productDisplayType === 3) {
            this.loadAccountProductsListAgGrid(accountId);
        } else {
            this.filterPromise = this.accountProductsListTile
                .loadDataFiltered(
                    this.selectedOwner,
                    this.model.GroupId,
                    this.model.ColorMatId,
                    this.model.ManufacturerId,
                    this.model.ModelId,
                    this.model.LocationId,
                    this.searchKeyword,
                    this.userAccountId,
                    this.diplayingRecords,
                    this.selectedQty,
                    this.model.AccountProductConditionId,
                    this.pageNumber,
                    this.model.TagIds
                )
                .then((result: any) => {
                    this.dataUpdater(result);
                    return new Promise((resolve) => {
                        resolve(true);
                    });
                });
        }
    }
    @HostListener("scroll", ["$event"])
    public onScroll(event) {
        sessionStorage.setItem(
            "product_tile_scroll_position",
            event.srcElement.scrollTop
        );
    }

    public scrollTile() {
        const scrollPosition =
            sessionStorage.getItem("product_tile_scroll_position") || "0";
        setTimeout(() => {
            $(".product-results").scrollTop(parseInt(scrollPosition));
        }, 500);
    }

    public clearScrollTile() {
        sessionStorage.setItem("product_tile_scroll_position", "0");
        this.scrollTile();
    }

    public loadAccountProductsListAgGrid(accountId) {
        this.accountProductsListAgGrid.isAdmin =
            this.isAdmin() || super.isEditor();
        this.accountProductsListAgGrid.isSuperAdmin = this.isSuperAdmin;
        this.accountProductsListAgGrid.accountId = this.userAccountId;
        let payload = {
            GroupId: this.model.GroupId,
            ManufacturerId: this.model.ManufacturerId,
            ColorMatId: this.model.ColorMatId,
            ModelId: this.model.ModelId,
            LocationIds: this.model.LocationId,
            productDisplayType: this.productDisplayType,
            keyWord: this.searchKeyword,
            ConditionId: this.model.AccountProductConditionId,
            AccountId: accountId,
            TagIds: this.model.TagIds,
        };

        this.filterPromise = this.accountProductsListAgGrid
            .initGridData(
                accountId,
                this.model.GroupId,
                this.model.ColorMatId,
                this.model.ManufacturerId,
                this.model.ModelId,
                this.model.LocationId,
                this.searchKeyword,
                0,
                0,
                this.selectedQty,
                this.pageNumber,
                this.pageSize,
                this.model.AccountProductConditionId,
                this.model.TagIds
            )
            .then((result) => {
                this.totalRecords = result.totalRecords;

                this.dataUpdater(result);
                this.getModelsFiltered();
                this.productService
                    .getAllIdsForLabelPrinting(payload)
                    .subscribe((response) => {
                        this.productIds = response;
                    });

                return new Promise((resolve) => {
                    this.showSpinner = false;
                    resolve(true);
                });
            });

        return this.filterPromise;
    }

    public currentDispayRageFrom() {
        if (this.totalRecords === 0) {
            return 0;
        }
        return this.pageNumber * this.pageSize + 1;
    }

    public currentDispayRageTo() {
        const result = this.pageNumber * this.pageSize + this.pageSize;

        if (result > this.totalRecords) {
            return this.totalRecords;
        }

        return result;
    }

    public incrementToPage(step: number) {
        this.pageNumber += step;
        const accountId = !isNaN(this.ownerFilterValue)
            ? this.ownerFilterValue
            : this.userAccountId;

        this.loadAccountProductsListAgGrid(accountId);
    }

    public setGridDefaultPage() {
        this.pageSize = 100;
        this.pageNumber = 0;
        this.totalRecords = 0;
    }

    public gridColumnFilterChanged(filters) {
        this.hasActiveColumnFilter = filters && Object.keys(filters).length > 0;
    }

    public printBookedLabels() {
        if (this.productIds && this.productIds != "") {
            const printWindow = window.open(
                `#/products/public/${this.productIds}/${this.userAccountId}/print/product/label`,
                "PRINT",
                "height=400,width=600"
            );
        } else {
            this.toastr.error(
                "Products with booking data for printing not available",
                "Error"
            );
        }
    }

    public printProductLabels() {
        const printWindow = window.open(
            `#/products/public/0/${this.userAccountId}/print/allproduct/label`,
            "PRINT",
            "height=400,width=600"
        );
    }

    openLocationFilterModal() {
        this.locationModalRef = this.modalService.open(
            this.locationFilterModal
        );
        this.isNodeSelected(this.selectedNodes);
    }

    filterTree(value: string, treeModel: TreeModel) {
        if (value) {
            this.temp = [];
            for (let i = 0; i < this.locationTree.length; i++) {
                this.showChildren(
                    this.locationTree[i],
                    this.locationTree[i],
                    value,
                    treeModel
                );
            }
            this.filterNodes = Array.from(new Set(this.temp));
        } else {
            this.filterNodes = this.locationTree;
        }
    }

    showChildren(node, item, value, treeModel) {
        const nodeName = item.name.toLowerCase();
        const searchValue = value.toLowerCase();
        const nodeFound = nodeName.includes(searchValue);

        if (nodeFound) {
            setTimeout(() => {
                const someNode = treeModel.getNodeById(item.id);
                if (someNode) {
                    someNode.ensureVisible();
                }
            });
            this.temp.push(node);
        } else {
            if (item.children && item.children.length) {
                for (let i = 0; i < item.children.length; i++) {
                    this.showChildren(node, item.children[i], value, treeModel);
                }
            }
        }
    }

    onEvent(e) {
        this.model.LocationId = e.node.data.nestedid;
        this.filterLocation = e.node.data.nestedname;
        this.toggleNodeSelection(e.node.data);
        this.searchByKeyword();
        this.locationModalRef.close();
    }

    onSearch() {
        this.searchByKeyword();
        this.locationModalRef.close();
    }

    // onNodeActivated(event: any): void {
    //     // Update tree data to set the node as selected
    //     if (event.node && event.node.data) {
    //       event.node.data.selected = true;
    //     }
    // }

    toggleNodeSelection(node: any): void {
        if (this.selectedNodes.has(node)) {
            this.selectedNodes.delete(node);
        } else {
            this.selectedNodes.add(node);
        }
    }

    isNodeSelected(node: any): boolean {
        return this.selectedNodes.has(node);
    }

    get state(): ITreeState {
        return localStorage.treeState && JSON.parse(localStorage.treeState);
    }
    set state(state: ITreeState) {
        localStorage.treeState = JSON.stringify(state);
    }

    toggleSidebar() {
        const dom: any = document.querySelector("body");
        if (!this.isMenuOpen) {
            dom.classList.toggle(this.pushRLeftClass);
            this.isMenuOpen = true;
        } else {
            dom.classList.toggle(this.pushRLeftClass);
            this.isMenuOpen = false;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector("body");
        const result = dom.classList.contains(this.pushRLeftClass);
        return result;
    }

    openSearchTag(content: string) {
        this.completeListCopy = this.completeTagList;
        this.modalService.open(content, { size: "md" });
    }

    public getTags() {
        let accountid = super.currentAccountId();
        this.tagService.getLatestTagByAccountId(accountid, 5).subscribe(
            (response) => {
                let result = response.Data;
                if (result.length > 0) {
                    this.tagList = result.slice(0, 5);
                }
                this.completeTagList = result;
            },
            (err) => {
                this.toastr.error(err, "Error");
            },
            () => {
                this.showSpinner = false;
            }
        );
    }

    addTag(event) {
        const isTagInList = this.model.TagIds.some((tag) => tag === event.Id);
        const isTagInListLk = this.tagList.some((tag) => tag.Id === event.Id);
        if (!isTagInList) {
            if (!isTagInListLk) {
                this.tagList = [...this.tagList, event];
            }
            this.model.TagIds = [...this.model.TagIds, event.Id];
            this.searchByKeyword();
        }
    }

    removeTag(event) {
        const index = this.tagListSelected.indexOf(event.value.Id.toString());
        if (index !== -1) {
            this.tagList.splice(index, 1);
        }
    }

    filterTagList(value) {
        if (value.trim().length > 0) {
            this.completeTagList = this.completeTagList.filter((e) =>
                e.Name.includes(value)
            );
            debugger;
            return this.completeTagList;
        } else {
            this.completeTagList = this.completeListCopy;
        }
    }

    editProductTag(item: any) {
        this.tagModel = item;
    }

    formatSwedishDate(date: Date) {
        return this.datePipe.transform(date, "yyyy-MM-dd");
    }

    saveTag() {
        const isAdd = this.tagModel.Id == 0 ? true : false;
        this.tagModel.AccountId = super.issuperadmin()
            ? 0
            : super.currentAccountId();
        this.tagService
            .saveTag(this.tagModel)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .subscribe((response) => {
                if (response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                } else {
                    this.toastr.success(response.Message, "Success");
                    this.resetTagModel();
                    this.getTags();

                    if (isAdd) {
                        this.addTag({
                            Id: response.Data.Id,
                        });
                    }
                }
            });
    }

    resetTagModel() {
        this.tagModel = new TagModel();
    }

    isTagNameValid() {
        return this.tagModel.Name.trim().length > 0;
    }

    productTagDeleteConfirm(content: string, item: any) {
        this.tagModel = item;
        this.tagModel.IsDeleted = true;
        this.modalService.open(content, { size: "sm" });
    }

    deleteProductTag() {
        this.tagModel.AccountId = super.issuperadmin()
            ? 0
            : super.currentAccountId();
        this.tagService
            .saveTag(this.tagModel)
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if (response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                } else {
                    this.resetTagModel();
                    this.getTags();
                }
            });
    }

    openManageTag(content: string) {
        this.modalService.open(content, { size: "md" });
    }
}
