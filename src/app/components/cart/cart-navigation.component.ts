import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, RoutesRecognized } from '@angular/router';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
import { CartComponent } from './cart.component';
@Component({
    selector: 'app-cart-navigation',
    templateUrl: './cart-navigation.component.html',
    styleUrls: ['../layout/layout.component.scss', '../cart/cart.css', '../products/products.css']
})
export class CartNavigationComponent extends BaseComponent implements OnInit { 
    @Output() backToCartOutput: EventEmitter<any> = new EventEmitter();
    @Output() confirmCheckoutOutput: EventEmitter<any> = new EventEmitter();
    @Output() sendToEmailOutput: EventEmitter<any> = new EventEmitter();
    // @Output() downloadPdfOutput: EventEmitter<any> = new EventEmitter();
    @Output() downloadCartPdfOutput: EventEmitter<any> = new EventEmitter();
    @Output() downloadExcelOutput: EventEmitter<any> = new EventEmitter();
    @Input() source: string;

    public showBtnModel = 0;
    public showActionButtons: boolean = true;

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private router: Router
    ){
        super(location, router, route);

        this.router.events
        .filter(e => e instanceof RoutesRecognized)
        .pairwise()
        .subscribe((event: any[]) => { 
            localStorage.setItem('previousUrl', event[0].urlAfterRedirects);
        });
    }
    public ngOnInit(){
        if (this.source == 'product-details') this.showActionButtons = false;
    };

    public navigate(value: number){
        this.showBtnModel = value;
        switch(value){
            case 0:
                this.router.navigateByUrl('/products');
            break;
            case 1:
                this.router.navigateByUrl('/cart');
            break;
            case 2:
                this.router.navigateByUrl('/order/history');
            break;
        }
    }

    public close(){
        const previousUrl = localStorage.getItem('previousUrl');

        if (previousUrl && previousUrl.includes('log/quantity')){
            this.router.navigateByUrl(previousUrl);
        } else if (previousUrl && previousUrl.includes('/cart')){
            this.router.navigateByUrl(previousUrl);
        }  else {
            this.router.navigateByUrl('products');
        }
    }

    public backToCart() {
        this.backToCartOutput.emit();
    }

    public confirmCheckout() {
        this.confirmCheckoutOutput.emit();
    }

    public sendToEmailDialog() {
        this.sendToEmailOutput.emit();
    }

    // public downloadPdf() {
    //     this.downloadPdfOutput.emit();
    // }

    public downloadCartPdf() {
        this.downloadCartPdfOutput.emit();
    }

    public downloadExcel() {
        this.downloadExcelOutput.emit();
    }
}