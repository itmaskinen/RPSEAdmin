import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { AccountProductQtyLogService } from '../../services/accoutProductQtyLog.service';
import { BaseComponent } from '../../core/components';

import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-account-product-transaction-log',
  templateUrl: './account-product-transaction-log.component.html',
  styles: []
})
export class AccountProductTransactionLogComponent extends BaseComponent implements OnInit {
  private productId: number;

  public showSpinner: boolean;
  public data: any[];

  constructor(
    private productLogService: AccountProductQtyLogService, 
    private toastr: ToastsManager,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute){
        super(location, router, route);
    }

  ngOnInit() {
  }

  public loadTransactionLogs(id: number) {
    //this.productLogService.getProductTransactionLogByProductId(id)

    return new Promise((resolve) => {
      this.productId = id;
      this.productLogService.getProductTransactionLogByProductId(id)
      .catch((err: any) => {
          this.toastr.error(err, 'Error');
          return Observable.throw(err);
      }).finally(() => {
          this.showSpinner = false;
      })
      .subscribe((response) => {
          let data = response.Data;

          this.data = data;
          // this.productQtyLogDataCount = data.length;
          // this.productQtyLogData.next(data);
          // this.constructProductTreeData(data).then((treeData) => {
          //     this.productQtyTreeData = treeData;
          // });
          resolve(data);
      });
  });
  }
}
