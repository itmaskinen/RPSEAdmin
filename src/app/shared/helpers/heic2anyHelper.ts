import heic2any from "heic2any";

export class Heic2anyHelper {

    public static async convertHeic(selectedFile) {
        let blob: Blob = selectedFile;
        let file: File = selectedFile;
        let convertion: any;

        if (selectedFile.type) {
            return file;
        } else {
            convertion = await heic2any({blob, toType: "image/jpeg"}).then((jpgBlob: Blob) => {
                const newName = selectedFile.name.replace(/\.[^/.]+$/, ".jpg");
                return this.blobToFile(jpgBlob, newName);

            }).catch(err => {
                console.log(err);
            });
            return file = convertion;
        }
    }

    public static blobToFile = (theBlob: Blob, fileName: string) : File => {
        const b: any = theBlob;

        b.lastModified = new Date();
        b.name = fileName;

        return <File>theBlob;
    }
}
