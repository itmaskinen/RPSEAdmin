import { Injectable } from '@angular/core';

@Injectable()
export class MapperService {
    constructor() { }

    public mergedLocationLevels(data): any {
        let result: any = [];
        let emptyData =
        {
            Id: 0,
            Name: "",
            Address: "",
            AccountLocationLevelThreeName: "",
            AccountLocationLevelThreeId: 0,
            AccountId: 0,
            Qty: 0,
            TotalQty: 0
        };
        let lvl1 = data.accountLocationLevelOne;
        let lvl2 = data.accountLocationLevelTwo;
        let lvl3 = data.accountLocationLevelThree;
        let lvl4 = data.accountLocationLevelFour;

        for (let i = 0; i < lvl1.length; i++) {
            const obj1 = lvl1[i];
            obj1.isSelected = false;

            const filteredArr2 = lvl2.filter(obj2 => obj2.AccountLocationLevelOneId === obj1.Id);
          
            if (filteredArr2.length === 0) {
              result.push({'location': [obj1, emptyData, emptyData, emptyData]});
            } else {
              for (let j = 0; j < filteredArr2.length; j++) {
                const obj2 = filteredArr2[j];
                obj2.isSelected = false;
                const filteredArr3 = lvl3.filter(obj3 => obj3.AccountLocationLevelTwoId === obj2.Id);
          
                if (filteredArr3.length === 0) {
                  result.push({'location': [obj1, obj2, emptyData, emptyData]});
                } else {
                  for (let k = 0; k < filteredArr3.length; k++) {
                    const obj3 = filteredArr3[k];
                    obj3.isSelected = false;
                    const filteredArr4 = lvl4.filter(obj4 => obj4.AccountLocationLevelThreeId === obj3.Id);
                    
                    if (filteredArr4.length === 0) {
                        result.push({'location': [obj1, obj2, obj3, emptyData]});
                    } else {
                    for (let l = 0; l < filteredArr4.length; l++) {
                        const obj4 = filteredArr4[l];
                        obj4.isSelected = false;
                        result.push({'location': [obj1, obj2, obj3, obj4]});
                    }
                    }
                  }
                }
              }
            }
        }
        return JSON.parse(JSON.stringify(result));
    }

    // Get location based on data provider upon clicking
    public secondLevelLocation(firstLocation: any, secondLocation: any) {
        const locationTwoList: any = [];
        secondLocation.Data.forEach(element => {
            const locationOne = {
                location: []
            }
            // structure the data firstlocation > secondLocation
            element.isSelected = false;
            firstLocation.isSelected = false;
            locationOne.location.push(firstLocation);
            locationOne.location.push(element);
            locationTwoList.push(locationOne);
        });
        return JSON.parse(JSON.stringify(locationTwoList));
    }

    public mergedLevels(data): any {
        let result = [];

        let emptyData =
        {
            Id: 0,
            Name: "",
            Address: "",
            AccountLocationLevelTwoName: "",
            AccountLocationLevelTwoId: 0,
            AccountId: 0,
            Qty: 0,
            TotalQty: 0
        };

        data.accountLocationLevelOne.forEach(locationOne => {
            result.push({
                location: [locationOne, emptyData, emptyData]
            });

            const locationTwoItems: any[] = data.accountLocationLevelTwo.filter(e => e.AccountLocationLevelOneId === locationOne.Id);

            locationTwoItems.forEach(locationTwo => {
                result.push({
                    location: [locationOne, locationTwo, emptyData]
                });

                const locationThreeItems: any[] = data.accountLocationLevelThree.filter(e => e.AccountLocationLevelTwoId === locationTwo.Id);

                locationThreeItems.forEach(locationThree => {
                    result.push({
                        location: [locationOne, locationTwo, locationThree]
                    });
                });
            });
        });

        return JSON.parse(JSON.stringify(result));
    }
}
