import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { IPagedResults } from '../core/interfaces';

@Injectable()
export class TagService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getTagByAccountId(accountId: number): Observable<IPagedResults<any>>{
        let url = `Tag/GetByAccountId?accountId=${accountId}`;    
        return super.getAll<any>(url);
    }

    public getLatestTagByAccountId(accountId: number, size: number = 5): Observable<IPagedResults<any>>{
        let url = `Tag/GetLatestTagByAccountId?accountId=${accountId}&size=${size}`;    
        return super.getAll<any>(url);
    }

    public saveTag(data){
        let url = `Tag/Save`;
        return this.dataService.put(`${API_URL}${url}`,data);
    }
}
