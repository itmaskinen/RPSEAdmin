import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AccountModel } from '../../models';
import { AccountService } from '../../services';
import { ValidationService } from '../../shared/services/validation.service';

import * as $ from 'jquery';
@Component({
    selector: 'app-accounts-create',
    templateUrl: './accounts-create.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class AccountsCreateComponent implements OnInit {
    model : any;
    companyTypes: any;
    accountForm: any;
    isDuplicate: boolean = false;

    public showSpinner: boolean = false;
    public accountName: string = '';
    constructor(
        private accountService: AccountService, 
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private formBuilder: FormBuilder) {

        }

    public ngOnInit() : void {
        this.model = new AccountModel();
        this.accountForm = this.formBuilder.group({
            'Company': ['', Validators.required],
            'EmailAddress': ['', Validators.required, Validators.email]
        });
        $('#company-name').focus();
    }

    public changeCompany(){
        this.model.Name = this.model.Company.toLowerCase().split(' ').join('_');
        this.accountService.getAccountIsDuplicateByName(this.model.Company)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.isDuplicate = response.Extras;
            }
        });
    }

    public saveChanges() : void{
        this.showSpinner = true;
        this.accountService.saveAccountV3(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.router.navigate([`/accounts`]);
            }
        });
    }

}
