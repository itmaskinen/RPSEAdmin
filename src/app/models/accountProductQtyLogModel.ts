export class AccountProductQtyLogModel {
    public Id : number;
    public AccountProductId: number;
    public Qty: number;
    public Mode: number;
    public Remarks: string;
    public UpdatedBy: number;
    public IsMobile: boolean;
    public UpdatedDate: string;
}