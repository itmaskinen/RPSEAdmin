import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { NgbModal, ModalDismissReasons, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

import { ProductModel, AccountProductModel, CartProductModel } from '../../models';
import { ProductService } from '../../services';
import { ModalService } from '../../shared';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';

@Component({
    selector: 'app-account-products-list-compact',
    templateUrl: './account-products-list-compact.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()]
})
export class AccountProductsListCompactComponent extends BaseComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model: any;
    accountProductModel: any;
    cartModel: any;
    showSpinner: boolean = false;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    accountId: number = 0;
    productList: any;
    productCount = 0;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route:ActivatedRoute,
        private modalService: ModalService,
        private elem: ElementRef,
        private location: Location,
        private rateConfig: NgbRatingConfig) {
            super(location, router, route);
        }

    packageData: Subject<any> = new Subject<any[]>(); 
    public ngOnInit() 
    {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.rateConfig.max = 5;
    }

    public loadData(id){
        this.showSpinner = true;
        this.productService.getByAccountList(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productCount = response.Data.length;
        });
    }

    public loadDataFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, keyWord, currentAccountId, displaying){
        this.showSpinner = true;
        this.productService.getProductsFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, 0, keyWord, currentAccountId, displaying)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productCount = response.Data.length;
        });
    }

    public confirmProductAdd(productFormContent: string, item): void {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        
        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.AccountId = this.accountId;
        this.accountProductModel.Description = item.Description;
        this.accountProductModel.Location = item.Location;
        this.accountProductModel.QuantityOnStock = item.QuantityOnStock;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.IsMobile = false;
        this.accountProductModel.UpdatedBy = super.currentLoggedUser().id;
        this.modalService.open(productFormContent, { size: 'lg'});

    }

    public saveProductToAccount(){
        this.showSpinner = true;
        this.productService.saveToAccount(this.accountProductModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.router.navigate([`products`]);
            }
        });
    }

    public addtoCart(addCartFormContent: string, item){
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();
        
        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.QuantityOnStock;
        this.accountProductModel.AccountId = this.accountId;
 
        this.modalService.open(addCartFormContent, { size: 'lg' });
    }

    public cartQuantityChange(){
        this.cartModel.TotalPrice = this.cartModel.Quantity * this.accountProductModel.Price
    }

    public navigateToEdit(id){
        this.router.navigate([`products/${id}/edit`]);  
    }
}
