import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'app-button-group',
    templateUrl: './button-group.component.html',
    styleUrls: ['./button-group.component.scss']
})
export class ButtonGroupComponent implements ICellRendererAngularComp {
    public params;
    public buttonInfo: any[];

    constructor() { }

    agInit(params): void {
        this.params = params;
        this.buttonInfo = params.buttonInfo;
    }

    isPopup() {
        return true;
    }

    refresh(params: any): boolean {
        return true;
    }
}
