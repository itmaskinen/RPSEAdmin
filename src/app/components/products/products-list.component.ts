import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { ProductModel, AccountProductModel } from '../../models';
import { ProductService } from '../../services';
import { IPagedResults } from '../../core/interfaces';
import { ModalService } from '../../shared';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
@Component({
    selector: 'app-products-list',
    templateUrl: './products-list.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ProductsListComponent extends BaseComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model: any;
    accountProductModel: any;
    showSpinner: boolean = false;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    accountId: number = 0;
    productList: any;
    productCount = 0;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route:ActivatedRoute,
        private modalService: ModalService,
        private location: Location,
        private elem: ElementRef) {
            super(location, router, route)
        }

    packageData: Subject<any> = new Subject<any[]>(); 
    public ngOnInit() 
    {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        //this.loadData();
    }

    public loadData(){
        this.showSpinner = true;
        this.productService.getAllIsSystemOnly()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productCount = response.Data == null ? 0 : response.Data.length;
        });
    }

    public loadDataFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, keyWord){
        this.showSpinner = true;
        this.productService.getProductsFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, 0, keyWord)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productCount = response.Data.length;
        });
    }

    public confirmProductAdd(productFormContent: string, item): void {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        
        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.AccountId = this.accountId;
        this.modalService.open(productFormContent, { size: 'lg' });
    }

    public saveProductToAccount(){
        this.showSpinner = true;
        this.accountProductModel.IsMobile = false;
        this.accountProductModel.UpdatedBy = super.currentLoggedUser().id;
        this.productService.saveToAccount(this.accountProductModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.router.navigate([`account/${this.accountProductModel.AccountId}/products`]);
            }
        });
    }

    public navigateToEdit(id){
        this.router.navigate([`products/${id}/edit`]);  
    }
}
