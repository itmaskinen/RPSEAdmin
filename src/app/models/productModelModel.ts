export class ProductModelModel { 
    public Id : number = 0;
    public ShortCode: string = "";
    public LongName: string = "";
    public Description: string = "";

    public GroupId: number = 0;
    public ManufacturerId: number = 0;
    public CreatedByAccountId: number = 0;
}