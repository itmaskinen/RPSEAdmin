export class ProductGroupModel { 
    public Id : number = 0;
    public ShortCode: string = "";
    public Name: string = "";
    public Description: string = "";
    public MainPhotoByteString: string;
    public PhotoUrl: string;
}