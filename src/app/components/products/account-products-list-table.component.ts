import { Component, OnInit, ElementRef, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { NgbModal, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

import { ProductModel, AccountProductModel, CartProductModel, AccountProductQtyLogModel, ProductFilterModel } from '../../models';
import { ProductService, LocationService } from '../../services';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';

import * as moment from 'moment';
import { ModalService } from '../../shared';
declare var jsPDF: any; // Important


@Component({
    selector: 'app-account-products-list-table',
    templateUrl: './account-products-list-table.component.html',
    styleUrls: ['../layout/layout.component.scss', './products.css'],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()]
})
export class AccountProductsListTableComponent extends BaseComponent implements OnInit {
    //UPDATE
    public applyQtyTo: any;
    //UPDATE

    @Output() productQtyUpdate = new EventEmitter();
    isAdminOrUserEditor: boolean;
    loadingMessage: string = 'Loading...';
    model: any;
    accountProductModel: any;
    productQtyLogModel: any;

    cartModel: any;
    showSpinner: boolean = false;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    accountId: number = 0;
    productList: any;
    productCount = 1;
    
    selectedAccountId: number = 0;
    selectedGroupId: number = 0;
    selectedManufacturerId: number = 0;
    selectedColorMatId: number = 0;
    selectedModelId: number = 0;
    selectedLocationId: number = 0;
    selectedKeyword: string = '';
    selectedDisplaying: number = 0;
    selectedViewType: number = 0;
    selectedQtyView: number = 0;
    
    public locationLevelOneList: any = [];
    public locationLevelTwoList: any = [];
    public locationLevelThreeList: any = [];

    public locationLevelOneId: number = 0;
    public locationLevelTwoId: number = 0;
    public locationLevelThreeId: number = 0;

    public confirmPopupMessage: string = 'Are you sure you want to add this product in your database?';
    public qtyLabel: string = 'Initial Quantity';
    public confirmIconClass = 'fa fa-plus fa-lg success';
    public manufactuererFiltered = [];
    public groupFiltered = [];
    public colorMatFiltered = [];

    public qtyAllowed: boolean = true;
    public productFilter : any;
    p: number = 1;

    constructor(
        private locationService: LocationService,
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route:ActivatedRoute,
        private location: Location,
        private modalService: ModalService,
        private elem: ElementRef,
        private chRef: ChangeDetectorRef,
        private rateConfig: NgbRatingConfig) {
            super(location, router, route);
        }

    packageData: Subject<any> = new Subject<any[]>(); 

    public key: string = 'ProductName';
    public reverse: boolean = false;
    public sort(key){
        this.key = key;
        this.reverse = !this.reverse;
    }

    public ngOnInit() 
    {
        this.accountId = super.currentAccountId();
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();
        this.productFilter = new ProductFilterModel();
        this.rateConfig.max = 5;
    }

    public exportImagesToZip(){
        this.showSpinner = true;
        this.productService.getProductsFilteredImages(
            this.selectedAccountId, 
            this.selectedGroupId, 
            this.selectedColorMatId, 
            this.selectedManufacturerId, 
            this.selectedModelId, 
            this.selectedLocationId,
            this.selectedKeyword, 
            this.accountId, 
            10000, 
            0, 
            this.productFilter.qtyView)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => { 
             
              const blob = new Blob([response], { type: 'application/zip' });
              const url= window.URL.createObjectURL(blob);
              window.open(url);
    
              
            });
    }

    public loadDataFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, locationId, keyWord, currentAccountId, displaying, viewType, qtyView) : any{
        this.showSpinner = true;
        this.selectedAccountId = Number(accountId);
        this.selectedGroupId = groupId;
        this.selectedColorMatId = colorMatId;
        this.selectedManufacturerId = manufacturerId;
        this.selectedModelId = modelId;
        this.selectedLocationId = locationId;
        this.selectedKeyword = keyWord;
        this.selectedDisplaying = displaying;
        this.selectedViewType = viewType;
        this.selectedQtyView = qtyView;

        this.loadingMessage = displaying > 100 ? `Fetching...` : this.loadingMessage;
        return new Promise((resolve) => {
            this.productService.getProductsFiltered(Number(accountId), groupId, colorMatId, manufacturerId, modelId, locationId, keyWord, currentAccountId, displaying, 0, qtyView)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;

                let groupIds = [];
                let manufacturerIds = [];
                let colorMaterialIds = [];
                let modelIds = [];
        
                if(!super.issuperadminoradmin() && this.productFilter.GroupId === undefined){
                    groupIds = data.map((item) => {
                        return item.GroupId;
                    });
                    groupIds = Array.from(new Set(groupIds));
                }

                if(this.productFilter.GroupId !== undefined)
                {
                    if(groupId !== this.productFilter.GroupId) {
                        manufacturerIds = data.map((item) => {
                            return item.ManufacturerId;
                        });
                        manufacturerIds = Array.from(new Set(manufacturerIds));
                    }
                }
                
                if(this.productFilter.GroupId !== undefined && this.productFilter.ManufacturerId !== undefined)
                {
                    if(groupId !== this.productFilter.GroupId || manufacturerId !== this.productFilter.ManufacturerId) {
                        colorMaterialIds = data.map((item) => {
                            return item.ColorMatId;
                        });
                        colorMaterialIds = Array.from(new Set(colorMaterialIds));
                    }
    
                }
                
                modelIds = data.map((item) => {
                    return item.ModelId;
                });
                modelIds = Array.from(new Set(modelIds));

                //qtyTotals = data.reduce(this.getSum, 0);

                let returnData = {
                    groups: groupIds,
                    manufacturers : manufacturerIds,
                    colorMaterials : colorMaterialIds,
                    models: modelIds,
                    recordCount : data.length,
                    //qtyTotals : qtyTotals
                }

                this.productList = data;
                this.productCount = data.length;
                this.packageData.next(data);

                // store filter properties
                this.productFilter = { 
                AccountId: accountId, 
                GroupId: groupId, 
                ManufacturerId: manufacturerId, 
                ColorMatId: colorMatId, 
                ModelId: modelId, 
                Keyword: keyWord, 
                Displaying: displaying  };

                resolve(returnData);
            });
         });
    }

    public confirmProductAdd(productFormContent: string, item: any, mode? :number) {
        let promiseAll = Promise.all([
            this.loadAccountLocationLevelOneList(this.accountId),
            this.loadAccountLocationLevelTwoList(this.accountId),
            this.loadAccountLocationLevelThreeList(this.accountId),
            ]);

        promiseAll.then((result) =>{
            this.applyQtyTo = 1;

            //LOCATION
            if(item.LocationLevelOneId > 0)
            {
                this.applyQtyTo = 1;
                this.accountProductModel.LocationLevelOneId = item.LocationLevelOneId;
                let promiseOne = this.loadAccountLocationLevelTwoList(this.accountId, item.LocationLevelOneId);
                promiseOne.then((result) => {
                    if(item.LocationLevelTwoId > 0)
                    {
                        this.applyQtyTo = 2;
                        this.accountProductModel.LocationLevelTwoId = item.LocationLevelTwoId;
                        let promiseTwo = this.loadAccountLocationLevelThreeList(this.accountId, item.LocationLevelTwoId);
                        promiseTwo.then((result) => {
                            if(item.LocationLevelThreeId > 0)
                            {
                                this.applyQtyTo = 3;
                                this.accountProductModel.LocationLevelThreeId = item.LocationLevelThreeId;
                            }
                        })
                    }
                });
            }
        });

        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;
        this.model.CurrentQty = item.CurrentQty;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.AccountId = this.accountId;
        this.accountProductModel.Description = item.Description;
        this.accountProductModel.Location = item.Location;
        this.accountProductModel.Price = item.Price;

        this.productQtyLogModel.Mode = mode;
        this.productQtyLogModel.AccountProductId = item.AccountProductId;
        this.productQtyLogModel.Qty = this.accountProductModel.QuantityOnStock;
    
        if(item.AccountProductId > 0){
            this.confirmPopupMessage = mode === 1 ? 'Please indicate the quantity youll be adding up.' : 'Please indicate the quantity youll be removing.';
            this.confirmIconClass = mode === 1 ? 'fa fa-plus fa-lg success' : 'fa fa-minus fa-lg danger';
            this.qtyLabel = 'New Quantity';
        }
        else {
            this.confirmPopupMessage = 'Are you sure you want to add this product in your database?';
            this.qtyLabel = 'Initial Quantity';
        }

        this.modalService.open(productFormContent, { size: 'lg'});
    }

    public validateValue(val, maxval){
        if(Number(val) > maxval && this.productQtyLogModel.Mode === -1)
        {
            //this.accountProductModel.QuantityOnStock = maxval;
            this.qtyAllowed = false;
            this.toastr.error('Input value not allowed, should not exceed the current qty stock', 'Not Allowed');
            return false;
        }
    }

    public saveProductToAccount(){
        if (this.applyQtyTo == null) {
            this.applyQtyTo = 1;
        }

        this.showSpinner = true;
        this.productQtyLogModel.Qty = this.accountProductModel.QuantityOnStock;
        this.productQtyLogModel.IsMobile = false;
        this.productQtyLogModel.UpdatedBy = super.currentLoggedUser().id;
        
        let newMergedProductModel = Object.assign({ 
            ProductQtyLogModel : this.productQtyLogModel,
            AppliedTo: this.applyQtyTo === 1 ? this.accountProductModel.LocationLevelOneId : (this.applyQtyTo === 2 ? this.accountProductModel.LocationLevelTwoId : this.accountProductModel.LocationLevelThreeId)
        }, this.accountProductModel);

        this.productService.saveToAccount(newMergedProductModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                if(this.selectedAccountId == 0)
                {
                    this.toastr.success('Product Added to your database', 'Success')
                }
                this.reloadProductList();
                this.productQtyUpdate.emit(this.productQtyLogModel);
            }
        });
    }

    public reloadProductList(){
        this.loadDataFiltered(this.selectedAccountId, 
            this.selectedGroupId, 
            this.selectedColorMatId, 
            this.selectedManufacturerId, 
            this.selectedModelId, 
            this.selectedLocationId,
            this.selectedKeyword, 
            this.accountId, 
            this.selectedDisplaying, 
            this.selectedViewType, 
            this.selectedQtyView);
    }

    public addtoCart(addCartFormContent: string, item){
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();
        
        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.QuantityOnStock;
        this.accountProductModel.AccountId = this.accountId;
 
        this.modalService.open(addCartFormContent, { size: 'lg' });
    }

    public isHideEditQtyButton(accountHasThisProduct: boolean): boolean{
        return this.isSuperAdmin || (this.selectedAccountId == 0) && accountHasThisProduct === true;
    }

    public cartQuantityChange(){
        this.cartModel.TotalPrice = this.cartModel.Quantity * this.accountProductModel.Price
    }

    public navigateToEdit(id){
        if(!super.issuperadminoradmin()) {
            this.router.navigate([`products/${id}/details`]);
        }
        else {
            this.router.navigate([`products/${id}/edit`]);
        }
    }

    //LOCATION
    public loadAccountLocationLevelOneList(accountId: number){
        return new Promise((resolve) => { 
            //clear dropdown data
            this.accountProductModel.LocationLevelOneId = 0;
            this.accountProductModel.LocationLevelTwoId = 0;
            this.accountProductModel.LocationLevelThreeId = 0;
            this.locationLevelTwoList = [];
            this.locationLevelThreeList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelOne(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    this.locationLevelOneList = data;
                    this.accountProductModel.LocationLevelOneId = this.locationLevelOneList[0].Id;
                    this.selectLevelOne(this.accountProductModel.LocationLevelOneId);
                }
                else {
                    this.accountProductModel.LocationLevelOneId = 0;
                }
                resolve(data);
            });
        });
    }

    public loadAccountLocationLevelTwoList(accountId: number, levelOneId?:number){
        return new Promise((resolve) => {
            //clear dropdown data
            this.accountProductModel.LocationLevelTwoId = 0;
            this.accountProductModel.LocationLevelThreeId = 0;
            this.locationLevelThreeList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelTwo(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;

            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    if(levelOneId> 0) {
                        let locationLevelTwoFilteredList = data.filter((item) => {
                            return item.AccountLocationLevelOneId === levelOneId;
                        });
                        if(locationLevelTwoFilteredList.length > 0){
                            this.locationLevelTwoList = locationLevelTwoFilteredList;
                            this.accountProductModel.LocationLevelTwoId = locationLevelTwoFilteredList[0].Id;
                            this.selectLevelTwo(this.accountProductModel.LocationLevelTwoId);
                        }
                        else {
                            this.accountProductModel.LocationLevelTwoId = 0;
                        }
                    }
                    else {
                        this.locationLevelTwoList = data;
                    }
                }
                else {
                    this.accountProductModel.LocationLevelTwoId = 0;
                }
                resolve(data);
            });
        });
    }

    public loadAccountLocationLevelThreeList(accountId: number, levelTwoId?:number){
        return new Promise((resolve) =>{
            //clear dropdown data
            this.accountProductModel.LocationLevelThreeId = 0;
            this.locationLevelThreeList = [];

            this.accountId = accountId;
            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelThree(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                    this.showSpinner = false;
            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    if(levelTwoId> 0) {
                        let locationLevelThreeFilteredList = data.filter((item) => {
                            return item.AccountLocationLevelTwoId === levelTwoId;
                        });
                        
                        if(locationLevelThreeFilteredList.length > 0){
                            this.locationLevelThreeList = locationLevelThreeFilteredList;
                            this.accountProductModel.LocationLevelThreeId = locationLevelThreeFilteredList[0].Id;
                        }
                        else {
                            this.accountProductModel.LlocationLevelThreeId = 0;
                        }
                    }
                    else {
                        this.locationLevelThreeList = data;
                    }
                }
                else {
                    this.accountProductModel.LlocationLevelThreeId = 0;
                }
                resolve(data);
            });
        });
    }

    public selectLevelOne(id){
        this.onLevelOneChange(id);
    }

    public selectLevelTwo(id){
        this.onLevelTwoChange(id);
    }

    public onLevelOneChange(val){
        this.applyQtyTo = 1;
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelTwoList(this.accountId, value);
        }
    }

    public onLevelTwoChange(val){
        this.applyQtyTo = 2;
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelThreeList(this.accountId, value);
        }
    }

    public deleteProduct(item){
        this.showSpinner = true;
        this.productService.delete(item)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.toastr.success('Product Deleted!', 'Success');
                this.reloadProductList();
                this.productQtyUpdate.emit(this.productQtyLogModel);
            }
        });
    }

    public imageUrl(url){
        return super.imageUrl(url);
    }

    //EXPORT
    public exportToCSV(){
        this.showSpinner = true;
        //parsing data
        let parsePromise = new Promise((resolve) => {
            //get data
            this.loadingMessage = `Fetching...`;
            this.productService.getProductsFiltered(
            this.selectedAccountId, 
            this.selectedGroupId, 
            this.selectedColorMatId, 
            this.selectedManufacturerId, 
            this.selectedModelId, 
            this.selectedLocationId,
            this.selectedKeyword, 
            this.accountId, 
            10000, 
            0, 
            this.productFilter.qtyView)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => { 
                let data = response.Data;
                this.loadingMessage = `Parsing...`;
                let parsedData = data.map((item) => {
                    return {
                        'Product Number' : item.ProductName,
                        'Name' : item.ProductNumber,
                        'Location': item.LocationLevelOneName,
                        'Product Group': item.GroupName,
                        'Manufacturer': item.ManufacturerName,
                        'Color/Material': item.ColorMaterialName,
                        'Model': item.ModelName,
                        'Qty': item.CurrentQty,
                        'Height': item.Height,
                        'Width': item.Width,
                        'Depth': item.Length
                    }
                });
                resolve(parsedData);
            });
        });
        //passing parsed data to be written
        parsePromise.then((data) => {
            this.loadingMessage = `Creating file...`;
            super.jsonToCSV(data, 'Products', true).then(() => {
                this.showSpinner = false;
            });
            
        });
    }

    public exportToPDF(){
        this.showSpinner = true;
        this.loadingMessage = `Fetching...`;
        let parsePromise = new Promise((resolve) => {
            this.productService.getProductsFiltered(
            this.selectedAccountId, 
            this.selectedGroupId, 
            this.selectedColorMatId, 
            this.selectedManufacturerId, 
            this.selectedModelId, 
            this.selectedLocationId,
            this.selectedKeyword, 
            this.accountId, 
            10000, 
            0, 
            this.productFilter.qtyView)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => { 
                let data = response.Data;
                this.loadingMessage = `Parsing...`;
                let parsedData = data.map((item) => {
                    return {
                        ProductNumber: item.ProductNumber,
                        ProductName : item.ProductName,
                        Location: item.LocationLevelOneName ? item.LocationLevelOneName : '',
                        GroupName: item.GroupName,
                        Manufacturer: item.ManufacturerName,
                        ColorMaterial: item.ColorMaterialName,
                        CurrentQty: item.CurrentQty,
                        Model: item.ModelName
                    }
                });
                resolve(parsedData);
            });
        });

        let doc = new jsPDF('l', 'pt');
        let totalPagesExp = "{total_pages_count_string}";
        let dateInfo = moment(new Date()).format('YYYY-MM-DD');
        let headerTitle = `Products`;
        let pdfName = `Products-${dateInfo}.pdf`;
        let pageWidth = doc.internal.pageSize.getWidth();
        let leftStart = 20;
        let rightStart = (pageWidth / 2);
        let centerStart = rightStart - 60;
        let contentYStart = 30; 
        doc.page = 1; 
        doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);
        contentYStart += 20; 

        let contentColumns = [
        { 
            title: 'Product Number', dataKey: 'ProductNumber'
        },
        { 
            title: 'Name', dataKey: 'ProductName'
        },
        { 
            title: 'Product Group', dataKey: 'GroupName'
        },
        { 
            title: 'Manufacturer', dataKey: 'Manufacturer'
        },
        { 
            title: 'Color/Material', dataKey: 'ColorMaterial'
        },
        { 
            title: 'Model', dataKey: 'Model'
        },
        { 
            title: 'Location', dataKey: 'Location'
        },
        { 
            title: 'Qty', dataKey: 'CurrentQty'
        }
        ];

        doc.setFontType('bold');
        let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
        doc.text(headerTitle, pageWidth - (40 + headerWidth), contentYStart);
        contentYStart+= 10;

        parsePromise.then((data) => {
            var pageContent = ((data) => { 
                //HEADER
                doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);
                let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
                doc.text(headerTitle, pageWidth - (40 + headerWidth), 50);
                contentYStart += 10;

                // FOOTER
                var str = "Page " + data.pageCount;
                if (typeof doc.putTotalPages === 'function') {
                    str = str + " ";
                }
                doc.setFontSize(10);
                var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
                doc.text(str, leftStart, pageHeight  - 10);
            });
           

            doc.autoTable(contentColumns, data, {
                margin: { top: contentYStart, left: 20, right: 20, bottom: 20 },
                styles: { overflow: 'linebreak', cellPadding: 2, fontSize: 9 },
                theme: 'plain',
                headerStyles : {
                    fillColor: [40, 167, 69],
                    textColor: 'white'
                },
                columnStyles: {
                    text: {
                        columnWidth: 'wrap'
                    },
                    ProductName: {
                        columnWidth: 150           
                    },
                    ColorMaterial: {
                        columnWidth: 90
                    }
                },
                addPageContent: pageContent
            });

            contentYStart+= 50;

            doc.setFontSize(10);
            var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
            doc.text("Printed: "+ dateInfo, leftStart, pageHeight  - 20);
            doc.text("© Copyright 2018 RP.SE ",190, pageHeight - 20);
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            doc.save(pdfName);
            this.showSpinner = false;
        });
    }
}
