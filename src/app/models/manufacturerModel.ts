export class ManufacturerModel {
    public Id: number = 0;
    public ShortCode: string;
    public CompanyName: string = '';
    public ContactName: string;
    public ContactTitle: string;
    public EmailAddress: string;
    public Address: string;
    public City: string;
    public PostalCode: string;
    public Country: string;
    public Phone: string;
    public Fax: string;
    public HomePage: string; 
}