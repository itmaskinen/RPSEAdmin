import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';


import { ProductModel, AccountProductModel } from '../../../models';
import { ProductService } from '../../../services';
import { BaseComponent } from '../../../core/components';
import { FILE_URL } from '../../../constants';

@Component({
    selector: 'app-products-public-details',
    templateUrl: './products-public-details.component.html',
    styleUrls: ['../../layout/components/header/header.component.scss','../../layout/layout.component.scss', '../../products/products.css', './products-public-details.css'],
    animations: [componentsTransition()]
})
export class ProductsPublicDetailsComponent implements OnInit {
    photoSrc: string = '';
    model: any;
    accountId: number;
    constructor(private productService: ProductService,
        private route: ActivatedRoute)
    {
    };

    public ngOnInit() 
    {
        this.route.params.subscribe((params) => {
            let id = params['id']; // product id
            this.getProduct(id);
        });

        this.model = new ProductModel();
    }

    public imageUrl(url){
        if(!url){
            return '/assets/images/upload-empty.png';
        }
        if(url.indexOf('http') > -1) {
            return url;
        }
        return FILE_URL + url;
    }

    private getProduct(id){
        this.productService.getProduct(id,0,'')
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
            }
            else {
                this.model = response.Data;
                if(this.model.MainPhotoUrl) {
                    this.photoSrc = this.imageUrl(this.model.MainPhotoUrl);;
                }
                else {
                    this.photoSrc = "/assets/images/add_photo_image.png";
                }
            }
        });
    }

    public isProductNameEqualToGroup() {
        let result = false;
        if (this.model.ProductName.includes('-')) {
            if (!this.model.AddedFromMobile) {
                const nameArray = this.model.ProductName.split("-").map(name => name.trim());
                if (nameArray.length > 0) {
                    if (this.model.GroupName == nameArray[0] && this.model.ManufacturerName == nameArray[1]) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }
}
