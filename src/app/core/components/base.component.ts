import { ElementRef } from "@angular/core";
import { StringHelper } from '../../shared/helpers';

import { API_URL, FILE_URL } from '../../constants';
import { Router, ActivatedRoute, RouterStateSnapshot  } from '@angular/router';

import { Location } from '@angular/common';


export class BaseComponent {
    public isLoading: boolean = false;
    public isSaving: boolean = false;
    public accountId: number = 0;

    public secondaryUserData: any;
    public superAdminAccess: any;

    constructor(
       private baseLocation: Location,
       private baseRouter: Router,
       private baseRoute:ActivatedRoute) {
    }

    public getLastUrl(): string{
       return localStorage.getItem('lastUrl');
    }

    public setLastUrl(url: string){
        localStorage.setItem('lastUrl', url);
    }

    public lastAccessedURL(){
        return document.referrer;
    }

    public URLReferrerHasSuperAdminAccess(){
        return this.getLastUrl().indexOf('superAdminAccess') > -1;
    }

    public baseHasSuperAccessUrl(){
        let url = window.location.href;
        return url.indexOf('superAdminAccess') > -1 && Number(this.baseRoute.snapshot.queryParams['superAdminAccess']) > 0;
    }

    ///HELPER FUNCTIONS
    public imageUrl(url){
        if(!url){
            return '/assets/images/upload-empty.png';
        }
        if(url.indexOf('http') > -1) {
            return url;
        }
        return FILE_URL + url;
    }

    public getTextFromDropDownList (el: ElementRef): string {
        if (el === null) {
            throw 'Unable to get text from drop down list because the element is undefined. Please ensure the element ref is retrieved correctly.';
        }

        let text: string = el.nativeElement.value.trim();
        if ( text !== undefined && (text.toLowerCase() === 'all' || text.toLowerCase() === 'none')) {
            text = '';
        }

        return text;
    }

    public csvToJson(csvString: string) {
        const lines = csvString.trim().split("\n");
        const result = [];
        const headers = lines[0].split(",");        
        for (let i = 1 ; i < lines.length; i++) {
            const obj = {};
            const currentline = lines[i].split(",");
            for (let j = 0; j < headers.length; j++) {
                obj[headers[j].trim()] = currentline[j].trim();
            }
            result.push(obj);
        }
        return JSON.stringify(result);
    }
    //usage
    //util.jsonToCSV(ReportsPaymentClassList.Data, "Reports Classlist - Course : ( " + ReportsPaymentClassList.CourseId + " )", true);
    public jsonToCSV(JSONData : any, ReportTitle: string, ShowLabel : boolean){
        return new Promise((resolve) => {
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
            var CSV = 'sep=;';
            CSV += '\r';
            //CSV += ReportTitle + '\r\n\n';
            if (ShowLabel) {
                var row = "";
                for (var index in arrData[0]) {
                    row += index + ';';
                }
                row = row.slice(0, -1);
                CSV += row + '\r\n';
            }
            for (var i = 0; i < arrData.length; i++) {
                var row = "";
                for (var index in arrData[i]) {
                    row += '"' + (arrData[i][index] == null ? '' : arrData[i][index]) + '";';
                }
    
                row.slice(0, row.length - 1);
                CSV += row + '\r\n';
            }
    
            if (CSV == '') {
                alert("Invalid data");
                return;
            }
            let fileName = "Report_";
    
            fileName += ReportTitle.replace(/ /g, "_");
            let uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            let link = document.createElement("a");
            link.href = uri;
            //link.style = "visibility:hidden";
            link.download = fileName + ".csv";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            resolve(fileName + ".csv");
        });
    }

    //uses JSPDF
    public exportToPDF(data: any){
        var columns = [
          
        ];
        var finalData = data.Data.map(function (item) {
         
        });
  
    }
    
    public logo64base() {
        return 'iVBORw0KGgoAAAANSUhEUgAAAJYAAABKCAYAAABZ/a22AAAABGdBTUEAALGPC/xhBQAACklpQ0NQc1JHQiBJRUM2MTk2Ni0yLjEAAEiJnVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/stRzjPAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAALiMAAC4jAXilP3YAAAvBSURBVHic7Z15dFTlFcB/LzND2BdlsYCgtAYOdQGx1HNc2iKkWC1VwQVK1GOrxdMW11pRi/a0LsdKXaggtix1iUILFVyK4oK7QqQYGo1EVMAANeCBLECSmbz+cWdMmCQz775t5mX8nZMTSL7lvsl93/u+uz1j++jRfwduAP6HS5gxk1hpLU9ePJxZjzzrZKg7gRuBGnckS0sIqEc+i0pgF7AN2Aw8D+z0cO5rgTlALWB6OE+CxLV+gVzrTuBz5Fr/jcNrDQMXx7/GAa84GSyBETKAMFPKtlM8ooBN5ZvtDtUl/r2HG3JZpCvQBxiR9PMmYB2iYPNx8UaM0zn+vbvL46Yica3Dk35uAhuA1cACYLt24DyaV4OXgXvsy3go4bFdCb+3n5vGDHQyjB93rlXygJOBW5GVbAlwuIvjN7k4llMMYAxwM7JiLwW+oRkgL+n/1wEVwDGORWs0yRvSmZMfr+S88ac6Hi4LuQTYDdyeaUF84AJgB3Cv1Q7JigXwLeQ5O8OpNHlHdAJMZnZvdDpUNnMT8AkwLNOC+MDVwFbg2HQN21KsBPOBl5DnsD0aTcIn9GDoU3uYfVGh7WECwNHAFuDSDMvhB0OATcCvUzVKpVggG/rdwCTbYkQAQhR9sIthRw2xPUxAWAzMzrQQPvEAKbYB6RQL5GS2Elhoa/oYhE/qRrh0P7NOK7A1RMD4PXB9poXwiZuQw0wrrChWgsuQY+cJ6umbTPIG5jP+0a2cdfrJ6u4B5E/IhjcXuA0xVx2CRrEABgMbgd+qepmQNzgfMJnZ11BOGVgeAgZkWgifWIjsM78ibHOgu4ALgYmI5TY9jSah47pRsKKKqyZP4P7la2xO3S5fIAbMzogdxg5NyK6wN7JJPcyBPH2AB4EpDsZoj2rgLcR6rl0cEpjx/r2AoTizyYWRa/1Ryx/YZTRiKJyKGNDSYuSHALh4327+EgoRi8UcTN+K14DzXRzPQCzSlwBXYE/JJgNnAY78Wm3wIXCmy2MeAxQBVwJ9bfQ/E7ne5WBf2xMYwJPI5j79WDGT8Ek9OezFGuZOG+dw6lY4vZZkTKAcmIXczTfbHOcq1yRqxov9RAVyou2H/cPHrMQ/3PpjTEJ8Z+lN7E0mEGHiOzs48diRLk3vC3cAxwNVyn4TgNPcF8dT5iCG8kplvzHAD8Ddu7wv8Drw55StTAiP7YJRUc/s4+2suBllE/Bt9MpV5IEsXrMFsbBvVfa7HNx/fABcA/yX1h7zZhpNQiO6cULxDi6YEDg/YhV6U8I5NEdqBIm9wDRlnzOALl4oFshdXU5ce9vC6C5TTx0Q8UgET1kLzFO07wec6I0onvMWcL+ifX/gTK8UK8HDwBqaY42aiUGooCujHtvOuWec4rEYnqBRLJBTdFC5D9Ac4U/yWrEAxiMxX5OTf2H0FGvHjL5+iOE6ZYix2CqBe+a34DPgBUX7EX79RcPAP0n2N8ZMQgVdKVi6kwsLA/m5a+xT7e85g8EGRdshfi8VlyEW8q8eC4lV6/Ie2RRAaZlyRdt+iKU7qGxRtM3IM6gfov3iFY+vWt9cXsWFhUEz96ji3nvhbzy721hz3Qm9M7m5uQ2JG4+vWibnB89BrVlmI0AnrwTxgYOKtnmZVKyVSDwPZr0cOF49GLjPXfNoayK7Eia05GsaO3FCO6EIeAyAEMT+U0flpMOZu+LFDIljG03mSj26uz7b0KRbNfitWOuBs2nxvDbrJcPriZCfqYOuMUbRdjdQ55UgPpCcZ5mK3X4+Cm8HxtJyExiC2KZaPj+3Lw/+K3CrFcAPFW13eCaFP4xVtK30Y8WqBAqBD5J/kVitlkSD6EbjVEATxP+yV4L4wHfRRWiUe71iLUYiMVspVWK1+mRKfxY+7Upmv99cq2xf4okU/qCNKdvo1YoVRUJyV7bbokFWq2UNqsNGtnA+cK6i/QFkfxlEfoxECVslCqz2YsV6BTGCtq9UEYNoaS1bJvdlwarArVbDgEXKPs+jMzBmC4PQX+saYLvbinU9kuS6t90WBphVDQA8XBO4kJmjgXfRW9AXeyCL1wxAQma00ZhLwL1AvwrkQ5+TtmXYIPbpQV4vGsqyF153aXpfuAyp0aD9oNcDq9wXx1OmIrWytKnrHwHLwB3Fmoecjj5L2zIE0XW11I/qwi2vaPy3lnA15SdOPrLH2ITdTHBJRXcbLyz4nZB0vvVAMfaM519dq5PNezWSR/am5R6NAE0sGzmQbcWbHEzdJscgIcN9sH/DxBA3zTDkeD0aZ/69l0l4GNzlCCQtLRL/skMTku0zFPgecq2tAzKt8zYtgh/tKtbTyB/RuosiYhBdV82us/swu/h5m9OmZBQW8xt9wsSb1C+Ao4jvZbKIQ65Ve2dHET/fJJR+L3Ov1Miab3ZTThlYfokkleQCN5BkTtGsWBuQrN5d6mlDBrHNB9gw/UgeeSxw5gU73IfUF8sFFiJFUA7B6oo1G3G46pXKgGjJQYx+nbizNIimHDWPIClwucAq4Odt/SKdYlUie5c/2J46zwAaWFE4iJLSMtvDBIQFyKY6F1gK/KS9X6ZSrL8hZYvetz11xCBaUs3uwl5cVxzI6AUNM3ChbmtA+A1wUaoGbe2xmoDzSOWSsYhZJ6alxd36YJrZVFnbVT5CbF0VmRbEB7Yiq1TaxSZ5xXoJcVc4VioiBrGyOkqnDmReMGOt0lEF/BQJgOvoSlUN/Awxc1h6goWBxPn/anSp1O1jQKzsIJDH/MoGV4bMEqqQ8JdivDF8ZhNNwBvIYUTtdQgjGvgL3AzryDMw6xpYXXQ0qx91vXJfJlkG/CrTQnhEA2JS2giUAk/h4H06YeB05MVA7hCCaEkNNad04/rl1r09LvAqcvTtj7XsmSqkUMfjijmuRFb1TD/6PkSqwHRCmT2ThIEkeVQjodP7nIsmhHFTqSAewGey9MgB1L1p/0Bpgyrg4/iXVcqRoL1zLLbPQ2x6ma53tQ9d3QjfcTceKyQBfFsn9+P2JzU1JFzB7rVo34UzHbHtZZKsr6LiqoDmHvEHztnj5qieU4JEPWrQlSPPQdxTrIhB7NMDrJt+JE+vfdu1YX3iDmX7i4BAFvXyC3cUy4Douv3ERnXh1ne3uTKkz6xFbHgacuWdObZw8VEYZdnIIyiv0FS7ySruVrYvBL7vgRwdAueKFTGIbqjhi4m9ufmJQFvYX0C/1/qdF4J0BBwrllkr/sBF+b06gj/wIWX7cYhf9WuScKZYEYPYB3VsnDaIBSu1W5SsZAWS8qThag/kCDz2FcuA6LqDEIowZ/Ne9yTKPPcp25+G2La+pgUOH4UNPDNtMG+U+Gph95p/AO8o+9zghSBBxp5ihSC6oYZ943pyzRMdMob9r8r2x5HiZQm5iC3FMg/IJn1e995Eo1FXBcoSFiEefg3X4c1buQKJXrEiBrGyWj6e0p+HVwW55FNatLFpw+m4ITVqdIplQOzTeiDE3TsbvZEoe1iELlIC4BaaAydzGp1ihQ3MqnrWTB/MmjeDWu5JhTY3sD+SaJDzWFesFgU9/rjW9YIe2cpc9C+DnIm8Uzqnsa5Y8YIeS0cOZNvn2s86sDSi9yH2AW70QJZAYU2xIgbR92v45JzDudWbgh7ZzBLgS2Wfq9DXlupQWFIs80vZqC9pCGR1Y6dUoz8hdibH91rpFStiEPv4AOuLhvDoc2u9lyg7eRB9osEMpM5WTpJasRIBfMPzmf2O9p3THYo96FetMDkcwmzhURjlme8MDHIAn1ssRl+i8QqC/cpe27SvWCEJ4PtyQi+uLe4QITFO+Qy410a/mS7LEQjaVaxEAN+8Lr1oagry29Bc5QEkY1jDpcgrQ3KKthUrYhArr+O9aYNY2LH9gVq2oY8yhRwMYW5TsWJl9Ri9IzxQUe23PMloogX8iiy4B6nFquEsYHyaNhr3WtZHUbSuj2WamHX1PHtpAa8teS4DIh3Cgfj3mjTtegD7PZYlwXZkI3856eUCqZ7cE9lrpco2SRQLro33aY8eBOC9h/8Hq8fIvB/ClZcAAAAASUVORK5CYII='
    }

    public removeDuplicates(arr, prop) {
        let obj = {};
        return Object.keys(arr.reduce((prev, next) => {
            if(!obj[next[prop]]) obj[next[prop]] = next; 
            return obj;
        }, obj)).map((i) => obj[i]);
    }

    public randomString(length) {
        const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        length = length || 10;
        let string = '', rnd;
            while (length > 0) {
                rnd = Math.floor(Math.random() * chars.length);
                string += chars.charAt(rnd);
                length--;
            }
        return string;
    }

    public isVisible(data: any, propertyName: string): boolean {
        if (typeof data === "undefined" || data === null) {
            return false;
        }

        const fieldData = data[propertyName];

        if (typeof fieldData === "undefined" || fieldData === null) {
            return false;
        } 

        if (typeof fieldData === "string") {
            return StringHelper.isNullOrEmpty(fieldData);
        }

        return true;
    }

    public isNullOrEmpty(value: string) {
        return StringHelper.isNullOrEmpty(value);
    }

    ///PROPERTIES
    public currentLoggedUser(): any{
        let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        return currentUserData;
    }

    public secondaryAdminUser(): any{
        if(this.withSecondaryUserData())
        {
            return JSON.parse(localStorage.getItem('secondaryAdminUser'));
        }
        return {};
    }

    public currentRole(): string{
        return this.currentLoggedUser() ? this.currentLoggedUser().role : null;
    }

    public withSecondaryUserData(): boolean {
        return JSON.parse(localStorage.getItem('secondaryAdminUser'));
    }
    
    public hasSuperAccess(){
        return this.withSecondaryUserData() && this.baseHasSuperAccessUrl();
    }

    public currentAccountName(): any {
        return this.currentLoggedUser().currentUserRaw.Account.Company;
    }
    //@TODO : make sure to use this globally
    //apply to the components
    public isadmin(): boolean {
        return this.currentRole() ? this.currentRole() === 'admin' : false;
    }

    public issuperadmin(): boolean{
        return this.currentRole() ? this.currentRole() === 'superadmin' : false;
    }

    public issuperadminoradmin(): boolean {
        return this.currentRole() ? this.currentRole() === 'superadmin' || this.currentRole() === 'admin' : false;
    }

    public isEditor(): boolean {
        return this.currentRole() ? this.currentRole() === 'editor' : false;
    }

    public currentAccountId(): number {
        return this.currentLoggedUser().accountId;
    }

    public setProductFilterStore(
    groupId = [], 
    manufacturerId = [], 
    colorMatId = [], 
    modelId = [], 
    locationId = 0, 
    viewType = 3, 
    keyword = '', 
    conditionId = [],
    tagId = [],
    ownerId // default as table,
    )
    {
        if(ownerId === undefined)
        {
            return false;
        }
        
        
        let filter = {
            group : groupId,
            manufacturer: manufacturerId,
            colorMat: colorMatId,
            model: modelId,
            location: locationId,
            view: viewType,
            keyword: keyword,
            condition: conditionId,
            tag: tagId,
            ownerId: ownerId
        };
        //parse the store
        let filterStore : any[] = this.getProductFilterStore();

        //remove the item from the array if the same owner id found
        let filterStoreIterate = filterStore.filter((item) => {
            return item.ownerId !== ownerId;
        });

        if(filterStoreIterate.length > 0){
            filterStoreIterate.forEach(e => {
                let filterCopy = {...filter};
                filterCopy.ownerId = e.ownerId;
                e.filters = filterCopy;
            });
        }

        filterStoreIterate.push({'ownerId' : ownerId, 'filters' : filter });
        filterStore = filterStoreIterate;

        localStorage.setItem('productFilter', JSON.stringify(filterStore));
    }

    public getProductFilterStore(): any{
        let filterStore = localStorage.getItem('productFilter');
        filterStore = filterStore ? JSON.parse(filterStore) : [];
        return filterStore; 
    }

    public clearFilterStore(){
        localStorage.removeItem('productFilter');
    }

    public currentAccountEmail(): any {
        return this.currentLoggedUser().currentUserRaw.EmailAddress;
    }
}
