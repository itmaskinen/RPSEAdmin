import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared';
import { ProductsCatalogComponent } from './components';
import { ForgotCredentialsComponentComponent } from './components/forgot-credentials/forgot-credentials.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ProductsPublicDetailsComponent } from './components/public/products/products-public-details.component';
import { ProductLabelPrintComponent } from './components/public/products/product-label-print/product-label-print.component';

const routes: Routes = [
    { path: '', loadChildren: './components/layout/layout.module#LayoutModule', canActivate: [AuthGuard] },
    { path: 'login', loadChildren: './components/login/login.module#LoginModule' },
    { path: 'catalog', component: ProductsCatalogComponent },
    { path: 'signup', loadChildren: './components/signup/signup.module#SignupModule' },
    { path: 'error', loadChildren: './components/server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './components/access-denied/access-denied.module#AccessDeniedModule' },
    //WEB
    {
        path: 'products/public',
        children: [
            // { path: 'print/label', component: ProductsPublicDetailsComponent },

            { path: ':id/details', component: ProductsPublicDetailsComponent },
            { path: ':id/:accountId/print/product/label', component: ProductLabelPrintComponent },
            { path: ':id/:accountId/print/booking/label', component: ProductLabelPrintComponent },
            { path: ':id/:accountId/print/allproduct/label', component: ProductLabelPrintComponent }
        ]
    },
    { path: 'products/public/:id/details', component: ProductsPublicDetailsComponent },
    { path: 'forgot-password', component: ForgotCredentialsComponentComponent },
    { path: 'reset-password', component: ResetPasswordComponent },
    { path: 'not-found', loadChildren: './components/not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [FormsModule, RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
