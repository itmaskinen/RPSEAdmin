import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HttpModule } from "@angular/http";

import { DataService, UtilityService } from "../../core/services";

import { LayoutRoutingModule } from "./layout-routing.module";
import { LayoutComponent } from "./layout.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { HeaderComponent } from "./components/header/header.component";
import { SpinnerComponent } from "../../shared/modules/spinner/spinner.component";

import { NgxPaginationModule } from "ngx-pagination";
import { ToastModule, ToastOptions } from "ng2-toastr";
import { NotficationOptions } from "../../core/options/toastr.options";
import { QRCodeModule } from "angularx-qrcode";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import {
    AccountsComponent,
    AccountsCreateComponent,
    AccountsListComponent,
    AccountsEditComponent,
    UsersComponent,
    UsersCreateComponent,
    UsersListComponent,
    UsersEditComponent,
    ProductsComponent,
    ProductsListComponent,
    ProductsCreateEditComponent,
    ProductBookingComponent,
    ProductsGroupsComponent,
    ProductsGroupsListComponent,
    ProductsDetailComponent,
    ProductsModelsComponent,
    ProductsModelsListComponent,
    ProductsModelsCreateEditComponent,
    ProductsColorMaterialsComponent,
    ProductsColorMaterialsListComponent,
    ProductsColorMaterialsCreateEditComponent,
    AccountProductsListComponent,
    AccountProductsListCompactComponent,
    AccountProductsListTableComponent,
    AccountProductsListTileComponent,
    ManufacturersComponent,
    ManufacturersListComponent,
    LocationsListComponent,
    LocationsCreateEditComponent,
    AccountProductConditionComponent,
} from "..";

import { ProductsLocationsComponent } from "../products/products-locations.component";
import { AccountProductsQtyComponent } from "../product-log/qty/account-product-qty.component";
import { AccountProductsCurrentQtyListComponent } from "../product-log/qty/account-product-current-qty-list.component";
import { CartComponent } from "../cart/cart.component";
import { OrderHistoryComponent } from "../cart/order-history.component";
import { ManufacturersCreateEdiComponent } from "../manufacturers/manufacturers-create-edit.component";
import { ProductsGroupsCreateEditComponent } from "../product-groups/products-groups-create-edit.component";
import { AccountProductsQtyLogListComponent } from "../product-log/qty/account-product-qty-log-list.component";
import { CartNavigationComponent } from "../cart/cart-navigation.component";

import {
    AccountService,
    UserService,
    CommonService,
    AuthenticationService,
    ProductService,
    LocationService,
    OrderService,
    AccountProductConditionService,
} from "../../services";

import { AccountProductQtyLogService } from "../../services/accoutProductQtyLog.service";

// Pipes
import { Ng2OrderModule } from "ng2-order-pipe";
import {
    DateFormatPipe,
    UppercasePipe,
    LocalDateFormatPipe,
    NullableBooleanPipe,
} from "../../shared/pipes";
import { CalendarModule, PickListModule, GrowlModule } from "primeng/primeng";

// import { TreeviewModule } from 'ngx-treeview';
import { TreeModule } from "angular-tree-component";
import { AccountProductTransactionLogComponent } from "../product-log/account-product-transaction-log.component";
import { AgGridModule } from "ag-grid-angular";
import { DashboardService } from "../../services/dashboard.service";
import { ButtonGroupComponent } from "../../core/components/ag-grid-controls/button-group/button-group.component";
import { AccountProductsListAgGridComponent } from "../products/account-products-list-aggrid.component";
import { ModalService } from "../../shared/services/modal.service";
import { DndDirective } from "../../directives/dnd.directive";
import { registerLocaleData } from "@angular/common";
import { NgZorroAntdModule } from "ng-zorro-antd";
import en from "@angular/common/locales/en";
registerLocaleData(en);
import { NZ_I18N, en_US } from "ng-zorro-antd";
import { TagsListComponent } from "../tags/tags-list/tags-list.component";
import { TagService } from "../../services/tag.service";
import { NgSelectModule } from "@ng-select/ng-select";
@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        AgGridModule.withComponents([ButtonGroupComponent]),
        HttpModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbModule,
        NgbModule.forRoot(),
        NgxPaginationModule,
        ToastModule.forRoot(),
        CalendarModule,
        PickListModule,
        GrowlModule,
        InfiniteScrollModule,
        Ng2OrderModule,
        //TreeviewModule.forRoot(),
        TreeModule.forRoot(),
        QRCodeModule,
        NgZorroAntdModule.forRoot(),
        NgSelectModule,
    ],
    declarations: [
        SpinnerComponent,
        LayoutComponent,
        SidebarComponent,
        HeaderComponent,
        AccountsComponent,
        AccountsCreateComponent,
        AccountsEditComponent,
        AccountsListComponent,
        UsersComponent,
        UsersCreateComponent,
        UsersListComponent,
        UsersEditComponent,
        ProductsComponent,
        ProductsListComponent,
        ProductsCreateEditComponent,
        ProductBookingComponent,
        ProductsGroupsComponent,
        ProductsGroupsListComponent,
        ProductsGroupsCreateEditComponent,
        ProductsModelsComponent,
        ProductsModelsListComponent,
        ProductsLocationsComponent,
        ProductsDetailComponent,
        ProductsColorMaterialsComponent,
        ProductsColorMaterialsListComponent,
        ProductsColorMaterialsCreateEditComponent,
        AccountProductsListComponent,
        AccountProductsListCompactComponent,
        AccountProductsListTableComponent,
        AccountProductsListTileComponent,
        AccountProductsListAgGridComponent,
        ManufacturersComponent,
        ManufacturersListComponent,
        ManufacturersCreateEdiComponent,
        LocationsListComponent,
        LocationsCreateEditComponent,
        ProductsModelsCreateEditComponent,
        AccountProductsQtyLogListComponent,
        AccountProductsQtyComponent,
        AccountProductsCurrentQtyListComponent,
        AccountProductTransactionLogComponent,
        CartNavigationComponent,
        CartComponent,
        OrderHistoryComponent,
        AccountProductConditionComponent,
        TagsListComponent,
        // Pipes
        DateFormatPipe,
        UppercasePipe,
        LocalDateFormatPipe,
        NullableBooleanPipe,
        ButtonGroupComponent,
        DndDirective,
    ],

    providers: [
        DataService,
        UtilityService,
        AccountService,
        UserService,
        CommonService,
        ModalService,
        AuthenticationService,
        ProductService,
        LocationService,
        AccountProductQtyLogService,
        OrderService,
        AccountProductConditionService,
        TagService,
        { provide: ToastOptions, useClass: NotficationOptions },
        DashboardService,
        { provide: NZ_I18N, useValue: en_US },
    ],
})
export class LayoutModule {}
