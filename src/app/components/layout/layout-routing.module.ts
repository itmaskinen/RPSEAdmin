import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { 
    AccountsComponent, AccountsCreateComponent, AccountsEditComponent,
    UsersComponent, UsersCreateComponent, UsersEditComponent,
    ProductsComponent, ProductsCreateEditComponent,
    ProductsGroupsComponent, ProductsModelsComponent, ProductsColorMaterialsComponent, 
    ManufacturersComponent, ManufacturersCreateEdiComponent, ProductBookingComponent
} from '../../components';
import { ProductsColorMaterialsCreateEditComponent } from '../color-materials/products-color-materials-create-edit.component';
import { ProductsGroupsCreateEditComponent } from '../product-groups/products-groups-create-edit.component';
import { CartComponent } from '../cart/cart.component';
import { OrderHistoryComponent } from '../cart/order-history.component';
import { ProductsDetailComponent } from '../products/product-details.component';
import { AccountProductsQtyLogListComponent } from '../product-log/qty/account-product-qty-log-list.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            //TEMPLATE MODULES (let's remove this as soon as we can finish majority of the modules to copy from)
            { path: '', redirectTo: 'products' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            //MAIN MODULES
            { path: 'accounts/create', component:  AccountsCreateComponent },
            { path: 'accounts', component:  AccountsComponent},
            { path: 'accounts/:id/edit', component:  AccountsEditComponent },
            { path: 'accounts/:id/edit/:mode', component:  AccountsEditComponent },
            { path: 'users', component:  UsersComponent},
            { path: 'users/create', component:  UsersCreateComponent },
            { path: 'users/create/:account', component:  UsersCreateComponent },
            { path: 'users/:id/edit', component:  UsersEditComponent },
            { path: 'products', component:  ProductsComponent },
            { path: 'account/:id/products', component:  ProductsComponent },
            { path: 'products/create', component:  ProductsCreateEditComponent },
            { path: 'products/:id/details', component:  ProductsDetailComponent },
            { path: 'products/create/:groupId/:manufacturerId/:modelId/:colorMatId', component:  ProductsCreateEditComponent },
            { path: 'account/:accountId/products/create/:groupId/:manufacturerId/:modelId/:colorMatId', component:  ProductsCreateEditComponent },
            { path: 'account/:accountId/products/create', component:  ProductsCreateEditComponent },
            { path: 'products/:id/edit', component:  ProductsCreateEditComponent },
            { path: 'products/booking', component:  ProductBookingComponent },
            { path: 'product-groups', component:  ProductsGroupsComponent },
            { path: 'product-groups/refresh', component:  ProductsGroupsComponent },
            { path: 'product-groups/create', component:  ProductsGroupsCreateEditComponent },
            { path: 'product-groups/:id/edit', component:  ProductsGroupsCreateEditComponent },
            { path: 'product-models', component:  ProductsModelsComponent },
            { path: 'product-color-materials', component:  ProductsColorMaterialsComponent },
            { path: 'product-color-materials/refresh', component:  ProductsColorMaterialsComponent },
            { path: 'product-color-materials/create', component:  ProductsColorMaterialsCreateEditComponent },
            { path: 'product-color-materials/:id/edit', component:  ProductsColorMaterialsCreateEditComponent },
            { path: 'product-manufacturers', component:  ManufacturersComponent },
            { path: 'product-manufacturers/refresh', component:  ManufacturersComponent },
            { path: 'product-manufacturers/create', component:  ManufacturersCreateEdiComponent },
            { path: 'product-manufacturers/:id/edit', component:  ManufacturersCreateEdiComponent },
            { path: 'cart', component:  CartComponent },
            { path: 'order/history', component:  OrderHistoryComponent },
            { path: 'log/quantity/:accountId', component: AccountProductsQtyLogListComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
