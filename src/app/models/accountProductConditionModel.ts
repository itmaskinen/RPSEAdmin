export class AccountProductConditionModel{
    public Id : number = 0;
    public AccountId : string = '';
    public Code : string = '';
    public Name : string = '';
    public Description : string = '';
    public NewProductConditionAssignment: number = 0;
}