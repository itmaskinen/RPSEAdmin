import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { DataService, UtilityService } from './core/services';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ToastModule } from 'ng2-toastr'; 

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AuthGuard, ModalService, MapperService } from './shared';
import { CalendarModule } from 'primeng/calendar';

import { 
    AccountService,
    AuthenticationService, LocationService, ProductService
} from './services';
import { ProductsCatalogComponent } from './components';
import { ForgotCredentialsComponentComponent } from './components/forgot-credentials/forgot-credentials.component';
import { ToastrModule } from 'ngx-toastr';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ProductsPublicDetailsComponent } from './components/public/products/products-public-details.component';
import { ProductLabelPrintComponent } from './components/public/products/product-label-print/product-label-print.component';
import { NgSelectModule } from '@ng-select/ng-select';


// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-5/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule, 
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-right'
        }), 
        ToastModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        NgSelectModule,
        FormsModule,
        CalendarModule
    ],
    declarations: [
        AppComponent,
        //public
        ProductsCatalogComponent,
        ForgotCredentialsComponentComponent,
        ResetPasswordComponent,
        ProductsPublicDetailsComponent,
        ProductLabelPrintComponent
    ],
    providers: [
        AuthGuard,
        DataService, 
        UtilityService,
        AuthenticationService,
        ProductService,
        ModalService,
        MapperService,
        LocationService,
        AccountService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
