
let apiPath = '';
let sitePath = '';
let filePath = '';
let version = '3.8'

if(window.location.href.indexOf('localhost') > -1 || window.location.href.indexOf('http://127.0.0.1') > -1){
    // apiPath = 'http://localhost:51552/';
    // sitePath = 'http://localhost:60745/';
    apiPath = 'http://20.234.104.200:8080/api';
    sitePath = 'http://20.234.104.200:8080';
    filePath = 'https://rpangularapp.blob.core.windows.net/rp-files-staging/';
}
else if(window.location.href.indexOf('staging.itmaskinen.se:9012') > -1){
    apiPath = 'http://staging.itmaskinen.se:9011';
    sitePath = 'http://staging.itmaskinen.se:9012';
    filePath = 'https://rpangularapp.blob.core.windows.net/rp-files/';
}
else if(window.location.href.indexOf('fiatest.rp.se') > -1){
    apiPath = 'https://fiatest.rp.se/api/';
    sitePath = 'https://fiatest.rp.se/';
    filePath = 'https://rpangularapp.blob.core.windows.net/rp-files-staging/';
} else if(window.location.href.indexOf('azurewebsites') > -1){
    apiPath = 'https://rpseitmwebapidemo.azurewebsites.net';
    sitePath = 'https://rpseitmwebdemo.azurewebsites.net';
    filePath = 'http://rpfiles.itmaskinen.se/';
}
else if(window.location.href.indexOf('rpapp') > -1){
    apiPath = 'http://rpapi.rpapp.net';
    sitePath = 'http://rp.rpapp.net';
    filePath = 'http://rpfiles.itmaskinen.se/';
}
else if(window.location.href.indexOf('20.234.104.200:8080') > -1){
    apiPath = 'http://20.234.104.200:8080/api';
    sitePath = 'http://20.234.104.200:8080';
    filePath = 'http://20.234.104.200:8080/files';
}
else {
    // apiPath = 'http://rpapi.itmaskinen.se';
    // sitePath = 'http://rp.itmaskinen.se';
    // filePath = 'http://rpfiles.itmaskinen.se/';

    apiPath = 'https://rpappapi.azurewebsites.net/';
    sitePath = 'https://fia.rp.se/';
    filePath = 'https://rpangularapp.blob.core.windows.net/rp-files/';
}

//let fileServerPath = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:9100/' : 'http://rpfiles.itmaskinen.se';

export const API_URL = apiPath;
export const SITE_URL = sitePath;
export const FILE_URL = filePath;
export const APP_VERSION = version;