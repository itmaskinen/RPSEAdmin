import { DatePipe, Location } from '@angular/common';
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import 'rxjs/add/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import { isNumeric } from "rxjs/util/isNumeric";
import { SITE_URL } from "../../constants/index";
import { BaseComponent } from '../../core/components';
import { AccountModel, AccountProductConditionModel, AccountProductModel, AccountProductQtyLogModel, CartProductModel, ProductModel, ProductModelModel, ProductPhotoModel } from '../../models';
import { OrderModel } from '../../models/orderModel';
import { componentsTransition } from '../../router.animations';
import { AccountProductConditionService, AccountService, LocationService, OrderService, ProductService } from '../../services';
import { MapperService, ModalService } from '../../shared';

import { AccountProductsQtyComponent } from '../product-log/qty/account-product-qty.component';
import { ProductsLocationsComponent } from './products-locations.component';
import * as mtimezone from 'moment-timezone';
import * as moment from 'moment';
import { Heic2anyHelper } from './../../shared/helpers/heic2anyHelper';
import { TagService } from '../../services/tag.service';
import { TagModel } from '../../models/tagModel';

@Component({
    selector: 'app-products-create-edit',
    templateUrl: './products-create-edit.component.html',
    styleUrls: ['../layout/layout.component.scss', '../cart/cart.css',"./products.css"],
    animations: [componentsTransition()]
})
export class ProductsCreateEditComponent extends BaseComponent implements OnInit {
    @Input() action: string;

    //UPDATE
    public applyQtyTo: any;
    public applyQtyDstTo: any;

    public accountProductDstModel: any;
    public productQtyLogDstModel: any;

    public locationLevelOneDstList: any = [];
    public locationLevelTwoDstList: any = [];
    public locationLevelThreeDstList: any = [];
    public locationLevelFourDstList: any = [];
    //UPDATE

    @ViewChild('fileUpload') fileUpload : ElementRef;
    @ViewChild('otherFileUpload') otherFileUpload : ElementRef;
    @ViewChild('price') price : ElementRef;
    @ViewChild('Weight') weight : ElementRef;
    @ViewChild('kgCO2') kgCO2 : ElementRef;
    @ViewChild('accountProductQty') accountProductQty : AccountProductsQtyComponent;
    @ViewChild('productsLocations') productsLocations: ProductsLocationsComponent;
    @ViewChild('confirmCartContent')
    private confirmCartContentTpl: TemplateRef<any>;

    @ViewChild('confirmChangeSystemProductContent') confirmChangeSystemProductContent: any;
    @ViewChild('productTransferQtyFormContent') productTransferQtyFormContent: any;

    showSpinner = false;
    showMoveQuantitySpinner = false;
    showAddQuantitySpinner = false;
    disableButtons: boolean;
    showPhotoSpinner = false;
    isAdminOrUserEditor: boolean;
    isSuperAdmin = false;
    isLogsClicked: boolean = false;
    mode = 'create';
    accountId = 0;
    productId = 0;
    otherPhotoDndFile: any;
    mainPhotoDndFile: any;
    rotationAngle = 0;
    otherPhotoId = 0;
    isImageRotated = false;
    rotatedImage: any;
    otherPhotoIndex: number;

    deleteProductMessage: string = "Are you sure you want to delete this Product?";
    btnLogsLabel: string = 'Show'

    productDetailsPublicPath: string = SITE_URL + "#/products/public/product_id/details";

    volume: number;
    model: any;
    accountModel: any;
    productModelModel: any;
    productPhotoModel: any;
    productQtyLogModel: any;

    productManufacturers: any;
    productColorMats: any;
    productGroups: any;
    productModels: any;
    recentlySavedProductModel: any;

    manufacturerCode: string;
    colorMatsCode: string;
    groupsCode: string;
    modelCode: string;

    defaultPhotoSrc: any;
    photoSrc: any;
    hasMainPhoto: boolean = false;
    photoSizeCls: string = 'col-sm-2';
    productNumDefault: boolean = true;
    productNumManual: boolean = false;

    currentUser: any;
    canEdit: boolean = false;
    panelTitle: string = '';
    search: string = '';

    productPhotoList: any = [];
    newOtherPhoto: any = {
        Show: false
    };
    PhotoProcessType: number = 0;
    mainPhotoWidth: number = 0;
    mainPhotoHeight: number = 0;
    tempPhotoCounterId: number = 0;
    productOtherPhotos: File[] = [];
    productMainPhotos: File[] = [];
    qtyModuleData: any = [];

    public locationLevelOneList: any = [];
    public locationLevelTwoList: any = [];
    public locationLevelThreeList: any = [];
    public locationLevelFourList: any = [];

    public accountProductConditionList: Array<AccountProductConditionModel> = [];

    public confirmPopupMessage: string = 'Are you sure you want to add this product in your database?';
    public qtyLabel: string = 'Initial Quantity';
    public confirmIconClass = 'fa fa-plus fa-lg success';
    private productSaveMessage: string = 'Product successfully added to your database';
    public productGroupList: any = [];
    public productGroupListCount: number = 0;
    public selectedModel: any;
    productTitle: string = "Product";
    public qtyAllowed: boolean = true;
    //CART
    currentTotalQty: number = 0;
    accountProductModel: any;
    cartModel: any;
    orderModel: any;

    public focusPhotoSrc: string;

    locationOneList = [];
    locationTwoList = [];
    locationThreeList = [];
    locationFourList = [];
    mergedLocations: any;
    allLocations: any;
    allLocationsCopy: any;
    allSecondLocations: any;

    sameLocation = false;
    hideDestination = true;
    hideDestionationSecond = true;

    availableQtyTobeMoved = 0;

    filteredLocationTwoList:any;
    filteredLocationThreeList:any;
    filteredLocationFourList:any;

    showLevelOne: boolean = false;
    showLevelTwo: boolean = false;
    showLevelThree: boolean = false;
    showLevelFour: boolean = false;
    enableQty: boolean = false;
    hasLocationData: boolean = true;
    hasChanges: boolean = false;

    public moveQty: number;
    public moveQtyExceed: boolean;

    tagList: any[] = [];
    tagListSelected: any[] = [];
    tagModel: TagModel = new TagModel();
    completeTagList: any[] = [];
    completeListCopy: any[];
    
    constructor(
        private checkRef: ChangeDetectorRef,
        private productService: ProductService,
        private accountService: AccountService,
        private locationService: LocationService,
        private orderService: OrderService,
        private accountProductConditionService: AccountProductConditionService,
        private toastr: ToastsManager,
        private router: Router,
        private route:ActivatedRoute,
        private location: Location,
        private modalService: ModalService,
        public activeModal: NgbActiveModal,
        private mapperService: MapperService,
        private tagService: TagService,
        private elem: ElementRef,
        private datePipe: DatePipe) {
            super(location, router, route)
        }
    public ngOnInit()
    {
        this.moveQtyExceed = false;
        this.moveQty = 0;
        this.allLocationsCopy = [];
        this.model = new ProductModel();
        this.productModelModel = new ProductModelModel();
        this.accountModel = new AccountModel();
        this.productPhotoModel = new ProductPhotoModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();
        this.accountProductModel = new AccountProductModel();
        this.accountProductDstModel = new AccountProductModel();
        this.productQtyLogDstModel = new AccountProductModel();

        this.getProductManufacturers();
        this.getProductColorMaterials();
        this.getProductGroups();
        this.getTags();
        this.setDefaults();

        this.mode = this.action;

        let currentUser = super.currentLoggedUser();
        this.currentUser = currentUser;
        this.disableButtons = true;

        this.model.ProductNumber = '';

        let productFilter = super.getProductFilterStore();
        productFilter = productFilter[productFilter.length-1];

        let id = sessionStorage.getItem('product_grid_selected_row');
        let accountId = productFilter ? productFilter.ownerId : currentUser.accountId;
        this.mode = !id ? 'create' : 'edit';

        this.getProductConditionByAccount(accountId);

        this.route.params.subscribe((params) => {
            this.canEdit = this.canEditValue();
            if (id)
            {
                this.productSaveMessage = 'Product successfully updated on your database';
                this.accountId = accountId;
                this.getProduct(Number(id));
                this.panelTitle = this.canEdit ? `Edit ${this.productTitle}` : `View ${this.productTitle}`;
                this.photoSizeCls = 'col-sm-2';
            }
            else{
                //only trigger on create
                //extra create urls
                const groupId = !!productFilter ? Number(productFilter.filters.group) : productFilter;
                const manufacturerId = !!productFilter ? Number(productFilter.filters.manufacturer) : productFilter;
                const modelId = !!productFilter ? Number(productFilter.filters.model) : productFilter;
                const colorMatId = !!productFilter ? Number(productFilter.filters.colorMat) : productFilter;

                if(groupId) {
                    this.model.GroupId = Number(groupId);
                    this.checkRef.detectChanges();
                    setTimeout(() => {
                        this.groupsChange();
                    }, 1000);
                }
                if(manufacturerId) {
                    this.model.ManufacturerId = Number(manufacturerId);
                    this.checkRef.detectChanges();
                    setTimeout(() => {
                        this.manufacturerChange();
                    }, 1000);
                }
                if(modelId) {
                    this.model.ModelId = Number(modelId);
                    this.checkRef.detectChanges();
                    setTimeout(() => {
                        this.modelChange();
                    }, 1000);
                }
                if(colorMatId) {
                    this.model.ColorMatId = Number(colorMatId);
                    this.checkRef.detectChanges();
                    setTimeout(() => {
                        this.colorMaterialsChange();
                    }, 1000);
                }
            }

            this.model.Length = 0;
            this.model.Height = 0;
            this.model.Width = 0;
            this.model.Weight = '0';
            this.model.AccountProductConditionId = 0;
            this.model.Price = '0';
            this.model.KgCo2 = '0';
            this.volume = 0;

            this.disableButtons = false;

            if(accountId)
            {
                this.accountId = accountId;
                this.model.AccountId = accountId;
                this.getAccountData(accountId);
            }

        });
        $('#productName').focus();

    }

    public getProductConditionByAccount(accountId: number){
        return new Promise((resolve) =>{
            this.accountProductConditionService.getProductConditionByAccount(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .subscribe((res) =>{
                let data = res.Data;
                this.accountProductConditionList = data;
                resolve(data);
            });
        });
    }

    public setDefaults(){
        this.defaultPhotoSrc = "/assets/images/upload-empty.png";
        this.photoSrc = "/assets/images/upload-empty.png";
        this.productTitle = super.currentRole() === 'superadmin' ? 'System Product' : 'Product';
        this.panelTitle = `Create a New ${ this.productTitle }`;
    }

    public getAccountData(id){
        this.accountService.getAccountV3(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((data) => {
            if(data.Data)
            {
                this.accountModel = data.Data;
                this.loadAccountLocationLevelOneDstList(id);
            }
        });
    }

    public getProductManufacturers(){
        this.productService.getAllManufacturers(0)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.productManufacturers = data;
        });
    }

    public getProductColorMaterials(){
        this.productService.getAllColorMat(0)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.productColorMats = data;
        });
    }

    public getProductGroups(){
        this.productService.getAllGroups(0)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.productGroups = data;
        });
    }

    public getTags() {
        let accountid = super.currentAccountId();
        this.tagService.getLatestTagByAccountId(accountid, 5)
            .subscribe(
                (response) => {
                    let result = response.Data;
                    if (result.length > 0) {
                        this.tagList = result.slice(0, 5);
                       }
                    this.completeTagList = result;
                },
                (err) => {
                    this.toastr.error(err, 'Error'); 
                },
                () => {
                    this.showSpinner = false;
                }
            );
    }

    addTag(event) {
        this.tagList = [...this.tagList, event];
        this.tagListSelected = [...this.tagListSelected, event.Id];
    }

    removeTag(event) {
        const index = this.tagListSelected.indexOf(event.value.Id.toString());
        if (index !== -1) {
            this.tagList.splice(index, 1);
        }
    }

    compareFn = (o1: any, o2: any): boolean => (o1 && o2 ? o1.Id === o2.Id : o1 === o2)

    public showProductQrCode(content: any){
        this.modalService.open(content,{ size : 'sm' });
    }

    public getProductModels(){
        this.productService.getAllModels()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.productModels = data;
        });
    }

    public getProductNumberIncrement(groupCode, colorMatCode, manufacturerCode, modelCode){
        return new Promise((resolve) => {
            this.productService.getProductNumberIncrementValue(groupCode, colorMatCode, manufacturerCode, modelCode)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                 this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                resolve(data);
            });
        })
    }

    public getProduct(id){
        this.showSpinner = true;
        this.productId = id;
        this.productService.getProduct(id, this.accountId)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.model = response.Data;
                if(this.model.TagIds !== "" && this.model.TagIds && this.model.TagIds.length > 0) {
                    const tagids = this.model.TagIds.split(",").map(e => Number(e));
                    this.tagListSelected = this.tagList.filter(tag => tagids.includes(tag.Id)).map(e => e.Id);
                }
               
                let createdDate = mtimezone.tz(moment(this.model.CreatedDate).format(), 'Europe/Stockholm').format('MM/DD/YYYY hh:mm:ss a');
                this.model.CreatedDate = new Date(createdDate);

                if(this.model.Height > 0 && this.model.Width > 0 && this.model.Length > 0){
                    this.volume = Number(((this.model.Length / 100) * (this.model.Width / 100) * (this.model.Height / 100)).toFixed(2));
                }
                this.productDetailsPublicPath = this.productDetailsPublicPath.replace("product_id", this.model.Id);
                this.hasMainPhoto = this.model.MainPhotoUrl !== ''  && !!this.model.MainPhotoUrl;
                this.model.Price = this.model.Price ? parseFloat(this.model.Price).toFixed(2) : '0';
                if(this.model.Price)
                    this.model.Price = this.model.Price.replace('.',',');
                this.model.Weight = this.model.Weight ? parseFloat(this.model.Weight).toFixed(2) : '0';
                if(this.model.Weight)
                    this.model.Weight = this.model.Weight.replace('.',',');
                this.model.KgCo2 = this.model.KgCo2 ? parseFloat(this.model.KgCo2).toFixed(1) : '0';
                if(this.model.KgCo2)
                    this.model.KgCo2 = this.model.KgCo2.replace('.',',');
                this.photoSrc = super.imageUrl(this.model.MainPhotoUrl);
                if(this.accountId > 0){
                    // this.getAccountData(this.model.AccountId);
                    this.invokeAccountProductQtyModules(id);
                }
                this.getProductPhotos(id);

                this.canEdit = this.canEditValue(this.model.AccountId);
                this.getModelsFiltered();

                this.deleteProductMessage = this.model.CurrentQty > 0
                ? "The product has a total of " + this.model.CurrentQty + " pcs in stock. " + this.deleteProductMessage
                : this.deleteProductMessage;
            }
        });
    }

    public invokeAccountProductQtyModules(id: number){
     //   if (!this.isSuperAdmin) {
            this.accountProductQty.isAdmin = super.issuperadminoradmin();//this.canEditValue(this.model.AccountId);
            this.accountProductQty.loadCurrentQtyData(id).then((data) => {
                this.qtyModuleData = data;
            });
            this.checkRef.detectChanges();
            this.accountProductQty.loadQtyLogData(id);
            this.accountProductQty.loadTransactionLogData(id);
       // }
    }

    ///only takes care for the correct sequence of filtering and building the product #
    public groupsChange(el?: ElementRef){
        this.getModelsFiltered().then((res)=>{
            this.getFiltersData().then(() => {
                this.buildProductNumberString();
            });
        });

    }
    ///only takes care for the correct sequence of filtering and building the product #
    public manufacturerChange(e?: any){
        this.getModelsFiltered().then((res)=>{
            this.getFiltersData().then(() => {
                this.buildProductNumberString();
            });
        });
    }
    ///only takes care for the correct sequence of filtering and building the product #
    public modelChange(e?: any){
        this.getFiltersData().then(() => {
            this.buildProductNumberString();
        });
    }
    ///only takes care for the correct sequence of filtering and building the product #
    public colorMaterialsChange(e?: any){
        this.getFiltersData().then(() => {
            this.buildProductNumberString();
        });
    }

    public triggerProductNumDefault(e){
        this.productNumDefault = true;
        this.productNumManual = false;
        this.model.ProductNumber = '';
        this.model.ProductNumberDisplay = '';
    }

    public triggerProductNumNumeric(e){
        this.productNumDefault = false;
        this.productNumManual = false;
        this.model.ProductNumber = '';
        this.model.ProductNumberDisplay = '';
    }

    public triggerProductNumManualInput(e){
        this.productNumDefault = false;
        this.productNumManual = true;
        this.model.ProductNumber = '';
        this.model.ProductNumberDisplay = '';
    }

    public getFiltersData(){
        return new Promise((resolve) => {
            this.groupsCode = this.elem.nativeElement.querySelector('#groupSelect option:checked').getAttribute('data-code');
            this.manufacturerCode = this.elem.nativeElement.querySelector('#manufacturerSelect option:checked').getAttribute('data-code');
            this.modelCode = this.elem.nativeElement.querySelector('#modelSelect option:checked').getAttribute('data-code');
            this.colorMatsCode = this.elem.nativeElement.querySelector('#colorMatSelect option:checked').getAttribute('data-code');

            this.groupsCode = this.groupsCode ? this.groupsCode : '000';
            this.manufacturerCode = this.manufacturerCode ? this.manufacturerCode : '000';
            this.modelCode = this.modelCode ? this.modelCode : '000';
            this.colorMatsCode = this.colorMatsCode ? this.colorMatsCode : '000';
            resolve(true);
        });
    }

    ///handles the final concatenation of codes
    ///will overwrite the product id if all of the dropdowns have finally selected
    ///will also make sure the proper ordering of the codes
    public buildProductNumberString(){
        this.model.ProductNumber = `${this.groupsCode}-${this.manufacturerCode}-${this.colorMatsCode}-${this.modelCode}-${(this.mode === 'create' ? '000000' : this.model.ProductNumber.slice(-6))}`;
    }

    public invokeFileInput(){
        setTimeout(() => {
            const imageUploadElement = document.getElementById('fileUpload')
            if (imageUploadElement) {
                imageUploadElement.click();
            }
        }, 500);
    }

    public invokeOtherFileInput(){
        setTimeout(() => {
            const otherImageUploadElement = document.getElementById('otherFileUpload')
            if (otherImageUploadElement) {
                otherImageUploadElement.click();
            }
        }, 500);
    }

    public extractMainPhotoElement(){
       const fileElement = this.fileUpload.nativeElement;
       return fileElement;
    }

    public extractOtherPhotoElement(){
        const fileElement = this.otherFileUpload.nativeElement;
        return fileElement;
     }

    //CART
    public recalculateQty(data){
        let totalQty = data.totalQty;
        this.cartModel.Quantity = totalQty;
        this.accountProductModel.QuantityOnStock = this.currentTotalQty - this.cartModel.Quantity;


        this.orderModel.OrderDetails = data.items;
    }

    public openCart(event: any, cartFormContent: string, item){
        this.orderModel = new OrderModel();
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.CurrentQty;
        this.accountProductModel.AccountId = this.accountId;

        this.currentTotalQty = item.CurrentQty;

        this.modalService.open(cartFormContent);
        event.stopPropagation();
    }

    public populateOrderData(){
        this.orderModel.UserId = super.currentLoggedUser().id;
        this.orderModel.CreatedBy = super.currentLoggedUser().username;
        this.orderModel.LastUpdatedBy = super.currentLoggedUser().username;
        this.orderModel.UserShippingAddress = this.cartModel.ShippingAddress;
        this.orderModel.ProductId = this.accountProductModel.ProductId;
    }

    public addToCart(){
        this.showSpinner = true;
        this.populateOrderData();
        this.orderService.Save(this.orderModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
               //add new product?
               this.modalService.open(this.confirmCartContentTpl);
            }
        });
    }

    public checkOut(){
        this.activeModal.close();
        this.router.navigateByUrl('/cart');
    }

    public addMoreProducts(){
        this.router.navigateByUrl('/products');
    }

    //@TODO : Transfer to base class
    public splice(src, newString, position){
        return src.substr(0, position) + newString + src.substr(position);
    }

    public navigateToList(){
        sessionStorage.removeItem('product_grid_selected_row');
        sessionStorage.removeItem('product_number_grid_selected_row');
        sessionStorage.removeItem('product_name_grid_selected_row');
        this.activeModal.close();
        this.modalService.getData(this.hasChanges);
    }

    //PRODUCT MODEL
    //@TODO: Transfer to PRODUCT CREATE/EDIT Component
    //FETCH EVERYTHING
    public getProductModelModels(){
        this.showSpinner = true;
        this.productService.getProductModelListData(0)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            this.productGroupList = response.Data;
            this.productGroupListCount = response.Data.Length;
        });
    }

    public openProductModelDialog(content:string){
        this.productModelModel = new ProductModelModel();
        //this.getProductModelModels();
        if(Number(this.model.GroupId) === 0 || Number(this.model.ManufacturerId) === 0) {
            this.toastr.warning('You need to select a group and manufacturer before opening Model Form.', 'Warning')
            return false
        };
        this.modalService.open(content, { size: 'lg'});
        $('#productModelModelName').focus();
    }

    openManageTag(content: string) {
        this.modalService.open(content, { size: 'md'});
    }

    openSearchTag(content: string) {
        this.completeListCopy =  this.completeTagList;
        this.modalService.open(content, { size: 'md'});
    }

    public editProductModel(item: any){
        this.productModelModel = new ProductModelModel();
        this.productModelModel = item;
    }

    public prouctDeleteConfirm(content: string){
        if(this.issuperadmin()) {
            this.modalService.open(content, { size: 'lg'});
        }
        else {
            this.modalService.open(content, { size: 'sm'});
        }

    }


    public confirmDeleteFromAccount(confirmContent: string){
        this.modalService.open(confirmContent, { size : 'lg'});
    }

    public delete(){
        if(this.canEdit) {
            this.deleteProduct();
        }
        else {
            this.deleteProductFromAccount();
        }
    }

    public deleteProduct(){
        this.showSpinner = true;
        this.model.AccountId = super.issuperadmin() ? 0 : this.accountId;
        this.productService.delete(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success('Product Deleted!', 'Success')
                this.activeModal.close();
            }
        });
    }

    public deleteProductFromAccount(){
        this.showSpinner = true;
        this.model.AccountId = super.currentAccountId();
        this.productService.deleteToAccount(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success('Product Deleted in you Account!', 'Success')
                this.router.navigateByUrl('/products');
            }
        });
    }

    public productModelDeleteConfirm(content: string, item: any){
        this.selectedModel = item;
        this.modalService.open(content, { size: 'sm'});
        $('#productModelModelName').focus();
    }

    public deleteProductModel(){
        this.productService.deleteProductModel(this.selectedModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.selectedModel = new ProductModelModel();
                this.productModelModel = new ProductModelModel();
                this.getProductModels();
                this.getModelsFiltered();
            }
        });
    }

    public saveProductModel(){
        if (!this.productModelModel.LongName || this.productModelModel.LongName.replace(/\s+/g, '').length == 0) {
            this.toastr.error("Please input a model name.");
            this.showSpinner = false;
            return;
        }

        this.productModelModel.CreatedByAccountId = this.accountId;
        this.productModelModel.GroupId = this.model.GroupId;
        this.productModelModel.ManufacturerId = this.model.ManufacturerId;

        this.productService.saveProductModel(this.productModelModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.productModelModel = new ProductModelModel();
                this.getModelsFiltered().then((res) => {
                    this.getFiltersData().then(() => {
                        this.recentlySavedProductModel = response.Data;
                        this.model.ModelId = this.recentlySavedProductModel.Id;
                        this.modelCode = this.recentlySavedProductModel.ShortCode;

                        this.buildProductNumberString();
                    });
                });
            }
        });
    }

    public getModelsFiltered(){
        this.showSpinner = true;
        return new Promise((resolve) =>{
            if(this.model.GroupId === 0 || this.model.ManufacturerId === 0) {
                this.showSpinner = false;
                resolve(true);
                return false;
            }

            const groups = [this.model.GroupId];
            const manufacturers = [this.model.ManufacturerId];
            
            this.productService.getModelsFiltered(groups.toString(), manufacturers.toString())
            .catch((err: any) => {
                this.showSpinner = false;
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response : any) => {
                let data = response.Data;

                if(data != null && data.length > 0)
                {
                    this.productModels = response.Data;
                    this.productGroupList = response.Data;
                    this.productGroupListCount = response.Data.Length;
                }
                else {
                    this.productModels = [];
                    this.productGroupList = [];
                    this.productGroupListCount = 0;
                }
                resolve(true);
            });

        });
    }

    //PHOTOS
    public getProductPhotos(productId: number){
        if (this.isImageRotated === false) {
            this.showPhotoSpinner = true;
        }
        this.productService.getProductPhoto(productId)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showPhotoSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.productPhotoList = response.Data;
            }
        });
    }

    public removePhotoFromList(data: any){
        let index = this.productPhotoList.findIndex(x => x.Id === data.Id);
        this.productPhotoList = this.productPhotoList.filter((x) => data.Id !== x.Id);
        this.productOtherPhotos.splice(index, 1);
    }

    public deleteProductPhoto(productPhoto){
        this.productPhotoList = this.productPhotoList.filter((x) => x.Id !== productPhoto.Id);
        this.showPhotoSpinner = true;
        this.productService.deleteProductPhoto(productPhoto)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showPhotoSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.removeProductPhotoOnDOM(productPhoto.Id);
            }
        });
    }

    public getProductPhotoValues(){
        let elements = this.elem.nativeElement.querySelectorAll('.productPhotos');
        let values = [];
        elements.forEach(function(element, i){
            values.push(element.value);
        });
        if(values.length > 0)
        {
            this.model.ItemNameId = this.model.ItemNameId + ',' + values.join(',');
        }
    }

    public saveChanges(){
        if(!this.isFormInvalid()){
            this.showSpinner = true;
            if (this.model.Price) {
                this.model.Price = this.model.Price.replace('.','').replace(',','.');
            }
            if (this.model.Weight) {
                this.model.Weight = this.model.Weight.replace('.','').replace(',','.');
            }
            if (this.model.KgCo2) {
                this.model.KgCo2 = this.model.KgCo2.replace('.','').replace(',','.');
            }

            this.model.TagIds = this.tagListSelected.length > 0 ? this.tagListSelected.toString() : ""; 


            this.model.AccountProductConditionId = Number(this.model.AccountProductConditionId);
            let imageCopy;
            if(!this.hasMainPhoto && this.mode === 'edit' && this.productPhotoList.length > 0)
            {
                imageCopy = this.productPhotoList[0];
                this.model.MainPhotoUrl = this.productPhotoList[0]["PhotoUrl"];
                this.deleteProductPhoto(this.productPhotoList[0])
            }

            this.productService.save(this.model)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                    this.hasChanges = false;
                }
                else {
                    if(this.model.Id === 0){
                        this.model.CurrentQty = 0;
                        let createdDate = mtimezone.tz(moment(response.Data.CreatedDate).format(), 'Europe/Stockholm').format('MM/DD/YYYY hh:mm:ss a');
                        this.model.CreatedDate = new Date(createdDate);
                        this.model.CreatedByName = response.Data.CreatedByName;
                        this.accountProductQty.loadCurrentQtyData(response.Extras);
                    }
                    if(imageCopy){
                        this.hasMainPhoto = true;
                        this.photoSrc = this.imageUrl(imageCopy['PhotoUrl']);
                    }

                    if(this.productOtherPhotos.length > 0 && this.mode === 'create') {
                        this.saveOtherProductPhotos(response.Extras);
                    }

                    this.mode = 'edit';
                    this.model.ProductNumber = '';
                    this.model.Id = response.Extras;
                    this.model.ProductNumber = response.Data.ProductNumber;
                    this.toastr.success(this.productSaveMessage, "Success");
                    this.hasChanges = true;
                    this.saveProductSuccessCallBack(response);
                }
            });
        }
    }

    public saveProductSuccessCallBack(response: any){
        this.productId = response.Extras;
        let photoElement = this.extractMainPhotoElement();

        if(photoElement.files.length > 0 || this.mainPhotoDndFile || this.otherPhotoDndFile || this.isImageRotated) {
            this.saveProductPhotos(photoElement, true);
        }

        this.setSelectedProduct(this.productId);
    }

    public setSelectedProduct(productId) {
        sessionStorage.setItem("product_grid_selected_row", productId);
    }

    public saveProductPhotos(e, isMain: boolean = false){
        return new Promise((resolve) => {
            if (this.isImageRotated === false) {
                this.showPhotoSpinner = true;
            }
            let selectedFile: File = this.isImageRotated ? this.rotatedImage : (this.mainPhotoDndFile && isMain ? this.mainPhotoDndFile : this.otherPhotoDndFile && !isMain ? this.otherPhotoDndFile : e.files[0]);
            let convertion = Heic2anyHelper.convertHeic(selectedFile);

            if(convertion){
                convertion.then((file => {
                    this.productService.saveProductPhoto(this.productId, this.accountId, isMain, file, this.rotationAngle, this.otherPhotoId)
                    .catch((err: any) => {
                        this.toastr.error(err, 'Error');
                        return Observable.throw(err);
                    }).finally(() => {
                        this.showPhotoSpinner = false;
                    })
                    .subscribe((response) => {
                        let res = JSON.parse(response._body);
                        if(res.ErrorCode) {
                            this.toastr.error(res.Message, "Error");
                        }
                        else {
                            this.newOtherPhoto.Show = false;
                            this.otherPhotoDndFile = null;
                            this.toastr.success(res.Message);
                            this.model.MainPhotoByteString = '';
                            if(isMain && this.model.MainPhotoUrl !== res.Data.PhotoUrl){
                                this.model.MainPhotoUrl = res.Data.PhotoUrl;
                                this.photoSrc = this.imageUrl(res.Data.PhotoUrl);
                            }
                        }
                        resolve(true);
                    });
                }))
                .catch(err => {
                    this.toastr.error(err, "Error");
                });
            } else {
                this.showPhotoSpinner = false;
                this.model.MainPhotoByteString = '';
                this.newOtherPhoto.Show = false;
                resolve(true);
            }
        });
    }

    public saveOtherProductPhotos(id: number){
        return new Promise((resolve) => {
            this.productService.saveOtherProductPhotos(id, this.productOtherPhotos).finally(() => {}).subscribe(() => {
                this.getProductPhotos(id);
                resolve(true);
            });
        });
    }

    public setProductPhotoAsMain(productPhoto){
        this.productService.save(this.model).toPromise().then(() => {
            this.showPhotoSpinner = true;
            this.productService.setMainPhoto(productPhoto)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showPhotoSpinner = false;
            })
            .subscribe((response) => {
                if(response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                }
                else {
                    this.ngOnInit();
                    this.model.MainPhotoByteString = '';
                }

            });
        }).then(() => this.getProductPhotos(this.accountId));
    }

    public removeProductPhotoOnDOM(id){
        let prodPhoto = this.elem.nativeElement.querySelector(`#prod-img-${id}-cont`);
        if (prodPhoto !== null) {
            prodPhoto.remove();
        }
    }

    toDataURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = () => {
            var reader = new FileReader();
            reader.onloadend = function () {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };

        if(xhr){
            xhr.open('GET', url, true);
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            xhr.setRequestHeader('Content-Type', 'application/xml');
            xhr.responseType = 'blob';
            xhr.send();
        }

    }

    public fetchPhotoUrlPreview(e){
        this.photoSizeCls = 'col-sm-2';
        this.isImageRotated = false;
        let reader = new FileReader();

        reader.onload = () => {
            if(this.photoSrc.indexOf(`assets/images`) > -1 ){
                this.hasMainPhoto = !!reader.result;
                this.photoSrc = reader.result.toString().replace("application/octet-stream","image/jpeg");
            }
            else {
                this.productOtherPhotos.push(this.otherPhotoDndFile ? this.otherPhotoDndFile : e.files[e.files.length-1]);
                if(this.mode === 'create'){
                    let data = {
                        "Id": this.tempPhotoCounterId++,
                        "Photo": this.otherPhotoDndFile ? this.otherPhotoDndFile.name : e.files[e.files.length-1].name,
                        "PhotoByteString": reader.result.toString().replace("application/octet-stream","image/jpeg"),
                        "PhotoUrl": "",
                        "ProductId": this.productId,
                        "Show": false
                    };
                    this.productPhotoList.push(data);
                } else{
                    this.saveProductPhotos(e).then(() => this.getProductPhotos(this.productId));
                }
            }
        };

        if(!this.hasMainPhoto){
            this.mainPhotoDndFile = e.dataTransfer ? e.dataTransfer.files[0] : this.mainPhotoDndFile;
        } else {
            this.otherPhotoDndFile = e.dataTransfer ? e.dataTransfer.files[0] : this.otherPhotoDndFile;
        }

        if( (!!e.files && e.files[0] instanceof Blob) || (e.dataTransfer && e.dataTransfer.files[0] instanceof Blob)){
            let file;
            if (e.files !== undefined) {
                file = e.files[0];
            } else {
                file = e.dataTransfer.files[0];
            }

            const convertion = Heic2anyHelper.convertHeic(file);
            if (file.type === '') {
                this.showPhotoSpinner = true;
                convertion.then((data => {
                    reader.readAsDataURL(data);
                    this.showPhotoSpinner = false;
                })).catch(err => {
                    this.toastr.error(err, "Error");
                    this.showPhotoSpinner = false;
                });
            } else {
                reader.readAsDataURL(e.dataTransfer ? e.dataTransfer.files[0] : e.files[e.files.length-1]);
            }
        }
    }

    public processPhotoType(val){
        this.PhotoProcessType = val;
    }

    public getMainPhotoProperties(){
        let mainPhoto = document.getElementById('imagePreview');
        let w = mainPhoto.clientWidth;
        let h = mainPhoto.clientHeight;
        let isPortrait = h > w;
        if(isPortrait) {
            w = 190;
            h = 270;
        }
        else {
            w = 210;
            h = 150;
        }
        return {
            width: w,
            height: h
        };
    }

    public realImgDimension(img) {
        var i = new Image();
        i.src = img.src;
        return {
            width: i.width,
            height: i.height
        };
    }


    //LOCATIONS
    public loadProductLocations(item: any, accountId: number, showAllLocations: boolean){
        this.reloadModalData();

        return new Promise((resolve) => {
            this.showMoveQuantitySpinner = !showAllLocations;
            this.showAddQuantitySpinner = showAllLocations;
            this.locationService
                .getAccountProductLocations(item.Id, accountId, showAllLocations, '')
                .catch((err: any) => {
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showMoveQuantitySpinner = false;
                    this.showAddQuantitySpinner = false;
                })
                .subscribe((response: any) => {
                    let data = response.Data;
                    if(showAllLocations){
                        this.locationOneList = this.addNewProperty(data.accountLocationLevelOne);
                        this.locationTwoList = this.addNewProperty(data.accountLocationLevelTwo);
                        this.locationThreeList = this.addNewProperty(data.accountLocationLevelThree);
                        this.locationFourList = this.addNewProperty(data.accountLocationLevelFour);
                        this.showLevelOne =  this.enableQty = this.hasLocationData = (this.locationOneList.length > 0 || this.accountId === 0);
                    } else{
                        this.mergedLocations = this.mapperService.mergedLocationLevels(data);
                    }

                    resolve(data);
                });
        });
    }

    public loadAccountLocationLevelOneDstList(accountId: number){
        return new Promise((resolve) => {
            //clear dropdown data
            this.applyQtyDstTo = 1;
            this.accountProductDstModel.LocationLevelOneId = 0;
            this.accountProductDstModel.LocationLevelTwoId = 0;
            this.accountProductDstModel.LocationLevelThreeId = 0;
            this.accountProductDstModel.LocationLevelFourId = 0;
            this.locationLevelTwoDstList = [];
            this.locationLevelThreeDstList = [];
            this.locationLevelFourDstList = [];

            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelOne(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    this.locationLevelOneDstList = data;
                    this.accountProductDstModel.LocationLevelOneId = this.locationLevelOneDstList[0].Id;
                    this.selectLevelOneDst(this.accountProductDstModel.LocationLevelOneId);
                }
                else {
                    this.accountProductDstModel.LocationLevelOneId = 0;
                }
                resolve(data);
            });
        });
    }

    public loadAccountLocationLevelTwoDstList(accountId: number, levelOneId?:number){
        return new Promise((resolve) => {
            //clear dropdown data
            this.applyQtyDstTo = 1;
            this.accountProductDstModel.LocationLevelTwoId = 0;
            this.accountProductDstModel.LocationLevelThreeId = 0;
            this.accountProductDstModel.LocationLevelFourId = 0;
            this.locationLevelThreeDstList = [];
            this.locationLevelFourDstList = [];

            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelTwo(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;

            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    if(levelOneId> 0) {
                        let locationLevelTwoFilteredList = data.filter((item) => {
                            return item.AccountLocationLevelOneId === levelOneId;
                        });
                        if(locationLevelTwoFilteredList.length > 0){
                            this.applyQtyDstTo = 2;
                            this.locationLevelTwoDstList = locationLevelTwoFilteredList;
                            this.accountProductDstModel.LocationLevelTwoId = locationLevelTwoFilteredList[0].Id;
                            this.selectLevelTwoDst(this.accountProductDstModel.LocationLevelTwoId);
                        }
                        else {
                            this.accountProductDstModel.LocationLevelTwoId = 0;
                        }
                    }
                    else {
                        this.locationLevelTwoDstList = data;
                        //this.applyQtyDstTo = 2;
                    }
                }
                else {
                    this.accountProductDstModel.LocationLevelTwoId = 0;
                }
                resolve(data);
            });
        });
    }

    public loadAccountLocationLevelThreeDstList(accountId: number, levelTwoId?:number){
        return new Promise((resolve) =>{
            //clear dropdown data
            this.applyQtyDstTo = 1;
            this.accountProductDstModel.LocationLevelThreeId = 0;
            this.locationLevelThreeDstList = [];

            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelThree(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                    this.showSpinner = false;
            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    if(levelTwoId> 0) {
                        let locationLevelThreeFilteredList = data.filter((item) => {
                            return item.AccountLocationLevelTwoId === levelTwoId;
                        });

                        if(locationLevelThreeFilteredList.length > 0){
                            this.applyQtyDstTo = 3;
                            this.locationLevelThreeDstList = locationLevelThreeFilteredList;
                            this.accountProductDstModel.LocationLevelThreeId = locationLevelThreeFilteredList[0].Id;
                            this.selectLevelThreeDst(this.accountProductDstModel.LocationLevelThreeId);
                        }
                        else {
                            this.accountProductDstModel.LocationLevelThreeId = 0;
                        }
                    }
                    else {
                        //this.applyQtyDstTo = 3;
                        this.locationLevelThreeDstList = data;
                    }
                }
                else {
                    this.accountProductDstModel.LocationLevelThreeId = 0;
                }
                resolve(data);
            });
        });
    }

    public loadAccountLocationLevelFourDstList(accountId: number, levelThreeId?:number){
        return new Promise((resolve) =>{
            //clear dropdown data
            this.applyQtyDstTo = 1;
            this.accountProductDstModel.LocationLevelFourId = 0;
            this.locationLevelFourDstList = [];

            this.showSpinner = true;
            this.locationService.getAccountLocationsLevelThree(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                    this.showSpinner = false;
            })
            .subscribe((response : any) => {
                let data = response.Data;
                if(data.length > 0)
                {
                    if(levelThreeId> 0) {
                        let locationLevelFourFilteredList = data.filter((item) => {
                            return item.AccountLocationLevelThreeId === levelThreeId;
                        });

                        if(locationLevelFourFilteredList.length > 0){
                            this.applyQtyDstTo = 4;
                            this.locationLevelFourDstList = locationLevelFourFilteredList;
                            this.accountProductDstModel.LocationLevelFourId = locationLevelFourFilteredList[0].Id;
                        }
                        else {
                            this.accountProductDstModel.LocationLevelFourId = 0;
                        }
                    }
                    else {
                        //this.applyQtyDstTo = 3;
                        this.locationLevelFourDstList = data;
                    }
                }
                else {
                    this.accountProductDstModel.LocationLevelFourId = 0;
                }
                resolve(data);
            });
        });
    }

    public selectLevelOneDst(id){
        this.onLevelOneDstChange(id);
    }

    public selectLevelTwoDst(id){
        this.onLevelTwoDstChange(id);
    }

    public selectLevelThreeDst(id){
        this.onLevelThreeDstChange(id);
    }

    public onLevelOneDstChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelTwoDstList(this.accountId, value);
        }
    }

    public onLevelTwoDstChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelThreeDstList(this.accountId, value);
        }
    }

    public onLevelThreeDstChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelFourDstList(this.accountId, value);
        }
    }

    public confirmProductAdd(productFormContent: string, item: any, mode? :number) {
        if (this.accountProductModel.LocationLevelOneId > 0) {
            this.applyQtyTo = 1;

            if (this.accountProductModel.LocationLevelTwoId > 0) {
                this.applyQtyTo = 2;

                if (this.accountProductModel.LocationLevelThreeId > 0) {
                    this.applyQtyTo = 3;

                    if (this.accountProductModel.LocationLevelFourId > 0) {
                        this.applyQtyTo = 4;
                    }
                }
            }
        }

        this.hideDestination = true;
        this.hideDestionationSecond = true;
        this.allLocations = [];
        this.mergedLocations = [];
        this.availableQtyTobeMoved = 0;

        this.loadProductLocations(item, this.accountId === 0 ? super.currentAccountId() : this.accountId, this.accountId === 0 ? true : mode === 1).then(() => {
            this.loadProductLocations(item, this.accountId === 0 ? super.currentAccountId() : this.accountId, true ).then((data) => {
                this.allLocations = this.mapperService.mergedLocationLevels(data);
                this.allLocationsCopy = this.allLocations;
            });
        });

        this.accountProductModel = new AccountProductModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();


        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.AccountId = this.accountId;
        this.accountProductModel.Description = item.Description;
        this.accountProductModel.Location = item.Location;
        this.accountProductModel.Price = item.Price;

        this.productQtyLogModel.Mode = mode;
        this.productQtyLogModel.AccountProductId = this.accountId;
        this.productQtyLogModel.Qty = this.accountProductModel.QuantityOnStock;

        if(item.AccountProductId > 0){
            this.confirmPopupMessage = 'Please indicate the quantity you will be adding up.';
            this.confirmIconClass ='fa fa-plus fa-lg success';
            this.qtyLabel = 'New Quantity';
        }
        else {
            this.confirmPopupMessage = 'Are you sure you want to add this product in your database?';
            this.qtyLabel = 'Initial Quantity';
        }
        this.modalService.open(productFormContent, mode === 2 ? { windowClass: "fit-content"} : { size: 'lg' });
    }

    public moveProductQty(productFormContent: string, item: any, mode? :number) {
        this.confirmProductAdd(productFormContent, item, 2);

        this.accountProductDstModel = new AccountProductModel();
        this.accountProductDstModel.ProductId = item.Id;
        this.accountProductDstModel.AccountId = this.accountId;
        this.accountProductDstModel.Description = item.Description;
        this.accountProductDstModel.Location = item.Location;
        this.accountProductDstModel.Price = item.Price;

        this.productQtyLogDstModel = new AccountProductQtyLogModel();
        this.productQtyLogDstModel.Mode = mode;
        this.productQtyLogDstModel.AccountProductId = this.accountId;
        this.productQtyLogDstModel.Qty = 0;

        this.applyQtyDstTo = 1;
    }

    public saveProductToAccount(){
        this.productQtyLogModel.Qty = this.accountProductModel.QuantityOnStock;
        this.productQtyLogModel.IsMobile = false;
        this.productQtyLogModel.UpdatedBy = super.currentLoggedUser().id;
        this.accountProductModel.AccountId = this.currentUser.accountId;
        let newMergedProductModel = Object.assign({
            ProductQtyLogModel : this.productQtyLogModel,
            AvailableDate: this.accountProductModel.AvailableDate,
            AppliedTo: this.applyQtyTo === 1 ? this.accountProductModel.LocationLevelOneId : (this.applyQtyTo === 2 ? this.accountProductModel.LocationLevelTwoId : (this.applyQtyTo === 3 ? this.accountProductModel.LocationLevelThreeId : this.accountProductModel.LocationLevelFourId))
        }, this.accountProductModel);
        this.productService.saveToAccount(newMergedProductModel)
        .catch((err: any) => {
            this.showSpinner = false;
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.showSpinner = false;
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.toastr.success('Product Added to your database', 'Success')
                this.ngOnInit();
            }
        });
    }

    public onMoveQuantityValueChange(){
        this.moveQtyExceed = this.accountProductModel.QuantityOnStock && this.accountProductModel.QuantityOnStock > 0 && this.accountProductModel.QuantityOnStock > this.availableQtyTobeMoved;
    }

    public moveProductToAccount(dialog: any) {
        this.productQtyLogModel.Qty = this.accountProductModel.QuantityOnStock;
        this.productQtyLogModel.Mode = 2;
        this.productQtyLogModel.IsMobile = false;
        this.productQtyLogModel.UpdatedBy = super.currentLoggedUser().id;
        this.productQtyLogDstModel.Qty = this.accountProductModel.QuantityOnStock;
        this.accountProductModel.QuantityOnStock = this.availableQtyTobeMoved;
        
        let srcId = this.applyQtyTo === 1 ? this.accountProductModel.LocationLevelOneId : (this.applyQtyTo === 2 ? this.accountProductModel.LocationLevelTwoId : (this.applyQtyTo === 3 ? this.accountProductModel.LocationLevelThreeId : this.accountProductModel.LocationLevelFourId));
        let srcMergedProductModel = Object.assign({
            ProductQtyLogModel : this.productQtyLogModel,
            AppliedTo: srcId
        }, this.accountProductModel);

        this.productQtyLogDstModel.IsMobile = false;
        this.productQtyLogDstModel.UpdatedBy = super.currentLoggedUser().id;
        var dstId = this.applyQtyDstTo === 1 ? this.accountProductDstModel.LocationLevelOneId : (this.applyQtyDstTo === 2 ? this.accountProductDstModel.LocationLevelTwoId : (this.applyQtyTo === 3 ? this.accountProductDstModel.LocationLevelThreeId : this.accountProductDstModel.LocationLevelFourId));
        let dstMergedProductModel = Object.assign({
            ProductQtyLogModel : this.productQtyLogDstModel,
            AppliedTo: dstId,
            QuantityOnStock: this.accountProductModel.QuantityOnStock
        }, this.accountProductDstModel);

        if (this.availableQtyTobeMoved < this.productQtyLogDstModel.Qty) {
            this.toastr.error('Input value not allowed, should not exceed the available qty stock', 'Not Allowed');
            return;
        }

        dialog('Close click');

        this.showSpinner = true;

        this.productService.saveToAccount(srcMergedProductModel)
        .catch(err => {
            this.showSpinner = false;
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        })
        .subscribe(res => {
            if(res.StatusCode === 409) {
                this.toastr.error(res.Message, "Conflict");
            } 
            else if (res.ErrorCode){
                this.toastr.error(res.Message, "Error");
            }
            else {
                this.productService.saveToAccount(dstMergedProductModel).subscribe(() => {
                    this.toastr.success('Product Added to your database', 'Success')
                    this.uncheckMoveQuantityLocations(this.mergedLocations);
                    this.uncheckMoveQuantityLocations(this.allLocations);
                    this.confirmProductAdd(this.productTransferQtyFormContent, this.model, 2);
                    // this.ngOnInit();
                })
            }
            this.showSpinner = false;
        })
    }

    public validateValue(val, maxval){
        if(Number(val) > maxval && this.productQtyLogModel.Mode === -1)
        {
            //this.accountProductModel.QuantityOnStock = maxval;
            this.qtyAllowed = false;
            this.toastr.error('Input value not allowed, should not exceed the current qty stock', 'Not Allowed');
            return false;
        }
    }

    public exportToPDF(){
        this.productService.print(this.productId, this.accountId.toString()).toPromise().then((response) => {
            this.showSpinner = false;
            let url = window.URL.createObjectURL(response);
            let a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.href = url;
            a.download = "Product Catalog";
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove();
        }, (err => {
            this.showSpinner = false;
        }))
    }

    //@TODO : Transfer to base
    public canEditValue(accountId?: number): boolean{
        if(super.isadmin() || super.isEditor())
        {
            return accountId === Number(super.currentAccountId()) || this.mode === 'create';
        }
        else if(super.issuperadmin())
        {
            this.isSuperAdmin = true;
            return true;
        }
        return false;
    }

    public isFormInvalid(): boolean {
        let isInvalid =
        this.model.GroupId == 0 ||
        this.model.ColorMatId == 0 ||
        this.model.ManufacturerId == 0 ||
        this.model.ProductNumber === '' ||
        this.model.ProductName === '' ||
        (!this.model.Height || this.model.Height === 0) ||
        (!this.model.Width || this.model.Width === 0) ||
        (!this.model.Length || this.model.Length === 0);

        return isInvalid;
    }

    public onPriceChange(e){
        if(!isNumeric(e)){
            e.target.value = e.target.value.replace(/[^0-9+,.]/g,'');
        }
    }

    public onPriceLeave(e){
        let arrPrice = e.target.value.split(',');

        if(e.target.value !== '' && e.target.value.indexOf(',') === -1){
            e.target.value += ',00';
        }
        else{
            if(Number(arrPrice[0]) === 0){
                e.target.value = '0';
            }
        }

        this.price.nativeElement.blur();
    }

    public onPriceEnter(e){
        let arrPrice = e.target.value.split(',');

        if(Number(arrPrice[0]) === 0){
            e.target.value = '';
        }

        this.price.nativeElement.focus();
    }

    public onHeightWidthDepthChange() : number{
        if(this.model.Height > 0 && this.model.Width > 0 && this.model.Length > 0){
            this.volume = Number(((this.model.Length / 100) * (this.model.Width / 100) * (this.model.Height / 100)).toFixed(2));
        }
        else{
            this.volume = 0;
        }
        return this.volume;
    }

    public imageUrl(url){
        return super.imageUrl(url);
    }

    public onWeightChange(e){
        if(!isNumeric(e)){
            e.target.value = e.target.value.replace(/[^0-9+,.]/g,'');
        }
    }

    public onWeightLeave(e){
        let arrWeight = e.target.value.split(',');

        if(e.target.value !== '' && e.target.value.indexOf(',') === -1){
            e.target.value += ',00';
        }
        else{
            if(Number(arrWeight[0]) === 0){
                e.target.value = '0';
            }
        }

        this.weight.nativeElement.blur();
    }

    public onWeightEnter(e){
        let arrWeight = e.target.value.split(',');

        if(Number(arrWeight[0]) === 0){
            e.target.value = '';
        }

        this.weight.nativeElement.focus();
    }

    public findLocationQty(level: number, parentId: string, id: string) {
        var accountProduct = <any>[];
        accountProduct = this.qtyModuleData;
        let item = null;

        if (!accountProduct) {
            return 0;
        }
        switch(level) {
            case 1: {
                accountProduct.forEach((elem,i) =>{
                    var locationLevelOne = elem.LocationLevelOne;
                    locationLevelOne.forEach(element => {
                        if (element.Id == id && element.ParentId == parentId) {
                            item = element;
                        }
                    });
                })
            } break;
            case 2: {
                accountProduct.forEach((elem,i) =>{
                    var locationLevelTwo = elem.LocationLevelTwo;
                    locationLevelTwo.forEach(element => {
                        if (element.Id == id && element.ParentId == parentId) {
                            item = element;
                        }
                    });
                })
            } break;
            case 3: {
                accountProduct.forEach((elem,i) =>{
                    var locationLevelThree = elem.LocationLevelThree;
                    locationLevelThree.forEach(element => {
                        if (element.Id == id && element.ParentId == parentId) {
                            item = element;
                        }
                    });
                })
            } break;
            case 4: {
                accountProduct.forEach((elem,i) =>{
                    var locationLevelFour = elem.locationLevelFour;
                    locationLevelFour.forEach(element => {
                        if (element.Id == id && element.ParentId == parentId) {
                            item = element;
                        }
                    });
                })
            } break;
        }
        return item ? item.Qty : 0;
    }

    public canPreview(url) {
        return url.indexOf('/assets/images/upload-empty.png') <= -1;
    }
    public openImage(template, url, index = -1, photoId = -1) {
        this.isImageRotated = false;
        this.rotationAngle = 0;
        this.otherPhotoIndex = index;
        this.otherPhotoId = 0;
        this.rotatedImage = null;

        if (!this.canPreview(url)) {
            return;
        }

        if(index > -1){
            this.otherPhotoId = photoId;
        }

        this.focusPhotoSrc = (url.indexOf('image/jpeg;base64') > -1) ? url :  this.imageUrl(url);

        this.modalService.open(template, { windowClass: "fit-content"});
    }

    closeImage() {
        this.otherPhotoId = 0;
        this.rotationAngle = 0;
        this.rotatedImage = null;
    }

    rotateImage(angle){
        this.isImageRotated = true;
        const src = this.focusPhotoSrc;
        var image = this.otherPhotoIndex < 0 ? document.getElementById('imagePreview') : document.getElementById('otherImagePreview_' + this.otherPhotoIndex);

        let promise = new Promise((resolve) =>{
            this.toDataURL(src, function(dataUrl){
                const byteCharacters = atob(dataUrl.split(',')[1]);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                const byteArray = new Uint8Array(byteNumbers);
                const blob = new Blob([byteArray], {type: "image/jpeg"});
                blob['lastModifiedDate'] = new Date();
                var split = src.split('/');
                blob['name'] = split[split.length - 1];
                resolve(blob);
            });
        });

        this.rotationAngle += angle;
        document.getElementById('modalPreviewImage').style.transform = `rotate(${this.rotationAngle}deg)`;

        promise.then((data) => {
            this.rotatedImage = !this.rotatedImage ? data : this.rotatedImage;
            this.saveProductPhotos(image, this.otherPhotoIndex === -1).then(() => {
                this.getProductPhotos(this.productId);
                this.isImageRotated = false;
            });
        });
    }

    public removeMainPhoto() {
        this.photoSrc = this.imageUrl(null);
        this.model.MainPhotoUrl = null;
    }

    public changeToSystemProduct(product){

        this.showSpinner = true;
        this.productService.changeToSystemProduct(product.Id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            this.toastr.success('Successfully changed to System Product.','Success!');
            product.AccountId = 0;

        });
    }

    public confirmChangeSystemProduct(){
        this.modalService.open(this.confirmChangeSystemProductContent, { size : 'sm' });
    }

    public onLevelOneClick({target: {value}}): void{
        let parent = Number(value);
        this.locationOneList = this.clearSelection(parent,  this.locationOneList);
        this.clearSelection(0, this.locationTwoList, true);
        this.clearSelection(0, this.locationThreeList, true);
        this.clearSelection(0, this.locationFourList, true);
        this.accountProductModel.LocationLevelOneId = this.accountProductModel.LocationLevelTwoId = this.accountProductModel.LocationLevelThreeId = this.accountProductModel.LocationLevelFourId = 0;
        this.applyQtyTo =1;
        this.showLevelTwo = false;
        this.showLevelThree = false;
        this.showLevelFour = false;

        if(parent === this.accountProductModel.LocationLevelOneId){
            let index = this.locationOneList.findIndex(x => x.Id == parent);
            this.locationOneList[index].isSelected = false;

            return;
        }
        else{
            this.showLevelTwo = true;
            this.accountProductModel.LocationLevelOneId = parent;
        }

        this.filteredLocationTwoList = this.locationTwoList.filter(x => x.AccountLocationLevelOneId == parent);
        this.showLevelTwo = this.filteredLocationTwoList.length > 0;
    }

    public onLevelTwoClick({target: {value}}): void{
        let parent = Number(value);
        this.locationTwoList = this.clearSelection(parent,  this.locationTwoList);
        this.clearSelection(0, this.locationThreeList, true);
        this.clearSelection(0, this.locationFourList, true);
        this.accountProductModel.LocationLevelTwoId = this.accountProductModel.LocationLevelThreeId = this.accountProductModel.LocationLevelFourId = 0;
        this.applyQtyTo = 2;
        this.showLevelThree = false;
        this.showLevelFour = false;

        if(parent === this.accountProductModel.LocationLevelTwoId){
            let index = this.filteredLocationTwoList.findIndex(x => x.Id == parent);
            this.filteredLocationTwoList[index].isSelected = false;
            this.applyQtyTo = 1;
            return;
        }
        else{
            this.showLevelThree = true;
            this.accountProductModel.LocationLevelTwoId = parent;

        }

        this.filteredLocationThreeList = this.locationThreeList.filter(x => x.AccountLocationLevelTwoId == parent);
        this.showLevelThree = this.filteredLocationThreeList.length > 0;
    }

    public onLevelThreeClick({target: {value}}): void{
        let parent = Number(value);
        this.locationThreeList = this.clearSelection(parent,  this.locationThreeList);
        this.clearSelection(0, this.locationFourList, true);
        this.accountProductModel.LocationLevelThreeId = this.accountProductModel.LocationLevelFourId = 0;
        this.applyQtyTo = 3;
        this.showLevelFour = false;

        if(parent === this.accountProductModel.LocationLevelThreeId){
            let index = this.filteredLocationThreeList.findIndex(x => x.Id == parent);
            this.filteredLocationThreeList[index].isSelected = false;
            this.accountProductModel.LocationLevelThreeId = 0;
            this.applyQtyTo = 2;
            return;
        }
        else{
            this.showLevelFour = true;
            this.accountProductModel.LocationLevelThreeId = parent;

        }

        this.filteredLocationFourList = this.locationFourList.filter(x => x.AccountLocationLevelThreeId == parent);
        this.showLevelFour = this.filteredLocationFourList.length > 0;
    }

    public onLevelFourClick({ target: { value } }): void {
        let parent = Number(value);
        this.locationFourList = this.clearSelection(parent, this.locationFourList);
        this.accountProductModel.LocationLevelFourId = 0;
        this.applyQtyTo = 4;

        if(parent === this.accountProductModel.LocationLevelFourId){
            let index = this.filteredLocationFourList.findIndex(x => x.Id == parent);
            this.filteredLocationFourList[index].isSelected = false;
            this.applyQtyTo = 3;

            return;
        }

        this.accountProductModel.LocationLevelFourId = parent;
    }

    private  clearSelection(id: number, data: any, clearAll: boolean = false): any{
        data.forEach(element => {
            if(!clearAll)
                element.isSelected = id === element.Id;
            else
                element.isSelected = false;
        });

        return data;
    }

    private addNewProperty(data: any) : any{
        data.map((x) => {
            x['isSelected'] = false;
        });

        return data;
    }


    public onSourceLocationClick(data): void 
    {
        let locationOneId = data.location[0].Id;
        let locationTwoId  = data.location[1].Id;
        let locationThreeId = data.location[2].Id;
        let locationFourId = data.location[3].Id;

        this.allLocations = [...this.allLocationsCopy];
        let indexToRemoved = this.allLocations.find(x => x.location[0].Id === locationOneId &&
            x.location[1].Id === locationTwoId &&
            x.location[2].Id === locationThreeId &&
            x.location[3].Id === locationFourId);

        if(indexToRemoved.location !== undefined){
            this.allLocations = this.allLocations.filter(arr => !this.arraysOfObjectsAreEqual(arr.location, indexToRemoved.location));
        }

        this.accountProductModel.LocationLevelOneId = locationOneId > 0 ? locationOneId : null;
        this.accountProductModel.LocationLevelTwoId = locationTwoId > 0 ? locationTwoId : null;
        this.accountProductModel.LocationLevelThreeId = locationThreeId > 0 ? locationThreeId : null;
        this.accountProductModel.LocationLevelFourId = locationFourId > 0 ? locationFourId : null;
        
        let item = this.mergedLocations.find(x => x.location[0].isSelected === true);

        if(item){
            item.location[0].isSelected = false;
        }

        this.applyQtyTo = locationFourId > 0 ? 4 : locationThreeId > 0 ? 3 : locationTwoId > 0 ? 2 : 1;

        data.location[0].isSelected = true;

        this.availableQtyTobeMoved = this.applyQtyTo === 4 ? data.location[3].TotalQty :
                            this.applyQtyTo === 3 ?  data.location[2].TotalQty :
                            this.applyQtyTo === 2 ? data.location[1].TotalQty :
                            data.location[0].TotalQty;

        this.hideDestination = false;

    }

    arraysOfObjectsAreEqual(arr1, arr2) {
        if (arr1.length !== arr2.length) {
            return false;
        }
        return arr1.every(obj1 => arr2.some(obj2 => this.objectsAreEqual(obj1, obj2)));
    }
    
    objectsAreEqual(obj1, obj2) {
        return obj1.Id === obj2.Id && obj1.Name === obj2.Name;
    }

    public onDestinationLocationClick(data): void 
    {
        let locationOneId = data.location[0].Id;
        let locationTwoId  = data.location.length >= 2 ? data.location[1].Id : 0;
        let locationThreeId = data.location.length >= 3 ? data.location[2].Id : 0;
        let locationFourId = data.location.length >= 4 ? data.location[3].Id : 0;

        this.accountProductDstModel.LocationLevelOneId = locationOneId > 0 ? locationOneId : null;
        this.accountProductDstModel.LocationLevelTwoId = locationTwoId > 0 ? locationTwoId : null;
        this.accountProductDstModel.LocationLevelThreeId = locationThreeId > 0 ? locationThreeId : null;
        this.accountProductDstModel.LocationLevelFourId = locationFourId > 0 ? locationFourId : null;

        this.applyQtyDstTo = locationFourId > 0 ? 4 : locationThreeId > 0 ? 3 : locationTwoId > 0 ? 2 : 1;
        let item = this.allLocations.find(x => x.location[0].isSelected === true);
        if(item){
            item.location[0].isSelected = false;
        }
        data.location[0].isSelected = true;
    }

    uncheckMoveQuantityLocations(data): any{
        return data.map(x => {
            x.location[0].isSelected = false;
        });
    }

    private reloadModalData(): void{
        this.showLevelOne = this.showLevelTwo = this.showLevelThree = this.showLevelFour = this.enableQty = false;
        this.hasLocationData = true;
        this.accountProductModel.LocationLevelOneId = this.accountProductModel.LocationLevelTwoId = this.accountProductModel.LocationLevelThreeId =
        this.accountProductModel.LocationLevelFourId = this.accountProductModel.QuantityOnStock = this.applyQtyTo = 0;
        this.clearSelection(0, this.locationOneList, true);
        this.clearSelection(0, this.locationTwoList, true);
        this.clearSelection(0, this.locationThreeList, true);
        this.clearSelection(0, this.locationFourList, true);
    }

    public disableAddQtySaving() : boolean{
        if(this.showLevelOne && !this.showLevelTwo && !this.showLevelThree && !this.showLevelFour){
            return this.showAddQuantitySpinner
                || !this.accountProductModel.QuantityOnStock
                || this.accountProductModel.QuantityOnStock === 0
                || this.accountProductModel.LocationLevelOneId === 0
        }
        if(this.showLevelOne && this.showLevelTwo && !this.showLevelThree && !this.showLevelFour){
            return this.showAddQuantitySpinner
                || !this.accountProductModel.QuantityOnStock
                || this.accountProductModel.QuantityOnStock === 0
                || this.accountProductModel.LocationLevelOneId === 0
                || this.accountProductModel.LocationLevelTwoId === 0
        }
        if(this.showLevelOne && this.showLevelTwo && this.showLevelThree && !this.showLevelFour){
            return this.showAddQuantitySpinner
                || !this.accountProductModel.QuantityOnStock
                || this.accountProductModel.QuantityOnStock === 0
                || this.accountProductModel.LocationLevelOneId === 0
                || this.accountProductModel.LocationLevelTwoId === 0
                || this.accountProductModel.LocationLevelThreeId === 0
        }
        if(this.showLevelOne && this.showLevelTwo && this.showLevelThree && this.showLevelFour){
            return this.showAddQuantitySpinner
                || !this.accountProductModel.QuantityOnStock
                || this.accountProductModel.QuantityOnStock === 0
                || this.accountProductModel.LocationLevelOneId === 0
                || this.accountProductModel.LocationLevelTwoId === 0
                || this.accountProductModel.LocationLevelThreeId === 0
                || this.accountProductModel.LocationLevelFourId === 0
        }
    }

    public disableMoveQtySaving() : boolean{
        return this.showMoveQuantitySpinner
            || !this.accountProductModel.QuantityOnStock
            || this.accountProductModel.QuantityOnStock === 0
            || this.accountProductModel.LocationLevelOneId === 0
            || this.accountProductDstModel.LocationLevelOneId === 0
            || this.moveQtyExceed;
    }

    public onKgCO2Change(e){
        if(!isNumeric(e)){
            e.target.value = e.target.value.replace(/[^0-9+,.]/g,'');
        }
    }

    public onKgCO2Leave(e){
        let arrPrice = e.target.value.split(',');

        if(e.target.value !== '' && e.target.value.indexOf(',') === -1){
            e.target.value += ',0';
        }
        else{
            if(Number(arrPrice[0]) === 0){
                e.target.value = '0';
            }
        }

        this.kgCO2.nativeElement.blur();
    }

    public onKgCO2Enter(e){
        let arrPrice = e.target.value.split(',');

        if(Number(arrPrice[0]) === 0){
            e.target.value = '';
        }

        this.kgCO2.nativeElement.focus();
    }

    getItemQuantity(item: any) {
        const quantity = item.location.length >= 4 && item.location[3].Id > 0 ? item.location[3].TotalQty :
        item.location.length >= 3 && item.location[2].Id > 0 ? item.location[2].TotalQty :
        item.location.length >= 2 && item.location[1].Id > 0 ? item.location[1].TotalQty :
        item.location.length >= 1 && item.location[0].Id > 0 ? item.location[0].TotalQty : 0;

        return quantity;
    }

    showAdvanceLogs() {
        this.isLogsClicked = !this.isLogsClicked;
        this.btnLogsLabel = (this.isLogsClicked ? 'Hide' : 'Show')
    }

    saveTag(){
        const isAdd = this.tagModel.Id == 0 ? true : false;
        this.tagModel.AccountId = super.issuperadmin() ? 0 : super.currentAccountId();
        this.tagService.saveTag(this.tagModel)
         .catch((err: any) => {
            return Observable.throw(err);
        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
              this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.toastr.success(response.Message, "Success");
                this.resetTagModel();
                this.getTags();

                if (isAdd) {
                    this.addTag({
                        Id: response.Data.Id
                    });
                }
            }
        });
    }

    resetTagModel() {
        this.tagModel = new TagModel();
    }

    isTagNameValid() {
        return this.tagModel.Name.trim().length > 0;
    }

    productTagDeleteConfirm(content: string, item: any){
        this.tagModel = item;
        this.tagModel.IsDeleted = true;
        this.modalService.open(content, { size: 'sm'});
    }

    deleteProductTag() {
        this.tagModel.AccountId = super.issuperadmin() ? 0 : super.currentAccountId();
        this.tagService.saveTag(this.tagModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.resetTagModel();
                this.getTags();
            }
        });
    }

    editProductTag(item: any){
        this.tagModel = item;
    }

    filterTagList(value) {
        if (value.trim().length > 0) {
            this.completeTagList = this.completeTagList.filter(e => e.Name.includes(value));debugger
            return this.completeTagList;
        }
        else {
            this.completeTagList = this.completeListCopy;
        }
    }

    formatSwedishDate(date: Date) {
        return this.datePipe.transform(date, 'yyyy-MM-dd');
    }

}
