import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AuthenticationService } from '../../services';
@Component({
    selector: 'app-forgot-credentials-component',
    templateUrl: './forgot-credentials.component.html',
    styleUrls: ['../login/login.component.scss', '../layout/layout.component.scss']
})
export class ForgotCredentialsComponentComponent  implements OnInit { 
    public showSpinner: boolean = false;
    public model: any = {};
    public status: string = '';
    
    constructor(private authService: AuthenticationService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,){}

    public ngOnInit(){
        this.model = {
            UserName : '',
            EmailAddress: ''
        }
    }
    public onSubmitForgotPassword(){
        this.showSpinner = true;
        this.authService.forgotPassword(this.model)
            .catch((err: any) => {
                console.log(err);
                this.toastr.error(err.Message, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.Success){
                    this.toastr.success(response.Message, 'Email Sent');
                    this.router.navigate(['login']);
                }
                else {
                    this.status = 'Account does not exist!'
                }
            });
    }
}