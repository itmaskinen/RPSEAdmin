import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { IPagedResults } from '../core/interfaces';

@Injectable()
export class AccountService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getAccountsList(page:number, pageSize: number = 10, filter: any): Observable<IPagedResults<any>>{
        let url = `Account/GetAll`;    
        return super.getAll<any>(url);
    }

    public getAccountsWithAssocList(id: number): Observable<IPagedResults<any>>{
        let url = `Account/GetAllWithAssocById?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getAccountsWithAssocDropdown(id: number){
        let url = `Account/GetAssociatedAccountsByAccountId?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getAccount(id: number){
        let url = `Account/Get?id=${id}`;    
        return super.getById<any>(url);
    }

    public getAccountV3(id: number){
        let url = `V3/Account/Get?id=${id}`;    
        return super.getById<any>(url);
    }

    public saveAccount(data){
        let url = `Account/Save`;
        return super.save<any>(url,data);
    }

    public saveAccountV3(data){
        let url = `V3/Account/Save`;
        return super.save<any>(url,data);
    }

    public deleteAccount(data){
        let url = `Account/Delete`;
        return super.save<any>(url,data);
    }

    public getAccountIsDuplicateByName(companyName: string){
        let url = `Account/IsDuplicate?companyName=${companyName}`;    
        return super.getAll<any>(url);
    }

    public getCompanyTypesList() {
        let url = `Common/GetAllCompanyTypes`;    
        return super.getAll<any>(url);
    }

    public saveAccountAssoc(data){
        let url = `Account/SaveAccountAssociation`;
        return super.save<any>(url,data);
    }

    public deleteAccountAssoc(data){
        let url = `Account/DeleteAccoutnAssociation`;
        return super.save<any>(url,data);
    }
}
