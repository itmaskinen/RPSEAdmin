import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ProductService } from '../../services';
import { ToastsManager } from '../../../../node_modules/ng2-toastr';

import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
    selector: 'app-products-locations',
    templateUrl: './products-locations.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ProductsLocationsComponent implements OnInit { 
    public showSpinner: boolean = false;
    public productLocation: any;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private route: ActivatedRoute
    ){}

    public ngOnInit(){
        this.route.params.subscribe((params) => {
            const id = params['id']; // product id
            const accountId = params['accountId']; //account id
            if(accountId && id){
                this.loadData(accountId, id);
            }
        });
    }

    public loadData(accountId: number, productId: number){
        this.productService.getAccountProducts(accountId, productId)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            if(data){
                this.productLocation = data[0];
            }
        });
    }
}