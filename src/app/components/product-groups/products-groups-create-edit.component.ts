import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';
import { ProductService } from '../../services';
import { ProductGroupModel } from '../../models/productGroupModel';

import * as $ from 'jquery';
@Component({
    selector: 'app-product-group-create-edit',
    templateUrl: './products-groups-create-edit.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ProductsGroupsCreateEditComponent extends BaseComponent implements OnInit {  
    model: any;
    accountId: 0;
    defaultPhotoSrc: string ="";
    title: string = 'Create a New Product Group';
    mode: string = 'create';
    currentDate: Date;
    defaultImageSrc: string = '/assets/images/add_photo_image.png'
    showSpinner = false;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() { 
        this.currentDate = new Date();
            this.model = new ProductGroupModel();
            this.mode = 'create';
            this.route.params.subscribe((params) => {
            const id = params['id']; // id
            const accountId = params['accountId']; //account id
            if (id)
            {
                this.loadData(Number(id));
                this.title =  'Edit Product Group';
                this.accountId = accountId;
                this.mode = 'edit';
               
            }
            });
            $('#group-name').focus();
        }

    public loadData(id: number){
            this.showSpinner = true;
            this.productService.getProductGroup(id)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
             
                this.model = response.Data;
            });
        }
    public getImage(){
       
        return super.imageUrl(this.model.PhotoUrl +'?date='+ this.currentDate.toString());
    }

    public saveChanges(){
            this.showSpinner = true;
 
            this.model.AccountId = this.accountId;
            this.productService.saveProductGroup(this.model)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.ErrorCode) {
                    this.toastr.error(response.Message, "Error"); 
                }
                else {
                    this.toastr.success(`Successfully ${this.mode == 'create' ? 'added' : 'updated'} product group.`,'Success!');
                    this.router.navigate([`product-groups`]);
                }
            });
        }

    public fileChange(fileInput: any){
    
        let self = this;
        let file = <File>fileInput.target.files[0];
            if(file.size > 2000000)
        {
           return false;
        }
        var reader = new FileReader();
        reader.onload = function () {
            self.model.MainPhotoByteString = reader.result;
      
        };
        reader.onerror = function (error) {
        };
    
        reader.readAsDataURL(file);
    }


}