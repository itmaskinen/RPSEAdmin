import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

import { ProductService } from '../../services';
@Component({
    selector: 'app-products-models-list',
    templateUrl: './products-models-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ProductsModelsListComponent implements OnInit {
    showSpinner = false;
    isAdminOrUserEditor: boolean;
    productModelCount = 0;
    public dataTable: any;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route:ActivatedRoute,
        private chRef: ChangeDetectorRef) {}
        packageData: Subject<any> = new Subject<any[]>();

    public ngOnInit() {
        this.loadData();
    }

    public  loadData(){
        this.showSpinner = true;
        this.productService.getAllModelsList()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productModelCount = response.Data.length;

            this.chRef.detectChanges();
            const table: any = $('.table');
            this.dataTable = table.DataTable();
        });
    }
}
