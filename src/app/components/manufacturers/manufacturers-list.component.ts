import { Component, OnInit, ChangeDetectorRef, ViewChild} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

import { ProductService } from '../../services';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { ButtonGroupComponent } from '../../core/components/ag-grid-controls/button-group/button-group.component';
import { ModalService } from '../../shared';

declare var jsPDF: any; // Important
@Component({
    selector: 'app-manufacturers-list',
    templateUrl: './manufacturers-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ManufacturersListComponent extends BaseComponent implements OnInit {
    showSpinner = false;
    isAdminOrUserEditor: boolean;
    manufacturersCount = 0;

    selectedManufacturer: any;
    public colDef: any;
    public defaultColDef: any;
    public frameworkComponents: any;
    @ViewChild('deleteContent') deleteContent: any;

    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modalService: ModalService,
        private chRef: ChangeDetectorRef) {
            super(location, router, route);
        }
        packageData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
        manufacturerList: any = [];

    public key: string = 'CompanyName';
    public reverse: boolean = false;
    public sort(key){
        this.key = key;
        this.reverse = !this.reverse;
    }

    public ngOnInit() {
        this.prepareColumnDef();
        this.loadData();
    }

    public  loadData(){
        this.showSpinner = true;
        this.productService.getAllManufacturersList()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            this.packageData.next(response.Data);
            this.manufacturersCount = response.Data.length;
            this.manufacturerList = response.Data;
        });
    }

    public editManufacturer(id){
        this.router.navigate([`/product-manufacturers/${id}/edit`])
    }

    public confirmManufacturerDelete(content: string, item){
        this.selectedManufacturer = item;
        this.modalService.open(content, { size : 'sm' });
    }

    public deleteManufacturer(){
        if(!this.selectedManufacturer){
            this.toastr.error('Please select a manufacturer to delete', 'Error');
            return false;
        }

        this.showSpinner = true;
            this.productService.deleteManufacturer(this.selectedManufacturer)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.ErrorCode) {
                    this.toastr.error(response.Message, "Error"); 
                }
                else {
                    //workaround to autorefresh dom
                    //need to find a cleaner way but this is it for now
                    //set up a router for each listing component that uses datatable
                    this.router.navigateByUrl('/product-manufacturers/refresh').then(() => {
                        this.router.navigateByUrl('/product-manufacturers');
                    });
                }
            });
    }

    public exportToCSV(){
        let data = this.manufacturerList.map((item) => {
            return {
                CompanyName : item.CompanyName,
                ContactName : item.ContactName,
                Address : item.Address,
                Phone : item.Phone
            }
        });
        super.jsonToCSV(data, 'Manufacturers', true);
    }

    public exportToPDF(){
        let doc = new jsPDF('p', 'pt');
        let totalPagesExp = "{total_pages_count_string}";
        let dateInfo = moment(new Date()).format('YYYY-MM-DD');
        let headerTitle = `Manufacturers`;
        let pdfName = `Manufacturers-${dateInfo}.pdf`;
        let pageWidth = doc.internal.pageSize.getWidth();
        let leftStart = 40;
        let rightStart = (pageWidth / 2);
        let centerStart = rightStart - 60;
        let contentYStart = 50; 
        doc.page = 1; 
        doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);
        contentYStart += 10; 

        let contentColumns = [
        { 
            title: 'Company Name', dataKey: 'CompanyName'
        },
        { 
            title: 'Contact Name', dataKey: 'ContactName'
        },
        { 
            title: 'Address', dataKey: 'Address'
        },
        { 
            title: 'Phone', dataKey: 'Phone'
        }];

        let data = this.manufacturerList.map((item) => {
            return {
                CompanyName : item.CompanyName,
                ContactName : item.ContactName ? item.ContactName : '',
                Address : item.Address ? item.Address : '',
                Phone : item.Phone ? item.Phone : ''
            }
        });
        
        var pageContent = ((data) => { 
            //HEADER
            doc.setFontType('bold');
            doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);
            let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
            doc.text(headerTitle, pageWidth - (60 + headerWidth), 50);
            contentYStart += 10;

            // FOOTER
            var str = "Page " + data.pageCount;
            if (typeof doc.putTotalPages === 'function') {
                str = str + " ";
            }
            doc.setFontSize(10);
            var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
            doc.text(str, leftStart, pageHeight  - 10);
        });

        doc.autoTable(contentColumns, data, {
            margin: { top: contentYStart, left: leftStart, right: 40, bottom: 20 },
            styles: { cellPadding: 2 },
            theme: 'plain',
            headerStyles : {
                fillColor: [40, 167, 69],
                textColor: 'white'
            },
            addPageContent: pageContent
        });
        contentYStart+= 50;

        doc.setFontSize(10);
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        doc.text("Printed: "+ dateInfo, leftStart, pageHeight  - 20);
        doc.text("© Copyright 2018 RP.SE ",190, pageHeight - 20);

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp);
        }

        doc.save(pdfName);
    }

    public prepareColumnDef() {
        this.colDef = [
            { headerName: 'CompanyName', field: 'CompanyName', cellStyle: {  cursor: 'pointer' },
            cellRenderer: 'buttonGroup',
            cellRendererParams : {
                buttonInfo: [
                    {
                      getContent: (params) => {
                        return  `<u>${params.data.CompanyName}</u>`;
                      },
                      onClick: (params) => {
                          this.editManufacturer(params.data.Id);
                      }
                    }
                  ]
            }
            },
            { headerName: 'Contact Name', field: 'ContactName'},
            { headerName: 'Address', field: 'Address'},
            { headerName: 'Phone', field: 'Phone'},
            { headerName: 'Actions ', field: 'Id', width: 50, cellStyle: { textAlign: 'center', cursor: 'pointer' }, sortable: false,
                cellRenderer: 'buttonGroup',
                cellRendererParams: {
                buttonInfo: [
                    {
                    getContent: (params) => {
                        return  '<i class="fa fa-pencil text-center"></i>';
                    },
                    onClick: (params) => {
                        this.editManufacturer(params.data.Id);
                    }
                    },
                    {
                        getContent: (params) => {
                        return  '<i class="fa fa-trash text-center"></i>';
                        },
                        onClick: (params) => {
                            this.confirmManufacturerDelete(this.deleteContent, params.data);
                        }
                    }
                ]
                }
           }
        ];

        this.defaultColDef = {
            resizable: true,
            sortable: true,
            cellRenderer: (params) => {
                return params.value || '';
            },
            comparator: (str1, str2) => {
                return (str1 || '').localeCompare((str2 || ''), 'sv');
            },
            filter: false,
            filterParams: {
                newRowsAction: 'keep',
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                }
            }
        };

        
       this.frameworkComponents = {
         buttonGroup: ButtonGroupComponent
      };
    }

    public onGridReady(params) {
        params.api.sizeColumnsToFit();
    }
}
