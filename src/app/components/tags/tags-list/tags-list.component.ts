import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../core/components';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { ModalService } from '../../../shared';
import { Location } from '@angular/common';
import { TagModel } from '../../../models/tagModel';
import { TagService } from '../../../services/tag.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.scss']
})
export class TagsListComponent extends BaseComponent implements OnInit {

  tagModel: TagModel = new TagModel();
  accountId: number;
  tagList: any[] = [];
  tagForUpdate: any;

  constructor(
    private route: ActivatedRoute, 
    private location: Location,
    private toastr: ToastsManager,
    private modalService: ModalService,
    private router: Router,
    private tagService: TagService
  ) {
    super(location, router, route);
  }

  ngOnInit() {
    this.accountId = super.currentAccountId();
    this.getTags();
  }

  getTags() {
    this.tagService.getTagByAccountId(this.accountId)
    .catch((err: any) => {
      return Observable.throw(err);
  })
  .subscribe((res) =>{
      let data = res.Data;
      this.tagList = data;
  });
  }

  createTag(content: string) : void{
    this.resetTagModel();
    this.tagModel.AccountId = this.accountId;
    this.tagModel.CreatedByUserId = super.currentLoggedUser().id;
    this.modalService.open(content, { size: 'lg' });
  }

  editTag(content: string, item){
    this.tagModel = item;
    this.modalService.open(content, { size: 'lg'});
  }

  confirmDeleteTagDiag(content, item) {
    this.tagModel = item;
    this.tagModel.IsDeleted = true;
    this.modalService.open(content, { size : 'sm' })
  }

  deleteTag() {
    this.saveTag();
  }

  saveTag(){
    this.tagService.saveTag(this.tagModel)
     .catch((err: any) => {
        return Observable.throw(err);
    })
    .subscribe((response) => {
        if(response.ErrorCode)
        {
          this.toastr.error(response.Message, "Error"); 
        }
        else {
          this.toastr.success(response.Message, "Success");
          this.resetTagModel();
          this.getTags();
        }
    });
  }

  resetTagModel() {
    this.tagModel = new TagModel();
  }

  isTagNameValid() {
    return this.tagModel.Name.trim().length > 0;
  }

}
