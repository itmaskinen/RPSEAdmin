import { Injectable } from '@angular/core';
import { DataService, BaseDataService } from '../core/services';
import { API_URL } from '../constants';

@Injectable()
export class DashboardService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }

    public getSummary(accountId?: number, userId?: number) {
        return super.getAll<any>(`dashboard/summary?accountId=${accountId}&userId=${userId}`);
    }

    public getRecentProductLogs(id?: number) {
        return super.getAll<any>(`dashboard/recent-product-logs?accountId=${id}`);
    }

    public getTopManufacturers(id?: number) {
        return super.getAll<any>(`dashboard/top-manufacturers?accountId=${id}`);
    }

    public getTopProductGroups(id?: number) {
        return super.getAll<any>(`dashboard/top-product-groups?accountId=${id}`);
    }

    public getTopUsers(id?: number) {
        return super.getAll<any>(`dashboard/top-users?accountId=${id}`);
    }

    public getRemoveProductsLogs(id?: number) {
        return super.getAll<any>(`dashboard/no-of-product-remove?accountId=${id}`);
    }
}
