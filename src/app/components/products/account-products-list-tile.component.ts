import { Component, OnInit, ViewChild, TemplateRef, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';

 
import { NgbActiveModal, NgbModal, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

import { ProductModel, AccountProductModel, CartProductModel, ProductFilterModel } from '../../models';
import { ProductService, OrderService } from '../../services';
import { OrderModel } from '../../models/orderModel';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';

import * as moment from 'moment';
import { ModalService } from '../../shared';
import * as modalClass  from "../../shared/modules/dialogOptions";
import { ProductBookingComponent, ProductsCreateEditComponent } from '..';
declare var jsPDF: any; // Important

@Component({
    selector: 'app-account-products-list-tile',
    templateUrl: './account-products-list-tile.component.html',
    styleUrls: ['../layout/layout.component.scss', './products.css' ,'../cart/cart.css'],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()]
})
export class AccountProductsListTileComponent extends BaseComponent implements OnInit {
    @Output() productAddedToCart = new EventEmitter();
    @Input() totalRecords: number;
    @Input() userOrders: any;
    @ViewChild('confirmCartContent')

    private confirmCartContentTpl: TemplateRef<any>;
    isAdminOrUserEditor: boolean;
    loadingMessage: string = 'Loading...';
    model: any;
    accountProductModel: any;
    cartModel: any;
    orderModel: any;

    showSpinner: boolean = false;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    accountId: number = 0;
    productList: any;
    productCount = 0;

    selectedAccountId = 0;
    selectedGroupId = [];
    selectedManufacturerId =[];
    selectedColorMatId = [];
    selectedModelId = [];
    selectedLocationId = 0;
    selectedKeyword = '';
    selectedDisplaying = 0;
    selectedViewType = 0;
    selectedQtyView = 0;
    selectedConditionId = [];

    currentTotalQty: number = 0;
    public productFilter : any;
    public qtyView : 0;
    
    constructor(
        private productService: ProductService,
        private toastr: ToastrService,
        private router: Router,
        private orderService: OrderService,
        private modalService: ModalService,
        private location : Location,
        private route : ActivatedRoute,
        private rateConfig: NgbRatingConfig) {
            super(location, router, route);
        }

    packageData: Subject<any> = new Subject<any[]>();
    public ngOnInit()
    {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.productFilter = new ProductFilterModel();
        this.rateConfig.max = 5;
        this.isSuperAdmin = super.issuperadmin();
    }

    //for public
    public loadAll(){
        this.showSpinner = true;
        this.productService.getAll()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productCount = response.Data.length;
        });
    }

    public loadDataFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, locationId, keyWord, currentAccountId, displaying, qtyView, conditionId, pageNumber, tagIds) : any{
        this.selectedAccountId = Number(accountId);
        this.selectedGroupId = groupId;
        this.selectedColorMatId = colorMatId;
        this.selectedManufacturerId = manufacturerId;
        this.selectedModelId = modelId;
        this.selectedLocationId = locationId;
        this.selectedKeyword = keyWord;
        this.selectedDisplaying = displaying;
        this.selectedQtyView = qtyView;
        this.showSpinner = true;
        this.selectedConditionId = conditionId;

        const sortOrder = this.getSortOrder().Value;
        const payload = 
        {
            accountId : this.selectedAccountId,
            groupId: this.selectedGroupId,
            colorMatId: this.selectedColorMatId,
            manufacturerId: this.selectedManufacturerId,
            modelId: this.selectedModelId,
            locationIds: this.selectedLocationId,
            conditionId: this.selectedConditionId,
            keyWord: this.selectedKeyword,
            currentAccountId: this.selectedAccountId,
            qtyView: this.selectedQtyView,
            tagIds: tagIds,
            columnFilters : {}
        };

        return new Promise((resolve) => {
            this.productService.getGrid(payload, 0, displaying, sortOrder)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Products;

                let groupIds = [];
                let manufacturerIds = [];
                let colorMaterialIds = [];
                let modelIds = [];
 
                if(!super.issuperadminoradmin() && this.productFilter.GroupId === undefined){
                    groupIds = data.map((item) => {
                        return item.GroupId;
                    });
                    groupIds = Array.from(new Set(groupIds));
                }
                
                if(this.productFilter.GroupId !== undefined)
                {
                    if(groupId !== this.productFilter.GroupId) {
                        manufacturerIds = data.map((item) => {
                            return item.ManufacturerId;
                        });
                        manufacturerIds = Array.from(new Set(manufacturerIds));
                    }
                }
                
                if(this.productFilter.GroupId !== undefined && this.productFilter.ManufacturerId !== undefined)
                {
                    if(groupId !== this.productFilter.GroupId || manufacturerId !== this.productFilter.ManufacturerId) {
                        colorMaterialIds = data.map((item) => {
                            return item.ColorMatId;
                        });
                        colorMaterialIds = Array.from(new Set(colorMaterialIds));
                    }
    
                }

                modelIds = data.map((item) => {
                    return item.ModelId;
                });
                modelIds = Array.from(new Set(modelIds));

                //qtyTotals = data.reduce(this.getSum, 0);

                let returnData = {
                    groups: groupIds,
                    manufacturers : manufacturerIds,
                    colorMaterials : colorMaterialIds,
                    models: modelIds,
                    recordCount : data.length,
                    totalRecords: response.TotalRecords,
                    //qtyTotals : qtyTotals
                }

                this.productList = data;
                this.productCount = data.length;
                this.packageData.next(data);

                // store filter properties
                this.productFilter = { 
                AccountId: accountId, 
                GroupId: groupId, 
                ManufacturerId: manufacturerId, 
                ColorMatId: colorMatId, 
                ModelId: modelId, 
                Keyword: keyWord, 
                Displaying: displaying  };

                resolve(returnData);
            });
        });
    }

    public saveProductToAccount(){
        this.showSpinner = true;
        this.accountProductModel.UpdatedBy = super.currentLoggedUser().id;
        this.accountProductModel.IsMobile = false;
        this.productService.saveToAccount(this.accountProductModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
                this.router.navigate([`products`]);
            }
        });
    }

    public initSortOrder() {
        const sortOrder = this.getSortOrder();

        if (!sortOrder.Column) {
            this.setSortOrder("CreatedDate Desc");
        }
    }

    public setSortOrder(sortOrder) {
        sessionStorage.setItem("product_grid_sortorder", sortOrder);
    }

    public getSortOrder() {
        const sortOrder =
            sessionStorage.getItem("product_grid_sortorder") || ``;
        if (!sortOrder) {
            return { Column: "", Sort: "" };
        }

        const sortOrderSplit = sortOrder.split(" ");

        return {
            Value: sortOrder,
            Column: sortOrderSplit[0],
            Sort: sortOrderSplit[1],
        };
    }

    public openCart(event: any, cartFormContent: string, item){
        this.orderModel = new OrderModel();
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.CurrentQty;
        this.accountProductModel.AccountId = this.accountId;

        this.currentTotalQty = item.CurrentQty;

        this.modalService.open(cartFormContent, { size: 'lg'});
        event.stopPropagation();
    }

    public cartQuantityChange(){
        this.accountProductModel.QuantityOnStock = this.currentTotalQty - this.cartModel.Quantity;
        this.cartModel.TotalPrice = this.cartModel.Quantity * this.accountProductModel.Price
    }

    public navigateToEdit(id){
        sessionStorage.setItem("product_grid_selected_row", id);
        if(!super.issuperadminoradmin() && this.currentRole() !== 'editor') {
            this.router.navigate([`products/${id}/details`]);
        }
        else {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop
            
            let modal = this.modalService.open(ProductsCreateEditComponent, modalClass.modalMyClass);
            modal.result.then(() => {
                document.documentElement.scrollTop = scrollTop;
            });
        }
    }

    public navigateToDetails(id){
        this.router.navigate([`products/${id}/details`]);
    }

    public recalculateQty(data){
        let totalQty = data.totalQty;
        this.cartModel.Quantity = totalQty;
        this.accountProductModel.QuantityOnStock = this.currentTotalQty - this.cartModel.Quantity;

        
        this.orderModel.OrderDetails = data.items;
    }

    public populateOrderData(){
        this.orderModel.ProductId = this.accountProductModel.ProductId;
        if (this.userOrders.length > 0) {
            const order = this.orderService.updateIfExistingPendingOrder(this.orderModel, this.userOrders);
            if (order.length > 0) {
               this.orderModel = order.filter(e => e.ProductId == this.orderModel.ProductId)[0];
            }
        }

        this.orderModel.UserId = super.currentLoggedUser().id;
        this.orderModel.CreatedBy = super.currentLoggedUser().username;
        this.orderModel.LastUpdatedBy = super.currentLoggedUser().username;
        this.orderModel.UserShippingAddress = this.cartModel.ShippingAddress;
        this.orderModel.ProductId = this.accountProductModel.ProductId;
    }

    public addToCart(){
        this.showSpinner = true;
        this.populateOrderData();
        this.orderService.Save(this.orderModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
               //add new product?
               this.modalService.open(this.confirmCartContentTpl);
               this.productAddedToCart.emit(true);
            }
        });
    }

    public showGeneratingToastr(message){
        return this.toastr.warning(`<span class="fa fa-circle-o-notch fa-spin"></span> ${message}`, null, {
            enableHtml: true,
            timeOut: 0,
            extendedTimeOut: 0,
            tapToDismiss: false,
            toastClass: 'ngx-toastr toast-generating'
        });
    }

    public exportToCSV(){
        this.showSpinner = true;

        const activeToast = this.showGeneratingToastr('Generating Excel...');
        //parsing data
        let parsePromise = new Promise((resolve) => {
            //get data
            this.loadingMessage = `Fetching...`;
            const sortOrder = this.getSortOrder().Value;
            const payload = 
            {
                accountId : this.selectedAccountId,
                groupId: this.selectedGroupId,
                colorMatId: this.selectedColorMatId,
                manufacturerId: this.selectedManufacturerId,
                modelId: this.selectedModelId,
                locationIds: this.selectedLocationId,
                conditionId: this.selectedConditionId,
                keyWord: this.selectedKeyword,
                currentAccountId: this.selectedAccountId,
                qtyView: this.selectedQtyView,
                columnFilters : {}
            };
                
        this.productService.getGrid(payload,0, 1000, sortOrder)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => { 
                let data = response.Products;
                this.loadingMessage = `Parsing...`;
                let parsedData = data.map((item) => {
                    return {
                        'Product Number' : item.ProductName,
                        'Name' : item.ProductNumber,
                        'Location': item.Locations,
                        'Product Group': item.GroupName,
                        'Manufacturer': item.ManufacturerName,
                        'Color/Material': item.ColorMaterialName,
                        'Model': item.ModelName,
                        'Qty': item.CurrentQty,
                        'Height': item.Height,
                        'Width': item.Width,
                        'Depth': item.Length
                    }
                });
                resolve(parsedData);
            });
        });
        //passing parsed data to be written
        parsePromise.then((data) => {
            this.loadingMessage = `Creating file...`;
            super.jsonToCSV(data, 'Products', true).then(() => {
                this.showSpinner = false;
                this.toastr.remove(activeToast.toastId);
            });
            
        });
    }

    public exportToPDF(){
        
        const activeToast = this.showGeneratingToastr('Generating PDF...');

        this.showSpinner = true;
        this.loadingMessage = `Fetching...`;
        let parsePromise = new Promise((resolve) => {
        const sortOrder = this.getSortOrder().Value;
        const payload = 
        {
            accountId : this.selectedAccountId,
            groupId: this.selectedGroupId,
            colorMatId: this.selectedColorMatId,
            manufacturerId: this.selectedManufacturerId,
            modelId: this.selectedModelId,
            locationIds: this.selectedLocationId,
            conditionId: this.selectedConditionId,
            keyWord: this.selectedKeyword,
            currentAccountId: this.selectedAccountId,
            qtyView: this.selectedQtyView,
            columnFilters : {}
        };

        this.productService.getGrid(payload, 0, 200, sortOrder)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => { 
                let data = response.Products;
                this.loadingMessage = `Parsing...`;
                let parsedData = data.map((item) => {
                    return {
                        ProductNumber: item.ProductNumber,
                        ProductName : item.ProductName,
                        Location: item.LocationLevelOneName ? item.LocationLevelOneName : '',
                        GroupName: item.GroupName,
                        Manufacturer: item.ManufacturerName,
                        ColorMaterial: item.ColorMaterialName,
                        CurrentQty: item.CurrentQty,
                        Model: item.ModelName
                    }
                });
                resolve(parsedData);
            });
        });

        let doc = new jsPDF('l', 'pt');
        let totalPagesExp = "{total_pages_count_string}";
        let dateInfo = moment(new Date()).format('YYYY-MM-DD');
        let headerTitle = `Products`;
        let pdfName = `Products-${dateInfo}.pdf`;
        let pageWidth = doc.internal.pageSize.getWidth();
        let leftStart = 20;
        let rightStart = (pageWidth / 2);
        let centerStart = rightStart - 60;
        let contentYStart = 30; 
        doc.page = 1; 
        doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);
        contentYStart += 20; 

        let contentColumns = [
        { 
            title: 'Product Number', dataKey: 'ProductNumber'
        },
        { 
            title: 'Name', dataKey: 'ProductName'
        },
        { 
            title: 'Product Group', dataKey: 'GroupName'
        },
        { 
            title: 'Manufacturer', dataKey: 'Manufacturer'
        },
        { 
            title: 'Color/Material', dataKey: 'ColorMaterial'
        },
        { 
            title: 'Model', dataKey: 'Model'
        },
        { 
            title: 'Location', dataKey: 'Location'
        },
        { 
            title: 'Qty', dataKey: 'CurrentQty'
        }
        ];

        doc.setFontType('bold');
        let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
        doc.text(headerTitle, pageWidth - (40 + headerWidth), contentYStart);
        contentYStart+= 10;

        parsePromise.then((data) => {
            var pageContent = ((data) => { 
                //HEADER
                doc.addImage(super.logo64base(), 'JPEG', leftStart, 20, 60, 30);
                let headerWidth = doc.getStringUnitWidth(headerTitle) * 12;
                doc.text(headerTitle, pageWidth - (40 + headerWidth), 50);
                contentYStart += 10;

                // FOOTER
                var str = "Page " + data.pageCount;
                if (typeof doc.putTotalPages === 'function') {
                    str = str + " ";
                }
                doc.setFontSize(10);
                var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
                doc.text(str, leftStart, pageHeight  - 10);
            });
           

            doc.autoTable(contentColumns, data, {
                margin: { top: contentYStart, left: 20, right: 20, bottom: 20 },
                styles: { overflow: 'linebreak', cellPadding: 2, fontSize: 9 },
                theme: 'plain',
                headerStyles : {
                    fillColor: [40, 167, 69],
                    textColor: 'white'
                },
                columnStyles: {
                    text: {
                        columnWidth: 'wrap'
                    },
                    ProductName: {
                        columnWidth: 150           
                    },
                    ColorMaterial: {
                        columnWidth: 90
                    }
                },
                addPageContent: pageContent
            });

            contentYStart+= 50;

            doc.setFontSize(10);
            var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
            doc.text("Printed: "+ dateInfo, leftStart, pageHeight  - 20);
            doc.text("© Copyright 2018 RP.SE ",190, pageHeight - 20);
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            doc.save(pdfName);
            this.showSpinner = false;
            this.toastr.remove(activeToast.toastId);
        });
    }

    public checkOut(){
        this.router.navigateByUrl('/cart');
    }

    public addMoreProducts(){
        this.router.navigateByUrl('/products');
    }
    //transfer to base class
    public removeDuplicates(arr, prop) {
        let obj = {};
        return Object.keys(arr.reduce((prev, next) => {
            if(!obj[next[prop]]) obj[next[prop]] = next;
            return obj;
        }, obj)).map((i) => obj[i]);
    }

    public imageUrl(url){
        return super.imageUrl(url);
    }

    public exportImagesToZip(){

        const activeToast = this.showGeneratingToastr('Generating Images...');

        this.showSpinner = true;
        this.loadingMessage = `Fetching...`;
        this.productService.getProductsFilteredImages(
            this.accountId, 
            this.productFilter.GroupId, 
            this.productFilter.ColorMatId, 
            this.productFilter.ManufacturerId, 
            this.productFilter.ModelId, 
            this.productFilter.LocationId,
            this.productFilter.Keyword, 
            this.accountId, 
            1000, 
            0, 
            this.qtyView)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
                this.toastr.remove(activeToast.toastId);
            })
            .subscribe((response) => {
              const blob = new Blob([response], { type: 'application/zip' });
              const url= window.URL.createObjectURL(blob);
              window.open(url);
    
              
            });
    }

    public navigateToProductBooking(data){
        if(data.CurrentQty === 0){
            this.toastr.info("Please add quantity first to the product.", "Invalid Quantity");
        } else {
            sessionStorage.setItem("product_grid_selected_row", data.Id);
            if (super.issuperadminoradmin() || this.currentRole() === 'editor') {
                this.modalService.open(ProductBookingComponent, modalClass.modalMyClass);
            }
        }
    }
}
