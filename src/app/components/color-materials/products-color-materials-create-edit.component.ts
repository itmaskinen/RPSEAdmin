import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';
import { ProductService } from '../../services';
import { ManufacturerModel } from '../../models/manufacturerModel';
import { ProductColorMaterialModel } from '../../models';

import * as $ from 'jquery';
@Component({
    selector: 'app-product-color-material-create-edit',
    templateUrl: './products-color-materials-create-edit.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ProductsColorMaterialsCreateEditComponent extends BaseComponent implements OnInit {  
    model: any;
    accountId: 0;
    title: string = 'Create a New Color / Material';
    mode: string = 'create';
    photoSrc: string;
    showSpinner = false;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() { 
            this.photoSrc = "/assets/images/add_photo_image.png";
            this.model = new ProductColorMaterialModel();
            this.mode = 'create';
            this.route.params.subscribe((params) => {
            const id = params['id']; // id
            const accountId = params['accountId']; //account id
            if (id)
            {
                this.loadData(Number(id));
                this.title =  'Edit Color / Material';
                this.accountId = accountId;
                this.mode = 'edit';
            }
            });
            $('#color-mat-name').focus();
        }

        public fileChange(fileInput: any){
            let self = this;
            let file = <File>fileInput.target.files[0];
             if(file.size > 2000000)
            {
                this.toastr.error(`Photo upload limits 2MB of file, this file is ${ (file.size / 1024 / 1024).toFixed(2) } KB. Please choose another one.`, "Error");
                return false;
            }
            var reader = new FileReader();
            reader.onload = function () {
                self.photoSrc = reader.result;
                self.model.ImageBase64 = reader.result.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
            };
            reader.onerror = function (error) {
            };
    
            reader.readAsDataURL(file);
        }

    public loadData(id: number){
            this.showSpinner = true;
            this.productService.getColorMaterial(id)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                this.model = response.Data;
                this.photoSrc =  super.imageUrl(this.model.Image);
            });
        }

    public saveChanges(){
            this.showSpinner = true;
            this.productService.saveProductColorMaterial(this.model)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.ErrorCode) {
                    this.toastr.error(response.Message, "Error"); 
                }
                else {
                    this.toastr.success(`Successfully ${this.mode == 'create' ? 'added' : 'updated'} color / material.`,'Success!');
                    this.router.navigate([`product-color-materials`]);
                }
            });
        }


}