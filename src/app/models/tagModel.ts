export class TagModel {
    public Id: number = 0;
    public CreatedByUserId: number = 0;
    public AccountId: number = 0;
    public Name: string = "";
    public IsDeleted: boolean = false;
}