import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';

import { BaseComponent } from '../../core/components';

import 'datatables.net';
import 'datatables.net-bs4';

import { AccountProductConditionService } from '../../services';
import { Observable } from 'rxjs';
import { AccountProductConditionModel } from '../../models';
import { ModalService } from '../../shared';
import { ToastsManager } from 'ng2-toastr';
@Component({
    selector: 'app-account-product-condition-list',
    templateUrl: './account-product-condition-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})

export class AccountProductConditionComponent extends BaseComponent implements OnInit {
    accountProductConditionList: any = [];
    accountProductConditionListForUpdate: any = [];
    accountId: number = 0;
    accountProductConditionModel: any
    code: string = '';
    name: string = '';
    description: string = '';
    newProductCondition: any = 0;

    constructor(private router: Router,
        private route:ActivatedRoute, 
        private location: Location,
        private toastr: ToastsManager,
        private modalService: ModalService,
        private accountProductConditionService: AccountProductConditionService) {
            super(location, router, route);
        }
    
    packageData: Subject<any> = new Subject<any[]>();  
    dataList : Array<any> = [];

    public ngOnInit() {
        this.accountProductConditionModel = new AccountProductConditionModel();
        this.accountId = super.currentAccountId();
        this.accountProductConditionModel.AccountId = this.accountId;
        this.getProductConditionByAccount(this.accountId);
    }

    public getProductConditionByAccount(accountId: number){
        return new Promise((resolve) =>{
            this.accountProductConditionService.getProductConditionByAccount(accountId)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .subscribe((res) =>{
                let data = res.Data;
                this.accountProductConditionList = data;
                this.accountProductConditionListForUpdate = data;
                resolve(data);
            });
        });
    }

    public saveProductCondition(isDelete = false){
        if(!isDelete) {
            this.setFormValueToSubmit();
        }

        this.accountProductConditionModel.NewProductConditionAssignment = this.newProductCondition;
        this.accountProductConditionService.saveAccountProductCondition(this.accountProductConditionModel)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.toastr.success(response.Message, "Success");

                if(this.accountProductConditionModel.Id === 0) {
                    this.accountProductConditionModel.Id = response.Data.Id;
                    this.accountProductConditionList.push(this.accountProductConditionModel);
                    return;
                }

                this.upadateListData(this.accountProductConditionModel);
            }
        });
    }

    public editProductCondition(content: string, item){
        this.setModalFormValue(item);
        this.accountProductConditionModel = item;
        this.modalService.open(content, { size: 'lg'});
    }

    public createProductCondition(content: string) : void{
        let item = new AccountProductConditionModel();
        item.AccountId = `${this.accountId}`;
        this.setModalFormValue(item);
        this.accountProductConditionModel = item;
        this.modalService.open(content, { size: 'lg' });
    }

    public confirmDeleteProductCondition(content, item) {
        item.IsDeleted = true;
        this.accountProductConditionModel = item;
        this.accountProductConditionListForUpdate = this.accountProductConditionList.filter(e => e.Id != item.Id);
        this.modalService.open(content, { size : 'sm' })
    }

    public deleteProductCondition() {
        this.saveProductCondition(true);
        this.accountProductConditionList = this.accountProductConditionList.filter(x => !x.IsDeleted);
    }

    public onValueChange(){
        this.description = this.code
        ? this.code  + (this.name ? ' - ' + this.name : '')
        : this.name;
    }

    private setModalFormValue(data: AccountProductConditionModel){
        this.code = data.Code;
        this.name = data.Name;
        this.description = data.Description;
    }

    private setFormValueToSubmit(){
        this.accountProductConditionModel.Code = this.code;
        this.accountProductConditionModel.Name = this.name;
        this.accountProductConditionModel.Description = this.description;
    }

    private upadateListData(data: AccountProductConditionModel){
        let accountProductCondition = this.accountProductConditionList.find(x => x.Id == data.Id);
        accountProductCondition.Code = data.Code;
        accountProductCondition.Name = data.Name;
        accountProductCondition.Description = data.Description;
    }
}
