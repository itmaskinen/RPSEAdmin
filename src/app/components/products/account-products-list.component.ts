import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

import { ProductModel, AccountProductModel, CartProductModel } from '../../models';
import { ProductService, LocationService } from '../../services';
import { BaseComponent } from '../../core/components';
import { ModalService } from '../../shared';

@Component({
    selector: 'app-account-products-list',
    templateUrl: './account-products-list.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    providers: [NgbRatingConfig],
    animations: [componentsTransition()]
})
export class AccountProductsListComponent extends BaseComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model: any;
    accountProductModel: any;
    cartModel: any;
    showSpinner: boolean = false;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    accountId: number = 0;
    selectedAccountId: number = 0;
    productList: any;
    productCount = 0;

    public locationLevelOneList: any = [];
    public locationLevelTwoList: any = [];
    public locationLevelThreeList: any = [];

    public locationLevelOneId: number = 0;
    public locationLevelTwoId: number = 0;
    public locationLevelThreeId: number = 0;

    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router,
        private route:ActivatedRoute,
        private modalService: ModalService,
        private elem: ElementRef,
        private location: Location,
        private locationService: LocationService,
        private rateConfig: NgbRatingConfig) {
            super(location, router, route);
        }

    packageData: Subject<any> = new Subject<any[]>(); 
    public ngOnInit() 
    {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.rateConfig.max = 5;
    }

    public loadData(id){
        this.accountId = id;
        this.selectedAccountId = id;
        this.showSpinner = true;
        this.productService.getByAccountList(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(response.Data);
            this.productCount = response.Data.length;
        });
    }

    public loadDataFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, keyWord, currentAccountId, displaying) {
        this.selectedAccountId = accountId;
        this.showSpinner = true;
        this.productService.getProductsFiltered(accountId, groupId, colorMatId, manufacturerId, modelId, 0, keyWord, currentAccountId, displaying)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
            return 0;
        })
        .subscribe((response) => {
            let data = response.Data;
            this.packageData.next(data);
            this.productCount = data.length;
        });
    }

    public confirmProductAdd(productFormContent: string, item): void {
        this.loadAccountLocationLevelOneList(this.accountId);

        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        
        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.Id = item.AccountProductId;
        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.AccountId = this.accountId;
        this.accountProductModel.Description = item.Description;
        this.accountProductModel.Location = item.Location;
        this.accountProductModel.QuantityOnStock = item.QuantityOnStock;
        this.accountProductModel.Price = item.Price;
        this.modalService.open(productFormContent, { size: 'lg'});

    }

    public saveProductToAccount(){
        this.showSpinner = true;
        this.accountProductModel.UpdatedBy = super.currentLoggedUser().id;
        this.accountProductModel.IsMobile = false;
        this.productService.saveToAccount(this.accountProductModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.router.navigate([`account/${this.accountId}/products`]);
            }
        });
    }

    public addtoCart(addCartFormContent: string, item){
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();
        
        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.QuantityOnStock;
        this.accountProductModel.AccountId = this.accountId;
 
        this.modalService.open(addCartFormContent, { size: 'lg'});
    }

    public cartQuantityChange(){
        this.cartModel.TotalPrice = this.cartModel.Quantity * this.accountProductModel.Price
    }

    public navigateToEdit(id){
        this.router.navigate([`products/${id}/edit`]);  
    }

    public isHideEditQtyButton(accountHasThisProduct: boolean): boolean{
        return this.selectedAccountId == 0 && accountHasThisProduct === true;
    }

    //LOCATION
    public loadAccountLocationLevelOneList(accountId: number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelOne(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                this.showSpinner = false;
        })
        .subscribe((response : any) => {
            let data = response.Data;
            if(data.length > 0)
            {
                this.locationLevelOneList = data;
                this.locationLevelOneId = this.locationLevelOneList[0].Id;
                this.selectLevelOne(this.locationLevelOneId);
            }
            else {
                this.locationLevelOneId = 0;
            }
        });
    }

    public loadAccountLocationLevelTwoList(accountId: number, levelOneId?:number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelTwo(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            this.showSpinner = false;

        })
        .subscribe((response : any) => {
            let data = response.Data;
            if(data.length > 0)
            {
                if(levelOneId> 0) {
                    let locationLevelTwoFilteredList = data.filter((item) => {
                        return item.AccountLocationLevelOneId === levelOneId;
                    });
                    if(locationLevelTwoFilteredList.length > 0){
                        this.locationLevelTwoList = locationLevelTwoFilteredList;
                        this.locationLevelTwoId = locationLevelTwoFilteredList[0].Id;
                        this.selectLevelTwo(this.locationLevelTwoId);
                    }
                    else {
                        this.locationLevelTwoId = 0;
                    }
                }
                else {
                    this.locationLevelTwoList = data;
                }
            }
            else {
                this.locationLevelTwoId = 0;
            }
        });
    }

    public loadAccountLocationLevelThreeList(accountId: number, levelTwoId?:number){
        this.accountId = accountId;
        this.showSpinner = true;
        this.locationService.getAccountLocationsLevelThree(accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                this.showSpinner = false;
        })
        .subscribe((response : any) => {
            let data = response.Data;
            if(data.length > 0)
            {
                if(levelTwoId> 0) {
                    let locationLevelThreeFilteredList = data.filter((item) => {
                        return item.AccountLocationLevelTwoId === levelTwoId;
                    });
                    
                    if(locationLevelThreeFilteredList.length > 0){
                        this.locationLevelThreeList = locationLevelThreeFilteredList;
                        this.locationLevelThreeId = locationLevelThreeFilteredList[0].Id;
                    }
                    else {
                        this.locationLevelThreeId = 0;
                    }
                }
                else {
                    this.locationLevelThreeList = data;
                }
            }
            else {
                this.locationLevelThreeId = 0;
            }
        });
    }

    public selectLevelOne(id){
        this.onLevelOneChange(id);
    }

    public selectLevelTwo(id){
        this.onLevelTwoChange(id);
    }

    public onLevelOneChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelTwoList(this.accountId, value);
        }
    }

    public onLevelTwoChange(val){
        let value = Number(val);
        if(value > 0)
        {
            this.loadAccountLocationLevelThreeList(this.accountId, value);
        }
    }
}
