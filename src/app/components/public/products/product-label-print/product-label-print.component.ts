import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { API_URL } from "../../../../constants";
import { BaseComponent } from "../../../../core/components";
import { AccountService, LocationService, ProductService } from "../../../../services";
import { Location } from "@angular/common";
import { AccountModel } from "../../../../models";
import { ToastrService } from "ngx-toastr";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-product-label-print",
  templateUrl: "./product-label-print.component.html",
  styleUrls: ["./product-label-print.component.css"],
})
export class ProductLabelPrintComponent
  extends BaseComponent
  implements OnInit {
  public bookingModel: any;
  public bookings: any = [];
  public products: any = [];
  public sourceLocation: string;
  public destinationLocation: string;
  public accountId: any;
  public bookedOrAllProduct: any;
  public accountModel: AccountModel;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private locationService: LocationService,
    private router: Router,
    private location: Location,
    private accountService: AccountService,
    private toastr: ToastrService,
  ) {
    super(location, router, route);
  }

  async ngOnInit() {
    // Identify where the print request is from
    this.bookedOrAllProduct = this.route.snapshot.url[3].path;

    const params = this.route.snapshot.params;
    this.accountId = params["accountId"];
    let ids = params["id"];
    this.getAccount(this.accountId);

    // Get all locations of account
    const locations = await this.locationService
      .getAllLocationsOfAccount(this.accountId)
      .toPromise();

    // Print request for booked products label list page
    if (this.bookedOrAllProduct == "product") {
      ids = ids.split(",");

      for (let i = 0; i < ids.length; i++) {
        // Get product bookings
        const val = await this.productService
          .getProductBooking(ids[i])
          .toPromise();

        if (val.Success && val.Data) {
          // Remove zero qty (some bad/old data)
          val.Data = val.Data.filter((e) => e.Quantity > 0);

          for (let i = 0; i < val.Data.length; i++) {
            let qty = val.Data[i].Quantity;
            let productName = val.Data[i].ProductName;
            let comment = val.Data[i].Comment;
            let productNum = val.Data[i].ProductNumber;
            let room = val.Data[i].Room;
            let productId = val.Data[i].ProductId;

            // Get src and dst location ids
            let srcLocationIds = val.Data[i].SourceLocationId
              ? val.Data[i].SourceLocationId.split(",")
              : [];
            let dstLocationIds = val.Data[i].DestinationLocationId
              ? val.Data[i].DestinationLocationId.split(",")
              : [];

            // Format src location name
            let srcLocationNames: any = [];
            if (srcLocationIds) {
              if (
                srcLocationIds[0] &&
                locations.Data.accountLocationLevelOne
                  .filter((e) => e.Id == srcLocationIds[0])
                  .map((e) => e.Name)[0] != ""
              )
                srcLocationNames.push(
                  locations.Data.accountLocationLevelOne
                    .filter(
                      (e) => e.Id == srcLocationIds[0]
                    )
                    .map((e) => e.Name)[0]
                );
              if (
                srcLocationIds[1] &&
                locations.Data.accountLocationLevelTwo
                  .filter((e) => e.Id == srcLocationIds[1])
                  .map((e) => e.Name)[0] != ""
              )
                srcLocationNames.push(
                  locations.Data.accountLocationLevelTwo
                    .filter(
                      (e) => e.Id == srcLocationIds[1]
                    )
                    .map((e) => e.Name)[0]
                );
              if (
                srcLocationIds[2] &&
                locations.Data.accountLocationLevelThree
                  .filter((e) => e.Id == srcLocationIds[2])
                  .map((e) => e.Name)[0] != ""
              )
                srcLocationNames.push(
                  locations.Data.accountLocationLevelThree
                    .filter(
                      (e) => e.Id == srcLocationIds[2]
                    )
                    .map((e) => e.Name)[0]
                );
              if (
                srcLocationIds[3] &&
                locations.Data.accountLocationLevelFour
                  .filter((e) => e.Id == srcLocationIds[3])
                  .map((e) => e.Name)[0] != ""
              )
                srcLocationNames.push(
                  locations.Data.accountLocationLevelFour
                    .filter(
                      (e) => e.Id == srcLocationIds[3]
                    )
                    .map((e) => e.Name)[0]
                );
            }

            // Format dst location name
            let dstLocationNames: any = [];
            if (dstLocationIds) {
              if (
                dstLocationIds[0] &&
                locations.Data.accountLocationLevelOne
                  .filter((e) => e.Id == dstLocationIds[0])
                  .map((e) => e.Name)[0] != ""
              )
                dstLocationNames.push(
                  locations.Data.accountLocationLevelOne
                    .filter(
                      (e) => e.Id == dstLocationIds[0]
                    )
                    .map((e) => e.Name)[0]
                );
              if (
                dstLocationIds[1] &&
                locations.Data.accountLocationLevelTwo
                  .filter((e) => e.Id == dstLocationIds[1])
                  .map((e) => e.Name)[0] != ""
              )
                dstLocationNames.push(
                  locations.Data.accountLocationLevelTwo
                    .filter(
                      (e) => e.Id == dstLocationIds[1]
                    )
                    .map((e) => e.Name)[0]
                );
              if (
                dstLocationIds[2] &&
                locations.Data.accountLocationLevelThree
                  .filter((e) => e.Id == dstLocationIds[2])
                  .map((e) => e.Name)[0] != ""
              )
                dstLocationNames.push(
                  locations.Data.accountLocationLevelThree
                    .filter(
                      (e) => e.Id == dstLocationIds[2]
                    )
                    .map((e) => e.Name)[0]
                );
              if (
                dstLocationIds[3] &&
                locations.Data.accountLocationLevelFour
                  .filter((e) => e.Id == dstLocationIds[3])
                  .map((e) => e.Name)[0] != ""
              )
                dstLocationNames.push(
                  locations.Data.accountLocationLevelFour
                    .filter(
                      (e) => e.Id == dstLocationIds[3]
                    )
                    .map((e) => e.Name)[0]
                );
            }

            // Create duplicate copies if qty > 1
            if (val.Data[i].Quantity > 1) {
              for (let i = 1; i <= qty; i++) {
                this.bookingModel = {
                  imageSrc: `${API_URL}/product/qr?productId=${productId
                    }&production=${API_URL.includes("rpappapi") ? true : false
                    }`,
                  name: productName,
                  srcLocation: srcLocationNames
                    ? srcLocationNames.join(" \\ ")
                    : "",
                  dstLocation: dstLocationNames
                    ? dstLocationNames.join(" > ")
                    : "",
                  comment: comment,
                  productNum: productNum,
                  bookingCount: `${i}(${qty})`,
                  room: room,
                };
                this.bookings.push(this.bookingModel);
              }
            } else {
              this.bookingModel = {
                imageSrc: `${API_URL}/product/qr?productId=${productId
                  }&production=${API_URL.includes("rpappapi") ? true : false
                  }`,
                name: productName,
                srcLocation: srcLocationNames
                  ? srcLocationNames.join(" \\ ")
                  : "",
                dstLocation: dstLocationNames
                  ? dstLocationNames.join(" > ")
                  : "",
                comment: comment,
                productNum: productNum,
                bookingCount: `1(1)`,
                room: room,
              };
              this.bookings.push(this.bookingModel);
            }
          }
        }
      }

      if (this.bookings.length > 0) {
        setTimeout(() => {
          window.print();
          window.close();
        }, 2000);
      } else {
        window.close();
      }
    }
    // Print all product labels on list
    else if (this.bookedOrAllProduct == "allproduct") {
      const storedData = JSON.parse(
        localStorage.getItem("product_labels_for_print")
      );

      if (storedData && storedData.length > 0) {
        for (let i = 0; i < storedData.length; i++) {
          let val = storedData[i];
          var parts = val.ProductNumber.split('-');
          let productNumber = parts[parts.length - 1];
          let quantity = val.Quantity || 1;

          for (let j = 0; j < quantity; j++) {
            this.products.push(
                {
                    id: val.Id,
                    qrSrc: `${API_URL}/product/qr?productId=${val.Id
                    }&production=${API_URL.includes("rpappapi") ? true : false
                    }`,
                    accountLogoSrc: this.accountModel.AccountLogoURL,
                    productName: val.ProductName,
                    productNumber: productNumber,
                    price: val.Price,
                    groupName: val.GroupName
                }
            );
          }
        }
      }

      if (this.products.length > 0) {
        setTimeout(() => {
          window.print();
          window.close();
          localStorage.removeItem('product_labels_for_print');
        }, 2000);
      } else {
        window.close();
      }
    }
    // Print request for booked products label
    else {
      const storedData = JSON.parse(
        localStorage.getItem("book_labels_for_print")
      );

      if (storedData && storedData.length > 0) {
        for (let i = 0; i < storedData.length; i++) {
          let val = storedData[i];
          let qty = val.Quantity;
          let productName = val.ProductName;
          let comment = val.Comment;
          let productNum = val.ProductNumber;
          let room = val.Room;
          let srcLocation = val.SourceLocation.replaceAll(">", " \\ ");
          let dstLocation = val.DestinationLocation;
          let productId = val.ProductId;
          // Create duplicate copies if qty > 1
          if (qty > 1) {
            for (let i = 1; i <= qty; i++) {
              this.bookingModel = {
                imageSrc: `${API_URL}/product/qr?productId=${productId
                  }&production=${API_URL.includes("rpappapi") ? true : false
                  }`,
                name: productName,
                srcLocation: srcLocation,
                dstLocation: dstLocation,
                comment: comment,
                productNum: productNum,
                bookingCount: `${i}(${qty})`,
                room: room,
              };
              this.bookings.push(this.bookingModel);
            }
          } else {
            this.bookingModel = {
              imageSrc: `${API_URL}/product/qr?productId=${productId
                }&production=${API_URL.includes("rpappapi") ? true : false
                }`,
              name: productName,
              srcLocation: srcLocation,
              dstLocation: dstLocation,
              comment: comment,
              productNum: productNum,
              bookingCount: `1(1)`,
              room: room,
            };
            this.bookings.push(this.bookingModel);
          }
        }
      }

      if (this.bookings.length > 0) {
        setTimeout(() => {
          window.print();
          window.close();
          localStorage.removeItem('book_labels_for_print');
        }, 2000);
      } else {
        window.close();
      }
    }
  }

  public getImage(imgSrc){
    return super.imageUrl(imgSrc);
}

public getAccount(id: number) : void{
    this.accountService.getAccountV3(id)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
        setTimeout(() => {
        }, 1000);
    })
    .subscribe((data) => {
        if(data.Data) {
            this.accountModel = data.Data;
            let urlSrc = this.getImage(this.accountModel.AccountLogoURL)
            this.accountModel.AccountLogoURL = urlSrc.includes("upload-empty") ? "" : urlSrc
        }
    });
}
}
