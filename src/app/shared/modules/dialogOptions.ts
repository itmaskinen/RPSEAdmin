import { NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";

export const modalLarge: NgbModalOptions = {
    size: 'lg',
    backdrop: 'static',
    keyboard: false
}

export const modalSmall: NgbModalOptions = {
    size: 'sm',
    backdrop: 'static',
    keyboard: false
}

export const modalMyClass: NgbModalOptions = {
    windowClass: "my-class",
    // size: 'lg',
    backdrop: 'static',
    keyboard: false
}

export const modalIssueClass: NgbModalOptions = {
    windowClass: "issueClass",
    backdrop: 'static',
    keyboard: false
}

export const modalCreateType: NgbModalOptions = {
    size: 'lg',
    windowClass: "modal-create-type",
    backdrop: 'static',
    keyboard: false
}

export const modalDatasource: NgbModalOptions = {
    windowClass: 'modal-datasource-editor',
    backdrop: 'static',
    keyboard: false
}
