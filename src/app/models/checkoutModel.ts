export class CheckoutModel {
    public Id : number = 0;
    public OrderNumber : string;
    public UserId : number;
    public UserShippingAddress: string;
    public Notes: string;
    public AdditionalInstructions: string;
    public EmailCheckoutCCs: string;

    public CreatedDate : string;
    public CreatedBy : string;
    public LastUpdatedDate : string;
    public LastUpdatedBy : string;
    public OrderDetails : any = [];
}