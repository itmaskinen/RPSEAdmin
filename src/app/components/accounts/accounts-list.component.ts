import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { routerTransition } from '../../router.animations';
import { ToastsManager } from 'ng2-toastr';
import { AccountService } from '../../services';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-accounts-list',
    templateUrl: './accounts-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class AccountsListComponent implements OnInit {
    public accountId: number = 0;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public showSpinner: boolean = false;
    public dataTable: any;
    public canEdit: boolean = false;
    public isForAssociate: boolean = false;
    constructor(private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private accountService: AccountService) {}

    packageData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
    public colDef: any;
    public defaultColDef: any;
    public gridApi: any;

    public key: string = 'Company';
    public reverse: boolean = false;
    public sort(key){
        this.key = key;
        this.reverse = !this.reverse;
    }
    public ngOnInit() {
        this.prepareColumnDef();
    }

    public editAccount(id:number){
        if(this.isForAssociate) return false;
        this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
    }
    public editAccountModule(id:number){
        this.router.navigate([`${id}/edit/modules`], { relativeTo: this.route });  
    }
    public editAccountUser(id:number){
        this.router.navigate([`${id}/edit/users`], { relativeTo: this.route });  
    }

    public editAccountSite(id:number){
        this.router.navigate([`${id}/edit/objects`], { relativeTo: this.route });  
    }

    public loadData(page?: number){
        this.showSpinner = true;
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 
        this.accountService.getAccountsList(this.currentPage, this.itemPerPage, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }
    
    public loadDataWithAssoc(id: number){
        this.showSpinner = true;
        this.accountService.getAccountsWithAssocList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }

    public checkAccountAssoc(e) {
        let accountId = Number(this.accountId);
        let assocAccountId = Number(e.value);
        if(e.checked) {
            this.saveAccountAssoc(accountId, assocAccountId);
        }
        else {
            this.deleteAccountAssoc(accountId, assocAccountId);
        }
    }

    public saveAccountAssoc(accountId: number, assocAccountId: number){
        let model = {
            AccountId: accountId,
            AssociatedAccountId: assocAccountId
        }
        this.showSpinner = true;
        this.accountService.saveAccountAssoc(model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            this.loadDataWithAssoc(accountId);
        });
    }

    public deleteAccountAssoc(accountId: number, assocAccountId: number){
        let model = {
            AccountId: accountId,
            AssociatedAccountId: assocAccountId
        }
        this.showSpinner = true;
        this.accountService.deleteAccountAssoc(model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
        });
    }

    public gridCellClicked(event) {
        if (event.colDef.unclickable) return;
        this.editAccount(event.data.Id);
    } 
      
    public prepareColumnDef() {
        this.colDef = [
            { headerName: 'Company', field: 'Company', cellStyle: {  cursor: 'pointer' },
            cellRenderer(params) {
                return  `<u>${params.data.Company}</u>`;
             }
            },
            { headerName: 'Email Address', field: 'EmailAddress',  cellStyle: {  cursor: 'pointer' }},
            { headerName: 'Contact Name', field: 'ContactName', cellStyle: {  cursor: 'pointer' }},
            { headerName: 'Contact Number', field: 'ContactNumber', cellStyle: {  cursor: 'pointer' }},
            { headerName: 'Full Address', field: 'FullAddress', cellStyle: {  cursor: 'pointer' } },
            { headerName: 'Actions ', field: 'Id', width: 50, cellStyle: { textAlign: 'center', cursor: 'pointer' }, sortable: false,
             cellRenderer(params) {
                 return `<a><i style="color:#ffc107" class="fa fa-pencil text-center"></i></a>`;
              }
           }
        ];

        this.defaultColDef = {
            resizable: true,
            sortable: true,
            cellRenderer: (params) => {
                return params.value || '';
            },
            comparator: (str1, str2) => {
                return (str1 || '').localeCompare((str2 || ''), 'sv');
            },
            filter: false,
            filterParams: {
                newRowsAction: 'keep',
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                }
            }
        };
    }

    public onGridReady(params) {
        this.gridApi =  params.api; 
        this.gridApi.sizeColumnsToFit();
    }
}
