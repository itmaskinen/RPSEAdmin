import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { Observable } from 'rxjs/Observable';
import { LocationModel } from '../../models';
import { LocationService } from '../../services';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'app-locations-create-edit',
    templateUrl: './locations-create-edit.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class LocationsCreateEditComponent implements OnInit {
    public model: any;
    public isAdminOrUserEditor: boolean;

    constructor(
        private router: Router,
        private route:ActivatedRoute,
        private locationService: LocationService,
        private toastr: ToastsManager
    ) {

    }
    public ngOnInit() {
        this.model = new LocationModel();
    }

    public locationSaveChanges(levelId: number, accountId: number): void{
        this.model.LocationLevel = levelId;
        this.model.AccountId = accountId;
        this.locationService.saveLocation(this.model)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.router.navigate([`/accounts`]);
            }
        });
    }
}
