import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { componentsTransition } from '../../../router.animations';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';


import { ProductModel, AccountProductModel } from '../../../models';
import { ProductService } from '../../../services';

@Component({
    selector: 'app-products-catalog',
    templateUrl: './products-catalog.component.html',
    styleUrls: ['../../layout/components/header/header.component.scss', '../../layout/layout.component.scss'],
    animations: [componentsTransition()]
})
export class ProductsCatalogComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model: any;
    accountProductModel: any;
    cartModel: any;

    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;
    accountId: number = 0;
    productList: any;
    productCount = 0;
    constructor(
        private productService: ProductService,
        private toastr: ToastsManager,
        private router: Router) {}

    packageData: Subject<any> = new Subject<any[]>(); 
    public ngOnInit() 
    {
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        //this.rateConfig.max = 5;
        this.loadAll();
    }


    //for public
    public loadAll(){

        this.productService.getAll()
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => {
            this.packageData.next(response.Data);
            this.productCount = response.Data.length;
        });
    }

    public navigateToEdit(id){
        this.router.navigate([`products/${id}/edit`]);  
    }
}
