import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from '../../../../services';

import { Observable } from 'rxjs/Observable';
import { BaseComponent } from '../../../../core/components';
import { Location } from '@angular/common';
import * as $ from 'jquery';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseComponent implements OnInit {
    pushRightClass: string = 'push-right';
    pushRLeftClass: string = 'push-left';
    public currentUser: any;
    public hasSecondaryAdminAccount: any;
    constructor(
        private authService: AuthenticationService,
        private translate: TranslateService,
        private location: Location,
        private route: ActivatedRoute,
        public router: Router) {
        super(location, router, route);

        // this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        // this.translate.setDefaultLang('en');
        //const browserLang = this.translate.getBrowserLang();
        //this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            // if (
            //     val instanceof NavigationEnd &&
            //     window.innerWidth <= 992 &&
            //     this.isToggled()
            // ) {
            //     this.toggleSidebar();
            // }
            // else {
            //     this.toggleSidebar();
            // }
            this.toggleSidebar();
        });
    }

    ngOnInit() {
        let currentUser = super.currentLoggedUser();
        if (currentUser.role === 'superadmin') {
            $('.navbar').css({ 'height' : '56px' });
        }
        this.currentUser = currentUser;
        this.hasSecondaryAdminAccount = localStorage.getItem('secondaryAdminUser');
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
        dom.classList.toggle(this.pushRLeftClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        const dom: any = document.querySelector('body');
        if(this.currentUser.id === -1)
        {
            localStorage.removeItem('productFilter');
            localStorage.removeItem('currentUser');
            localStorage.removeItem('isLoggedin');
            localStorage.removeItem('secondaryAdminUser');
            dom.classList.remove(this.pushRLeftClass);
            dom.classList.remove(this.pushRightClass);

        }
        else {
            let model = {
                userId : this.currentUser.id
            };
            this.authService.logoff(model)
            .catch((err: any) => {
                return Observable.throw(err);
            }).finally(() => {

            })
            .subscribe((response) => {
                localStorage.removeItem('productFilter');
                localStorage.removeItem('currentUser');
                localStorage.removeItem('isLoggedin');
                localStorage.removeItem('secondaryAdminUser');
                dom.classList.remove(this.pushRLeftClass);
                dom.classList.remove(this.pushRightClass);
    
            });
        }


    }

    removeSecondaryAccount(){
        localStorage.removeItem('secondaryAdminUser');
        window.close();
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    editUser(){
        let id = this.currentUser.id;
        if(id > 0)
        {
            this.router.navigate([`users/${id}/edit`]);
        }
    }
}
