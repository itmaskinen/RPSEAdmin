import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { ProductsGroupsListComponent } from './products-groups-list.component';

@Component({
    selector: 'app-products-groups',
    templateUrl: './products-groups.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ProductsGroupsComponent implements OnInit {
    @ViewChild('productsGroupsList') groupList: ProductsGroupsListComponent;

    isAdminOrUserEditor: boolean;
    constructor(
        private router: Router
    ) {}
    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
    }

    public navigateToCreate(){
        this.router.navigate([`product-groups/create`]);
    }

    public exportToCSV(){
        this.groupList.exportToCSV();
    }

    public exportToPDF(){
        this.groupList.exportToPDF();
    }
}
