import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class ModalService{
    getData$: Observable<any>;
    private myMethodSubject = new Subject<any>();

    private defaultOption: NgbModalOptions = { backdrop: 'static', keyboard: false };

    constructor(private modalservice : NgbModal) {
        this.getData$ = this.myMethodSubject.asObservable();
    }

    public open(content: any, options?: NgbModalOptions) : NgbModalRef{
        return this.modalservice.open(content, options ? this.mergeJson(options) : this.defaultOption )
    }

    private mergeJson(currentOptions? : NgbModalOptions) : NgbModalOptions{
        return {...currentOptions, ...this.defaultOption};
    }

    getData(data) {
        this.myMethodSubject.next(data);
    }

}
