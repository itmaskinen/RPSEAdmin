import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { ToastsManager } from 'ng2-toastr';

import { UserService } from '../../services';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { BaseComponent } from '../../core/components';
import { UserModel } from '../../models';
import { ModalService } from '../../shared';
@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class UsersListComponent extends BaseComponent implements OnInit {
    @Output() editUserView = new EventEmitter();

    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public accountId: number = 0;
    public model: any;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    public isAdmin = false;
    
    public selectedAdminId: number = 0;
    public selectedUser: any;

    constructor(private router: Router,
        private route:ActivatedRoute, 
        private location: Location,
        private toastr: ToastsManager,
        private userService: UserService,
        private modalService: ModalService) {
            super(location, router, route);
        }
    
    packageData: Subject<any> = new Subject<any[]>();  
    dataList : Array<any> = [];

    public ngOnInit() {
        if(this.isSuperAdmin)
        {
            this.loadData();
        }
    }

    public loadData(page?: number, id?: number, filter: string = ""): void{
        this.showSpinner = true;
        if (page != null) {
            this.currentPage = page; 
        } 
        if(id > 0)
        {
            this.loadDataById(id);
        }
        else {
            this.userService.getUsersList(this.currentPage, this.itemPerPage)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                let data = response.Data.filter((item)=>{
                    return item.RoleName === 'admin';
                });
                this.packageData.next(data);
                if(filter !== '')
                {
                    this.dataList = data.filter((item) => {
                        return item.Account.Name.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                    });
                }        
                this.totalItems = response.TotalRecords;
                //this.toastr.success(response.Message, "Success");
            });
        }
    }
   
    public loadDataById(id:number){
        this.accountId = id;
        this.showSpinner = true;
        this.userService.getUsersByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }

    public editUser(id:number){
        this.editUserView.emit(id);
    }

    public deleteUser(){
        if(!this.selectedUser){
            this.toastr.error('Please select a user to delete.', 'Error');
            return false;
        }
        this.showSpinner = true;
        this.userService.deleteUser(this.selectedUser)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response : any) => {
            this.loadDataById(this.accountId);
        });
    }

    public resendCreateUserConfirmation(model: UserModel)
    {
        this.showSpinner = true;
        model.Resending = true;
        this.userService.resendCreateUserConfirmation(model)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response : any) => {
            if(response.Success){
                this.toastr.success(response.Message, "Create User Confirmation Email Sent.");
            }
           
            model.Resending = false;
        });

    }

    public editAccountUser(id:number){
        if(this.isSuperAdmin)
        {
            this.router.navigate([`accounts/${id}/edit/users`]);  
        }
    }

    public confirmDeleteUser(content: string, item: any){
        this.selectedUser = item;
        this.modalService.open(content, { size : 'sm'})
    }

}
