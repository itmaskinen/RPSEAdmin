import { Component, OnInit } from "@angular/core";
import { componentsTransition } from "../../../router.animations";
import { Subject } from "rxjs/Subject";
import { ToastsManager } from "ng2-toastr/ng2-toastr";
import { Observable } from "rxjs/Observable";
import { AccountProductQtyLogService } from "../../../services/accoutProductQtyLog.service";
import { BaseComponent } from "../../../core/components";
import { Router, ActivatedRoute } from "@angular/router";
import { Location, DatePipe } from "@angular/common";
import { BehaviorSubject } from "rxjs";
import { ButtonGroupComponent } from "../../../core/components/ag-grid-controls/button-group/button-group.component";
import { ModalService } from "../../../shared";

@Component({
    selector: "app-account-products-qty-log-list-component",
    templateUrl: "./account-product-qty-log-list.component.html",
    styleUrls: ["../../layout/layout.component.scss"],
    animations: [componentsTransition()],
})
export class AccountProductsQtyLogListComponent
    extends BaseComponent
    implements OnInit
{
    showSpinner: boolean = false;

    productQtyLogData: BehaviorSubject<any> = new BehaviorSubject<any[]>([]);
    productQtyLogDataArray: any =[];
    productQtyLogDataCount: number = 0;

    isAdminOrUserEditor: boolean;
    model: any;
    isSuperAdmin: boolean = false;
    isAdmin: boolean = false;

    accountId: number = 0;

    showProductName: boolean = false;
    standAloneMode: boolean = false;
    forDeletionQty: any;

    public colDef: any;
    public defaultColDef: any;
    public frameworkComponents: any;
    private columnFilters: any = null;
    pushRLeftClass: string = 'push-left';
    isMenuOpen: boolean = false;
    constructor(
        private productLogService: AccountProductQtyLogService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private modalService: ModalService
    ) {
        super(location, router, route);
    }

    public ngOnInit() {
        this.isSuperAdmin = super.issuperadmin();
        this.prepareColumnDef();

        if (
            this.route.snapshot.params != null &&
            this.route.snapshot.params.accountId != null
        ) {
            this.laodDataByaAccount(this.route.snapshot.params.accountId);
        }
    }

    private laodDataByaAccount(id: number) {
        this.showProductName = true;
        this.standAloneMode = true;

        const filter = {
            columnFilters: this.columnFilters,
        };

        this.productLogService
            .getProductQtyLogGridByAccount(id, filter)
            .subscribe((response: any) => {
                const data = response.Data;
                this.productQtyLogDataArray = data;
                this.productQtyLogDataCount = data.length;
                this.productQtyLogData.next(data);
            });
    }

    public loadDataByAccountProductId(id: number) {
        this.productLogService
            .getProductQtyLogByAccountProductId(id, super.currentAccountId())
            .catch((err: any) => {
                this.toastr.error(err, "Error");
                return Observable.throw(err);
            })
            .finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                let data = response.Data;
                this.productQtyLogDataArray = data;
                this.productQtyLogDataCount = data.length;
                this.productQtyLogData.next(data);
            });
    }

    public loadDataByProductId(productId: number) {
        return new Promise((resolve) => {
            this.productLogService
                .getProductQtyLogByProductId(
                    productId,
                    super.currentAccountId()
                )
                .catch((err: any) => {
                    this.toastr.error(err, "Error");
                    return Observable.throw(err);
                })
                .finally(() => {
                    this.showSpinner = false;
                })
                .subscribe((response) => {
                    let data = response.Data;
                    this.productQtyLogDataArray = data;
                    this.productQtyLogDataCount = data.length;
                    this.productQtyLogData.next(data);
                    resolve(this.productQtyLogDataCount);
                });
        });
    }

    private prepareColumnDef() {
        this.colDef = [
            {
                headerName: "Product Name",
                field: "ProductName",
                width: 250,
                cellStyle: { cursor: "pointer" },
                cellRenderer: "buttonGroup",
                cellRendererParams: {
                    buttonInfo: [
                        {
                            getContent: (params) => {
                                return `<u>${params.data.ProductName}</u>`;
                            },
                            onClick: (params) => {
                                this.navigateToProductDetails(
                                    params.data.ProductId
                                );
                            },
                        },
                    ],
                },
            },
            { headerName: "Group", field: "ProductGroupName", width: 150 },
            {
                headerName: "Manufacturer",
                field: "ManufacturerName",
                width: 150,
            },
            {
                headerName: "Color / Material",
                field: "ProductColorMaterialName",
            },
            { headerName: "Model", field: "ProductModelName" },
            {
                headerName: "Location",
                field: "LocationLevelOneName",
                width: 230,
                cellRenderer: (params) => {
                    const data = params.data;

                    let location =
                        data.LocationLevelOneName &&
                        data.LocationLevelOneName !== " "
                            ? data.LocationLevelOneName
                            : "";

                    if (
                        location &&
                        data.LocationLevelTwoName &&
                        data.LocationLevelTwoName !== " " &&
                        data.AppliedTo !== data.LocationLevelOneId
                    ) {
                        location += `, ${data.LocationLevelTwoName}`;

                        if (
                            data.LocationLevelThreeName &&
                            data.LocationLevelThreeName !== " " &&
                            data.AppliedTo !== data.LocationLevelTwoId
                        ) {
                            location += `, ${data.LocationLevelThreeName}`;

                            if (
                                data.LocationLevelFourName &&
                                data.LocationLevelFourName !== " " &&
                                data.AppliedTo !== data.LocationLevelThreeId
                            ) {
                                location += `, ${data.LocationLevelFourName}`;
                            }
                        }
                    }
                    return `<span> ${location}</span>`;
                },
            },
            {
                headerName: "Quantity",
                field: "Qty",
                type: "numericColumn",
                width: 80,
                filter: false,
                comparator: (num1, num2) => {
                    return num1 - num2;
                },
            },
            {
                headerName: "Mode ",
                field: "Mode",
                width: 60,
                cellStyle: { textAlign: "center" },
                filter: false,
                comparator: (num1, num2) => {
                    return num1 - num2;
                },
                cellRenderer(params) {
                    return `<span class="text-center">${
                        params.data.Mode >= 1 ? "+" : "-"
                    }</span>`;
                },
            },
            {
                headerName: "Date",
                field: "UpdatedDate",
                width: 150,
                cellStyle: { textAlign: "center" },
                filter: false,
                cellRenderer(params) {
                    return new DatePipe("en-US").transform(
                        params.data.UpdatedDate,
                        "dd-MM-yyyy, h:mm a"
                    );
                },
            },
            { headerName: "Product #", field: "ProductNumber" },
        ];

        this.defaultColDef = {
            resizable: true,
            sortable: true,
            cellRenderer: (params) => {
                return params.value || "";
            },
            comparator: (str1, str2) => {
                return (str1 || "").localeCompare(str2 || "", "sv");
            },
            filter: true,
            filterParams: {
                filterOptions: ["contains", "equals", "startsWith", "endsWith"],
                newRowsAction: "keep",
                textCustomComparator: (filter, value, filterText) => {
                    return true;
                },
            },
        };

        this.frameworkComponents = {
            buttonGroup: ButtonGroupComponent,
        };
    }

    public navigateToProductDetails(id) {
        this.router.navigate([`products/${id}/details`]);
    }

    public gridFilterChanged(event: any) {
        this.columnFilters = event.api.getFilterModel();
        this.laodDataByaAccount(this.route.snapshot.params.accountId);
    }

    public deleteQtyEntry() {
        if(!this.forDeletionQty){
            this.toastr.error('Please select a quantity entry before deleting', 'Error')
            return false;
        }

        this.productLogService
            .deleteProductQtyLog(this.forDeletionQty)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {})
            .subscribe((response) => {
                if (response.ErrorCode) {
                    this.toastr.error(response.Message, "Error");
                } else {
                    this.toastr.success(
                        `Successfully deleted quantity entry.`,
                        "Success"
                    );

                    this.loadDataByProductId(this.forDeletionQty.ProductId);
                }
            });
    }

    public confirmDeleteQty(content: string, item) {
        this.forDeletionQty = item;
        this.modalService.open(content, { size : 'sm' });
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        if (!this.isMenuOpen) {
            dom.classList.toggle(this.pushRLeftClass);
            this.isMenuOpen = true;
        }
        else {
            dom.classList.toggle(this.pushRLeftClass)
            this.isMenuOpen = false;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        const result =  dom.classList.contains(this.pushRLeftClass);
        return result;
    }
}
