export class ProductColorMaterialModel { 
    public Id : number = 0;
    public ShortCode: string = "";
    public Name: string = "";
    public Description: string = "";
    public HexValue: string = "#000";
    public ExcludeFromMobile: boolean = false;
    public IsImage: boolean = false;
}