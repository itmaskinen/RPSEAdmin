export class CartProductModel {
    public AccountId: number = 0;
    public ProductId: string = "";
    public Description: string = "";
    public Quantity: number = 0;
    public TotalPrice: number = 0;
    public ShippingAddress: string = "";
    public ExtraInstructions: string = "";
    public CreatedDate: string = "";
    public LastUpdatedDate: string = "";
}