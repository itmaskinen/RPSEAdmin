export class OrderDetailModel {
    public Id : number = 0;
    public UserOrderId : number = 0;
    public LocationLevelOneId : number = 0;
    public LocationLevelTwoId : number = 0;
    public LocationLevelThreeId : number = 0;
    public LocationLevelOneQty : number = 0;
    public LocationLevelTwoQty : number = 0;
    public LocationLevelThreeQty : number = 0;

    public CreatedDate : string;
    public CreatedBy : string;
    public LastUpdatedDate : string;
    public LastUpdatedBy : string;
}