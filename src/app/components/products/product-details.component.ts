import { Component, OnInit, ViewChild, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { componentsTransition, routerTransition } from '../../router.animations';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { BaseComponent } from '../../core/components';
import { ProductModel, AccountProductModel, CartProductModel, AccountProductQtyLogModel } from '../../models';
import { OrderModel } from '../../models/orderModel';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProductService, AccountService, LocationService, OrderService } from '../../services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare var jsPDF: any; // Important
import * as moment from 'moment';
import { AccountProductsQtyComponent } from '../product-log/qty/account-product-qty.component';
import { ModalService } from '../../shared';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['../layout/layout.component.scss', '../products/products.css', '../cart/cart.css'],
    animations: [componentsTransition(), routerTransition()]
})
export class ProductsDetailComponent extends BaseComponent implements OnInit { 
    @ViewChild('accountProductQty') accountProductQty : AccountProductsQtyComponent;
    @ViewChild('confirmCartContent')
    private confirmCartContentTpl: TemplateRef<any>;

    showSpinner: boolean = false;
    showPhotoSpinner: boolean = false;

    photoSrc: string = '';
    model: any;
    cartModel: any;
    orderModel: any;
    accountProductModel: any;
    productQtyLogModel: any;
    filteredLocation: string;
    qtyModuleData: any = [];

    mainPhotoWidth: number = 0;
    mainPhotoHeight: number = 0;
    displayQty: boolean = false;
    //cart
    currentTotalQty: number = 0;

    public focusPhotoSrc: string;
    userOrders: any;

    constructor(
        private productService: ProductService,
        private orderService: OrderService,
        private toastr: ToastsManager,
        private router: Router,
        private route: ActivatedRoute,
        private modalService: ModalService,
        private checkRef: ChangeDetectorRef,
        private location: Location){
        super(location, router, route);
    };

    public ngOnInit(){
        this.model = new ProductModel();
        this.accountProductModel = new AccountProductModel();
        this.productQtyLogModel = new AccountProductQtyLogModel();

        this.setDefaults();

        this.route.params.subscribe((params) => {
            let id = params['id']; // product id
            this.getProduct(id);
        });

        this.getCartItems();
    }

    public setDefaults(){
        //this.photoSrc = "/assets/images/add_photo_image.png";
        this.accountId = super.currentAccountId();
    }

    public getProduct(id){
        this.showSpinner = true;
        this.filteredLocation = super.getProductFilterStore()[0].filters.location;
        this.productService.getProduct(id, this.accountId, this.filteredLocation)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.model = response.Data;
                if(this.model.MainPhotoUrl) {
                    this.photoSrc = super.imageUrl(this.model.MainPhotoUrl);
                }
                else {
                    this.photoSrc = "/assets/images/add_photo_image.png";
                }
            }
        });
    }

    public addToCart(){
        this.showSpinner = true;
        this.populateOrderData();
        this.orderService.Save(this.orderModel)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            if(response.ErrorCode) {
                this.toastr.error(response.Message, "Error");
            }
            else {
               //add new product?
               this.modalService.open(this.confirmCartContentTpl);
            }
        });
    }

    //CART
    public recalculateQty(data){
        let totalQty = data.totalQty;
        this.cartModel.Quantity = totalQty;
        this.accountProductModel.QuantityOnStock = this.currentTotalQty - this.cartModel.Quantity;

        
        this.orderModel.OrderDetails = data.items;
    }

    public openCart(event: any, cartFormContent: string, item){
        this.orderModel = new OrderModel();
        this.cartModel = new CartProductModel();
        this.accountProductModel = new AccountProductModel();

        this.model.ProductName = item.ProductName;
        this.model.ProductNumber = item.ProductNumber;

        this.accountProductModel.ProductId = item.Id;
        this.accountProductModel.Price = item.Price;
        this.accountProductModel.QuantityOnStock = item.CurrentQty;
        this.accountProductModel.AccountId = this.accountId;

        this.currentTotalQty = item.CurrentQty;

        this.modalService.open(cartFormContent, { size: 'lg' });
        event.stopPropagation();
    }

    public checkOut(){
        this.router.navigateByUrl('/cart');
    }

    public addMoreProducts(){
        this.router.navigateByUrl('/products');
    }

    public navigateToList(){
        if(this.accountId)
        {
            this.router.navigate([`account/${this.accountId}/products`]);
        }
        else 
        {
            this.router.navigate([`products`]);
        }
    }

    public invokeAccountProductQtyModules(id: number){
        if(!super.issuperadmin()) { 
            this.accountProductQty.isAdmin = super.isadmin() || super.isEditor();
            this.accountProductQty.loadCurrentQtyData(id).then((data) => {
                this.qtyModuleData = data;
            });
            this.checkRef.detectChanges();
            this.accountProductQty.loadQtyLogData(id);           
        }
    }

    public populateOrderData(){
        this.orderModel.ProductId = this.accountProductModel.ProductId;
        if (this.userOrders.length > 0) {
            const order = this.orderService.updateIfExistingPendingOrder(this.orderModel, this.userOrders);
            if (order.length > 0) {
               this.orderModel = order.filter(e => e.ProductId == this.orderModel.ProductId)[0];
            }
        }

        this.orderModel.UserId = super.currentLoggedUser().id;
        this.orderModel.CreatedBy = super.currentLoggedUser().username;
        this.orderModel.LastUpdatedBy = super.currentLoggedUser().username;
        this.orderModel.UserShippingAddress = this.cartModel.ShippingAddress;
    }

    public getMainPhotoProperties(){
        let mainPhoto = document.getElementById('imagePreview');
        let w = mainPhoto.clientWidth;
        let h = mainPhoto.clientHeight;
        let isPortrait = h > w;
        if(isPortrait) {
            w = 190;
            h = 270;
        }
        else {
            w = 210;
            h = 150;
        }
        return {
            width: w,
            height: h
        };
    }

    public exportToPDF(){
        this.showSpinner = true;
        this.productService.print(this.route.snapshot.params["id"], super.currentAccountId().toString(), this.filteredLocation).toPromise().then((response) => {
            this.showSpinner = false;
            let url = window.URL.createObjectURL(response);
            let a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.href = url;
            a.download = "Product Catalog";
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove();
        }, (err => {
            this.showSpinner = false;
        }))
        // window.location.href = this.productService.print(this.route.snapshot.params["id"], super.currentAccountId().toString(), this.filteredLocation);
    }

    public toggleDisplayQty(){
        if(!this.displayQty) {
            this.displayQty = true;
            this.checkRef.detectChanges();
            this.invokeAccountProductQtyModules(this.model.Id);
        }
        else {
            this.displayQty = false;
        }
    }

    public openImage(template, url) {
        this.focusPhotoSrc = url;
        
        this.modalService.open(template, { windowClass: "fit-content" });
    }

    public getCartItems() {
        this.orderService.getOrderByUsers(super.currentLoggedUser().id, super.currentAccountId())
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            this.userOrders = response.Data;
        });
    }
}