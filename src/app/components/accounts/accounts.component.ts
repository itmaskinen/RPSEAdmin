import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { AccountsListComponent } from './accounts-list.component';

@Component({
    selector: 'app-accounts',
    templateUrl: './accounts.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class AccountsComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    @ViewChild('accountsList') accountList: AccountsListComponent;
    constructor(
        private router: Router,
        private route:ActivatedRoute,
    ) {

    }
    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.accountList.editAccount(currentUser.accountId)
        }
        else if(currentUser.role === 'superadmin')
        {
            this.accountList.canEdit = true;
            this.accountList.loadData();
        }

    }
    public navigateToCreate(){
        this.router.navigate([`accounts/create`]); 
    }
}
