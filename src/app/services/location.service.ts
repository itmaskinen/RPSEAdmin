import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { IPagedResults } from '../core/interfaces';

@Injectable()
export class LocationService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }

    public getLocationsByAccountList(id: number): Observable<IPagedResults<any>>{
        let url = `Location/GetAllByAccountId?id=${id}`;
        return super.getAll<any>(url);
    }

    public getLocationsByAccountAndLevelList(id: number, levelId: number): Observable<IPagedResults<any>>{
        let url = `Location/GetAllByAccountIdAndLevelId?id=${id}&levelId=${levelId}`;
        return super.getAll<any>(url);
    }

    public getLocation(id?: number): Observable<any>{
        let url = `Location/Get?id=${id}`;
        return super.getAll<any>(url);
    }

    public saveLocation(data){
        let url = `Location/Save`;
        return super.save<any>(url,data);
    }

    public deleteLocation(data){
        let url = `Location/Delete`;
        return super.save<any>(url,data);
    }

    public getAccountLocationsLevelOneAll(){
        let url = `Location/GetAccountLocationsLevelOne?accountId=`;
        return super.getAll<any>(url);
    }

    public getAccountProductLocations(productId: number, accountId: number, showAllLocations: boolean, filteredLocationIds: any){
        let url = `Location/GetAccountProductLocations?productId=${productId}&accountId=${accountId}&showAllLocations=${showAllLocations}&filteredLocationIds=${filteredLocationIds}`;
        return super.getAll<any>(url);
    }

    public getAccountLocationsLevelOne(accountId: number){
        let url = `Location/GetAccountLocationsLevelOne?accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public getAccountLocationsLevelTwo(accountId: number){
        let url = `Location/GetAccountLocationsLevelTwo?accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public getAccountLocationsLevelThree(accountId: number){
        let url = `Location/GetAccountLocationsLevelThree?accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public getAccountLocationsLevelFour(accountId: number){
        let url = `Location/GetAccountLocationsLevelFour?accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public saveAccountLocationsLevelOne(data){
        let url = `Location/SaveAccountLocationsLevelOne`;
        return super.save<any>(url,data);
    }

    public saveAccountLocationsLevelTwo(data){
        let url = `Location/SaveAccountLocationsLevelTwo`;
        return super.save<any>(url,data);
    }

    public saveAccountLocationsLevelThree(data){
        let url = `Location/SaveAccountLocationsLevelThree`;
        return super.save<any>(url,data);
    }

    public saveAccountLocationsLevelFour(data){
        let url = `Location/SaveAccountLocationsLevelFour`;
        return super.save<any>(url,data);
    }

    public deleteAccountLocationsLevelOne(data){
        let url = `Location/DeleteAccountLocationsLevelOne`;
        return super.save<any>(url,data);
    }

    public deleteAccountLocationsLevelTwo(data){
        let url = `Location/DeleteAccountLocationsLevelTwo`;
        return super.save<any>(url,data);
    }

    public deleteAccountLocationsLevelThree(data){
        let url = `Location/DeleteAccountLocationsLevelThree`;
        return super.save<any>(url,data);
    }

    public deleteAccountLocationsLevelFour(data){
        let url = `Location/DeleteAccountLocationsLevelFour`;
        return super.save<any>(url,data);
    }

    public getProductLocationLevels(accountId){
        let url = `Location/Product/LocationLevels?accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public checkIfLocationHasProducts(id: number, lvl: number): Observable<any>{
        let url = `Location/CheckIfLocationHasProducts?id=${id}&lvl=${lvl}`;
        return super.getAll<any>(url);
    }

    public getAllLocationsOfAccount(accountId: number){
        let url = `Location/GetAllLocationsOfAccount?accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public getLocationTree(accountId: number){
        let url = `Location/GetLocationTree?accountId=${accountId}`;
        return super.getAll<any>(url);
    }
}
