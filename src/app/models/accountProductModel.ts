export class AccountProductModel {
    public Id: number = 0;
    public AccountId: number = 0;
    public ProductId: string = "";
    public Description: string = "";
    public QuantityOnStock: string = "";
    public Price: string = "";
    public Location: string = "";
    public CreatedDate: string = "";
    public LastUpdatedDate: string = "";
    public UpdatedBy: number = 0;
    public IsMobile: boolean = false;

    public LocationLevelOneId: number = 0;
    public LocationLevelTwoId: number = 0;
    public LocationLevelThreeId: number = 0;

    public LocationLevelOneName: string;
    public LocationLevelTwoName: string;
    public LocationLevelThreeName: string;

    public AvailableDate: Date
}