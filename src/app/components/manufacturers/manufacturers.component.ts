import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { ManufacturersListComponent } from './manufacturers-list.component';

@Component({
    selector: 'app-manufacturers',
    templateUrl: './manufacturers.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ManufacturersComponent implements OnInit {
    @ViewChild('manufacturersList') manufacturersList: ManufacturersListComponent;
    isAdminOrUserEditor: boolean;
    constructor(
        private router: Router
    ) {}
    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
    }

    public navigateToCreate(){
        this.router.navigate(['product-manufacturers/create']);
    }

    public exportToCSV(){
        this.manufacturersList.exportToCSV();
    }

    public exportTOPDF(){
        this.manufacturersList.exportToPDF();
    }
}
