import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';

import { AccountModel, LocationModel, UserModel } from '../../models';
import { AccountService, LocationService, UserService } from '../../services';

import { AccountsListComponent, UsersListComponent, LocationsListComponent } from '..';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { ModalService } from '../../shared';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';

@Component({
    selector: 'app-accounts-edit',
    templateUrl: './accounts-edit.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class AccountsEditComponent extends BaseComponent implements OnInit {
    //@ViewChild('accountsModuleList') accountsModuleList: AccountsModuleListComponent;
    @ViewChild('accountsList') accountsList: AccountsListComponent;
    @ViewChild('usersList') usersList: UsersListComponent;
    
    @ViewChild('locationLevels') locationsListLevel: LocationsListComponent;

    @ViewChild('locationLevelOneAdd') locationLevelOneAdd: ElementRef;
    @ViewChild('locationLevelTwoAdd') locationLevelTwoAdd: ElementRef;
    @ViewChild('locationLevelThreeAdd') locationLevelThreeAdd: ElementRef;

    model : any;
    userModel: any;
    locationModel: any;

    companyTypes: any;
    accountId: number = 0;
    accountsModuleListObject: any;
    mode: string = '';
    isAccountEditMode = false;
    canSave = false;
    isSuperAdmin = false;
    isAdmin = false;
    public showSpinner: boolean = false;

    public locationLevelTitle: string = '';
    public selectedLevelId: number = 0;

    public userRolesList: any = [];
    public userCreateEditTitle: string = "CREATE A USER";
    public userMode: string = 'create';

    public sendLoginDetail: boolean = false;
    public defaultImageSrc: string = '/assets/images/add_photo_image.png'
    pushRLeftClass: string = 'push-left';
    isMenuOpen: boolean = false;
    constructor(
        private accountService: AccountService, 
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private modalService: ModalService,
        private locationService: LocationService,
        private userService: UserService,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() : void {
        this.route.params.subscribe((params) => {
        if (!params) {
            return;
        }
        const id = params['id'];
        this.mode = 'edit';
        this.accountId = id;
        this.model = new AccountModel();
        this.locationModel = new LocationModel();
        this.userModel = new UserModel();

        this.loadData(id);
        });
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'superadmin')
        {
            this.isSuperAdmin = true;
            this.canSave = true;
            this.isAccountEditMode = true;
            this.accountsList.isForAssociate = true;
            this.accountsList.accountId = this.accountId;
            //this.accountsList.loadDataWithAssoc(this.accountId);
        }
        else if(currentUser.role === 'admin'){
            this.isAdmin = true;
            //this.loadData(this.accountId);
        }
        this.loadUserRoles();
        this.usersList.loadDataById(this.accountId);     
    }

    public loadData(id: number) : void{
        this.showSpinner = true;
        this.accountService.getAccountV3(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
            setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((data) => {
            if(data.Data)
            {
                this.model = data.Data;
            }   
            this.locationsListLevel.isAdmin = true;
            this.locationsListLevel.loadAccountLocationLevelOneList(this.accountId);
            // only load the first level
            // this.locationsListLevel.loadAccountLocationLevelTwoList(this.accountId);
            // this.locationsListLevel.loadAccountLocationLevelThreeList(this.accountId);
            $('#company-name').focus();
        });
    }

    public onIsActiveChange(value: boolean){
        this.model.IsActive = value;
    }

    public onIsDeletedChange(value: boolean){
        this.model.IsDeleted = value;
    } 

    public saveChanges() : void{
        if (this.model.EmailCheckoutCCs && this.model.EmailCheckoutCCs !== "") {
            this.model.EmailCheckoutCCs = this.model.EmailCheckoutCCs.replace(/\n/g, ';');
            let emailAddresses = this.model.EmailCheckoutCCs.split(';').map(email => email.trim());

            let validEmails = emailAddresses.filter(email => {
                let regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                return regex.test(email);
            });
        
            // Join valid email addresses back into a single string separated by semicolons
            this.model.EmailCheckoutCCs = validEmails.join(';');
        }
     
        this.accountService.saveAccountV3(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            setTimeout(() => {
                this.router.navigate([`/accounts`]);
            }, 1000);
        });
    }

    public delete() : void{
        this.accountService.deleteAccount(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                setTimeout(() => {
                    this.router.navigate([`/accounts`]);
                }, 1000);
            } 
            this.ngOnInit();
        });
    }

    public createUser(content: string) : void { 
        this.sendLoginDetail = true;

        this.userCreateEditTitle = "CREATE A USER";
        this.userMode = 'create';
        this.userModel = new UserModel();
        this.modalService.open(content, { size: 'lg' });
        $('#first-name').focus();
    }
        
    public editUser(content: string, id: number){
        this.sendLoginDetail = false;

        this.userCreateEditTitle = "View / Edit User";
        this.userMode = 'edit';
        this.modalService.open(content, { size: 'lg' });
        this.loadUserData(id);
        $('#first-name').focus();
    }

    public confirmDelete(content: string): void {
        this.modalService.open(content, { size: 'sm' });
    }
    //@TODO : Transfer this to the location-create-edit component
    public saveLocation(): void {
        this.showSpinner = true;
        this.locationModel.LocationLevel = this.selectedLevelId;
        this.locationModel.AccountId = this.accountId;
        this.locationService.saveLocation(this.locationModel)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {
            this.showSpinner = false;
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                this.ngOnInit();
            }
        });
    }
    //@TODO : Transfer this to the location-create-edit component
    public editLocation(locParams: any){
        this.locationService.getLocation(locParams.locationId)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
        })
        .subscribe((data) => { 
            if(data.Data) {
                if(locParams.levelId === 1) {
                    this.locationLevelOneAdd.nativeElement.click();
                }
                else if(locParams.levelId === 2) {
                    this.locationLevelTwoAdd.nativeElement.click();
                }
                else if(locParams.levelId === 3 ){
                    this.locationLevelThreeAdd.nativeElement.click();
                }
                this.locationModel = data.Data;
            }
        });
    }

    public saveUser(dialogClose){
        this.showSpinner = true;
        this.userModel.AccountId = this.accountId;
        this.userService.saveUser(this.userModel, this.sendLoginDetail)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                let currentUser = JSON.parse(localStorage.getItem('currentUser'))
                currentUser.currentUserRaw.EmailAddress = this.userModel.EmailAddress;
                localStorage.setItem("currentUser",JSON.stringify(currentUser));
                
                dialogClose('Close click')
            }
            this.usersList.loadDataById(this.accountId);
        });
    }
    
    public loadUserData(id: number){
        this.showSpinner = true;
        this.userService.getUser(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            this.userModel = response.Data;
            this.userModel.RoleId = this.userModel.Role.Id;
            this.userModel.Password = '';
            this.userModel.PasswordReType = '';
        });
    }

    public loadUserRoles(): void {
        //getAllUserRoles
        this.userService.getAllUserRoles()
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            this.userRolesList = response.Data;
        });
    }

    public isPasswordMatched(){
        return this.model.Password ? this.model.PasswordReType === this.model.Password : true;
    }

    public getImage(){
        return super.imageUrl(this.model.AccountLogoURL);
    }

    public fileChange(fileInput: any){
        let self = this;
        let file = <File>fileInput.target.files[0];
        if(file.size > 2000000) return false;

        var reader = new FileReader();
        reader.onload = function () {
            self.model.MainPhotoByteString = reader.result;
        };
        reader.onerror = function (error) {};
        reader.readAsDataURL(file);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        if (!this.isMenuOpen) {
            dom.classList.toggle(this.pushRLeftClass);
            this.isMenuOpen = true;
        }
        else {
            dom.classList.toggle(this.pushRLeftClass)
            this.isMenuOpen = false;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        const result =  dom.classList.contains(this.pushRLeftClass);
        return result;
    }
}
