import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';
import { AuthenticationService } from '../../services';

import { Location } from '@angular/common';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent extends BaseComponent implements OnInit {
    model: any = {};
    username : any = '';
    public showSpinner: boolean = false;

    private staticSuperAdminUserName = 'superadmin';
    private staticSuperAdminPassword = 'TinTin-18';
    private staticAdminUserName = 'admin_ikea';
    private staticAdminUserPassword = 'ikea_admin';
    private staticUserName = 'test_user';
    private staticUserPassword = 'test123';
    constructor(
        private authService: AuthenticationService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private location: Location
        )
        {
            super(location, router, route);
        }


    public ngOnInit(): void {
        // if(DeviceHelper.isMobile()) {
        //     this.router.navigate(['/mobile/login']);
        // }
    }

    public onLoggedin(): void
    {
        let isLoggedIn = false;
        let role = 'user';
        let displayName = '';
        this.showSpinner = true;

        if(this.model.UserName === this.staticSuperAdminUserName && this.model.Password === this.staticSuperAdminPassword){
            isLoggedIn = true;
            let userData = {
                id: -1,
                isLoggedIn : isLoggedIn,
                username: this.model.UserName,
                role: 'superadmin',
                displayName: 'SUPERADMIN',
                accountId: 0,
                currentUserRaw : []
            };

            localStorage.setItem('currentUser', JSON.stringify(userData));
            localStorage.setItem('isLoggedin', JSON.stringify(isLoggedIn));
            this.router.navigate(['/dashboard']);
        } //FOR TEST ONLY
        else if(this.model.UserName === this.staticAdminUserName && this.model.Password === this.staticAdminUserPassword){
            isLoggedIn = true;
            let userData = {
                id: 1,
                isLoggedIn : isLoggedIn,
                username: this.model.UserName,
                role: 'admin',
                displayName: 'ADMIN IKEA SE',
                accountId: 1,
                currentUserRaw : []
            };
            localStorage.setItem('currentUser', JSON.stringify(userData));
            localStorage.setItem('isLoggedin', JSON.stringify(isLoggedIn));
            this.router.navigate([`/account/${userData.accountId}/products`]);
        } //FOR TEST ONLY
        else if(this.model.UserName === this.staticUserName && this.model.Password === this.staticUserPassword){
            isLoggedIn = true;
            let userData = {
                id: 2,
                isLoggedIn : isLoggedIn,
                username: this.model.UserName,
                role: 'user',
                displayName: 'Vincent Bjorson',
                accountId: 1,
                currentUserRaw : []
            };
            localStorage.setItem('currentUser', JSON.stringify(userData));
            localStorage.setItem('isLoggedin', JSON.stringify(isLoggedIn));
            this.router.navigate([`/account/${userData.accountId}/products`]);
        }
        else
        {
            //log users
            this.logInUser();
            //this.toastr.error('Invalid Credentials', 'Error');
        }
    }
    //@TODO : make this code shareable - this is used on the authguard
    public logInUser(userName?:string, password?:string){
        this.showSpinner = true;
        if(userName && password)
        {
            this.model = {
                UserName: userName,
                Password : password
            }
        }
        this.authService.auth(this.model)
            .catch((err: any) => {
                this.toastr.error('Wrong credentials', 'Error');
                return Observable.throw(err);
            }).finally(() => {
                this.showSpinner = false;
            })
            .subscribe((response) => {
                if(response.Success){
                    let currentUser = response.Data.CurrentUser;
                    let displayName = currentUser.FirstName || currentUser.LastName ? currentUser.FirstName + ' ' + currentUser.LastName : currentUser.UserName
                    let userData = {
                        id : currentUser.Id,
                        isLoggedIn : true,
                        username: currentUser.UserName,
                        role: currentUser.RoleName,
                        displayName: displayName,
                        accountId: currentUser.AccountId,
                        currentUserRaw : currentUser
                    };
                    localStorage.setItem('currentUser', JSON.stringify(userData));
                    localStorage.setItem('isLoggedin', JSON.stringify(true));
                    // if((!currentUser.FirstName && !currentUser.LastName) || currentUser.RoleName === 'user')
                    // {
                        // this.router.navigate([`/users/${currentUser.Id}/edit`]); // force change account
                        this.router.navigate([`/account/${userData.accountId}/products`]);
                        // this.router.navigate(['/dashboard']);
                    // }
                    // else {
                    //     this.router.navigate([`/accounts/${userData.accountId}/edit/users`]);
                    // }
                }
                else {
                    this.toastr.error('We could not find your username/password', "Error");
                }
            });
    }
}
