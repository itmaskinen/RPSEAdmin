import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-products-models-create-edit',
    templateUrl: './products-models-create-edit.component.html',
    styleUrls: ['../layout/layout.component.scss'],
    animations: [routerTransition()]
})
export class ProductsModelsCreateEditComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    constructor() {
        
    }
    public ngOnInit() {
       
    }
}
