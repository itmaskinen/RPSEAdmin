export class OrderModel {
    public Id : number = 0;
    public OrderNumber : string;
    public UserId : number;
    public ProductId: number;
    public Status: number;
    public UserShippingAddress: string;
    public Notes: string;
    public AdditionalInstructions: string;

    public CreatedDate : string;
    public CreatedBy : string;
    public LastUpdatedDate : string;
    public LastUpdatedBy : string;
    public OrderDetails : any = [];
}