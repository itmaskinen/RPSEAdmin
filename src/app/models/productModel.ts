import  * as moment from 'moment-timezone';

export class ProductModel {
    public Id: number = 0;
    public AccountId: number = 0;
    public ProductName: string = "";
    public ProductNumber: string = "";
    public Description: string = "";
    public Width: string = "";
    public Height: string = "";
    public Length: string = ""
    public Weight: string = "";;
    public Price: string = "";
    public Size: string = "";
    public MainPhotoUrl: string = "";
    public Location: string = "";
    public LocationId: number = 0;
    public ManufacturerId: number = 0;
    public ColorMatId: number = 0;
    public GroupId: number = 0;
    public ModelId: number = 0;
    public CreatedDate: string = "";
    public LastUpdatedDate: string = "";
    public ProductNumberDisplay: string = "";
    public AccountHasThisProduct: boolean = false;
    public PhotoProcessType: number = 0;
    public AddedFromMobile: boolean = false;
    public TagIds: string;
}

export class ProductPhotoModel {
    public Id: number = 0;
    public ProductId: number = 0;
    public PhotoUrl: string = "";
    public PhotoByteString: string = "";
}

export class ProductsFilter {
    public AccountId= 0;
    public Keyword: string;
    public ManufacturerId = 0;
    public ColorMatId = 0;
    public GroupId = 0;
    public ConditionId = -1;
    public ModelId = 0;
    public Displaying = 0;
    public QtyView = 0;
    public CurrentAccountId = 0;
    public Role = 'viewer';
}

export class ProductBookingModel {
    public Id = 0;
    public ProductId = 0;
    public Quantity = 0;
    public BookedDate = moment(new Date('2023-03-01')).format('YYYY-MM-DD');
    public DestinationLocationId = '0';
    public DestinationLocation = ''; // Destination location name
    public SourceLocationId = '0';
    public SourceLocation = ''; // Source location name
    public Room = '';
    public Comment = '';
    public IsUpdatingRow = false;
    public IsAddRow = false;
}