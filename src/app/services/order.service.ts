import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

@Injectable()
export class OrderService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }

    public getOrder(id:number){
        let url = `Order/Get?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getOrderByUsers(id:number, accountId:any){
        let url = `Order/GetByUser?id=${id}&accountId=${accountId}`;
        return super.getAll<any>(url);
    }

    public getOrderHistory(id:number){
        let url = `Order/History?userId=${id}`;    
        return super.getAll<any>(url);
    }

    public getOrderCount(id: number){
        let url = `Order/Count/User?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getOrderDetail(id:number){
        let url = `Order/GetDetail?id=${id}`;    
        return super.getAll<any>(url);
    }

    public Save(data){
        let url = `Order/Save`;
        return super.save<any>(url,data);
    }

    public UpdateOrderDetail(data){
        let url = `Order/UpdateDetail`;
        return super.save<any>(url,data);
    }

    public Delete(data){
        let url = `Order/Delete`;
        return super.save<any>(url,data);
    }

    public DeleteOrderDetail(data){
        let url = `Order/DeleteOrderDetail`;
        return super.save<any>(url,data);
    }

    public checkOut(data){
        let url = `Order/Checkout`;
        return super.save<any>(url,data);
    }

    public checkOutEmail(data){
        let url = `Order/Checkout/Email`;
        return super.save<any>(url,data);
    }

    public getCheckOut(id){
        let url = `Order/Checkout?id=${id}`;
        return super.getAll<any>(url);
    }

    public updateIfExistingPendingOrder(newOrder: any, existingPendingOrders: any) {
        const orders = existingPendingOrders.filter(e => e.ProductId == newOrder.ProductId);
        if (orders.length > 0) {
            orders.forEach(existingOrder => {
                let matchFound = false; // Flag to track if a matching detail is found
                newOrder.OrderDetails.forEach(newDetail => {
                    const matchingExistingDetail = existingOrder.OrderDetails.find(existingDetail =>
                        existingDetail.LocationLevelOneId === newDetail.LocationLevelOneId &&
                        existingDetail.LocationLevelTwoId === newDetail.LocationLevelTwoId &&
                        existingDetail.LocationLevelThreeId === newDetail.LocationLevelThreeId &&
                        existingDetail.LocationLevelFourId === newDetail.LocationLevelFourId
                    );
                    if (matchingExistingDetail) {
                        // If a matching detail is found, update its CurrentQty
                        matchingExistingDetail.CurrentQty += newDetail.CurrentQty;
                        matchFound = true; // Set flag to true
                    }
                });
                // If no matching detail is found and matchFound is still false, push all new details
                if (!matchFound) {
                    existingOrder.OrderDetails.push(...newOrder.OrderDetails);
                }
            });
            return existingPendingOrders;
        } else {
            return [];
        }
    }
    
}